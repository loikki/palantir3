import "Turbine.UI";

-- Defines a few constant variables

MODE_ALWAYS = 1
MODE_LOW = 2
MODE_AUTO = 3

test_color = Turbine.UI.Color( 0, 0, 1 )

Potions = class(Turbine.UI.Window);

function Potions:Constructor()

  -- Define variables
  -- global variables
  opacity = 0.5
  -- Potion window
  background_width = 114
  background_height = 33
  self.vertical_shift = 140
  -- Potions
  potion_width = 28
  potion_height = 25
  potion_horizontal_shift = 0
  potion_vertical_shift = 8

  -- Potions available
  available_width = 27
  available_height = 33
  available_horizontal_shift = 0
  available_vertical_shift = 0
  available_opacity = 0.2

  -- Quickslot
  quickslot_color = Turbine.UI.Color(0.01, 0, 0, 0)
  quickslot_size = 36
  undefined_quickslot = Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,"")
  -- Reset
  reset_color = Turbine.UI.Color(.9,0.01,0.01,0.01)
  reset_width = 24

  reset_height = 8
  -- Main Window
  main_window_width = 200
  main_window_height = quickslot_size + reset_height
  -- Cooldown main window
  cooldown_window_width = 90
  cooldown_window_height = 17
  cooldown_window_vertical_shift = -5

  -- Cooldown
  cooldown_width = 19
  cooldown_height = 6
  cooldown_vertical_shift = 10
  cooldown_horizontal_shift = 2

  -- Counters
  counter_width = 10
  counter_height = 11
  counter_vertical_shift = 28
  counter_horizontal_shift = 5
  counter_vertical_shift_low = 2

  -- Data
  self.data = {
    morale_type = "",
    morale_data = "",
    morale_threshold = 0.3,
    morale_cooldown = 120,
    power_type = "",
    power_data = "",
    power_threshold = 0.3,
    power_cooldown = 120,
    display_mode = 1,
    enabled = 1,
    mode = 1,
  }

  -- Timers
  self.morale_timer = DU_81.Utilsx.Timer()
  self.morale_timer.AutoReset = true
  self.morale_timer.Interval = 1

  self.power_timer = DU_81.Utilsx.Timer()
  self.power_timer.AutoReset = true
  self.power_timer.Interval = 1

  -- Define the windows
  -- Define the full potion window
  Turbine.UI.Window.Constructor(self);
  self:SetVisible(true);
  self:SetMouseVisible(false);
  self:SetHeight(main_window_height)
  self:SetWidth(main_window_width)
  self:SetWantsUpdates(true)

  -- Define the window containing the buttons
  self.potions_window = Turbine.UI.Window();
  self.potions_window:SetParent(self)
  self.potions_window:SetMouseVisible(false)
  self.potions_window:SetSize(background_width, background_height)
  self.potions_window:SetPosition((main_window_width - background_width)/2, 0)
  self.potions_window:SetBackground(
    "DU_81/Palantir/Resources/Potions/background.tga");
  self.potions_window:SetOpacity(opacity)
  self.potions_window:SetZOrder(20)

  -- Mask when potions are available
  -- Morale
  x = background_width / 2 + 1
  self.morale_avail = Turbine.UI.Window();
  self.morale_avail:SetParent(self.potions_window)
  self.morale_avail:SetPosition(
    x - available_horizontal_shift - available_width, available_vertical_shift)
  self.morale_avail:SetSize(available_width, available_height)
  self.morale_avail:SetVisible(false)
  self.morale_avail:SetMouseVisible(false)
  self.morale_avail:SetBackground(
    "DU_81/Palantir/Resources/Potions/morale/ind.tga");
  self.morale_avail:SetOpacity(available_opacity)

  -- Power
  self.power_avail = Turbine.UI.Window();
  self.power_avail:SetParent(self.potions_window)
  self.power_avail:SetPosition(
    x + available_horizontal_shift, available_vertical_shift)
  self.power_avail:SetSize(available_width, available_height)
  self.power_avail:SetVisible(false)
  self.power_avail:SetMouseVisible(false)
  self.power_avail:SetBackground(
    "DU_81/Palantir/Resources/Potions/power/ind.tga");
  self.power_avail:SetOpacity(available_opacity)


  -- Quickslot for the potions
  self.quickslot_window = Turbine.UI.Window();
  self.quickslot_window:SetParent(self)
  self.quickslot_window:SetPosition(0, 0)
  self.quickslot_window:SetSize(main_window_width, main_window_height)
  self.quickslot_window:SetVisible(true)
  self.quickslot_window:SetMouseVisible(false)

  -- morale
  self.morale_quickslot = Turbine.UI.Lotro.Quickslot();
  self.morale_quickslot:SetParent(self.quickslot_window)
  self.morale_quickslot:SetBackColor(back_color);

  -- power
  self.power_quickslot = Turbine.UI.Lotro.Quickslot();
  self.power_quickslot:SetParent(self.quickslot_window)
  self.power_quickslot:SetBackColor(back_color);

  -- Quickslot reset
  -- morale
  self.morale_reset = Turbine.UI.Window();
  self.morale_reset:SetParent(self.quickslot_window);
  self.morale_reset:SetSize(reset_width, reset_height);
  self.morale_reset:SetPosition(
    (quickslot_size - reset_width)/2, quickslot_size)
  self.morale_reset:SetBackColor(reset_color)

  -- power
  self.power_reset = Turbine.UI.Window();
  self.power_reset:SetParent(self.quickslot_window);
  self.power_reset:SetSize(reset_width, reset_height);
  self.power_reset:SetPosition(
    main_window_width - (quickslot_size + reset_width)/2, quickslot_size)
  self.power_reset:SetBackColor(reset_color)

  -- Define the cooldown bars
  -- Main window
  self.cooldown = Turbine.UI.Window()
  self.cooldown:SetParent(self)
  self.cooldown:SetVisible(true)
  self.cooldown:SetMouseVisible(false)
  self.cooldown:SetSize(cooldown_window_width, cooldown_window_height)
  self.cooldown:SetPosition(
    (main_window_width - cooldown_window_width)/2,
    cooldown_window_vertical_shift)
  self.cooldown:SetBackground(
    "DU_81/Palantir/Resources/Potions/bar_background.tga");
  self.cooldown:SetZOrder(20)

  -- Morale
  x = cooldown_window_width/2
  y = cooldown_vertical_shift
  self.morale_cooldown_color = Turbine.UI.Window()
  self.morale_cooldown_color:SetParent(self.cooldown)
  self.morale_cooldown_color:SetVisible(true)
  self.morale_cooldown_color:SetMouseVisible(false)
  self.morale_cooldown_color:SetSize(cooldown_width, cooldown_height)
  self.morale_cooldown_color:SetPosition(
    x - cooldown_horizontal_shift - cooldown_width, y)
  self.morale_cooldown_color:SetBackground(
    "DU_81/Palantir/Resources/Potions/morale/bar_green.tga");
  self.morale_cooldown_color:SetOpacity(opacity)
  self.morale_cooldown_color:SetZOrder(26)

  -- Mask
  self.morale_cooldown_mask = Turbine.UI.Window()
  self.morale_cooldown_mask:SetParent(self.cooldown)
  self.morale_cooldown_mask:SetVisible(true)
  self.morale_cooldown_mask:SetMouseVisible(false)
  self.morale_cooldown_mask:SetSize(cooldown_width, cooldown_height)
  self.morale_cooldown_mask:SetPosition(
    x - cooldown_horizontal_shift - cooldown_width, y)
  self.morale_cooldown_mask:SetBackground(
    "DU_81/Palantir/Resources/Potions/morale/bar_mask.tga");
  self.morale_cooldown_mask:SetZOrder(26)
  self.morale_cooldown_mask:SetWidth(0)


  -- Power
  self.power_cooldown_color = Turbine.UI.Window()
  self.power_cooldown_color:SetParent(self.cooldown)
  self.power_cooldown_color:SetVisible(true)
  self.power_cooldown_color:SetMouseVisible(false)
  self.power_cooldown_color:SetSize(cooldown_width, cooldown_height)
  self.power_cooldown_color:SetPosition(
    x + cooldown_horizontal_shift, y)
  self.power_cooldown_color:SetBackground(
    "DU_81/Palantir/Resources/Potions/power/bar_green.tga");
  self.power_cooldown_color:SetOpacity(opacity)
  self.power_cooldown_color:SetZOrder(26)

  -- Mask
  self.power_cooldown_mask = Turbine.UI.Window()
  self.power_cooldown_mask:SetParent(self.cooldown)
  self.power_cooldown_mask:SetVisible(true)
  self.power_cooldown_mask:SetMouseVisible(false)
  self.power_cooldown_mask:SetSize(cooldown_width, cooldown_height)
  self.power_cooldown_mask:SetPosition(
    x + cooldown_horizontal_shift, y)
  self.power_cooldown_mask:SetBackground(
    "DU_81/Palantir/Resources/Potions/power/bar_mask.tga");
  self.power_cooldown_mask:SetZOrder(26)
  self.power_cooldown_mask:SetWidth(0)

  -- Counters
  -- Morale
  -- Tens
  x = background_width / 2
  y = counter_vertical_shift
  self.morale_counter_tens = Turbine.UI.Window()
  self.morale_counter_tens:SetParent(self.potions_window)
  self.morale_counter_tens:SetVisible(false)
  self.morale_counter_tens:SetMouseVisible(false)
  self.morale_counter_tens:SetSize(counter_width, counter_height)
  self.morale_counter_tens:SetPosition(
    x - counter_horizontal_shift - 2 * counter_width, y)

  -- Units
  self.morale_counter_units = Turbine.UI.Window()
  self.morale_counter_units:SetParent(self.potions_window)
  self.morale_counter_units:SetVisible(false)
  self.morale_counter_units:SetMouseVisible(false)
  self.morale_counter_units:SetSize(counter_width, counter_height)
  self.morale_counter_units:SetPosition(
    x - counter_horizontal_shift - counter_width, y + counter_vertical_shift_low)

  -- Power
  -- Tens
  self.power_counter_tens = Turbine.UI.Window()
  self.power_counter_tens:SetParent(self.potions_window)
  self.power_counter_tens:SetVisible(false)
  self.power_counter_tens:SetMouseVisible(false)
  self.power_counter_tens:SetSize(counter_width, counter_height)
  self.power_counter_tens:SetPosition(
    x + counter_horizontal_shift, y + counter_vertical_shift_low)

  -- Units
  self.power_counter_units = Turbine.UI.Window()
  self.power_counter_units:SetParent(self.potions_window)
  self.power_counter_units:SetVisible(false)
  self.power_counter_units:SetMouseVisible(false)
  self.power_counter_units:SetSize(counter_width, counter_height)
  self.power_counter_units:SetPosition(
    x + counter_horizontal_shift + counter_width, y)

  -- Events

  -- Update
  self.Update = function()
    -- Decide if the quickslots are available
    self:DoMoraleAvailable()
    self:DoPowerAvailable()
    self:Refresh()
  end

  -- Quickslots
  self.morale_quickslot.MouseClick = function(sender, args)
    -- Ensure that the cooldown is finished
    if self.morale_timer.Enabled then
      return
    end
    self.morale_timer:Start()
    self.morale_cooldown_color:SetBackground(
      "DU_81/Palantir/Resources/Potions/morale/bar_red.tga")

    count = self:GetQuantity(self.morale_quickslot)
    self:SetQuantityMorale(count - 1)
  end

  self.power_quickslot.MouseClick = function(sender, args)
    -- Ensure that the cooldown is finished
    if self.power_timer.Enabled then
      return
    end
    self.power_timer:Start()
    self.power_cooldown_color:SetBackground(
      "DU_81/Palantir/Resources/Potions/power/bar_red.tga")
    count = self:GetQuantity(self.power_quickslot)
    self:SetQuantityPower(count - 1)
  end

  -- Quickslots reset
  self.morale_reset.MouseClick = function(sender, args)
    self.morale_quickslot:SetShortcut(undefined_quickslot)
    self.data.morale_type = ""
    self.data.morale_data = ""
  end
  self.power_reset.MouseClick = function(sender, args)
    self.power_quickslot:SetShortcut(undefined_quickslot)
    self.data.power_type = ""
    self.data.power_data = ""
  end

  -- Quickslot changed
  self.morale_quickslot.ShortcutChanged = function(sender, args)
    self.data.morale_data = self.morale_quickslot:GetShortcut():GetData()
    self.data.morale_type = self.morale_quickslot:GetShortcut():GetType()
    count = self:GetQuantity(self.morale_quickslot)
    self:SetQuantityMorale(count)
  end
  self.power_quickslot.ShortcutChanged = function(sender, args)
    self.data.power_data = self.power_quickslot:GetShortcut():GetData()
    self.data.power_type = self.power_quickslot:GetShortcut():GetType()
    count = self:GetQuantity(self.power_quickslot)
    self:SetQuantityPower(count)
  end

  -- Timers
  self.morale_timer.TimerElapsed = function(sender, args)

    -- Update display
    if (self.morale_timer.count == 0 or self.morale_timer.count == self.data.morale_cooldown) then
      self.morale_timer:Stop()
      self.morale_cooldown_color:SetBackground(
        "DU_81/Palantir/Resources/Potions/morale/bar_green.tga")
      self.morale_cooldown_mask:SetWidth(0)

      -- Automatic drinking
      if (self.data.mode == MODE_AUTO) then
        frac = self:GetMoraleFraction()
        if (frac <= self.data.morale_threshold) then
          self.morale_quickslot.MouseClick(nil, nil)
        end
      end
    else
      percent = self.morale_timer.count / self.data.morale_cooldown
      pix = cooldown_width * percent
      self.morale_cooldown_mask:SetWidth(0 + pix)
    end
  end

  self.power_timer.TimerElapsed = function(sender, args)
    -- Update display
    if (self.power_timer.count == 0 or self.power_timer.count == self.data.power_cooldown) then
      self.power_timer:Stop()
      self.power_cooldown_color:SetBackground(
        "DU_81/Palantir/Resources/Potions/power/bar_green.tga")
      self.power_cooldown_mask:SetWidth(0)

      -- Automatic drinking
      if (self.data.mode == MODE_AUTO) then
        frac = self:GetPowerFraction()
        if (frac <= self.data.power_threshold) then
          self.power_quickslot.MouseClick(nil, nil)
        end
      end

    else
      percent = self.power_timer.count / self.data.power_cooldown
      pix = cooldown_width * percent
      self.power_cooldown_mask:SetWidth(0 + pix)
    end
  end

  -- Define the functions

  function self:HideQuickslots(mouse_visible)
    -- Hide everything related to the quickslots
    self.quickslot_window:SetOpacity(0.01)

    -- quickslot invisible
    self.morale_quickslot:SetVisible(mouse_visible);
    self.morale_quickslot:SetMouseVisible(mouse_visible);
    self.power_quickslot:SetVisible(mouse_visible);
    self.power_quickslot:SetMouseVisible(mouse_visible);

    -- set position + size
    x = main_window_width / 2 - 1
    self.morale_quickslot:SetSize(potion_width, potion_height)
    self.morale_quickslot:SetPosition(
      x - potion_horizontal_shift - potion_width, potion_vertical_shift)
    self.power_quickslot:SetSize(potion_width, potion_height)
    self.power_quickslot:SetPosition(
      x + potion_horizontal_shift, potion_vertical_shift)

    -- reset invisible
    self.morale_reset:SetVisible(false);
    self.power_reset:SetVisible(false);
    self.morale_reset:SetMouseVisible(false)
    self.power_reset:SetMouseVisible(false)
  end



  function self:ShowQuickslots()
    -- Show everything related to the quickslots
    self.quickslot_window:SetOpacity(1.)
    -- quickslots visible
    self.morale_quickslot:SetVisible(true);
    self.morale_quickslot:SetMouseVisible(true);
    self.power_quickslot:SetVisible(true);
    self.power_quickslot:SetMouseVisible(true);

    -- set position + size
    self.morale_quickslot:SetSize(quickslot_size, quickslot_size)
    self.morale_quickslot:SetPosition(0, 0)
    self.power_quickslot:SetSize(quickslot_size, quickslot_size)
    self.power_quickslot:SetPosition(
      main_window_width - quickslot_size, 0)

    -- reset visible
    self.morale_reset:SetVisible(true);
    self.morale_reset:SetMouseVisible(true)
    self.power_reset:SetVisible(true);
    self.power_reset:SetMouseVisible(true)
  end


  function self:SetDisplayMode(num)
    -- Toggle the visibility of the different windows
    self.data.display_mode = num
    if (num == 0) then
      -- Hide everything
      self.potions_window:SetVisible(false);
      self:HideQuickslots(false)
    elseif (num == 1) then
      -- Show only buttons and set quickslots behind them
      self.potions_window:SetVisible(true);
      self:HideQuickslots(true)
    elseif (num == 2) then
      -- Show Everything and set quickslots on sides
      self.potions_window:SetVisible(true);
      self:ShowQuickslots()
    end
  end

  function self:GetMoraleFraction()
    player = Turbine.Gameplay.LocalPlayer.GetInstance()
    frac = player:GetMorale() / player:GetMaxMorale()
    return frac
  end

  function self:DoMoraleAvailable()
    -- Enable the morale quickslot if morale is low enough
    -- Hide mask if no quickslots
    if (self.data.display_mode ~= 1) then
      return
    end

    -- Show mask
    frac = self:GetMoraleFraction()
    visible = (frac <= self.data.morale_threshold)

    -- Automatic drinking
    if (self.data.mode == MODE_AUTO and visible) then
      self.morale_quickslot.MouseClick(nil, nil)
      return
    end

    -- Make slot visible
    self.morale_avail:SetVisible(visible)
    if (self.data.mode == MODE_ALWAYS or
        self.data.mode == MODE_AUTO) then
      visible = true
    end
    self.morale_quickslot:SetMouseVisible(visible)
  end


  function self:GetPowerFraction()
    player = Turbine.Gameplay.LocalPlayer.GetInstance()
    frac = nil
    if player:GetClass() == Turbine.Gameplay.Class.Beorning then
      frac = player:GetClassAttributes():GetWrath() / 100
    else
      frac = player:GetPower() / player:GetMaxPower()
    end
    return frac
  end

  function self:DoPowerAvailable()
    -- Enable the power quickslot if power is low enough
    -- Hide mask if no quickslots
    if (self.data.display_mode ~= 1) then
      return
    end

    -- Show mask
    frac = self:GetPowerFraction()
    visible = (frac <= self.data.power_threshold)

    -- Automatic drinking
    if (self.data.mode == MODE_AUTO and visible) then
      self.power_quickslot.MouseClick(nil, nil)
      return
    end


    -- Make slot visible
    self.power_avail:SetVisible(visible)
    if (self.data.mode == MODE_ALWAYS or
        self.data.mode == MODE_AUTO) then
      visible = true
    end
    self.power_quickslot:SetMouseVisible(visible)
  end

  function self:GetQuantity(quickslot)
    -- Get the quantity of the item from the quickslot
    item = quickslot:GetShortcut()
    -- Deal with the case where quickslot is an item
    if (item:GetData() ~= "") then
      if (item:GetType() == Turbine.UI.Lotro.ShortcutType.Item) then
        item = item:GetItem()
        success, count = pcall(item.GetQuantity)
        if success then
          return count
        else
          return -1
        end
      end
    end
    -- Other cases
    return -1
  end

  function self:SetQuantityMorale(count)
    -- Set the counter
    if count < 0 then
      self.morale_counter_tens:SetVisible(false)
      self.morale_counter_units:SetVisible(false)
      return
    end
    if count > 99 then
      count = 99
    end

    if count < 10 then
      self.morale_counter_tens:SetVisible(false)
      self.morale_counter_units:SetVisible(true)
      self.morale_counter_units:SetBackground(
        "DU_81/Palantir/Resources/Potions/morale/" ..string.sub(count,1)..".tga")
    else
      self.morale_counter_tens:SetVisible(true)
      self.morale_counter_tens:SetBackground(
        "DU_81/Palantir/Resources/Potions/morale/" ..string.sub(count,1,1)..".tga")
      self.morale_counter_units:SetVisible(true)
      self.morale_counter_units:SetBackground(
        "DU_81/Palantir/Resources/Potions/power/" ..string.sub(count,2)..".tga")
    end
  end

  function self:SetQuantityPower(count)
    -- Set the counter
    if count < 0 then
      self.power_counter_tens:SetVisible(false)
      self.power_counter_units:SetVisible(false)
      return
    end
    if count > 99 then
      count = 99
    end

    if count < 10 then
      self.power_counter_tens:SetVisible(false)
      self.power_counter_units:SetVisible(true)
      self.power_counter_units:SetBackground(
        "DU_81/Palantir/Resources/Potions/power/" ..string.sub(count,1)..".tga")
    else
      self.power_counter_tens:SetVisible(true)
      self.power_counter_tens:SetBackground(
        "DU_81/Palantir/Resources/Potions/power/" ..string.sub(count,1,1)..".tga")
      self.power_counter_units:SetVisible(true)
      self.power_counter_units:SetBackground(
        "DU_81/Palantir/Resources/Potions/power/" ..string.sub(count,2)..".tga")
    end
  end

  function self:Refresh()
    player = Turbine.Gameplay.LocalPlayer.GetInstance()
    in_combat = player:IsInCombat()
    parent = self:GetParent()
    show_only_combat = parent.onlyInCombat

    test = self.data.display_mode == 2 or not show_only_combat
    test = test or (show_only_combat and in_combat)

    if (self.data.enabled and test) then
      self:SetVisible(true)
      self:SetMouseVisible(true)
    else
      self:SetVisible(false)
      self:SetMouseVisible(false)
    end
  end

  -- Call required functions
  self:SetDisplayMode(1)


  count = self:GetQuantity(self.morale_quickslot)
  self:SetQuantityMorale(count)
  count = self:GetQuantity(self.power_quickslot)
  self:SetQuantityPower(count)
end
