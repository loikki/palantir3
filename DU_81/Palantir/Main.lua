import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "DU_81.Palantir";

oM=OptionsManager();

local pluginName = "Palantir III";
local pluginVersion = "v3.3.2";
-- modify as well Palantir.plugincompendium & Palantir.plugin



local displayWidth, displayHeight = Turbine.UI.Display.GetSize();
local windowWidth,windowHeight = oM.mWindow:GetSize();
local width = (displayWidth-windowWidth)/2;
local height = (displayHeight-windowHeight)/2;
oM.defaultOptionsMain.PositionX=width;
oM.defaultOptionsMain.PositionY=height;
player = Turbine.Gameplay.LocalPlayer.GetInstance();
local main={};
local color={};
local text={};
local dbb={};
local mez={};
local qslot={};

LocalPlayer = Turbine.Gameplay.LocalPlayer.GetInstance();
EffectList = LocalPlayer:GetEffects();
-- Add and remove Calbacks functions
PalantirInCombatChanged =     function(sender, args)
  oM.mWindow.mzb.InCombatChanged(LocalPlayer:IsInCombat());
end

function InstallCallbacks()
DU_81.Utilsx.AddCallback(EffectList, "EffectAdded", PalantirEffectAddedCallback);
DU_81.Utilsx.AddCallback(EffectList, "EffectRemoved", PalantirEffectRemovedCallback);
DU_81.Utilsx.AddCallback(EffectList, "EffectCleared", PalantirEffectClearedCallback); -- May happend in death
DU_81.Utilsx.AddCallback(LocalPlayer, "InCombatChanged", PalantirInCombatChanged);
DU_81.Utilsx.AddCallback(Turbine.Chat, "Received", ParseMezText);

end

function UnloadPlugin()
  oM.mWindow.dbb.wqs:SetShortcut(nil);
  oM.mWindow.dbb.fqs:SetShortcut(nil);
  oM.mWindow.dbb.dqs:SetShortcut(nil);
  oM.mWindow.dbb.pqs:SetShortcut(nil);
  oM.mWindow.dbb.Refresh();
  oM.SaveSettings();

  oM.mWindow.potions.morale_quickslot:SetShortcut(nil)
  oM.mWindow.potions.power_quickslot:SetShortcut(nil)


  --remove callbacks here
  if (PalantirEffectChangedCallback ~= nil) then
    DU_81.Utilsx.RemoveCallback(EffectList, "EffectAdded", PalantirEffectAddedCallback);
    DU_81.Utilsx.RemoveCallback(EffectList, "EffectRemoved", PalantirEffectRemovedCallback);
    DU_81.Utilsx.RemoveCallback(EffectList, "EffectCleared", PalantirEffectClearedCallback);
    DU_81.Utilsx.RemoveCallback(LocalPlayer, "InCombatChanged", PalantirInCombatChanged);
    DU_81.Utilsx.RemoveCallback(Turbine.Chat, "Received", ParseMezText);
    PalantirEffectCAddedCallback = nil;
    PalantirEffectRemovedCallback = nil;
    PalantirEffectClearedCallback = nil;
    PalantirInCombatChanged = nil;
    ParseMezText = nil;
  end
end

-- Install callbacks once and Register once the unload function for the Palantir
window = Turbine.UI.Window();
window:SetWantsUpdates(true);
window.Update = function(sender,args)
  if (Plugins[pluginName] ~= nil) then
    Plugins[pluginName].Unload = function(self,sender,args)
                    UnloadPlugin();
                  end
    window:SetWantsUpdates(false);
    InstallCallbacks();
  end
end

function loadHandle()

end

function qsHandle()


end

function settingsDecode(t)
  local t2={};
  euroFormat=(tonumber("1,000")==1); -- will be true if the number is formatted with a comma for decimal place, false otherwise
  for k,v in pairs(t) do
    -- Needed to load English saved data with french Lotro, & viceversa
    if ((tonumber(v) == nil) and (type(v)=="string") and (string.find(v, "0x") == nil) and (string.find(v, "%d") == 1)) then
      if euroFormat then
        v=tonumber((string.gsub(v, "%.", ",")));
      else
        v=tonumber((string.gsub(v, "%,", ".")));
      end
    elseif(tonumber(v)~=nil and string.sub(v,1,2)~="0x")then
      v=tonumber(v);
    elseif (type(v)=="table")then
      v=settingsDecode(v);
    end
    t2[k]=v;
  end
  return t2
end

local tempLoad = Turbine.PluginData.Load(Turbine.DataScope.Account,"DU_81_Scope");
if(tempLoad~=nil)then
  tempLoad=settingsDecode(tempLoad);
  local scopeVal = tempLoad.Value;
  local scope="";
  if(scopeVal==1)then
    scope=Turbine.DataScope.Account;
  elseif(scopeVal==2)then
    scope=Turbine.DataScope.Character;
  else
    scope=Turbine.DataScope.Server
  end

  tempLoad = Turbine.PluginData.Load(scope,"DU_81_Main");
  if (tempLoad~=nil)then
    main  = settingsDecode(tempLoad);
  else
    main  = oM.defaultOptionsMain;
  end
  tempLoad=Turbine.PluginData.Load(scope,"DU_81_Color");
  if (tempLoad~=nil)then
    color = settingsDecode(tempLoad);
  else
    color = oM.defaultOptionsColor;
  end
  tempLoad=Turbine.PluginData.Load(scope,"DU_81_Text");
  if (tempLoad~=nil)then
    text  = settingsDecode(tempLoad);
  else
    text  = oM.defaultOptionsText;
  end
  tempLoad=Turbine.PluginData.Load(scope,"DU_81_Dbb");
  if (tempLoad~=nil)then
    dbb   = settingsDecode(tempLoad);
  else
    dbb   = oM.defaultOptionsDbb;
  end
  -- bad argument #1 to '?' (table expected, got nil) error if file doesn't exist
  tempLoad=Turbine.PluginData.Load(scope,"DU_81_Mez");
  if (tempLoad~=nil)then
    mez   = settingsDecode(tempLoad);
  else
    mez   = oM.defaultOptionsMez;
  end
  oM.loadSettings(settingsDecode(main),settingsDecode(color),settingsDecode(text),
                  settingsDecode(dbb),settingsDecode(mez));
  qslot = Turbine.PluginData.Load(Turbine.DataScope.Character,"DU_81_Quickslot");
  if(qslot ~= nil)then
    oM.AddShortcuts(settingsDecode(qslot));
  end
  qslot = Turbine.PluginData.Load(Turbine.DataScope.Character,"DU_81_Potions_data");
  if(qslot ~= nil)then
    oM.loadPotionsSettings(settingsDecode(qslot));
  else
    oM.sw.potions_default.MouseClick(nil, nil)
  end
else
  main  = oM.defaultOptionsMain;
  color = oM.defaultOptionsColor;
  text  = oM.defaultOptionsText;
  dbb   = oM.defaultOptionsDbb;
  mez   = oM.defaultOptionsMez;
  oM.loadSettings(main,color,text,dbb,mez);
end

-- Set [Show Options] button in Lotro plugin's manager
optionsPanel = Turbine.UI.Control();
--optionsPanel:SetBackColor( Turbine.UI.Color( 0.3, 0.3, 0.3 ) );
optionsPanel:SetSize( 600, 600 );
optionsButton=Turbine.UI.Lotro.Button();
optionsButton:SetSize(200,30);
optionsButton:SetText("Show Options");
optionsButton:SetParent(optionsPanel);
optionsButton.MouseClick=function(sender,args)
  if (oM.mWindow.holdSetup==false)then
      oM.sw.showSetup();
    else
      oM.mWindow.setupPending=true;
    end
end
if (plugin == nil)then
plugin = Turbine.UI.Control();

end
plugin.GetOptionsPanel = function( self )
    return optionsPanel;

end
Turbine.Shell.WriteLine(pluginName.." "..pluginVersion.." by Loikki");
Turbine.Shell.WriteLine("New features: new option to replace power by class specific resource and restore the options for the mez bars")
