import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
--import "DU_81.Utilsx.Class";

MainBar = class( Turbine.UI.Window );

function MainBar:Constructor(side)
	Turbine.UI.Window.Constructor( self );
	--self:SetBackColor(Turbine.UI.Color(.1,0,0,0));
	self:SetVisible(true);
	self.side = side;
	self.defaultColor={r=1,g=1,b=1};
	self.textDisplayType=3;
	self.color1={pct=0.50,r=255,g=255,b=0};
	self.color2={pct=0.25,r=255,g=0,b=0};
	self:SetSize(350,300);
	self:SetMouseVisible(false);
	self.enableBlending=false;
	self.enableTwoColor=false;
	self.enableEffects=false
	self.textWindow = Turbine.UI.Window();
	self.textWindow:SetVisible(true);
	self.textWindow:SetSize(350,300);
	self.textWindow:SetParent(self);
	self.textWindow:SetMouseVisible(false);
	self.textWindow:SetZOrder(99);
	self.textWindow:SetBackColor(Turbine.UI.Color(.01,0,0,0));
	--self.textWindow:SetBackColorBlendMode(2);
	--self.textWindow:SetBlendMode(2);
	self.contWindow=Turbine.UI.Control();
	self.contWindow:SetVisible(true);
	self.contWindow:SetSize(350,300);
	self.contWindow:SetParent(self);
	self.contWindow:SetMouseVisible(false);
	self.contWindow:SetZOrder(0);
	

	self.bgholder=Turbine.UI.Window()
	self.bgholder:SetVisible(true)
	self.bgholder:SetParent(self.contWindow);
	self.bgholder:SetSize(65,190);
	self.bgholder:SetZOrder(0);
	self.bgholder:SetPosition(143,55);
	self.bgholder:SetMouseVisible(false);
	
	self.bg=Turbine.UI.Control()
	self.bg:SetParent(self.bgholder);
	self.bg:SetSize(65,190);
	self.bg:SetBackground("DU_81/Palantir/Resources/bg3.tga");
	self.bg:SetZOrder(0);
	self.bg:SetMouseVisible(false);
	
	self.fillholder=Turbine.UI.Window()
	self.fillholder:SetVisible(true)
	self.fillholder:SetParent(self.contWindow);
	self.fillholder:SetSize(65,190);
	
	self.fillholder:SetZOrder(50);
	self.fillholder:SetPosition(143,55);
	self.fillholder:SetMouseVisible(false);
	self.fillholder:SetBackColor(Turbine.UI.Color(.001,0,0,0));
	
	self.bfillholder=Turbine.UI.Window();
	self.bfillholder:SetVisible(true)
	self.bfillholder:SetParent(self.contWindow);
	self.bfillholder:SetSize(65,190);
	self.bfillholder:SetZOrder(50);
	self.bfillholder:SetPosition(143,55);
	self.bfillholder:SetBackColor(Turbine.UI.Color(.001,0,0,0));
	
	self.dfillholder=Turbine.UI.Window();
	self.dfillholder:SetVisible(true)
	self.dfillholder:SetParent(self.contWindow);
	self.dfillholder:SetSize(65,190);
	self.dfillholder:SetZOrder(50);
	self.dfillholder:SetPosition(143,55);
	self.dfillholder:SetBackColor(Turbine.UI.Color(.001,0,0,0));
	
	self.fill=Turbine.UI.Control();
	self.fill:SetParent(self.fillholder);
	self.fill:SetSize(65,190);
	self.fill:SetBackground("DU_81/Palantir/Resources/inner.tga");
	self.fill:SetZOrder(0);
	self.bfill=Turbine.UI.Control();
	self.bfill:SetParent(self.bfillholder);
	self.bfill:SetSize(65,190);
	self.bfill:SetPosition(0,0);
	self.bfill:SetBackground("DU_81/Palantir/Resources/inner.tga");
	self.dfill=Turbine.UI.Control();
	self.dfill:SetParent(self.dfillholder);
	self.dfill:SetSize(65,190);
	self.dfill:SetPosition(0,0);
	self.dfill:SetBackground("DU_81/Palantir/Resources/inner.tga");
	
	self.fill:SetMouseVisible(false);
	self.overlay = Turbine.UI.Control();
	self.overlay:SetSize(65,190);
	self.overlay:SetPosition(0,0);
	self.overlay:SetParent(self.fill);
	self.overlay:SetBackColor(Turbine.UI.Color(1,1,0,0));
	--self.overlay:SetZOrder(0);
	self.overlay:SetBackColorBlendMode(5);
	self.overlay:SetMouseVisible(false);
	
	

	self.boverlay = Turbine.UI.Control();
	self.boverlay:SetSize(65,190);
	self.boverlay:SetPosition(0,0);
	self.boverlay:SetParent(self.bfill);
	self.boverlay:SetBackColor(Turbine.UI.Color(1,1,0,1));
	--self.boverlay:SetZOrder(0);
	self.boverlay:SetBackColorBlendMode(5);
	self.boverlay:SetMouseVisible(false);
	
	self.doverlay = Turbine.UI.Control();
	self.doverlay:SetSize(65,190);
	self.doverlay:SetPosition(0,0);
	self.doverlay:SetParent(self.dfill);
	self.doverlay:SetBackColor(Turbine.UI.Color(1,1,0,0));
	--self.doverlay:SetZOrder(0);
	self.doverlay:SetBackColorBlendMode(5);
	self.doverlay:SetMouseVisible(false);
	
	self.label=Turbine.UI.Label();
	self.label:SetParent(self.textWindow);
	self.label:SetPosition(0,0);
	self.label:SetForeColor(Turbine.UI.Color(1,0,1,0));
	self.label:SetBackColor(Turbine.UI.Color(.01,0,0,0));
	self.label:SetZOrder(99);
	--self.label:SetBackColorBlendMode(4);
	--self.label:SetBlendMode(4);
	self.label:SetSize(200,30);
	self.label:SetMouseVisible(false);
	self.label:SetFontStyle( Turbine.UI.FontStyle.Outline );
	self.label:SetOutlineColor( Turbine.UI.Color( 1,0, 0, 0 ) );
	self.label:SetFont(Turbine.UI.Lotro.Font.TrajanProBold16);
	self.label:SetText("100%\n999,999/999,999 ");
	

	
	
	if (self.side==0)then
		self.bg:SetBackground("DU_81/Palantir/Resources/bg3.tga");
		self.fill:SetBackground("DU_81/Palantir/Resources/inner.tga");
		self.bfill:SetBackground("DU_81/Palantir/Resources/inner.tga");
		self.dfill:SetBackground("DU_81/Palantir/Resources/inner.tga");
		self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
		self.label:SetPosition(60,130);
	else
		self.bg:SetBackground("DU_81/Palantir/Resources/bg4.tga");
		self.fill:SetBackground("DU_81/Palantir/Resources/innerb.tga");
		self.bfill:SetBackground("DU_81/Palantir/Resources/innerb.tga");
		self.dfill:SetBackground("DU_81/Palantir/Resources/innerb.tga");
		self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
		self.label:SetPosition(95,130);
	end
	
	self.SetDisplay=function(cur,maxx,tcur,tmax,dmax)

		self.maxloss=dmax-maxx;
	
		self.dif = ((maxx)-cur)/maxx;
		self.tdif = (tmax - tcur)/tmax;
		
		
		self.barDif = ((self.maxloss+maxx+tmax)-cur)/(tmax+maxx+self.maxloss);
		self.barpct = (maxx)/(tmax+maxx);
		self.tpct = (1-self.tdif);
		self.bHeight = (190*(((maxx+tmax+self.maxloss)-(maxx+self.maxloss))/(maxx+tmax+self.maxloss)));
		self.dHeight = (190*(((maxx+tmax+self.maxloss)-(maxx+tmax))/(maxx+tmax+self.maxloss)));
		self.bfillholder:SetHeight(self.bHeight*self.tpct);
		self.bfill:SetHeight(190);
		self.bfill:SetTop(0-(self.barDif*190)+self.bHeight-(self.bHeight-(self.bHeight*self.tpct)));
		self.bfillholder:SetTop(((self.barDif*190)+55+(self.bHeight-(self.bHeight*self.tpct)))-self.bHeight);
		self.dfillholder:SetTop(55);
		self.dfill:SetTop(0);
		self.dfillholder:SetHeight(self.dHeight);
		self.dfill:SetHeight(190);
		self.fill:SetTop(0-(self.barDif*190));
		self.fillholder:SetTop((self.barDif*190)+55);
		--Turbine.Shell.WriteLine(self.dHeight);
		
		--self.bfillholder:SetHeight((1-self.barpct)*190);
		--self.boverlay:SetHeight((1-self.barpct)*190);
		self.pct = (1-(self.dif));
	--Turbine.Shell.WriteLine(self.barDif.." "..self.bfillholder:GetTop());
		self.SetValText(cur,maxx,tcur,tmax); 
			if(self.enableEffects==true)then
				if (self.enableBlending == true and self.enableTwoColor==true)then
					if (self.color1.pct > self.color2.pct)then
						if (self.pct >= self.color1.pct)then
							self.colorValPct=(1-(self.pct - self.color1.pct) /(1-self.color1.pct));
							self.newColorR=((self.color1.r-self.defaultColor.r)*self.colorValPct)+self.defaultColor.r;
							
							self.newColorG=((self.color1.g - self.defaultColor.g)*self.colorValPct)+self.defaultColor.g;
							self.newColorB=((self.color1.b - self.defaultColor.b)*self.colorValPct)+self.defaultColor.b;
							self.SetColor(self.newColorR,self.newColorG,self.newColorB);
						elseif (self.pct < self.color1.pct)then
							if (self.pct > self.color2.pct)then
								self.colorValPct=(1-(self.pct - self.color2.pct)/(self.color1.pct - self.color2.pct));
								self.newColorR=((self.color2.r-self.color1.r)*self.colorValPct)+self.color1.r;
								self.newColorG=((self.color2.g-self.color1.g)*self.colorValPct)+self.color1.g;
								self.newColorB=((self.color2.b-self.color1.b)*self.colorValPct)+self.color1.b;
								self.SetColor(self.newColorR,self.newColorG,self.newColorB);
							else
								self.SetColor(self.color2.r,self.color2.g,self.color2.b);
							end
						end
					else
						--if color2.pct > self.color1.pct
						if (self.pct >= self.color2.pct)then
						self.colorValPct=1-(self.pct - self.color2.pct) /(1-self.color2.pct);
						self.newColorR=((self.color2.r-self.defaultColor.r)*self.colorValPct)+self.defaultColor.r;
						self.newColorG=((self.color2.g - self.defaultColor.g)*self.colorValPct)+self.defaultColor.g;
						self.newColorB=((self.color2.b - self.defaultColor.b)*self.colorValPct)+self.defaultColor.b;
						self.SetColor(self.newColorR,self.newColorG,self.newColorB);
						elseif (self.pct < self.color2.pct)then
							if (self.pct > self.color1.pct)then
								self.colorValPct=(1-(self.pct - self.color1.pct)/(self.color2.pct - self.color1.pct));
								self.newColorR=((self.color1.r-self.color2.r)*self.colorValPct)+self.color2.r;
								self.newColorG=((self.color1.g-self.color2.g)*self.colorValPct)+self.color2.g;
								self.newColorB=((self.color1.b-self.color2.b)*self.colorValPct)+self.color2.b;
								self.SetColor(self.newColorR,self.newColorG,self.newColorB);
							else
								self.SetColor(self.color1.r,self.color1.g,self.color1.b);
							end
						end
					end
				elseif(self.enableTwoColor==false and self.enableBlending==true)then
				
					if (self.pct >= self.color1.pct)then
							self.colorValPct=1-(self.pct - self.color2.pct) /(1-self.color2.pct);
							self.newColorR=((self.color1.r-self.defaultColor.r)*self.colorValPct)+self.defaultColor.r;
							self.newColorG=((self.color1.g - self.defaultColor.g)*self.colorValPct)+self.defaultColor.g;
							self.newColorB=((self.color1.b - self.defaultColor.b)*self.colorValPct)+self.defaultColor.b;
							self.SetColor(self.newColorR,self.newColorG,self.newColorB);
					else
							self.SetColor(self.color1.r,self.color1.g,self.color1.b);
					end		
				elseif(self.enableTwoColor==false and self.enableBlending==false)then
					if (self.pct >= self.color1.pct)then
						self.SetColor(self.defaultColor.r,self.defaultColor.g,self.defaultColor.b);
					else
						self.SetColor(self.color1.r,self.color1.g,self.color1.b);
					end
				else
					if (self.color1.pct > self.color2.pct)then
						if (self.pct > self.color1.pct)then
							self.SetColor(self.defaultColor.r,self.defaultColor.g,self.defaultColor.b);
						elseif (self.pct <= self.color1.pct)then
							if (self.pct > self.color2.pct)then
								self.SetColor(self.color1.r,self.color1.g,self.color1.b);
							else
								self.SetColor(self.color2.r,self.color2.g,self.color2.b);
							end
						end
					else
						--if color2.pct > self.color1.pct
						if (self.pct > self.color2.pct)then
							self.SetColor(self.defaultColor.r,self.defaultColor.g,self.defaultColor.b);
						elseif (self.pct <= self.color2.pct)then
							if (self.pct > self.color1.pct)then
								self.SetColor(self.color2.r,self.color2.g,self.color2.b);
							else
								self.SetColor(self.color1.r,self.color1.g,self.color1.b);
							end
						end
					end
				end
			end
	end
	
	self.SetColor=function(r,g,b)

		self.color = Turbine.UI.Color((r/255),(g/255),(b/255))
		self.overlay:SetBackColor(self.color);
	end
	
	
	
	self.SetValText=function(cur,maxx,tcur,tmax)
		
		
		if (self.textDisplayType==1)then
			if(tmax~=0)then
				self.label:SetText((math.floor((100-((((maxx+tmax)-(cur+tcur))/(maxx+tmax))*100))+0.5)).."% ("..(math.floor((100-(((tmax-tcur)/tmax)*100))+0.5)).."%)");
			else	
				self.label:SetText((math.floor((100-(((maxx-cur)/maxx)*100))+0.5)).."%");
			end
		elseif (self.textDisplayType==2)then
			if(tmax~=0)then
				self.label:SetText(math.floor((tcur+cur)+.5).." ("..math.floor((tcur)+.5)..")");
			else	
				self.label:SetText(math.floor((cur)+.5))
			end
		elseif (self.textDisplayType==3)then
			if(tmax~=0)then
				self.label:SetText(math.floor((cur+tcur)+.5).."/"..maxx+tmax.." ("..math.floor((tcur)+.5)..") ");
			else
				self.label:SetText(math.floor(cur+.5).."/"..maxx);
			end
		else
			if(tmax~=0)then
				self.label:SetText(((math.floor((100-((((maxx+tmax)-(cur+tcur))/(maxx+tmax))*100))+0.5)).."% ("..(math.floor((100-(((tmax-tcur)/tmax)*100))+0.5)).."%)\n"..math.floor((cur+tcur)+.5).."/"..maxx+tmax.." ("..math.floor((tcur)+.5))..") ");
			else	
				self.label:SetText((math.floor((100-(((maxx-cur)/maxx)*100))+0.5)).."%\n"..math.floor(cur+.5).."/"..maxx.." ");
			end
		end
	end
	
	self.SetGfxOpacity=function(op)
		self.fillholder:SetOpacity(op);
		self.bfillholder:SetOpacity(op);
		self.dfillholder:SetOpacity(op);
		self.bgholder:SetOpacity(op);
	end
	
	self.SetTextOpacity=function(op)
		self.textWindow:SetOpacity(op);

		
	end
	
	function self:SetTextColor(color)
		
		self.label:SetForeColor(color);
	end
	
	self.SetTextPosition=function(pos)
		if (self.side==0)then
			if (pos==Turbine.UI.ContentAlignment.TopRight)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.TopRight);
				self.label:SetPosition(-40,60);
			elseif (pos==Turbine.UI.ContentAlignment.TopCenter)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.BottomCenter);
				self.label:SetPosition(75,30);
			elseif (pos==Turbine.UI.ContentAlignment.TopLeft)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.TopLeft);
				self.label:SetPosition(205,60);
			elseif (pos==Turbine.UI.ContentAlignment.MiddleLeft)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleLeft);
				self.label:SetPosition(175,130);
			elseif (pos==Turbine.UI.ContentAlignment.BottomLeft)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.BottomLeft);
				self.label:SetPosition(205,212);
			elseif (pos==Turbine.UI.ContentAlignment.BottomCenter)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.TopCenter);
				self.label:SetPosition(85,245);
			elseif (pos==Turbine.UI.ContentAlignment.BottomRight)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.BottomRight);
				self.label:SetPosition(-30,212);
			elseif (pos==Turbine.UI.ContentAlignment.MiddleRight)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleRight);
				self.label:SetPosition(-55,130);
			end
		else
			if (pos==Turbine.UI.ContentAlignment.TopRight)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.TopLeft);
				self.label:SetPosition(190,60);
			elseif (pos==Turbine.UI.ContentAlignment.TopCenter)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.BottomCenter);
				self.label:SetPosition(75,30);
			elseif (pos==Turbine.UI.ContentAlignment.TopLeft)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.TopRight);
				self.label:SetPosition(-55,60);
			elseif (pos==Turbine.UI.ContentAlignment.MiddleLeft)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleRight);
				self.label:SetPosition(-20,130);
			elseif (pos==Turbine.UI.ContentAlignment.BottomLeft)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.BottomRight);
				self.label:SetPosition(-60,212);
			elseif (pos==Turbine.UI.ContentAlignment.BottomCenter)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.TopCenter);
				self.label:SetPosition(67,245);
			elseif (pos==Turbine.UI.ContentAlignment.BottomRight)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.BottomLeft);
				self.label:SetPosition(188,212);
			elseif (pos==Turbine.UI.ContentAlignment.MiddleRight)then
				self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleLeft);
				self.label:SetPosition(210,130);
			end
		end
	end
end