import "DU_81.Palantir";
import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
--import "DU_81.Utilsx.Class";



Header = class( Turbine.UI.Control );

function Header:Constructor()
	Turbine.UI.Control.Constructor( self );
	self:SetSize(400,30);
	self:SetMouseVisible(false);
	self:SetBackground("DU_81/Palantir/Resources/options_panel_divider.tga");
	self.lWindow=Turbine.UI.Control();
	self.lWindow:SetParent(self);
	self.lWindow:SetSize(400,30);
	self.lWindow:SetPosition(0,0);
	self.lWindow:SetMouseVisible(false);
	self.label=Turbine.UI.Label();
	self.label:SetParent(self.lWindow);
	self.label:SetFont(Turbine.UI.Lotro.Font.TrajanPro18);
	self.label:SetSize(400,30);
	self.label:SetPosition(0,0);
	self.label:SetForeColor(Turbine.UI.Color(244/255,255/255,51/255));
	self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
	self.label:SetFontStyle(Turbine.UI.FontStyle.Outline);
	self.label:SetOutlineColor(Turbine.UI.Color(0,0,0));
	self.label:SetMouseVisible(false);
	self.label:SetText("Bob");

	self.SetText=function(str)
		self.label:SetText(str);
	end
	
end