import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
--import "DU_81.Utilsx.Class";

MainWindow = class( Turbine.UI.Window );

function MainWindow:Constructor()

  Turbine.UI.Window.Constructor( self );
  self:SetVisible(true);
  self.l=self:GetLeft();
  self:SetSize(520,300);
  self:SetMouseVisible(false);
  self.Override = false;
  self.isLocked=true;
  self.doLayout=false;
  self.hideUI=false;
  self.mouseDown=false;
  self.dragXoffset=0;
  self.dragYoffset=0;
  self.onlyInCombat=false;
  self.inCombat=false;
  self.oocCriteriaMode = 1;
  self.showDebuffBar=true;
  self.dbbOpacity=1;
  self.dbbIndOpacity=1;
  self.barOpacity=1;
  self.txtOpacity=1;
  self.pvGroupTextDisplay=true;
  self.pvGroupTextPosition=true;
  self.holdSetup=true;
  self.setupPending=false;
  self.setupVisible=false;
  self.mainFading = false;
  self.dbbFading = false;
  self.mainHidden=false;
  self.dbbHidden=false;
  self.showMezBar=true;
  self.mezOpacity=1;
  self.mezFading = false;
  self.mezHidden=false;
  self.x=0;
  self.fadeOutTimer = DU_81.Utilsx.Timer();
  self.fadeOutDbbTimer=DU_81.Utilsx.Timer();
  self.fadeOutMezTimer=DU_81.Utilsx.Timer();
  function round(num, idp)
    local mult = 10^(idp or 0)
    return math.floor(num * mult + 0.5) / mult
  end
  self:SetWantsUpdates(true);
  self:SetWantsKeyEvents(true);
  self.posWindow=PosWindow();
  self.posWindow:SetParent(self);
  self.posWindow:SetSize(520,300);
  self.player = Turbine.Gameplay.LocalPlayer.GetInstance()
  self.moraleBar=MainBar(0);
  self.moraleBar:SetParent(self);
  self.moraleBar:SetPosition(0,0);
  self.powerBar=MainBar(1);
  self.powerBar:SetParent(self);
  self.powerBar:SetPosition(170,0);
  self.classSpecificPowerBar = false;

  self.moraleBar.SetColor(0,255,0);
  self.moraleBar.defaultColor={r=0,g=255,b=0};
  self.moraleBar.SetGfxOpacity(.5);
  self.powerBar.SetColor(0,150,255);
  self.powerBar.defaultColor={r=0,g=150,b=255};
  self.powerBar.SetGfxOpacity(.5);
  self.moraleBar.SetTextOpacity(1);
  self.powerBar.SetTextOpacity(1);


  -- Load Debuffbar
  self.dbb = DebuffBar();
  self.dbb:SetParent(self);
  self.dbb:SetLeft((self:GetWidth()-self.dbb:GetWidth())/2);
  self.dbb:SetTop(self:GetHeight()-self.dbb:GetHeight());

  -- Load Potions
  self.potions = Potions();
  self.potions:SetParent(self);
  self.potions:SetLeft((self:GetWidth()-self.potions:GetWidth())/2);
  self.potions:SetTop((self:GetHeight()-self.potions:GetHeight())/2
    + self.potions.vertical_shift)

  -- Load MezBar
  self.mzb = MezBar();
  self.mzb:SetParent(self);
  self.mzb:SetLeft((self:GetWidth()-self.mzb:GetWidth())/2);
  self.mzb:SetTop(0); --self:GetHeight()-self.mzb:GetHeight());



  if(self.linkText==true)then
    self.moraleBar.SetTextPosition(Turbine.UI.ContentAlignment.BottomRight);
    self.powerBar.SetTextPosition(Turbine.UI.ContentAlignment.BottomRight);
  else


  end
  self.setupReady=function()

  end

  self.displayAll=function()

    self:SetVisibility("visible");
    self.mainHidden=false;
    self.dbbHidden=false;
    self.mezHidden=false;
    if(self.showDebuffBar==true)then
      self.dbb.SetBarOpacity(self.dbbOpacity);
      if (self.dbb.displayMode>=2)then
        self.dbb:SetOpacity(1);
        self.dbb.SetIndOpacity(self.dbbIndOpacity);
        self.dbb.qsWindow:SetVisible(true);
      end
    end
    if(self.showMezBar==true)then
      self.mzb.SetBarOpacity(self.mezOpacity);
    end


  end

  self.FadeOutMain=function()
    self.holdSetup=true;
    self.mainFading=true;
    self.fadeOutTimer.AutoReset=true;
    self.fadeOutTimer.Interval=0.033;
    self.fadeOutTimer.Start();

  end

  self.FadeOutDbb=function()
    self.holdSetup=true;
    self.dbbFading=true;
    self.fadeOutDbbTimer.AutoReset=true;
    self.fadeOutDbbTimer.Interval=0.033;
    self.fadeOutDbbTimer.Start();
  end

  self.FadeOutMez=function()
    self.holdSetup=true;
    self.mezFading=true;
    self.fadeOutMezTimer.AutoReset=true;
    self.fadeOutMezTimer.Interval=0.033;
    self.fadeOutMezTimer.Start();
  end


  self.fadeOutDbbTimer.TimerElapsed=function(sender,args)
    dbbOpacityRate=0;
    dbbCurableRate=0;
    dbbIndRate=0;
    if (self.showDebuffBar==true)then
      dbbOpacityRate = self.dbbOpacity/30;
      if (self.dbb.displayMode>=2)then
        dbbCurableRate=.033;
        dbbIndRate=self.dbbIndOpacity/30;
      end
    end
    dbo= self.dbbOpacity - (self.fadeOutDbbTimer.count * dbbOpacityRate);
    dbi= self.dbbIndOpacity - (self.fadeOutDbbTimer.count * dbbIndRate);
    co= 1-(self.fadeOutDbbTimer.count * dbbCurableRate);
    self.dbb.SetBarOpacity(dbo);
    self.dbb.SetIndOpacity(dbi);
    self.dbb:SetOpacity(co);
    if (dbo <= 0)then
      self.fadeOutDbbTimer.Stop();
      self.dbbFading=false;
      self.holdSetup=false;
      self.dbbHidden=true;
      self.dbb.qsWindow:SetVisible(false);
      if (self.setupPending==true)then
        self.setupReady();
      end
    end
  end

  self.fadeOutMezTimer.TimerElapsed=function(sender,args)
    local barOpacityRate = self.mezOpacity/30;
    local bo = self.mezOpacity - (self.fadeOutMezTimer.count * barOpacityRate);
    if (self.showMezBar==false)then
      barOpacityRate = 0;
      bo = 0;
    end
    self.mzb.SetBarOpacity(bo);
    if (bo <= 0)then
      self.fadeOutMezTimer.Stop();
      self.mezFading=false;
      self.mezHidden=true;
    end

  end	--fadeOutMezTimer


  self.fadeOutTimer.TimerElapsed=function(sender,args)
    local barOpacityRate = self.barOpacity/30;
    local textOpacityRate = self.txtOpacity/30;
    local bo = self.barOpacity - (self.fadeOutTimer.count * barOpacityRate);
    local to = self.txtOpacity -(self.fadeOutTimer.count * textOpacityRate);
    self.SetBarOpacity(bo);
    self.SetTxtOpacity(to);


    if (bo <= 0)then
      self.fadeOutTimer.Stop();
      self:SetVisibility("hidden");
      self.SetBarOpacity(self.barOpacity);
      self.SetTxtOpacity(self.txtOpacity);
      self.holdSetup=false;
      self.mainHidden=true;
      self.mainFading=false;
    end
    --self.mWindow.dbb.qsWindow:SetOpacity(co);


  end

  function self:SetVisibility(bool)
    if(bool=="visible")then
      self.moraleBar:SetVisible(true);
      self.powerBar:SetVisible(true);
    elseif(bool=="hidden")then
      self.moraleBar:SetVisible(false);
      self.powerBar:SetVisible(false);
    end
  end

  self.shouldUseClassSpecific=function(player)
    class = player:GetClass()
    if class == Turbine.Gameplay.Class.Beorning then
      return true;
    end
    if not self.classSpecificPowerBar then
      return false;
    end
    if class == Turbine.Gameplay.Class.Champion or
      class == Turbine.Gameplay.Class.Hunter or
      class == Turbine.Gameplay.Class.RuneKeeper
    -- Not implemented yet by lotro
    -- or class == Turbine.Gameplay.class.Brawler
    then
      return true;
    else
      return false;
    end
  end

  self.getPower=function(player)
    if self.shouldUseClassSpecific(player) then
      -- Class specific
      if player:GetClass() == Turbine.Gameplay.Class.Beorning then
        -- Beo
        return player:GetClassAttributes():GetWrath();
      elseif player:GetClass() == Turbine.Gameplay.Class.Champion then
        -- Champ
        return player:GetClassAttributes():GetFervor();
      elseif player:GetClass() == Turbine.Gameplay.Class.Hunter then
        -- Hunter
        return player:GetClassAttributes():GetFocus();
      elseif player:GetClass() == Turbine.Gameplay.Class.RuneKeeper then
        -- RK
        return player:GetClassAttributes():GetAttunement();
      elseif player:GetClass() == Turbine.Gameplay.Class.Brawler then
        -- Brawler
        return player:GetClassAttributes():GetMettle();
      else
        -- default
        return player:GetPower();
      end
    else
      -- Normal power
      return player:GetPower();
    end;
  end

  self.getMaxPower=function(player)
    if self.shouldUseClassSpecific(player) then
      -- Class specific
      if player:GetClass() == Turbine.Gameplay.Class.Beorning then
        -- Beo
        return 100;
      elseif player:GetClass() == Turbine.Gameplay.Class.Champion then
        -- Champ
        return 5;
      elseif player:GetClass() == Turbine.Gameplay.Class.Hunter then
        -- Hunter
        return 9;
      elseif player:GetClass() == Turbine.Gameplay.Class.RuneKeeper then
        -- RK
        return 20;
      elseif player:GetClass() == Turbine.Gameplay.Class.Brawler then
        -- Brawler
        return 9;
      else
        -- default
        return player:GetMaxPower();
      end
    else
      -- Normal power
      return player:GetMaxPower();
    end;
  end

  self.getBaseMaxPower=function(player)
    if self.shouldUseClassSpecific(player) then
      -- Class specific
      if player:GetClass() == Turbine.Gameplay.Class.Beorning then
        -- Beo
        return 100;
      elseif player:GetClass() == Turbine.Gameplay.Class.Champion then
        -- Champ
        return 5;
      elseif player:GetClass() == Turbine.Gameplay.Class.Hunter then
        -- Hunter
        return 9;
      elseif player:GetClass() == Turbine.Gameplay.Class.RuneKeeper then
        -- RK
        return 20;
      elseif player:GetClass() == Turbine.Gameplay.Class.Brawler then
        -- Brawler
        return 9;
      else
        -- default
        return player:GetBaseMaxPower();
      end
    else
      -- Normal power
      return player:GetBaseMaxPower();
    end;
  end

  self.Update=function(sender,args)
    --self:SetVisible(false);
    --self:SetVisible(true);
    Power = self.getPower(self.player)
    MaxPower = self.getMaxPower(self.player)
    BaseMaxPower = self.getBaseMaxPower(self.player)

    if (self.Override == false)then
      if(self.onlyInCombat==true)then
        if(self.player:IsInCombat()==true)then
          self.inCombat=true;
          if(self.hideUI==false)then
            self.displayAll();
          end
          if (self.showDebuffBar==true)then
            --self.dbb:SetVisible(true);
          end
        else
          self.inCombat=false;
          if (self.dbb.GetIndicatorCount()==0 and self.dbbFading==false)then
              if(self.dbbHidden==false)then
                self.FadeOutDbb();
              end
              if(self.mezHidden==false)then
                self.FadeOutMez();
              end
            end
            --if(self.mainFading==true)then Turbine.Shell.WriteLine("true")else Turbine.Shell.WriteLine("false")end
          if(self.setupVisible==false and self.mainFading==false)then
            if(self.mainHidden==false)then
              if(self.oocCriteriaMode==1)then
                if(round(self.player:GetMorale(),0)==round(self.player:GetMaxMorale(),0) and round(Power,0)==round(MaxPower,0))then
                  self.FadeOutMain();
                end
              elseif(self.oocCriteriaMode==2)then
                if(round(self.player:GetMorale(),0)==round(self.player:GetMaxMorale(),0))then
                  self.FadeOutMain();
                end
              elseif(self.oocCriteriaMode==3)then
                  if(round(Power,0)==round(MaxPower,0))then
                    self.FadeOutMain();
                  end
              else
                self.FadeOutMain();
              end
            end
          end
        end
      end


      self.moraleBar.SetDisplay(round(self.player:GetMorale(),0),round(self.player:GetMaxMorale(),0),round(self.player:GetTemporaryMorale(),0),round(self.player:GetMaxTemporaryMorale(),0),round(self.player:GetBaseMaxMorale(),0));
      self.powerBar.SetDisplay(round(Power,0),round(MaxPower,0),0,0,round(BaseMaxPower,0));

    end
  end

  self.posWindow.topBar.MouseDown=function(sender,args)
    self.isLocked=false;
    self.mouseDown = true;
    self.dragXoffset=(self:GetLeft()+args.X)-self:GetLeft();
    self.dragYoffset=(self:GetTop()+args.Y)-self:GetTop();
    self.posWindow.doMouse(2);
  end

  self.posWindow.topBar.MouseUp=function(sender,args)
    self.isLocked=true;
    self.mouseDown = false;
    self.posWindow.doMouse(1);
  end

  self.posWindow.topBar.MouseMove=function(sender,args)
    if(self.mouseDown==true)then
    self:SetLeft((self:GetLeft()+args.X)-self.dragXoffset);
    self:SetTop((self:GetTop()+args.Y)-self.dragYoffset);

    self.PosChanged(self:GetLeft(),self:GetTop())

    self.posWindow.getLeft=self:GetLeft();
    self.posWindow.getTop=self:GetTop();
    self.posWindow:SetTooltip();
    end
  end

  self.PosChanged=function(x,y)

  end

  self.KeyDown = function(sender,args)
    if (args.Action == 268435579)then --Turbine.UI.Lotro.Action.RepositionUI=268435579;

      if(self.doLayout==true)then
        self.doLayout=false;
        self.positionMode();
        self.posWindow:SetVisible(false);
      else
        self.doLayout=true;
        if(self.hideUI==false)then
        self.positionMode();
        self.posWindow:SetVisible(true);
        end
      end
    end
    if (args.Action == 268435635)then -- Turbine.UI.Lotro.Action.ToggleHUD=268435635;

      if(self.hideUI==true)then
        self.hideUI=false;
        self.hidingUI();
        if(self.onlyInCombat==false)then
          self:SetVisibility("visible");
        elseif(self.onlyInCombat==true and self.inCombat==true)then
          self:SetVisibility("visible");
        end
        if(self.doLayout==true)then
          self.positionMode();
          self.posWindow:SetVisible(true);
        end
        if(self.showDebuffBar==true)then
          self.dbb:SetVisible(true);
        end
        if(self.showMezBar==true)then
          self.mzb:SetVisible(true);
        end
      else
        self.hideUI=true;
        self.hidingUI();
        self:SetVisibility("hidden");
        self.posWindow:SetVisible(false);
        self.dbb:SetVisible(false);
        self.mzb:SetVisible(false);

      end
    end
  end

  self.positionMode=function()

  end

  self.hidingUI=function()

  end


  self.SetBarOpacity=function(num)
    self.moraleBar.SetGfxOpacity(num);
    self.powerBar.SetGfxOpacity(num);
  end

  self.SetTxtOpacity=function(num)
    self.moraleBar.SetTextOpacity(num);
    self.powerBar.SetTextOpacity(num);
  end

  self.SetDbbPosition=function(pct)
    -- if (pct == nil) then pct  = 0; end --AntiBug
    self.dbb:SetTop((400*pct)+(220));
    self:SetHeight((self.dbb:GetTop()+self.dbb:GetHeight()))
    self.posWindow.SizeChanged();
  end

  self.SetMezAreaPosition=function(pct)
    --Turbine.Shell.WriteLine("SetMezAreaPosition: " .. tostring(pct));
    --if (pct == nil) then pct  = 0; end --AntiBug
    self.mzb:SetTop((500*pct)-80);
    self.posWindow.SizeChanged();
    self.mzb.PanelVerticalPosition = 80-(400*pct)+80;
  end

  self.SetEffects=function(bl,bar)
    bar.enableEffects=bl;
  end

  self.SetBlending=function(bl,bar)
    bar.enableBlending=bl;
  end

  self.SetTwoColor=function(bl,bar)
    bar.enableTwoColor=bl;
  end

  self.SetDefaultColor=function(color,bar)
    bar.SetColor(color.R*255,color.G*255,color.B*255);
    bar.defaultColor={r=(color.R*255),g=(color.G*255),b=(color.B*255)};

  end

  self.SetColorOne=function(color,bar)
    bar.color1.r=color.R*255;
    bar.color1.g=color.G*255;
    bar.color1.b=color.B*255;
  end

  self.SetColorTwo=function(color,bar)
    bar.color2.r=color.R*255;
    bar.color2.g=color.G*255;
    bar.color2.b=color.B*255;
  end

  self.SetPctOne=function(pct,bar)
    bar.color1.pct=pct;
  end

  self.SetPctTwo=function(pct,bar)
    bar.color2.pct=pct;
  end

  self.SetLevel=function(pct,bar)
    if(bar=="morale")then
      self.clevel=round(self.player:GetMaxMorale(),0)*pct;
      self.moraleBar.SetDisplay(self.clevel,round(self.player:GetMaxMorale(),0),0,0,round(self.player:GetBaseMaxMorale(),0));
      --self:SetVisibility("hidden");
      --self:SetVisibility("visible");
    else
      Power = self.getPower(self.player);
      MaxPower = self.getMaxPower(self.player);
      BaseMaxPower = self.getBaseMaxPower(self.player);
      self.clevel=round(MaxPower,0)*pct;
      self.powerBar.SetDisplay(self.clevel,round(MaxPower,0),0,0,round(BaseMaxPower,0));
      --self:SetVisibility("hidden");
      --self:SetVisibility("visible");
    end
  end

  self.SetTextPosition=function(posNum,bar)
    local pos;
    if(posNum==1)then
      pos=Turbine.UI.ContentAlignment.BottomRight;
    elseif(posNum==2)then
      pos=Turbine.UI.ContentAlignment.BottomCenter;
    elseif(posNum==3)then
      pos=Turbine.UI.ContentAlignment.BottomLeft;
    elseif(posNum==4)then
      pos=Turbine.UI.ContentAlignment.MiddleRight;
    elseif(posNum==5)then
      pos=Turbine.UI.ContentAlignment.MiddleLeft;
    elseif(posNum==6)then
      pos=Turbine.UI.ContentAlignment.TopRight;
    elseif(posNum==7)then
      pos=Turbine.UI.ContentAlignment.TopCenter;
    else
      pos=Turbine.UI.ContentAlignment.TopLeft;
    end
    bar.SetTextPosition(pos);
  end

  self.SetTextDisplay=function(disNum,bar)
    Power = self.getPower(self.player);
    MaxPower = self.getMaxPower(self.player);
    BaseMaxPower = self.getBaseMaxPower(self.player);
    bar.textDisplayType=disNum;
    self.moraleBar.SetDisplay(round(self.player:GetMorale(),0),round(self.player:GetMaxMorale(),0),round(self.player:GetTemporaryMorale(),0),round(self.player:GetMaxTemporaryMorale(),0),round(self.player:GetBaseMaxMorale(),0));
    self.powerBar.SetDisplay(round(Power,0),round(MaxPower,0),0,0,round(BaseMaxPower,0));

  end

  self.SetOverride=function(bool)
    if(bool==true)then
      self.Override=true;
      self.dbb.Override=true;
    else
      self.Override=false;
      self.dbb.Override=false;
    end
  end

  self.player.MaxTemporaryMoraleChanged=function(sender,args)
    if (self.moraleBar:IsVisible()==true)then
      self.moraleBar:SetVisible(false);
      self.moraleBar:SetVisible(true);
    else
      self.moraleBar:SetVisible(true);
      self.moraleBar:SetVisible(false);
    end
  end

  self.player.BaseMaxMoraleChanged=function(sender,args)
    if (self.moraleBar:IsVisible()==true)then
      self.moraleBar:SetVisible(false);
      self.moraleBar:SetVisible(true);
    else
      self.moraleBar:SetVisible(true);
      self.moraleBar:SetVisible(false);
    end
  end

  self.player.TemporaryMoraleChanged=function(sender,args)
    if (self.moraleBar:IsVisible()==true)then
      self.moraleBar:SetVisible(false);
      self.moraleBar:SetVisible(true);
    else
      self.moraleBar:SetVisible(true);
      self.moraleBar:SetVisible(false);
    end
  end

  self.WindowManager=function(str)
  --This should handle the main states. First, when the setup window opens, everything that is hidden via
  --out of combat should appear, without opening anything that is hidden via the setup window.

  end
end
--Turbine.Shell.WriteLine("Palantir Test:  MainWindow.lua");
