import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "DU_81.Palantir";


OptionsManager = class(DU_81.DUInterface.Window );
function OptionsManager:Constructor()
  Turbine.UI.Window.Constructor( self);
self.version=1.0;
self.defaultOptionsMain={Version=1,PositionX=372,PositionY=343,showOnlyInCombat=true,showInGameVitals=true;outOfCombatCriteria=1,settingScope=1,ShowWarnings=true,ShowTimer=true,BarOpacity=.5,BarSpacing=0,TextOpacity=.5,ClassSpecificPowerBar=false};
self.defaultOptionsColor={MoraleBarColor={r=0,g=255,b=0},MoraleTextColor={r=0,g=255,b=0},MbEnableColorEffects=true,MbEnableColorTransitions=true,MbPointOneColor={r=255,g=255,b=0},MbPointOneLocation=0.5,MbEnableSecondColor=true,MbPointTwoColor={r=255,g=0,b=0},MbPointTwoLocation=0.25,PowerBarColor={r=0,g=150,b=255},PowerTextColor={r=0,g=255,b=0},PbEnableColorEffects=true,PbEnableColorTransitions=true,PbPointOneColor={r=255,g=255,b=0},PbPointOneLocation=0.5,PbEnableSecondColor=true,PbPointTwoColor={r=255,g=0,b=0},PbPointTwoLocation=0.25}
self.defaultOptionsText={GroupTextPosition=true,VitalsTextPosition=1,GroupTextDisplay=true,VitalsTextDisplay=4,MoraleTextPosition=1,MoraleTextDisplay=4,PowerTextPosition=1,PowerTextDisplay=4};
self.defaultOptionsDbb={ShowDebuff=true,ShowFlash=false,DebuffDisplayMode=2;DebuffBarOpacity=.5,IndicatorOpacity=.5,VerticalPosition=0,WoundCooldown=30,FearCooldown=30,DiseaseCooldown=30,PoisonCooldown=30};
self.defaultOptionsMez={MezBarShow=true,MezBarShowTimer=true,MezBarTimerTick=60,MezBarOpacity=.4,MezBarVertical=.14,MezBarAutoSearch=true,LM_Mez=30,LM_Cmd=30,LM_Igr=30,MN_Sol=30,RK_Ice=20,RK_Heal=30,CA_Rid=30,CA_Trk=30};
self.default_options_potions={
  morale_threshold = 0.3, power_threshold = 0.3,
  enabled = true,
  morale_cooldown = 120, power_cooldown = 120,
  mode = 1,
};
self.mWindow = MainWindow();

function GetRealLocale()
  if(Turbine.Shell.IsCommand("zusatzmodule")==true)then
    return "de";
  elseif(Turbine.Shell.IsCommand("conseil")==true)then
    return "fr";
  else
    return "en";
  end
end

function self:GetSetupWindow()
return self.sw;
end
self.t = self.mWindow:GetTop();
self.settingScope=1;
self.palSetup = Turbine.ShellCommand();
self.setup = Turbine.Shell.AddCommand("palantir;pph",self.palSetup);
self.setupVisible=false;
self.referrer="main";
self.init=true;
self.mWidth=self.mWindow:GetWidth();
self.sw=SetupWindow(DU_81.Palantir.GetRealLocale());

self.sw:SetZOrder(30);
self.mWindow:SetZOrder(-50);
self.sw:SetVisible(false);
self.mWindow.SetBarOpacity(0);
self.mWindow.SetTxtOpacity(0);
self.mWindow.dbb:SetOpacity(0);
self.mWindow.dbb.SetBarOpacity(0);
self.mWindow.dbb.qsWindow:SetVisible(false);
self.mWindow.mzb:SetBarOpacity(0, true); -- MezBar
self.mWindow.SetOverride(true);
  self:SetSize(Turbine.UI.Display:GetWidth(),Turbine.UI.Display:GetHeight());
  self:SetMouseVisible(false);
  self:SetPosition(0,0);
  --self:SetBackColor(Turbine.UI.Color(0,0,0));
--self.setupVisible=true;
self.fadeInTimer = DU_81.Utilsx.Timer();
self.dbiTimer = DU_81.Utilsx.Timer();
self.barTimer = DU_81.Utilsx.Timer();
self.SetMainPosition=function(x,y)
    local displayWidth, displayHeight = Turbine.UI.Display.GetSize();
    local windowWidth,windowHeight = self.mWindow:GetSize();
    self.mWindow:SetPosition(((displayWidth-windowWidth)/2)+x,((displayHeight - windowHeight)/2)+y);
  end

self.mWindow.positionMode=function()
  if(self.mWindow.doLayout==true)then
    self.mWindow.SetOverride(true);
    self.mWindow.SetLevel(1,"morale");
    self.mWindow.SetLevel(1,"power");
    self.mWindow.displayAll();

  else
    if(self.sw:IsVisible()==false)then
      self.mWindow.SetOverride(false);
    end
  end
end

self.mWindow.hidingUI=function()
  if(self.mWindow.hideUI==true)then
    self.sw:SetVisible(false);
  else
    if(self.mWindow.setupVisible==true)then
      self.sw.showSetup();
    end
  end
end

self.SetBarSpacing=function(pct,source)
  self.pctBase = pct*460;
  self.mWindow.moraleBar:SetLeft(0-self.pctBase);
  self.mWindow.powerBar:SetLeft(170+self.pctBase);

end


self.SaveSettings = function()
  self.toSaveMain.PositionX=self.mWindow:GetLeft();
  self.toSaveMain.PositionY=self.mWindow:GetTop();
  local scope = "";
  if(tonumber(self.toSaveMain.settingScope)==1)then
    scope = Turbine.DataScope.Account;
  elseif(tonumber(self.toSaveMain.settingScope)==2)then
    scope = Turbine.DataScope.Character;
  else
    scope = Turbine.DataScope.Server;
  end
  local scopeSetting={}
  scopeSetting.Value=self.toSaveMain.settingScope;
  Turbine.PluginData.Save(Turbine.DataScope.Account,"DU_81_Scope",self:settingsEncode(scopeSetting));
  Turbine.PluginData.Save(scope, "DU_81_Main", self:settingsEncode(self.toSaveMain));
  Turbine.PluginData.Save(scope, "DU_81_Color", self:settingsEncode(self.toSaveColor));
  Turbine.PluginData.Save(scope, "DU_81_Text", self:settingsEncode(self.toSaveText));
  Turbine.PluginData.Save(scope, "DU_81_Dbb", self:settingsEncode(self.toSaveDbb));
  Turbine.PluginData.Save(scope, "DU_81_Mez", self:settingsEncode(self.toSaveMez));
  Turbine.PluginData.Save(Turbine.DataScope.Character, "DU_81_Quickslot", self:settingsEncode(self.mWindow.dbb.qslot));
  Turbine.PluginData.Save(Turbine.DataScope.Character, "DU_81_Potions_data", self:settingsEncode(self.mWindow.potions.data));
end

self.AddShortcuts = function(qslot)
  pcall(function()
    self.mWindow.dbb.wqs:SetShortcut(Turbine.UI.Lotro.Shortcut(qslot.sOneType,qslot.sOneData));
  end);
  pcall(function()
    self.mWindow.dbb.fqs:SetShortcut(Turbine.UI.Lotro.Shortcut(qslot.sTwoType,qslot.sTwoData));
  end);
  pcall(function()
    self.mWindow.dbb.dqs:SetShortcut(Turbine.UI.Lotro.Shortcut(qslot.sThreeType,qslot.sThreeData));
  end);
  pcall(function()
    self.mWindow.dbb.pqs:SetShortcut(Turbine.UI.Lotro.Shortcut(qslot.sFourType,qslot.sFourData));
  end);
  self.mWindow.dbb.qslot=qslot;
  if (qslot.sOneCD=="" or qslot.sOneCD==nil)then
    qslot.sOneCD="30"
  end
  if (qslot.sTwoCD=="" or qslot.sTwoCD==nil)then
    qslot.sTwoCD="30"
  end
  if (qslot.sThreeCD=="" or qslot.sThreeCD==nil)then
    qslot.sThreeCD="30"
  end
  if (qslot.sFourCD=="" or qslot.sFourCD==nil)then
    qslot.sFourCD="30"
  end
  if (qslot.sOneQty=="" or qslot.sOneQty==nil)then
    qslot.sOneQty="0"
  end
  if (qslot.sTwoQty=="" or qslot.sTwoQty==nil)then
    qslot.sTwoQty="0"
  end
  if (qslot.sThreeQty=="" or qslot.sThreeQty==nil)then
    qslot.sThreeQty="0"
  end
  if (qslot.sFourQty=="" or qslot.sFourQty==nil)then
    qslot.sFourQty="0"
  end
  self.mWindow.dbb.updateCooldowns(tonumber(qslot.sOneCD),tonumber(qslot.sTwoCD),tonumber(qslot.sThreeCD),tonumber(qslot.sFourCD));
  self.mWindow.dbb.updateQty(tonumber(qslot.sOneQty),tonumber(qslot.sTwoQty),tonumber(qslot.sThreeQty),tonumber(qslot.sFourQty));
end

self.loadPotionsSettings = function(qslot)
  self.mWindow.potions.data = qslot;
  pcall(function()
      self.mWindow.potions.morale_quickslot:SetShortcut(
        Turbine.UI.Lotro.Shortcut(qslot.morale_type,qslot.morale_data));
  end);

  pcall(function()
      self.mWindow.potions.power_quickslot:SetShortcut(
        Turbine.UI.Lotro.Shortcut(qslot.power_type,qslot.power_data));
  end);

  self.UpdateOptionsPotions()
  self.mWindow.potions:Refresh()
end

self.sw.showSetup = function()
  --Curable Cooldowns--
  self.sw.wnd:SetText(self.mWindow.dbb.qslot.sOneCD);
  self.sw.fr:SetText(self.mWindow.dbb.qslot.sTwoCD);
  self.sw.dis:SetText(self.mWindow.dbb.qslot.sThreeCD);
  self.sw.psn:SetText(self.mWindow.dbb.qslot.sFourCD);
  -- Timer Ticks
  self.sw.tick:SetText(self.mWindow.mzb.TimerTick);
  --testing for changes
  self.init=false;
  self.sw:SetVisible(true);
  self.sw:Activate();
  --self.sw:Focus();
  self.setupVisible=true;
  self.mWindow.potions:SetDisplayMode(2)
  self.mWindow.dbb:showQuickslots();
  self.mWindow.SetOverride(true);
  self.mWindow.SetLevel(1,"morale");
  self.mWindow.SetLevel(1,"power");
  self.mWindow.displayAll();
  self.mWindow.setupVisible=true;
  self.mWindow.setupPending=false;
  -- Some effects durations can be auto fixed when casting spell on self
  self.sw.LM_4:SetText(self.mWindow.mzb.LM_4);
  self.revertMez.LM_Igr = self.mWindow.mzb.LM_4;
  self.sw.MN_3:SetText(self.mWindow.mzb.MN_3);
  self.revertMez.MN_Sol = self.mWindow.mzb.MN_3;

  -- Potions
  self.UpdateOptionsPotions()
end

self.mWindow.setupReady=function()
  self.sw.showSetup();
end

self.sw.closeSetup = function()
  self.mWindow.potions:SetDisplayMode(1)
  self.mWindow.dbb:hideQuickslots();
  self.mWindow.setupVisible=false;
end

self.sw.Closing = function(sender,args)
  if(self.init==false)then
    self.sw.closeSetup();
    self.mWindow.dbb.Refresh();
    self.mWindow.mzb.updateTick(tonumber(self.sw.tick:GetText())); -- Treat 1st in MezBar.lua for mini 15"
    self.revertMez.MezBarTimerTick = self.mWindow.mzb.TimerTick; --Change in settings table as well for saving on disk

    -- Cds
    self.revertMez.LM_Mez = tonumber(self.sw.LM_1:GetText()) --LM_Mez=30,LM_Cmd=30,LM_Igr=30};
    self.mWindow.mzb.LM_1 = self.revertMez.LM_Mez;
    self.revertMez.LM_Cmd = tonumber(self.sw.LM_3:GetText()) --LM_Mez=30,LM_Cmd=30,LM_Igr=30};
    self.mWindow.mzb.LM_3 = self.revertMez.LM_Cmd;
    self.revertMez.LM_Igr = tonumber(self.sw.LM_4:GetText()) --LM_Mez=30,LM_Cmd=30,LM_Igr=30};
    self.mWindow.mzb.LM_4 = self.revertMez.LM_Igr;
    self.revertMez.MN_Sol = tonumber(self.sw.MN_3:GetText()) --MN_Sol=30
    self.mWindow.mzb.MN_3 = self.revertMez.MN_Sol;
    self.revertMez.RK_Ice = tonumber(self.sw.RK_1:GetText()) --RK_Ice=20,RK_Heal=30
    self.mWindow.mzb.RK_1 = self.revertMez.RK_Ice;
    self.revertMez.RK_Heal = tonumber(self.sw.RK_4:GetText()) --RK_Ice=20,RK_Heal=30
    self.mWindow.mzb.RK_4H = self.revertMez.RK_Heal;
    self.revertMez.CA_Rid = tonumber(self.sw.CA_1:GetText()) --CA_Rid=30,CA_Trk=30
    self.mWindow.mzb.CA_1 = self.revertMez.CA_Rid;
    self.revertMez.CA_Trk = tonumber(self.sw.CA_3:GetText()) --CA_Trk=30
    self.mWindow.mzb.CA_3 = self.revertMez.CA_Trk;


    --self.mWindow.dbb.updateQty(tonumber(qslot.sOneQty),tonumber(qslot.sTwoQty),tonumber(qslot.sThreeQty),tonumber(qslot.sFourQty));
    self.mWindow.dbb.updateCooldowns(tonumber(self.sw.wnd:GetText()),tonumber(self.sw.fr:GetText()),tonumber(self.sw.dis:GetText()),tonumber(self.sw.psn:GetText()));

    self.mWindow.SetLevel(1,"morale");
    self.mWindow.SetLevel(1,"power");
    self.mWindow.SetOverride(false);
    self.toSaveMain = table.copy(self.revertMain);
    self.toSaveColor = table.copy(self.revertColor);
    self.toSaveText = table.copy(self.revertText);
    self.toSaveDbb = table.copy(self.revertDbb);
    self.toSaveMez = table.copy(self.revertMez);

    self.mWindow.dbb.updateCooldowns(tonumber(self.sw.wnd:GetText()),tonumber(self.sw.fr:GetText()),tonumber(self.sw.dis:GetText()),tonumber(self.sw.psn:GetText()));
        --self.mWindow.dbb.updateQty(tonumber(qslot.sOneQty),tonumber(qslot.sTwoQty),tonumber(qslot.sThreeQty),tonumber(qslot.sFourQty));

    self.sw.revMain:SetEnabled(false);
    self.sw.revColor:SetEnabled(false);
    self.sw.revText:SetEnabled(false);
    self.sw.revMain4:SetEnabled(false);
    self.sw.revMain5:SetEnabled(false);
    self.mWindow.dbb.Refresh();
    self.SaveSettings();
  end
end

self.mWindow.PosChanged=function(x,y)
  self.toSaveMain.PositionX=x;
  self.toSaveMain.PositionY=y;
  if(self.mWindow.isLocked==false)then
    self.l = self.mWindow:GetLeft();
  end
end

self.Update=function(sender,args)
  --Turbine.Shell.WriteLine(self.toSaveMain.PositionX.." "..self.toSaveMain.PositionY);
end


self.palSetup.Execute = function(sf,cmd,args)
  if(args == "options")then
    if (self.mWindow.holdSetup==false)then
      self.sw.showSetup();
    else
      self.mWindow.setupPending=true;
    end
  elseif(args=="clear")then
    self.mWindow.dbb.wqs:SetShortcut(nil);
    self.mWindow.dbb.fqs:SetShortcut(nil);
    self.mWindow.dbb.dqs:SetShortcut(nil);
    self.mWindow.dbb.pqs:SetShortcut(nil);
    self.mWindow.dbb.qslot={};
    self.mWindow.dbb.Refresh();
    self.mWindow.potions.morale_quickslot:SetShortcut(nil)
    self.mWindow.potions.power_quickslot:SetShortcut(nil)
  elseif(args=="reset")then
    if(self.mWindow.doLayout==true)then
        self.mWindow.doLayout=false;
        self.mWindow.posWindow:SetVisible(false);
      else
        self.mWindow.doLayout=true;
        self.mWindow.posWindow:SetVisible(true);
      end
  else
    Turbine.Shell.WriteLine("usage: /palantir options - open Options panel");
    Turbine.Shell.WriteLine("/palantir reset - reset UI Positioning toggle");
    if (self.mWindow.holdSetup==false)then
      self.sw.showSetup();
    else
      self.mWindow.setupPending=true;
    end
  end
end

function table.copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end

function self:settingsEncode(t)
  local t2={};
  local t3={};
  for k,v in pairs(t) do
    if(type(v)=="number")then
      v=tostring(v);
    elseif (type(v)=="table")then
      v=self:settingsEncode(v);
    end
    t2[k]=v;
  end
  return t2
end

self:SetWantsUpdates(true);

--Options settings--


self.updateOptionsMain = function (main)
  self.l = main.PositionX;

  self.mWindow:SetPosition(main.PositionX,main.PositionY);

  --Show only in combat--
  self.sw.showincombat:SetChecked(main.showOnlyInCombat);
  self.mWindow.onlyInCombat=main.showOnlyInCombat;
  --Out of Combat Criteria--
  self.sw.noc.SetIndex(main.outOfCombatCriteria);
  self.mWindow.oocCriteriaMode=main.outOfCombatCriteria;
  --Show Default Vitals--
  self.sw.sv:SetChecked(main.showInGameVitals);
  Turbine.UI.Lotro.LotroUI.SetEnabled( Turbine.UI.Lotro.LotroUIElement.Vitals, main.showInGameVitals );
  --Settings Scope--
  self.sw.ac.SetIndex(main.settingScope);
  self.settingScope=main.settingScope;
  --Warning--
  self.sw.Warn:SetChecked(main.ShowWarnings);
  self.mWindow.mzb.ShowWarnings = main.ShowWarnings;
  --Timer--
  self.sw.ShowTimer:SetChecked(main.ShowTimer);
  self.mWindow.mzb.ShowTimer = main.ShowTimer;
  --Class Specific--
  self.sw.ClassSpecificPowerBar:SetChecked(main.ClassSpecificPowerBar);
  self.mWindow.classSpecificPowerBar = main.ClassSpecificPowerBar

  --Player Vitals Bar Opacity--
  self.sw.voSld:SetValue(main.BarOpacity);
  self.mWindow.SetBarOpacity(main.BarOpacity);
  self.mWindow.barOpacity=main.BarOpacity;
  --Player Vitals Bar Spacing--
  self.sw.bpSld:SetValue(main.BarSpacing);
  self.SetBarSpacing(main.BarSpacing);

  --Player Vitals Text Opacity--
  self.sw.txSld:SetValue(main.TextOpacity);
  self.mWindow.SetTxtOpacity(main.TextOpacity);
  self.mWindow.txtOpacity=main.TextOpacity;
end

self.updateOptionsDbb = function (Dbb)
  --Show Debuff Bar--
  self.sw.sdb:SetChecked(Dbb.ShowDebuff);
  self.mWindow.dbb:SetVisible(Dbb.ShowDebuff);
  self.mWindow.showDebuffBar=Dbb.ShowDebuff;
  self.sw.sdb2:SetEnabled(Dbb.ShowDebuff);
  self.sw.debuffDisplay.SetEnabled(Dbb.ShowDebuff);
  self.sw.dboSld:SetEnabled(Dbb.ShowDebuff);
  self.sw.dbiSld:SetEnabled(Dbb.ShowDebuff);
  self.sw.dbpSld:SetEnabled(Dbb.ShowDebuff);

  --Show Screen warning--
  self.sw.sdb2:SetChecked(Dbb.ShowFlash);
  self.mWindow.dbb:SetDisplayMode2(Dbb.ShowFlash);

  --Debuff Bar Display Mode--
  self.sw.debuffDisplay.SetIndex(Dbb.DebuffDisplayMode);
  self.mWindow.dbb:SetDisplayMode(Dbb.DebuffDisplayMode);

  --Debuff Bar Opacity--
  self.sw.dboSld:SetValue(Dbb.DebuffBarOpacity);
  self.mWindow.dbbOpacity=Dbb.DebuffBarOpacity;
  self.mWindow.dbb.SetBarOpacity(Dbb.DebuffBarOpacity);
  --Debuff Bar Indicator Opacity--
  self.sw.dbiSld:SetValue(Dbb.IndicatorOpacity);
  self.mWindow.dbbIndOpacity=Dbb.IndicatorOpacity;
  self.mWindow.dbb.SetIndOpacity(Dbb.IndicatorOpacity);
  --Debuff Bar Vertical Position--
  self.sw.dbpSld:SetValue(Dbb.VerticalPosition);
  self.mWindow.SetDbbPosition(Dbb.VerticalPosition);
  --Curable Cooldowns--
  self.sw.wnd:SetText(self.mWindow.dbb.qslot.sOneCD);
  self.sw.fr:SetText(self.mWindow.dbb.qslot.sTwoCD);
  self.sw.dis:SetText(self.mWindow.dbb.qslot.sThreeCD);
  self.sw.psn:SetText(self.mWindow.dbb.qslot.sFourCD);
end

--Color--

self.updateOptionsColor = function (color)
  self.sw.pmbBcOL:SetBackColor(Turbine.UI.Color((color.MoraleBarColor.r/255),(color.MoraleBarColor.g/255),(color.MoraleBarColor.b/255)));
  self.mWindow.SetDefaultColor(self.sw.pmbBcOL:GetBackColor(),self.mWindow.moraleBar);
  self.mWindow.moraleBar:SetTextColor(Turbine.UI.Color((color.MoraleTextColor.r/255),(color.MoraleTextColor.g/255),(color.MoraleTextColor.b/255)));
  self.sw.pmbTcOL:SetBackColor(Turbine.UI.Color((color.MoraleTextColor.r/255),(color.MoraleTextColor.g/255),(color.MoraleTextColor.b/255)));
  self.sw.pmbEce:SetChecked(color.MbEnableColorEffects);
  self.sw.pmbClrOneCp.SetEnabled(color.MbEnableColorEffects);
  self.sw.pmbCpOneSld:SetEnabled(color.MbEnableColorEffects);
  self.sw.pmbClrTwoCp.SetEnabled(color.MbEnableColorEffects);
  self.sw.pmbCpTwoSld:SetEnabled(color.MbEnableColorEffects);
  self.sw.pmbEsc:SetEnabled(color.MbEnableColorEffects);
  self.sw.pmbEct:SetEnabled(color.MbEnableColorEffects);
  self.mWindow.SetEffects(color.MbEnableColorEffects,self.mWindow.moraleBar);
  self.sw.pmbEct:SetChecked(color.MbEnableColorTransitions);
  self.mWindow.SetBlending(color.MbEnableColorTransitions,self.mWindow.moraleBar);
  self.sw.pmbClrOneOL:SetBackColor(Turbine.UI.Color((color.MbPointOneColor.r/255),(color.MbPointOneColor.g/255),(color.MbPointOneColor.b/255)));
  self.mWindow.SetColorOne(self.sw.pmbClrOneOL:GetBackColor(),self.mWindow.moraleBar);
  self.sw.pmbCpOneSld:SetValue(color.MbPointOneLocation);
  self.mWindow.SetPctOne(color.MbPointOneLocation,self.mWindow.moraleBar);
  self.sw.pmbEsc:SetChecked(color.MbEnableSecondColor);
  if(color.MbEnableColorEffects == true)then
    self.sw.pmbClrTwoCp.SetEnabled(color.MbEnableSecondColor);
    self.sw.pmbCpTwoSld:SetEnabled(color.MbEnableSecondColor);
  end
  self.mWindow.SetTwoColor(color.MbEnableSecondColor,self.mWindow.moraleBar);
  self.sw.pmbClrTwoOL:SetBackColor(Turbine.UI.Color((color.MbPointTwoColor.r/255),(color.MbPointTwoColor.g/255),(color.MbPointTwoColor.b/255)));
  self.mWindow.SetColorTwo(self.sw.pmbClrTwoOL:GetBackColor(),self.mWindow.moraleBar);
  self.sw.pmbCpTwoSld:SetValue(color.MbPointTwoLocation);
  self.mWindow.SetPctTwo(color.MbPointTwoLocation,self.mWindow.moraleBar);
  self.sw.ppbBcOL:SetBackColor(Turbine.UI.Color((color.PowerBarColor.r/255),(color.PowerBarColor.g/255),(color.PowerBarColor.b/255)));
  self.mWindow.SetDefaultColor(self.sw.ppbBcOL:GetBackColor(),self.mWindow.powerBar);
  self.mWindow.powerBar:SetTextColor(Turbine.UI.Color((color.PowerTextColor.r/255),(color.PowerTextColor.g/255),(color.PowerTextColor.b/255)));
  self.sw.ppbTcOL:SetBackColor(Turbine.UI.Color((color.PowerTextColor.r/255),(color.PowerTextColor.g/255),(color.PowerTextColor.b/255)));

  self.sw.ppbEce:SetChecked(color.PbEnableColorEffects);
  self.sw.ppbClrOneCp.SetEnabled(color.PbEnableColorEffects);
  self.sw.ppbCpOneSld:SetEnabled(color.PbEnableColorEffects);
  self.sw.ppbClrTwoCp.SetEnabled(color.PbEnableColorEffects);
  self.sw.ppbCpTwoSld:SetEnabled(color.PbEnableColorEffects);
  self.sw.ppbEsc:SetEnabled(color.PbEnableColorEffects);
  self.sw.ppbEct:SetEnabled(color.PbEnableColorEffects);
  self.mWindow.SetEffects(color.PbEnableColorEffects,self.mWindow.powerBar);
  self.sw.ppbEct:SetChecked(color.PbEnableColorTransitions);
  self.mWindow.SetBlending(color.PbEnableColorTransitions,self.mWindow.powerBar);
  self.sw.ppbClrOneOL:SetBackColor(Turbine.UI.Color((color.PbPointOneColor.r/255),(color.PbPointOneColor.g/255),(color.PbPointOneColor.b/255)));
  self.mWindow.SetColorOne(self.sw.ppbClrOneOL:GetBackColor(),self.mWindow.powerBar);
  self.sw.ppbCpOneSld:SetValue(color.PbPointOneLocation);
  self.mWindow.SetPctOne(color.PbPointOneLocation,self.mWindow.powerBar);
  self.sw.ppbEsc:SetChecked(color.PbEnableSecondColor);
  self.mWindow.SetTwoColor(color.PbEnableSecondColor,self.mWindow.powerBar);
  if(color.PbEnableColorEffects == true)then
    self.sw.ppbClrTwoCp.SetEnabled(color.PbEnableSecondColor);
    self.sw.ppbCpTwoSld:SetEnabled(color.PbEnableSecondColor);
  end
  self.sw.ppbClrTwoOL:SetBackColor(Turbine.UI.Color((color.PbPointTwoColor.r/255),(color.PbPointTwoColor.g/255),(color.PbPointTwoColor.b/255)));
  self.mWindow.SetColorTwo(self.sw.ppbClrTwoOL:GetBackColor(),self.mWindow.powerBar);
  self.sw.ppbCpTwoSld:SetValue(color.PbPointTwoLocation);
  self.mWindow.SetPctTwo(color.PbPointTwoLocation,self.mWindow.powerBar);


end


--Text--

self.updateOptionsText = function (text)
  self.sw.gTxtPos:SetChecked(text.GroupTextPosition);
  self.sw.txtPos.SetEnabled(text.GroupTextPosition);
  self.sw.pmbTxtPos.SetEnabled(not text.GroupTextPosition);
  self.sw.ppbTxtPos.SetEnabled(not text.GroupTextPosition);
  self.mWindow.pvGroupTextPosition=text.GroupTextPosition;
  self.sw.txtPos.SetIndex(text.VitalsTextPosition);
  if(self.mWindow.pvGroupTextPosition==true)then
    self.mWindow.SetTextPosition(text.VitalsTextPosition,self.mWindow.moraleBar);
    self.mWindow.SetTextPosition(text.VitalsTextPosition,self.mWindow.powerBar);
  end
  self.sw.gTxtDis:SetChecked(text.GroupTextDisplay);
  self.sw.txtDis.SetEnabled(text.GroupTextDisplay);
  self.sw.pmbTxtDis.SetEnabled(not text.GroupTextDisplay);
  self.sw.ppbTxtDis.SetEnabled(not text.GroupTextDisplay);
  self.mWindow.pvGroupTextDisplay=text.GroupTextDisplay;
  self.sw.txtDis.SetIndex(text.VitalsTextDisplay);
  if(self.mWindow.pvGroupTextDisplay==true)then
    self.mWindow.SetTextDisplay(text.VitalsTextDisplay,self.mWindow.moraleBar);
    self.mWindow.SetTextDisplay(text.VitalsTextDisplay,self.mWindow.powerBar);
  end

  self.sw.pmbTxtPos.SetIndex(text.MoraleTextPosition);
  if(self.mWindow.pvGroupTextPosition==false)then
    self.mWindow.SetTextPosition(text.MoraleTextPosition,self.mWindow.moraleBar);
  end
  self.sw.pmbTxtDis.SetIndex(text.MoraleTextDisplay);
  if(self.mWindow.pvGroupTextDisplay==false)then
    self.mWindow.SetTextDisplay(text.MoraleTextDisplay,self.mWindow.moraleBar);
  end

  self.sw.ppbTxtPos.SetIndex(text.PowerTextPosition);
  if(self.mWindow.pvGroupTextPosition==false)then
    self.mWindow.SetTextPosition(text.PowerTextPosition,self.mWindow.powerBar);
  end
  self.sw.ppbTxtDis.SetIndex(text.PowerTextDisplay);
  if(self.mWindow.pvGroupTextDisplay==false)then
    self.mWindow.SetTextDisplay(text.PowerTextDisplay,self.mWindow.powerBar);
  end

end


self.updateOptionsMez = function (opt)
  -- Mez settings
  --Show Effect timer bars--
  self.sw.mezCB1:SetChecked(opt.MezBarShow);
  self.mWindow.mzb:SetVisible(opt.MezBarShow);
  self.mWindow.showMezBar=opt.MezBarShow;
  self.sw.mezCB2:SetEnabled(opt.MezBarShow);
  self.sw.mezCB3:SetEnabled(opt.MezBarShow);
  self.sw.mezSld1:SetEnabled(opt.MezBarShow);
  self.sw.mezSld2:SetEnabled(opt.MezBarShow);
  -- Show/Hide Combat timer
  self.sw.mezCB2:SetChecked(opt.MezBarShowTimer);
  self.mWindow.mzb.TimerArea:SetVisible(opt.MezBarShowTimer);
  -- Combat Timer Ticks
  self.mWindow.mzb.updateTick(opt.MezBarTimerTick);
  self.sw.tick:SetText(self.mWindow.mzb.TimerTick);
  --Mez Bar Opacity--
  self.sw.mezSld1:SetValue(opt.MezBarOpacity);
  self.mWindow.mezOpacity=opt.MezBarOpacity;
  self.mWindow.mzb.SetBarOpacity(opt.MezBarOpacity);
  --Mez Bar Vertical Position--
  self.sw.mezSld2:SetValue(opt.MezBarVertical);
  self.mWindow.SetMezAreaPosition(opt.MezBarVertical);
  -- AutoCD
  self.sw.mezCB3:SetChecked(opt.MezBarAutoSearch);
  self.mWindow.mzb.AutoDetectDuration = opt.MezBarAutoSearch;
  -- CDs LM_Mez=30,LM_Cmd=30,LM_Igr=30,MN_Sol=30,RK_Ice=20,RK_Heal=30,CA_Rid=30,CA_Trk=30
  self.sw.LM_1:SetText(opt.LM_Mez);
  self.mWindow.mzb.LM_1 = self.revertMez.LM_Mez;
  self.sw.LM_3:SetText(opt.LM_Cmd);
  self.mWindow.mzb.LM_3 = self.revertMez.LM_Cmd;
  self.sw.LM_4:SetText(opt.LM_Igr);
  self.mWindow.mzb.LM_4 = self.revertMez.LM_Igr;
  self.sw.MN_3:SetText(opt.MN_Sol);
  self.mWindow.mzb.MN_3 = self.revertMez.MN_Sol;
  self.sw.RK_1:SetText(opt.RK_Ice);
  self.mWindow.mzb.RK_1 = self.revertMez.RK_Ice;
  self.sw.RK_4:SetText(opt.RK_Heal);
  self.mWindow.mzb.RK_4H = self.revertMez.RK_Heal;
  self.sw.CA_1:SetText(opt.CA_Rid);
  self.mWindow.mzb.CA_1 = self.revertMez.CA_Rid;
  self.sw.CA_3:SetText(opt.CA_Trk);
  self.mWindow.mzb.CA_3 = self.revertMez.CA_Trk;
end

-- Potions
self.ApplyOptionsPotions = function()
  self.mWindow.potions.data.enabled = self.sw.potions_enable:IsChecked()
  self.mWindow.potions.data.mode = self.sw.potions_automatic.GetIndex()
  self.mWindow.potions.data.morale_cooldown = tonumber(self.sw.potions_morale_cooldown:GetText())
  self.mWindow.potions.data.power_cooldown = tonumber(self.sw.potions_power_cooldown:GetText())
  self.mWindow.potions.data.morale_threshold = tonumber(self.sw.potions_morale_threshold:GetText()) / 100
  self.mWindow.potions.data.power_threshold = tonumber(self.sw.potions_power_threshold:GetText()) / 100
  self.mWindow.potions:Refresh()
end

self.UpdateOptionsPotions = function()
  -- Potions
  self.sw.potions_enable:SetChecked(self.mWindow.potions.data.enabled)
  self.sw.potions_automatic.SetIndex(self.mWindow.potions.data.mode)
  self.sw.potions_morale_cooldown:SetText(self.mWindow.potions.data.morale_cooldown)
  self.sw.potions_power_cooldown:SetText(self.mWindow.potions.data.power_cooldown)
  self.sw.potions_morale_threshold:SetText(100 * self.mWindow.potions.data.morale_threshold)
  self.sw.potions_power_threshold:SetText(100 * self.mWindow.potions.data.power_threshold)
end

self.InitSettings=function()
  --this will apply the full settings
  self.updateOptionsMain(self.toSaveMain);
  self.updateOptionsColor(self.toSaveColor);
  self.updateOptionsText(self.toSaveText);
  self.updateOptionsDbb(self.toSaveDbb);
  self.updateOptionsMez(self.toSaveMez);
end

self.DoIntro=function()
  self.fadeInTimer.AutoReset=true;
  self.fadeInTimer.Interval=0.033
  self.fadeInTimer.Start();
  self.mWindow.SetLevel(0,"morale");
  self.mWindow.SetLevel(0,"power");
  self.mWindow.dbb.cdHolder:SetOpacity(0);
end

self.fadeInTimer.TimerElapsed=function(sender,args)
  local barOpacityRate = self.mWindow.barOpacity/30;
  local textOpacityRate = self.mWindow.txtOpacity/30;
  local bo = self.fadeInTimer.count * barOpacityRate;
  local to = self.fadeInTimer.count * textOpacityRate;
  dbbOpacityRate=0;
  dbbCurableRate=0;
  dbbIndRate=0;
  self.mWindow.SetBarOpacity(bo);
  self.mWindow.SetTxtOpacity(to);
  if (self.mWindow.showDebuffBar==true)then
    dbbOpacityRate = self.mWindow.dbbOpacity/30;

    if (self.mWindow.dbb.displayMode>=2)then
      dbbCurableRate=.033;
      dbbIndRate=self.mWindow.dbbIndOpacity/30;
    end
  end
  dbo= self.fadeInTimer.count * dbbOpacityRate;
  dbi= self.fadeInTimer.count * dbbIndRate;
  co= self.fadeInTimer.count * dbbCurableRate;
  self.mWindow.dbb.SetBarOpacity(dbo);
  self.mWindow.dbb.SetIndOpacity(dbi);
  --self.mWindow.dbb.qsWindow:SetOpacity(co);
  self.mWindow.dbb:SetOpacity(co);

  -- MezBar fadein
  mezOpacityRate = 0;
  if (self.mWindow.showMezBar==true)then
    mezOpacityRate = self.mWindow.mezOpacity/30;
  end
  local mozz= 0;
  mozz = self.fadeInTimer.count * mezOpacityRate;
  --Turbine.Shell.WriteLine("Fadein Mez1: " .. tostring(mozz));


  self.mWindow.mzb.SetBarOpacity(mozz);
  if (bo >= self.mWindow.barOpacity)then
    self.mWindow.dbb.qsWindow:SetVisible(true);
    self.fadeInTimer.Stop();

    self.mWindow.dbb.ShowInd(true);
    self.mWindow.dbb.cdHolder:SetOpacity(self.mWindow.dbb.wLit:GetOpacity())
    self.dbiTimer.AutoReset=false;
    self.dbiTimer.Interval=.5;
    self.dbiTimer.Start();
    self.barTimer.AutoReset=true;
    self.barTimer.Interval=.033;
    self.barTimer.Start();
  end
end

self.dbiTimer.TimerElapsed=function(sender,args)
  self.mWindow.dbb.Override=false;
  self.mWindow.dbb.ShowInd(false);
end

self.barTimer.TimerElapsed=function(sender,args)
  local player=Turbine.Gameplay.LocalPlayer.GetInstance();
  mTarget = player:GetMorale()/player:GetMaxMorale();
  if player:GetClass() == Turbine.Gameplay.Class.Beorning then
    Power = player:GetClassAttributes():GetWrath();
    MaxPower = 100;
  else
    Power = player:GetPower();
    MaxPower = player:GetMaxPower();
  end;
  pTarget = Power/MaxPower;
  mRate = mTarget/45;
  pRate = pTarget/45;
  mLevel = self.barTimer.count * mRate;
  pLevel = self.barTimer.count * pRate;
  self.mWindow.SetLevel(mLevel,"morale")
  self.mWindow.SetLevel(pLevel,"power")
  if (mLevel >= player:GetMorale()/player:GetMaxMorale())then
    self.barTimer.Stop();
    self.InitSettings();
    self.mWindow:SetOverride(false);
    self.mWindow.holdSetup=false;
    if(self.mWindow.setupPending==true)then
      self.sw.showSetup();
    end
  end

end

self.BaseSettings=function(main,color,text,dbb,mez)
  --this will provide the initial display options - without affecting opacity,
  --so Palantir will be able to start off invisible, and then go through the intro

  --self.mWindow:SetPosition((displayWidth-windowWidth)/2,(displayHeight - windowHeight)/2);
  --self.defaultOptionsMain.PositionX=self.mWindow:GetLeft();
  --self.defaultOptionsMain.PositionY=self.mWindow:GetTop();

  self.mWindow:SetPosition(main.PositionX,main.PositionY);

  self.SetBarSpacing(main.BarSpacing);
  self.l = self.mWindow:GetLeft();
  self.mWidth=self.mWindow:GetWidth();
  self.mWindow.barOpacity=main.BarOpacity;



  self.mWindow.txtOpacity=main.TextOpacity;
  Turbine.UI.Lotro.LotroUI.SetEnabled( Turbine.UI.Lotro.LotroUIElement.Vitals, main.showInGameVitals );
  self.mWindow.dbb:SetVisible(main.ShowDebuff);
  self.mWindow.showDebuffBar=main.ShowDebuff;
  self.mWindow.dbb:SetDisplayMode(main.DebuffDisplayMode);
  self.mWindow.dbb:SetDisplayMode2(main.ShowFlash);

  self.mWindow.SetDbbPosition(dbb.VerticalPosition);
  self.mWindow.dbbOpacity=dbb.DebuffBarOpacity;
  self.mWindow.dbbIndOpacity=dbb.IndicatorOpacity;
  self.mWindow.dbb.SetIndOpacity(dbb.IndicatorOpacity);

  --Curable Cooldowns--
  self.mWindow.dbb.qslot.sOneCD=dbb.WoundCooldown;
  self.mWindow.dbb.qslot.sTwoCD=dbb.FearCooldown;
  self.mWindow.dbb.qslot.sThreeCD=dbb.DiseaseCooldown;
  self.mWindow.dbb.qslot.sFourCD=dbb.PoisonCooldown;

  self.mWindow.mzb:SetVisible(mez.MezBarShow);

  self.mWindow.mzb.TimerArea:SetVisible(mez.MezBarShowTimer);
  self.mWindow.mzb.TimerTick = mez.MezBarTimerTick;

  self.mWindow.mzb.SetBarOpacity(mez.MezBarOpacity); --MezBar
  self.mWindow.mezOpacity=mez.MezBarOpacity;
  self.mWindow.SetMezAreaPosition(mez.MezBarVertical);


  self.mWindow.SetDefaultColor(Turbine.UI.Color((color.MoraleBarColor.r/255),(color.MoraleBarColor.g/255),(color.MoraleBarColor.b/255)),self.mWindow.moraleBar);
  if(color.MoraleTextColor~=nil)then
  self.mWindow.moraleBar:SetTextColor(Turbine.UI.Color((color.MoraleTextColor.r/255),(color.MoraleTextColor.g/255),(color.MoraleTextColor.b/255)));
  else
  self.mWindow.moraleBar:SetTextColor(Turbine.UI.Color((self.defaultOptionsColor.MoraleTextColor.r/255),(self.defaultOptionsColor.MoraleTextColor.g/255),(self.defaultOptionsColor.MoraleTextColor.b/255)));
  color.MoraleTextColor=self.defaultOptionsColor.MoraleTextColor;
  end
  self.mWindow.SetEffects(color.MbEnableColorEffects,self.mWindow.moraleBar);
  self.mWindow.SetBlending(color.MbEnableColorTransitions,self.mWindow.moraleBar);
  self.mWindow.SetColorOne(Turbine.UI.Color((color.MbPointOneColor.r/255),(color.MbPointOneColor.g/255),(color.MbPointOneColor.b/255)),self.mWindow.moraleBar);
  self.mWindow.SetPctOne(color.MbPointOneLocation,self.mWindow.moraleBar);
  self.mWindow.SetTwoColor(color.MbEnableSecondColor,self.mWindow.moraleBar);
  self.mWindow.SetColorTwo(Turbine.UI.Color((color.MbPointTwoColor.r/255),(color.MbPointTwoColor.g/255),(color.MbPointTwoColor.b/255)),self.mWindow.moraleBar);
  self.mWindow.SetPctTwo(color.MbPointTwoLocation,self.mWindow.moraleBar);
  self.mWindow.SetDefaultColor(Turbine.UI.Color((color.PowerBarColor.r/255),(color.PowerBarColor.g/255),(color.PowerBarColor.b/255)),self.mWindow.powerBar);
  if(color.PowerTextColor~=nil)then
  self.mWindow.powerBar:SetTextColor(Turbine.UI.Color((color.PowerTextColor.r/255),(color.PowerTextColor.g/255),(color.PowerTextColor.b/255)));
  else
  self.mWindow.powerBar:SetTextColor(Turbine.UI.Color((self.defaultOptionsColor.PowerTextColor.r/255),(self.defaultOptionsColor.PowerTextColor.g/255),(self.defaultOptionsColor.PowerTextColor.b/255)));
  color.PowerTextColor=self.defaultOptionsColor.PowerTextColor;
  end
  self.mWindow.SetEffects(color.PbEnableColorEffects,self.mWindow.powerBar);
  self.mWindow.SetBlending(color.PbEnableColorTransitions,self.mWindow.powerBar);
  self.mWindow.SetColorOne(Turbine.UI.Color((color.PbPointOneColor.r/255),(color.PbPointOneColor.g/255),(color.PbPointOneColor.b/255)),self.mWindow.powerBar);
  self.mWindow.SetPctOne(color.PbPointOneLocation,self.mWindow.powerBar);
  self.mWindow.SetTwoColor(color.PbEnableSecondColor,self.mWindow.powerBar);
  self.mWindow.SetColorTwo(Turbine.UI.Color((color.PbPointTwoColor.r/255),(color.PbPointTwoColor.g/255),(color.PbPointTwoColor.b/255)),self.mWindow.powerBar);
  self.mWindow.SetPctTwo(color.PbPointTwoLocation,self.mWindow.powerBar);
  self.mWindow.pvGroupTextPosition=text.GroupTextPosition;
  if(self.mWindow.pvGroupTextPosition==true)then
    self.mWindow.SetTextPosition(text.VitalsTextPosition,self.mWindow.moraleBar);
    self.mWindow.SetTextPosition(text.VitalsTextPosition,self.mWindow.powerBar);
  end
  self.mWindow.pvGroupTextDisplay=text.GroupTextDisplay;
  if(self.mWindow.pvGroupTextDisplay==true)then
    self.mWindow.SetTextDisplay(text.VitalsTextDisplay,self.mWindow.moraleBar);
    self.mWindow.SetTextDisplay(text.VitalsTextDisplay,self.mWindow.powerBar);
  end
  if(self.mWindow.pvGroupTextPosition==false)then
    self.mWindow.SetTextPosition(text.MoraleTextPosition,self.mWindow.moraleBar);
  end
  if(self.mWindow.pvGroupTextDisplay==false)then
    self.mWindow.SetTextDisplay(text.MoraleTextDisplay,self.mWindow.moraleBar);
  end
  if(self.mWindow.pvGroupTextPosition==false)then
    self.mWindow.SetTextPosition(text.PowerTextPosition,self.mWindow.powerBar);
  end
  if(self.mWindow.pvGroupTextDisplay==false)then
    self.mWindow.SetTextDisplay(text.PowerTextDisplay,self.mWindow.powerBar);
  end

  --add in slots here

  self.DoIntro();
end

self.loadSettings=function(main,color,text,dbb,mez)
--this will eventually contain the loading code
self.toSaveMain = table.copy(main);
self.revertMain = table.copy(main);
self.toSaveColor = table.copy(color);
self.revertColor = table.copy(color);
self.toSaveText = table.copy(text);
self.revertText = table.copy(text);
self.toSaveDbb = table.copy(dbb);
self.revertDbb = table.copy(dbb);
self.toSaveMez = table.copy(mez);
self.revertMez = table.copy(mez);

self.BaseSettings(self.toSaveMain,self.toSaveColor,self.toSaveText,self.toSaveDbb,self.toSaveMez);
end



-- options handlers --

self.sw.recenter.MouseClick=function(sender,args)
    local displayWidth, displayHeight = Turbine.UI.Display.GetSize();
    local windowWidth,windowHeight = self.mWindow:GetSize();
    local width = (displayWidth-windowWidth)/2;
    local height = (displayHeight-windowHeight)/2;
    self.mWindow:SetPosition(width,height);
    self.mWindow.PosChanged(width,height);
end

self.sw.showincombat.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.onlyInCombat=self.sw.showincombat:IsChecked();
    self.revertMain.showOnlyInCombat=self.sw.showincombat:IsChecked();
    self.enableRevert("main");
  end
end

self.sw.noc.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.oocCriteriaMode=args.Index;
    self.revertMain.outOfCombatCriteria=args.Index;
    self.enableRevert("main");
  end
end

self.sw.sv.CheckedChanged=function()
  if(self.init==false)then
    Turbine.UI.Lotro.LotroUI.SetEnabled( Turbine.UI.Lotro.LotroUIElement.Vitals, self.sw.sv:IsChecked() );
    self.revertMain.showInGameVitals=self.sw.sv:IsChecked();
    self.enableRevert("main");
  end
end

self.sw.ac.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.settingScope=args.Index;
    self.revertMain.settingScope=args.Index;
    self.enableRevert("main");
  end
end

self.sw.Warn.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.mzb.ShowWarnings=self.sw.Warn:IsChecked();
    self.revertMain.ShowWarnings=self.sw.Warn:IsChecked();
    self.enableRevert("main");
  end
end

self.sw.ClassSpecificPowerBar.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.classSpecificPowerBar=self.sw.ClassSpecificPowerBar:IsChecked();
    self.revertMain.ClassSpecificPowerBar=self.sw.ClassSpecificPowerBar:IsChecked();
    self.enableRevert("main");
  end
end

self.sw.ShowTimer.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.mzb.ShowTimer=self.sw.ShowTimer:IsChecked();
    self.revertMain.ShowTimer=self.sw.ShowTimer:IsChecked();
    self.enableRevert("main");
  end
end

self.sw.voSld.valueChanged = function()
  if(self.init==false)then
    self.mWindow.SetBarOpacity(self.sw.voSld:GetValue());
    self.mWindow.barOpacity=self.sw.voSld:GetValue();
    self.revertMain.BarOpacity=self.sw.voSld:GetValue();
    self.enableRevert("main");
  end
end

self.sw.bpSld.valueChanged = function()
  if(self.init==false)then
    self.SetBarSpacing(self.sw.bpSld:GetValue(),"event");
    self.revertMain.BarSpacing=self.sw.bpSld:GetValue();
    self.enableRevert("main");
  end
end

self.sw.txSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetTxtOpacity(self.sw.txSld:GetValue());
    self.mWindow.txtOpacity=self.sw.txSld:GetValue();
    self.revertMain.TextOpacity=self.sw.txSld:GetValue()
    self.enableRevert("main");
  end
end

--DebuffBar settings
self.sw.sdb.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.dbb:SetVisible(self.sw.sdb:IsChecked());
    self.sw.debuffDisplay.SetEnabled(self.sw.sdb:IsChecked());
    self.sw.dboSld:SetEnabled(self.sw.sdb:IsChecked());
    self.sw.dbiSld:SetEnabled(self.sw.sdb:IsChecked());
    self.sw.dbpSld:SetEnabled(self.sw.sdb:IsChecked());
    self.sw.sdb2:SetEnabled(self.sw.sdb:IsChecked());
                --self.sw.sdb2Holder:SetVisible(self.sw.sdb:IsChecked());
    self.revertDbb.ShowDebuff = self.sw.sdb:IsChecked();
    self.mWindow.showDebuffBar = self.sw.sdb:IsChecked();
    self.enableRevert("dbb");
  end
end

self.sw.sdb2.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.dbb:SetDisplayMode2(self.sw.sdb2:IsChecked());
    self.revertDbb.ShowFlash = self.sw.sdb2:IsChecked();
    self.mWindow.ShowFlash = self.sw.sdb2:IsChecked();
    self.enableRevert("dbb");
  end
end

self.sw.debuffDisplay.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.dbb:SetDisplayMode(args.Index);
    self.revertDbb.DebuffDisplayMode=args.Index;
    self.enableRevert("dbb");
  end
end

self.sw.dboSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.dbb.SetBarOpacity(self.sw.dboSld:GetValue());
    self.mWindow.dbb.ShowInd(false);
    self.mWindow.dbbOpacity=self.sw.dboSld:GetValue();
    self.revertDbb.DebuffBarOpacity=self.sw.dboSld:GetValue();
    self.enableRevert("dbb");

  end
end

self.sw.dbiSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.dbb.SetIndOpacity(self.sw.dbiSld:GetValue());
    self.mWindow.dbb.ShowInd(true);
    self.mWindow.dbbIndOpacity=self.sw.dbiSld:GetValue();
    self.revertDbb.IndicatorOpacity=self.sw.dbiSld:GetValue();
    self.enableRevert("dbb");
  end
end

self.sw.dbpSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetDbbPosition(self.sw.dbpSld:GetValue());
    self.revertDbb.VerticalPosition=self.sw.dbpSld:GetValue();
    self.enableRevert("dbb");
  end
end





-- Mez page events
self.sw.mezCB1.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.mzb:SetVisible(self.sw.mezCB1:IsChecked());
    self.sw.mezCB2:SetEnabled(self.sw.mezCB1:IsChecked());  -- Cbt Timer CB
    self.sw.mezSld1:SetEnabled(self.sw.mezCB1:IsChecked());	-- Opacity slider
    self.sw.mezSld2:SetEnabled(self.sw.mezCB1:IsChecked()); -- Vertical pos slider
    self.sw.mezCB3:SetEnabled(self.sw.mezCB1:IsChecked());  -- Sel cooldown CB
    self.revertMez.MezBarShow = self.sw.mezCB1:IsChecked();
    self.mWindow.showMezBar = self.sw.mezCB1:IsChecked();
    self.enableRevert("mez");
  end
end

self.sw.mezCB2.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.mzb.TimerArea:SetVisible(self.sw.mezCB2:IsChecked());
    self.revertMez.MezBarShowTimer = self.sw.mezCB2:IsChecked();
    self.enableRevert("mez");
  end
end

self.sw.mezCB3.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.mzb.AutoDetectDuration = self.sw.mezCB3:IsChecked();
    self.revertMez.MezBarAutoSearch = self.sw.mezCB3:IsChecked();
    self.enableRevert("mez");
  end
end

self.sw.mezSld1.valueChanged=function()
  if(self.init==false)then
    self.mWindow.mzb.SetBarOpacity(self.sw.mezSld1:GetValue());
    self.mWindow.mezOpacity=self.sw.mezSld1:GetValue();
    self.revertMez.MezBarOpacity=self.sw.mezSld1:GetValue();
    self.enableRevert("mez");
  end
end

self.sw.mezSld2.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetMezAreaPosition(self.sw.mezSld2:GetValue());
    self.revertMez.MezBarVertical=self.sw.mezSld2:GetValue();
    self.enableRevert("mez");
  end
end






--color events

self.sw.pmbBcChanged=function()
  if(self.init==false)then
    self.mWindow.SetDefaultColor(self.sw.pmbBcCp.GetColor(),self.mWindow.moraleBar);
    self.mWindow.SetLevel(1,"morale");
    self.revertColor.MoraleBarColor={r=(self.sw.pmbBcCp.GetColor().R*255),g=(self.sw.pmbBcCp.GetColor().G*255),b=(self.sw.pmbBcCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end

self.sw.pmbTcChanged=function()
  if(self.init==false)then
    self.mWindow.moraleBar:SetTextColor(self.sw.pmbTcCp.GetColor());


    self.revertColor.MoraleTextColor={r=(self.sw.pmbTcCp.GetColor().R*255),g=(self.sw.pmbTcCp.GetColor().G*255),b=(self.sw.pmbTcCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end

self.sw.pmbEce.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.SetEffects(self.sw.pmbEce:IsChecked(),self.mWindow.moraleBar);
    self.sw.pmbClrOneCp.SetEnabled(self.sw.pmbEce:IsChecked());
    self.sw.pmbCpOneSld:SetEnabled(self.sw.pmbEce:IsChecked());
    self.sw.pmbClrTwoCp.SetEnabled(self.sw.pmbEce:IsChecked());
    self.sw.pmbCpTwoSld:SetEnabled(self.sw.pmbEce:IsChecked());
    self.sw.pmbEsc:SetEnabled(self.sw.pmbEce:IsChecked());
    self.sw.pmbEct:SetEnabled(self.sw.pmbEce:IsChecked());
    self.revertColor.MbEnableColorEffects=self.sw.pmbEce:IsChecked();
    self.enableRevert("color");
  end
end

self.sw.pmbEct.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.SetBlending(self.sw.pmbEct:IsChecked(),self.mWindow.moraleBar);
    self.revertColor.MbEnableColorTransitions=self.sw.pmbEct:IsChecked();
    self.enableRevert("color");
  end
end

self.sw.pmbClrOneChanged=function()
  if(self.init==false)then
    self.mWindow.SetColorOne(self.sw.pmbClrOneCp.GetColor(),self.mWindow.moraleBar);
    self.mWindow.SetLevel(self.sw.pmbCpOneSld:GetValue(),"morale");
    self.revertColor.MbPointOneColor={r=(self.sw.pmbClrOneCp.GetColor().R*255),g=(self.sw.pmbClrOneCp.GetColor().G*255),b=(self.sw.pmbClrOneCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end


self.sw.pmbCpOneSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetPctOne(self.sw.pmbCpOneSld:GetValue(),self.mWindow.moraleBar);
    self.mWindow.SetLevel(self.sw.pmbCpOneSld:GetValue(),"morale");
    self.revertColor.MbPointOneLocation=self.sw.pmbCpOneSld:GetValue();
    self.enableRevert("color");
  end
end

self.sw.pmbEsc.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.SetTwoColor(self.sw.pmbEsc:IsChecked(),self.mWindow.moraleBar);
    self.sw.pmbClrTwoCp.SetEnabled(self.sw.pmbEsc:IsChecked());
    self.sw.pmbCpTwoSld:SetEnabled(self.sw.pmbEsc:IsChecked());
    self.revertColor.MbEnableSecondColor=self.sw.pmbEsc:IsChecked();
    self.enableRevert("color");
  end
end

self.sw.pmbClrTwoChanged=function()
  if(self.init==false)then
    self.mWindow.SetColorTwo(self.sw.pmbClrTwoCp.GetColor(),self.mWindow.moraleBar);
    self.mWindow.SetLevel(self.sw.pmbCpTwoSld:GetValue(),"morale");
    self.revertColor.MbPointTwoColor={r=(self.sw.pmbClrTwoCp.GetColor().R*255),g=(self.sw.pmbClrTwoCp.GetColor().G*255),b=(self.sw.pmbClrTwoCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end

self.sw.pmbCpTwoSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetPctTwo(self.sw.pmbCpTwoSld:GetValue(),self.mWindow.moraleBar);
    self.mWindow.SetLevel(self.sw.pmbCpTwoSld:GetValue(),"morale");
    self.revertColor.MbPointTwoLocation=self.sw.pmbCpTwoSld:GetValue();
    self.enableRevert("color");
  end
end




--power bar--

self.sw.ppbBcChanged=function()
  if(self.init==false)then
    self.mWindow.SetDefaultColor(self.sw.ppbBcCp.GetColor(),self.mWindow.powerBar);
    self.mWindow.SetLevel(1,"powera");
    self.revertColor.PowerBarColor={r=(self.sw.ppbBcCp.GetColor().R*255),g=(self.sw.ppbBcCp.GetColor().G*255),b=(self.sw.ppbBcCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end
self.sw.ppbTcChanged=function()
  if(self.init==false)then
    self.mWindow.powerBar:SetTextColor(self.sw.ppbTcCp.GetColor());
    self.revertColor.PowerTextColor={r=(self.sw.ppbTcCp.GetColor().R*255),g=(self.sw.ppbTcCp.GetColor().G*255),b=(self.sw.ppbTcCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end
self.sw.ppbEce.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.SetEffects(self.sw.ppbEce:IsChecked(),self.mWindow.powerBar);
    self.sw.ppbClrOneCp.SetEnabled(self.sw.ppbEce:IsChecked());
    self.sw.ppbCpOneSld:SetEnabled(self.sw.ppbEce:IsChecked());
    self.sw.ppbClrTwoCp.SetEnabled(self.sw.ppbEce:IsChecked());
    self.sw.ppbCpTwoSld:SetEnabled(self.sw.ppbEce:IsChecked());
    self.sw.ppbEsc:SetEnabled(self.sw.ppbEce:IsChecked());
    self.sw.ppbEct:SetEnabled(self.sw.ppbEce:IsChecked());
    self.revertColor.PbEnableColorEffects=self.sw.ppbEce:IsChecked();
    self.enableRevert("color");
  end
end

self.sw.ppbEct.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.SetBlending(self.sw.ppbEct:IsChecked(),self.mWindow.powerBar);
    self.revertColor.PbEnableColorTransitions=self.sw.ppbEct:IsChecked();
    self.enableRevert("color");
  end
end

self.sw.ppbClrOneChanged=function()
  if(self.init==false)then
    self.mWindow.SetColorOne(self.sw.ppbClrOneCp.GetColor(),self.mWindow.powerBar);
    self.mWindow.SetLevel(self.sw.ppbCpOneSld:GetValue(),"power");
    self.revertColor.PbPointOneColor={r=(self.sw.ppbClrOneCp.GetColor().R*255),g=(self.sw.ppbClrOneCp.GetColor().G*255),b=(self.sw.ppbClrOneCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end

self.sw.ppbCpOneSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetPctOne(self.sw.ppbCpOneSld:GetValue(),self.mWindow.powerBar);
    self.mWindow.SetLevel(self.sw.ppbCpOneSld:GetValue(),"power");
    self.revertColor.PbPointOneLocation=self.sw.ppbCpOneSld:GetValue();
    self.enableRevert("color");

  end
end

self.sw.ppbEsc.CheckedChanged=function()
  if(self.init==false)then
    self.mWindow.SetTwoColor(self.sw.ppbEsc:IsChecked(),self.mWindow.powerBar);
    self.sw.ppbClrTwoCp.SetEnabled(self.sw.ppbEsc:IsChecked());
    self.sw.ppbCpTwoSld:SetEnabled(self.sw.ppbEsc:IsChecked());
    self.revertColor.PbEnableSecondColor=self.sw.ppbEsc:IsChecked();
    self.enableRevert("color");
  end
end

self.sw.ppbClrTwoChanged=function()
  if(self.init==false)then
    self.mWindow.SetColorTwo(self.sw.ppbClrTwoCp.GetColor(),self.mWindow.powerBar);
    self.mWindow.SetLevel(self.sw.ppbCpTwoSld:GetValue(),"power");
    self.revertColor.PbPointTwoColor={r=(self.sw.ppbClrTwoCp.GetColor().R*255),g=(self.sw.ppbClrTwoCp.GetColor().G*255),b=(self.sw.ppbClrTwoCp.GetColor().B*255)};
    self.enableRevert("color");
  end
end

self.sw.ppbCpTwoSld.valueChanged=function()
  if(self.init==false)then
    self.mWindow.SetPctTwo(self.sw.ppbCpTwoSld:GetValue(),self.mWindow.powerBar);
    self.mWindow.SetLevel(self.sw.ppbCpTwoSld:GetValue(),"power");
    self.revertColor.PbPointTwoLocation=self.sw.ppbCpTwoSld:GetValue();
    self.enableRevert("color");
  end
end

--Text Events --

self.sw.gTxtPos.CheckedChanged=function()
  if(self.init==false)then
    self.sw.txtPos.SetEnabled(self.sw.gTxtPos:IsChecked());
    self.sw.pmbTxtPos.SetEnabled(not self.sw.gTxtPos:IsChecked());
    self.sw.ppbTxtPos.SetEnabled(not self.sw.gTxtPos:IsChecked());
    self.mWindow.pvGroupTextPostion=self.sw.gTxtPos:IsChecked();
    self.revertText.GroupTextPosition=self.sw.gTxtPos:IsChecked();
    if(self.sw.gTxtPos:IsChecked()==false)then
      self.mWindow.SetTextPosition(self.revertText.MoraleTextPosition,self.mWindow.moraleBar);
      self.mWindow.SetTextPosition(self.revertText.PowerTextPosition,self.mWindow.powerBar);
    else
      self.mWindow.SetTextPosition(self.revertText.VitalsTextPosition,self.mWindow.moraleBar);
      self.mWindow.SetTextPosition(self.revertText.VitalsTextPosition,self.mWindow.powerBar);
    end
    self.enableRevert("text");
  end
end

self.sw.txtPos.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.SetTextPosition(args.Index,self.mWindow.moraleBar);
    self.mWindow.SetTextPosition(args.Index,self.mWindow.powerBar);
    self.revertText.VitalsTextPosition=args.Index;
    self.enableRevert("text");
  end
end

self.sw.gTxtDis.CheckedChanged=function()
  if(self.init==false)then
    self.sw.txtDis.SetEnabled(self.sw.gTxtDis:IsChecked());
    self.sw.pmbTxtDis.SetEnabled(not self.sw.gTxtDis:IsChecked());
    self.sw.ppbTxtDis.SetEnabled(not self.sw.gTxtDis:IsChecked());
    self.mWindow.pvGroupTextDisplay=self.sw.gTxtDis:IsChecked();
    self.revertText.GroupTextDisplay=self.sw.gTxtDis:IsChecked();
    self.enableRevert("text");
    if(self.sw.gTxtDis:IsChecked()==false)then
      self.mWindow.SetTextDisplay(self.revertText.MoraleTextDisplay,self.mWindow.moraleBar);
      self.mWindow.SetTextDisplay(self.revertText.PowerTextDisplay,self.mWindow.powerBar);
    else
      self.mWindow.SetTextDisplay(self.revertText.VitalsTextDisplay,self.mWindow.moraleBar);
      self.mWindow.SetTextDisplay(self.revertText.VitalsTextDisplay,self.mWindow.powerBar);
    end
  end
end

self.sw.txtDis.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.SetTextDisplay(args.Index,self.mWindow.moraleBar);
    self.mWindow.SetTextDisplay(args.Index,self.mWindow.powerBar);
    self.revertText.VitalsTextDisplay=args.Index;
    self.enableRevert("text");
  end
end

self.sw.pmbTxtPos.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.SetTextPosition(args.Index,self.mWindow.moraleBar);
    self.revertText.MoraleTextPosition=args.Index;
    self.enableRevert("text");
  end
end

self.sw.pmbTxtDis.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.SetTextDisplay(args.Index,self.mWindow.moraleBar);
    self.revertText.MoraleTextDisplay=args.Index;
    self.enableRevert("text");
  end
end

self.sw.ppbTxtPos.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.SetTextPosition(args.Index,self.mWindow.powerBar);
    self.revertText.PowerTextPosition=args.Index;
    self.enableRevert("text");
  end
end

self.sw.ppbTxtDis.IndexChanged=function(sender,args)
  if(self.init==false)then
    self.mWindow.SetTextDisplay(args.Index,self.mWindow.powerBar);
    self.revertText.PowerTextDisplay=args.Index;
    self.enableRevert("text");
  end
end

self.enableRevert=function(revertType)
  if(self.setupVisible==true)then
    if(revertType=="main")then
      self.sw.revMain:SetEnabled(true);
      self.sw.defMain:SetEnabled(true);
    elseif(revertType=="color")then
      self.sw.revColor:SetEnabled(true);
      self.sw.defColor:SetEnabled(true);
    elseif(revertType=="color")then
      self.sw.revText:SetEnabled(true);
      self.sw.defText:SetEnabled(true);
    elseif(revertType=="dbb")then
      self.sw.revMain4:SetEnabled(true);
      self.sw.defMain4:SetEnabled(true);
    else--if(revertType=="mez")
      self.sw.revMain5:SetEnabled(true);
      self.sw.defMain5:SetEnabled(true);
    end
  end
end

  -- Main page btns clicks
  self.sw.revMain.MouseClick=function(sender,args)
    self.revertMain = table.copy(self.toSaveMain);
    self.updateOptionsMain(self.revertMain);
    self.sw.revMain:SetEnabled(false);
  end
  self.sw.defMain.MouseClick=function(sender,args)
    self.referrer="main";
    self.sw.alertHolder:SetVisible(true);
  end
  self.sw.accMain.MouseClick=function(sender,args)
    self.sw:Close();
  end

  -- Color page btns clicks
  self.sw.revColor.MouseClick=function(sender,args)
    self.revertColor = table.copy(self.toSaveColor);
    self.updateOptionsColor(self.revertColor);
    self.sw.revColor:SetEnabled(false);
  end
  self.sw.defColor.MouseClick=function(sender,args)
    self.referrer="color";
    self.sw.alertHolder:SetVisible(true);
  end
  self.sw.accColor.MouseClick=function(sender,args)
    self.sw:Close();
  end

  -- Text page btns clicks
  self.sw.revText.MouseClick=function(sender,args)
    self.revertText = table.copy(self.toSaveText);
    self.updateOptionsText(self.revertText);
    self.sw.revText:SetEnabled(false);
  end
  self.sw.defText.MouseClick=function(sender,args)
    self.referrer="text";
    self.sw.alertHolder:SetVisible(true);
  end
  self.sw.accText.MouseClick=function(sender,args)
    self.sw:Close();
  end

  -- Main4 (Debuff bar) page btns clicks
  self.sw.revMain4.MouseClick=function(sender,args)
    self.revertDbb = table.copy(self.toSaveDbb);
    self.updateOptionsDbb(self.revertDbb);
    self.sw.revMain4:SetEnabled(false);
  end
  self.sw.defMain4.MouseClick=function(sender,args)
    self.referrer="dbb";
    self.sw.alertHolder:SetVisible(true);
  end
  self.sw.accMain4.MouseClick=function(sender,args)
    self.sw:Close();
  end

  -- Main5 (Effect bars) page btns clicks
  self.sw.revMain5.MouseClick=function(sender,args)
    self.revertMez = table.copy(self.toSaveMez);
    self.updateOptionsMez(self.revertMez);
    self.sw.revMain5:SetEnabled(false);
  end
  self.sw.defMain5.MouseClick=function(sender,args)
    self.referrer="dbb";
    self.sw.alertHolder:SetVisible(true);
  end
  self.sw.accMain5.MouseClick=function(sender,args)
    self.sw:Close();
  end

  -- Potions page btns clicks
  self.sw.potions_default.MouseClick = function(sender,args)
    self.mWindow.potions.data.morale_threshold = self.default_options_potions.morale_threshold
      self.mWindow.potions.data.power_threshold = self.default_options_potions.power_threshold
      self.mWindow.potions.data.morale_cooldown = self.default_options_potions.morale_cooldown
      self.mWindow.potions.data.power_cooldown = self.default_options_potions.power_cooldown
      self.mWindow.potions.data.enabled = self.default_options_potions.enabled
      self.mWindow.potions.data.mode = self.default_options_potions.mode
      self.mWindow.potions.data.display_mode = 1

      self.UpdateOptionsPotions()
  end

  self.sw.potions_accept.MouseClick = function(sender,args)
    self.ApplyOptionsPotions()
    self.sw:Close();
  end

  self.sw.potions_enable.CheckedChanged = function(sender, args)
    status = self.sw.potions_enable:IsChecked()
    self.mWindow.potions.data.enabled = status
    self.mWindow.potions:SetDisplayMode(2)
    self.mWindow.potions:Refresh()
  end

  self.sw.alertYes.MouseClick=function(sender,args)
    self.sw.alertHolder:SetVisible(false);
    if(self.referrer=="main")then
      self.revertMain = table.copy(self.defaultOptionsMain);
      self.updateOptionsMain(self.revertMain);
      self.sw.defMain:SetEnabled(false);
    elseif(self.referrer=="color")then
      self.revertColor = table.copy(self.defaultOptionsColor);
      self.updateOptionsColor(self.revertColor);
      self.sw.defColor:SetEnabled(false);
    elseif(self.referrer=="text")then
      self.revertText = table.copy(self.defaultOptionsText);
      self.updateOptionsText(self.revertText);
      self.sw.defText:SetEnabled(false);
    elseif(self.referrer=="dbb")then
      self.revertDbb = table.copy(self.defaultOptionsDbb);
      self.updateOptionsDbb(self.revertDbb);
      self.sw.defMain4:SetEnabled(false);
    else --if(self.referrer=="mez")then
      self.revertMez = table.copy(self.defaultOptionsMez);
      self.updateOptionsMez(self.revertMez);
      self.sw.defMain5:SetEnabled(false);
    end
  end

  self.sw.alertNo.MouseClick=function(sender,args)
    self.sw.alertHolder:SetVisible(false);
  end



end
