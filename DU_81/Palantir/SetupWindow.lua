import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "DU_81.Palantir";


SetupWindow = class(DU_81.DUInterface.Window );

function SetupWindow:Constructor(loc)
  DU_81.DUInterface.Window.Constructor( self,575,534 );

  --[[
  if(Turbine.Shell.IsCommand("zusatzmodule")==true)then
      import "DU_81.Palantir.Locale.DE_Mez";
      ARE_YOU_SURE="Bist Du sicher, dass Du die Standardeinstellungen zurücksetzen willst?";
  elseif(Turbine.Shell.IsCommand("conseil")==true)then
      import "DU_81.Palantir.Locale.FR_Mez";
      ARE_YOU_SURE="Êtes vous sur de vouloir restaurer les paramètres par défaut ?";
  else
    import "DU_81.Palantir.Locale.EN_Mez";
    ARE_YOU_SURE="Are you sure you want to restore the default settings?";
  end]]--

  self.locale=loc;
  if(loc=="en")then
    TITLE="Palantir Options";
    MISC="Misc.";
    RECENTER="Recenter Palantir";
    SHOW_IN_COMBAT="Show Only In Combat";
    OOC_CRITERIA="Out Of Combat Criteria";
    VITALS_100="Vitals 100%";
    MORALE_100="Morale 100%";
    POWER_100="Power 100%";
    OUT_OF_COMBAT="Out Of Combat";
    SHOW_VITALS="Show Game Vitals";
    SCOPE="Settings Scope";
    ACCOUNT="Account";
    SERVER="Server";
    CHAR="Character";
    SHOW_WARNING="WARNING when mobs talks you";
    SHOW_TIME="Show in-combat timer";
    USE_CLASS_SPECIFIC_POWER_BAR="Replace power bar by class specific resource";
    PLAYER_VITALS="Player Vitals";
    BAR_OPACITY="Bar Opacity";
    BAR_SPACING="Bar Spacing";
    TEXT_OPACITY="Text Opacity";

    DEBUFF_BAR="Debuff Bar";
    SHOW_DEBUFF="Show Debuff Bar";
    DISPLAY_MODE="Display Mode";
    DM_CURABLE="Cure able Support";
    DM_INDICATOR="Indicators Only";
    DM_FLASH="Full screen warning";
    DM_INSTANT="Instant cure";
    DEBUFF_INDICATOR="Debuff Indicator Opacity";
    VERTICAL_POS="Vertical Position";
    COOLDOWNS="Cooldowns";
    WOUND_CD="Wound Curable Cooldown (In Seconds)";
    FEAR_CD="Fear Curable Cooldown (In Seconds)";
    DISEASE_CD="Disease Curable Cooldown (In Seconds)";
    POISON_CD="Poison Curable Cooldown (In Seconds)";

    PLAYER_MORALE="Player Morale Bar";
    PLAYER_POWER="Player Power Bar";
    BAR_COLOR="Bar Color";
    TEXT_COLOR="Text Color";
    ENABLE_CE="Enable Color Effects";
    ENABLE_CT="Enable Color Transitions";
    POINT_ONE_COLOR="Point #1 Color";
    POINT_ONE_LOC="Point #1 Location";
    ENABLE_SECOND="Enable 2nd Color";
    POINT_TWO_COLOR="Point #2 Color";
    POINT_TWO_LOC="Point #2 Location";
    GROUP_TEXT_POS="Group Text Position";
    GROUP_TEXT_DIS="Group Text Display";
    VITALS_TEXT_POS="Vitals Text Position";
    BOTTOM_OUTSIDE="Bottom Outside";
    BOTTOM_CENTER="Bottom Center";
    BOTTOM_INSIDE="Bottom Inside";
    MIDDLE_OUTSIDE="Middle Outside";
    MIDDLE_INSIDE="Middle Inside";
    TOP_OUTSIDE="Top Outside";
    TOP_CENTER="Top Center";
    TOP_INSIDE="Top Inside";

    VITALS_TEXT_DIS="Vitals Text Display";
    PCT="Percentage";
    REAL_NUMBERS="Real Numbers";
    CUR_MAX="Current/Max";
    PCT_CUR_MAX="PCT & Current/Max";
    TEXT_POSITION="Text Position";
    TEXT_DISPLAY="Text Display";
    REVERT_TEXT="Revert Text";
    REVERT_MAIN="Revert Main";
    REVERT_COLOR="Revert Color";
    DEFAULT_TEXT="Default Text";
    DEFAULT_MAIN="Default Main";
    DEFAULT_COLOR="Default Color";
    COLOR="Color";
    MAIN="Main";
    TEXT="Text";
    ACCEPT="Accept";
    YES="Yes";
    NO="No";
    ARE_YOU_SURE="Are you sure you want to restore the default settings?";
    DBB_BUTTON="Debuffs";

    MEZ_BUTTON="Effects";
    MEZ_MAIN_TITLE="Effect time bars";
    MEZ_ACTIVATE="Activate effect indicators";
    MEZ_TIMER="Combat Timer";
    MEZ_TIMER_TICK="Remainder interval (seconds)";
    MEZ_OPACITY="Effect Indicator Opacity";   --DEBUFF_INDICATOR="Debuff Indicator Opacity";
    MEZ_VERTICAL="Vertical Position";       --VERTICAL_POS="Vertical Position";
    MEZ_LIST="Timer durations (seconds)";
    MEZ_LIST_AUTO="Try to detect duration (on self)";
    MEZ_LM_MEZ="Blinding Flash";
    MEZ_LM_ROOT="Herb-lore";
    MEZ_LM_DEBUFF="Sign of Power: Command";
    MEZ_LM_ANTISTUN="Sign of Power: Righteousness";
    MEZ_MIN_FEAR="Invocation of Elbereth";
    MEZ_MIN_DEAD="Song of the Dead";
    MEZ_MIN_SOLI="Soliloquy of Spirit";
    MEZ_MIN_CCOM="Fellowship's Heart";
    MEZ_RK_ICE="Writ of Cold" -- Writ of Cold - Tier 1
    MEZ_RK_FIRE1="Writ of Fire" -- Writ of Fire - Tier 1
    MEZ_RK_FIREAOE="Scathing Mockery"
    MEZ_RK_FIRE3="Essay of Fire" -- + Improved
    MEZ_RK_FINE="Thunderous Words" -- Môts de tonnerre
    MEZ_RK_DEGA="Harsh Debate" -- Débat houleux
    MEZ_RK_CRIT="Charged Runes" -- Runes chargées
    MEZ_RK_15="Winter-storm" -- Tempète hivernale - 5
    MEZ_RK_HEAL1="Writ of Health" -- Allégorie de la santé
    MEZ_RK_HEAL2="Prelude to Hope" --Prélude à l'espoir (amélioré)
    MEZ_CA_MEZ="Riddle";
    MEZ_CA_ROOT="A small snag";
    MEZ_CA_TRICK="Trick : All kinds...";
    MEZ_CA_DEBUFF="Addle";
    MEZ_CH_FEAR="Bard\'s Arrow"
    MEZ_CH_ROOT="Rain of Thorns"
    MEZ_CH_BUFF1="Fleetness"
    MEZ_CH_BUFF2="Needful Haste"

    -- Potions
    POTIONS_BUTTON = "Potions"
    POTIONS_MAIN_HEADER = "Potions"
    POTIONS_COOLDOWN_HEADER = "Cooldown"
    POTIONS_THRESHOLD_HEADER = "Threshold"
    POTIONS_ENABLED = "Enable Potions"
    POTIONS_MORALE_COOLDOWN = "Morale Potions Cooldown (in Seconds)"
    POTIONS_POWER_COOLDOWN = "Power Potions Cooldown (in Seconds)"
    POTIONS_AUTOMATIC_ALWAYS = "Always Clickable"
    POTIONS_AUTOMATIC_LOW = "Low Morale / Power"
    POTIONS_AUTOMATIC_AUTO = "Automatic Drinking"
    POTIONS_MODE = "Potions Mode"
    POTIONS_MORALE_THRESHOLD = "Morale Potions Threshold (in Percent)"
    POTIONS_POWER_THRESHOLD = "Power Potions Threshold (in Percent)"

  elseif(loc=="de")then
    TITLE="Palantir Optionen";
    MISC="Verschiedenes";
    RECENTER="Palantir neuzentrieren";
    SHOW_IN_COMBAT="Nur im Kampf anzeigen";
    OOC_CRITERIA="Außerhalb-des-Kampfes-Kriterien";
    VITALS_100="Vitalwerte 100%";
    MORALE_100="Moral 100%";
    POWER_100="Kraft 100%";
    OUT_OF_COMBAT="Außerhalb des Kampfes";
    SHOW_VITALS="Spiel-Vitalwerte anzeigen";
    SCOPE="Einstellung speichern pro";
    ACCOUNT="Account";
    SERVER="Server";
    CHAR="Charakter";
    SHOW_WARNING="ACHTUNG wenn mobs mit dir spricht";
    SHOW_TIME="Kampftimer anzeigen"
    USE_CLASS_SPECIFIC_POWER_BAR="Machtleiste durch klassenspezifische Ressource ersetzen";
    PLAYER_VITALS="Vitalwerte des Spielers";
    BAR_OPACITY="Deckkraft Leiste";
    BAR_SPACING="Abstand Leiste";
    TEXT_OPACITY="Deckkraft Text";
    DEBUFF_BAR="Debuff-Leiste";
    SHOW_DEBUFF="Debuff-Leiste anzeigen";
    DISPLAY_MODE="Anzeigemodus";
    DM_CURABLE="Heilunterstützung";
    DM_INDICATOR="Nur Anzeige";
    DM_FLASH="Screen-Anzeige";
    DM_INSTANT="sofortige Heilung";
    DEBUFF_INDICATOR="Deckkraft Debuffanzeige";
    VERTICAL_POS="Vertikale Position";
    COOLDOWNS="Cooldowns";
    WOUND_CD="Cooldown Wundtrank (in Sekunden)";
    FEAR_CD="Cooldown Furchttrank (in Sekunden)";
    DISEASE_CD="Cooldown Krankheitstrank (in Sekunden)";
    POISON_CD="Cooldown Gifttrank (in Sekunden)";
    PLAYER_MORALE="Spieler-Moralleiste";
    PLAYER_POWER="Spieler-Kraftleiste";
    BAR_COLOR="Leistenfarbe";
    TEXT_COLOR="Textfarbe";
    ENABLE_CE="Farbeffekte aktivieren";
    ENABLE_CT="Farbübergänge aktivieren";
    POINT_ONE_COLOR="Farbe für 1. Farbwechsel";
    POINT_ONE_LOC="1. Farbwechsel bei x%";
    ENABLE_SECOND="2. Farbe aktivieren";
    POINT_TWO_COLOR="Farbe für 2. Farbwechsel";
    POINT_TWO_LOC="2. Farbwechsel bei x%";
    GROUP_TEXT_POS="Gleiche Position für Vitalwerte";
    GROUP_TEXT_DIS="Gleiche Anzeige für Vitalwerte";
    VITALS_TEXT_POS="Position Vitalwerte";
    BOTTOM_OUTSIDE="Unten Außerhalb";
    BOTTOM_CENTER="Unten Mitte";
    BOTTOM_INSIDE="Unten Innerhalb";
    MIDDLE_OUTSIDE="Mitte Außerhalb";
    MIDDLE_INSIDE="Mitte Innerhalb";
    TOP_OUTSIDE="Oben Innerhalb";
    TOP_CENTER="Oben Mitte";
    TOP_INSIDE="Oben Innerhalb";
    VITALS_TEXT_DIS="Anzeige Vitalwerte";
    PCT="Prozent";
    REAL_NUMBERS="Zahlenwert";
    CUR_MAX="Aktuell/Max.";
    PCT_CUR_MAX="Prozent & Aktuell/Max.";
    TEXT_POSITION="Textposition";
    TEXT_DISPLAY="Textanzeige";
    REVERT_TEXT="Texteinst. rückg.";
    REVERT_MAIN="Haupteinst. rückg.";
    REVERT_COLOR="Farbeinst. rückg.";
    DEFAULT_TEXT="Texteinst. Std.";
    DEFAULT_MAIN="Haupteinst. Std.";
    DEFAULT_COLOR="Farbeinst. Std.";
    COLOR="Farbe";
    MAIN="Grundeinst.";
    TEXT="Text";
    ACCEPT="Übernehmen";
    YES="Ja";
    NO="Nein";
    ARE_YOU_SURE="Bist Du sicher, dass Du die Standardeinstellungen zurücksetzen willst?";
    DBB_BUTTON="Debuff";

    MEZ_BUTTON="Effects";
    MEZ_MAIN_TITLE="Effect time bars";
    MEZ_ACTIVATE="Activate effect indicators";
    MEZ_TIMER="Combat Timer";
    MEZ_TIMER_TICK="Remainder interval (seconds)";
    MEZ_OPACITY="Effect Indicator Opacity";   --DEBUFF_INDICATOR="Debuff Indicator Opacity";
    MEZ_VERTICAL="Vertikale Position";      --VERTICAL_POS="Vertical Position";
    MEZ_LIST="Timer durations (seconds)";
    MEZ_LIST_AUTO="Try to detect duration (on self)";
    MEZ_LM_MEZ="Blendender Blitzstrahl";
    --MEZ_LM_ROOT="Kr\195\164uterkunde";
    MEZ_LM_DEBUFF="Zeichen der Macht: Befehl";
    MEZ_LM_ANTISTUN="Z.d.M.: Rechtschaffenheit";
    --MEZ_MIN_FEAR="Schrei der Valar";
    --MEZ_MIN_DEAD="Lied der Toten";
    MEZ_MIN_SOLI="Selbstgespr\195\164ch des Geistes";
    --MEZ_MIN_CCOM="Fellowship's Heart TBD";
    MEZ_RK_ICE="Writ of Cold" -- Writ of Cold - Tier 1
    MEZ_RK_FIRE1="Writ of Fire" -- Writ of Fire - Tier 1
    MEZ_RK_FIREAOE="Scathing Mockery"
    MEZ_RK_FIRE3="Essay of Fire" -- + Improved
    MEZ_RK_FINE="Thunderous Words" -- Môts de tonnerre
    MEZ_RK_DEGA="Harsh Debate" -- Débat houleux
    MEZ_RK_CRIT="Charged Runes" -- Runes chargées
    MEZ_RK_15="Winter-storm" -- Tempète hivernale - 5
    MEZ_RK_HEAL1="Writ of Health" -- Allégorie de la santé
    MEZ_RK_HEAL2="Prelude to Hope" --Prélude à l'espoir (amélioré)
    MEZ_CA_MEZ="R\195\164tsel";
    --MEZ_CA_ROOT="Ein kleiner Schnitt";
    MEZ_CA_TRICK="Trick : All kinds...";
    --MEZ_CA_DEBUFF="Tumult";
    --MEZ_CH_FEAR="Bards Pfeil"
    --MEZ_CH_ROOT="Ruf des Raubtiers"
    --MEZ_CH_BUFF1="Schnellf\195\188ssigkeit"
    MEZ_CH_BUFF2="N\195\182tige Hast"

    -- Potions
    POTIONS_BUTTON = "Trank"
    POTIONS_MAIN_HEADER = "Trank"
    POTIONS_COOLDOWN_HEADER = "Abklingzeit"
    POTIONS_THRESHOLD_HEADER = "Schwellenwert"
    POTIONS_ENABLED = "Tränke aktivieren"
    POTIONS_MORALE_COOLDOWN = "Moral Trank Abklingzeit (in Sekunden)"
    POTIONS_POWER_COOLDOWN = "Kraft Trank Abklingzeit (in Sekunden)"
    POTIONS_AUTOMATIC_ALWAYS = "Immer auswählbar"
    POTIONS_AUTOMATIC_LOW = "Niederige Moral / Kraft"
    POTIONS_AUTOMATIC_AUTO = "Automatisches verwenden"
    POTIONS_MODE = "Trank Modus"
    POTIONS_MORALE_THRESHOLD = "Moral Trank Schwellenwert (in Prozent)"
    POTIONS_POWER_THRESHOLD = "Kraft Trank Schwellenwert (in Prozent)"

  elseif(loc=="fr")then
    TITLE="Options de Palantir";
    MISC="Divers";
    RECENTER="Recentrer Palantir";
    SHOW_IN_COMBAT="Montrer Seulement en combat";
    OOC_CRITERIA="Critère pour Hors Combat";
    VITALS_100="Carac Vitales 100%";
    MORALE_100="Moral 100%";
    POWER_100="Puissance 100%";
    OUT_OF_COMBAT="Hors Combat";
    SHOW_VITALS="Montrer les Carac Vitales";
    SCOPE="Paramètres liés à";
    ACCOUNT="Compte";
    SERVER="Serveur";
    CHAR="Personnage";
    SHOW_WARNING="Avertir quand un mob vous parle";
    SHOW_TIME="Afficher le temps en combat"
    USE_CLASS_SPECIFIC_POWER_BAR="Remplacer la puissance par la ressource de classe";
    PLAYER_VITALS="Barres Vitales";
    BAR_OPACITY="Opacité des barres";
    BAR_SPACING="Espacement des barres";
    TEXT_OPACITY="Opacité du texte";
    DEBUFF_BAR="Barre des debuffs";
    SHOW_DEBUFF="Montrer la barre des debuffs";
    DISPLAY_MODE="Mode de visualisation";
    DM_CURABLE="Barre des potions";
    DM_INDICATOR="Indicateurs seulement";
    DM_FLASH="Avertissement plein écran";
    DM_INSTANT="Potion instantanée";
    DEBUFF_INDICATOR="Opacité des indicateurs de debuffs";
    VERTICAL_POS="Position Verticale";
    COOLDOWNS="Temps de Recharge";
    WOUND_CD="Recharge Soins Blessure (En Secondes)";
    FEAR_CD="Recharge Soins Peur (En Secondes)";
    DISEASE_CD="Recharge Soins Maladie (En Secondes)";
    POISON_CD="Recharge Soins Poison (En Secondes)";
    PLAYER_MORALE="Barre de Moral";
    PLAYER_POWER="Barre de Puissance";
    BAR_COLOR="Couleur de la Barre";
    TEXT_COLOR="Couleur du Texte";
    ENABLE_CE="Autoriser les Changements de Couleur";
    ENABLE_CT="Autoriser les Transitions de Couleur";
    POINT_ONE_COLOR="Couleur du Seuil 1";
    POINT_ONE_LOC="Niveau du Seuil 1 (en %)";
    ENABLE_SECOND="Utiliser un autre seuil / couleur";
    POINT_TWO_COLOR="Couleur du Seuil 2";
    POINT_TWO_LOC="Niveau du Seuil 2 (en %)";
    GROUP_TEXT_POS="Position Commune des Textes";
    GROUP_TEXT_DIS="Affichage Commun des Textes";
    VITALS_TEXT_POS="Position des Informations Vitales";
    BOTTOM_OUTSIDE="Bas et Extérieur";
    BOTTOM_CENTER="Bas et Centré";
    BOTTOM_INSIDE="Bas et Intérieur";
    MIDDLE_OUTSIDE="Milieu et Extérieur";
    MIDDLE_INSIDE="Milieu et Intérieur";
    TOP_OUTSIDE="Haut et Extérieur";
    TOP_CENTER="Haut et Centré";
    TOP_INSIDE="Haut et Intérieur";
    VITALS_TEXT_DIS="Affichage des Informations";
    PCT="Pourcentage";
    REAL_NUMBERS="Nombre Réel";
    CUR_MAX="Actuel/Maxi";
    PCT_CUR_MAX="P% & Actuel/Maxi";
    TEXT_POSITION="Position du Texte";
    TEXT_DISPLAY="Affichage du Texte";
    REVERT_TEXT=" Annuler Modifs Texte";
    REVERT_MAIN=" Annuler Modifs Params Généraux";
    REVERT_COLOR=" Annuler Modifs Couleur";
    DEFAULT_TEXT=" Textes par Défaut";
    DEFAULT_MAIN=" Paramètres par Défaut";
    DEFAULT_COLOR=" Couleurs par Défaut";
    COLOR="Couleur";
    MAIN="Principal";
    TEXT="Texte";
    ACCEPT="Accepter";
    YES="Oui";
    NO="Non";
    ARE_YOU_SURE="Êtes vous sur de vouloir restaurer les paramètres par défaut ?";
    DBB_BUTTON="Debuffs";

    MEZ_BUTTON="Effets";
    MEZ_MAIN_TITLE="Minuteurs d'effet";
    MEZ_ACTIVATE="Activer les minuteurs";
    MEZ_TIMER="Minuteur de combat";
    MEZ_TIMER_TICK="Intervalles de rappel (secondes)";
    MEZ_OPACITY="Opacité des minuteurs";    --DEBUFF_INDICATOR="Debuff Indicator Opacity";
    MEZ_VERTICAL="Position verticale";      --VERTICAL_POS="Vertical Position";
    MEZ_LIST="Durées des effets (en secondes)";
    MEZ_LIST_AUTO="Détection des durées (sur soi)";
    MEZ_LM_MEZ="Lumi\195\168re aveuglante";
    --MEZ_LM_ROOT="Connaissance des plantes";
    MEZ_LM_DEBUFF="Signe de pouvoir : commandement";
    MEZ_LM_ANTISTUN="Signe de pouvoir : Int\195\169grit\195\169";
    --MEZ_MIN_FEAR="\195\137vocation d'Elbereth";
    --MEZ_MIN_DEAD="Chant des morts";
    MEZ_MIN_SOLI="Esprit de soliloque";
    --MEZ_MIN_CCOM="C\197\147ur de communaut\195\169";
    MEZ_RK_ICE="All\195\169gorie du froid" -- Writ of Cold - Tier 1
    MEZ_RK_FIRE1="All\195\169gorie du feu" -- Writ of Fire - Tier 1
    MEZ_RK_FIREAOE="Moquerie cinglante" -- Scathing Mockery + Improved
    MEZ_RK_FIRE3="Essai de feu" -- + Improved Essay of Fire
    MEZ_RK_FINE="Mots de tonnerre" -- Mots de tonnerre
    MEZ_RK_DEGA="D\195\169bat houleux" -- Débat houleux
    MEZ_RK_CRIT="Runes chargées" -- Runes chargées
    MEZ_RK_15="Temp\195\168te hivernale" -- Tempète hivernale - 5
    MEZ_RK_HEAL1="All\195\169gorie de la sant\195\169" -- Writ of Health
    MEZ_RK_HEAL2="Pr\195\169lude à l'espoir" -- (Improved) Prelude to Hope
    MEZ_CA_MEZ="Enigme";
    --MEZ_CA_ROOT="Petit accrochage";
    MEZ_CA_TRICK="Tours : Tous...";
    --MEZ_CA_DEBUFF="Embrouillement";
    --MEZ_CH_FEAR="Fl\195\170che de Barde"
    --MEZ_CH_ROOT="Pluie d\'\195\169pine"
    --MEZ_CH_BUFF1="C\195\169l\195\169rit\195\169"
    MEZ_CH_BUFF2="H\195\162te n\195\169cessaire"

    -- Potions
    POTIONS_BUTTON = "Potions"
    POTIONS_MAIN_HEADER = "Potions"
    POTIONS_COOLDOWN_HEADER = "Recharge"
    POTIONS_THRESHOLD_HEADER = "Limites"
    POTIONS_ENABLED = "Activer les Potions"
    POTIONS_MORALE_COOLDOWN = "Recharge des potions de moral (en secondes)"
    POTIONS_POWER_COOLDOWN = "Recharge des potions de puissance (en secondes)"
    POTIONS_AUTOMATIC_ALWAYS = "Toujours clickable"
    POTIONS_AUTOMATIC_LOW = "Seulement bas moral/puissance"
    POTIONS_AUTOMATIC_AUTO = "Consommation automatique"
    POTIONS_MODE = "Mode pour les potions"
    POTIONS_MORALE_THRESHOLD = "Limite de moral (en pourcent)"
    POTIONS_POWER_THRESHOLD = "Limite de puissance (en pourcent)"

  end







  -- Settings window's title
  self.tbHolder:SetWidth(255);
  self.tbRight:SetPosition(self.tbHolder:GetWidth()-35,0)
  self.tbHolder:SetLeft((self:GetWidth() - self.tbHolder:GetWidth())/2);
  self.tbCenter:SetSize(self.tbHolder:GetWidth()-70,42);
  self.tbCenter:SetPosition(self.tbLeft:GetLeft()+35,0)
  self.bg:SetSize(503,462);
  self.text:SetText(TITLE);
  self.text:SetWidth(self.tbHolder:GetWidth());
  self.text:SetHeight(20);
  self.text:SetLeft(-35);
  self.text:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.clButton:SetPosition(self:GetWidth()-23,24)






  -- General page
  self.tw=Turbine.UI.Window();
  self.tw:SetVisible(true);
  self.tw:SetParent(self);
  self.tw:SetSize(575,534);
  self.tw:SetPosition(0,0);
  self.tw:SetMouseVisible(false);

  self.sb = Turbine.UI.Lotro.ScrollBar();
  self.sb:SetOrientation(Turbine.UI.Orientation.Vertical);
  self.sb:SetParent(self.tw);
  self.sb:SetPosition(422,50);
  self.sb:SetSize(10,448);
  self.sb:SetZOrder(10);
  self.sb:SetBackColorBlendMode(0);
  self.sb:SetBlendMode(0);

  self.sp=Turbine.UI.ListBox();
  self.sp:SetParent(self.tw);
  self.sp:SetPosition(12,50);
  self.sp:SetSize(420,448);
  self.sp:SetVerticalScrollBar(self.sb);
  self.sp:SetOrientation( Turbine.UI.Orientation.Horizontal );
  self.sp:SetMaxItemsPerLine(1);
  self.sp:SetBackColorBlendMode(0);
  self.sp:SetBlendMode(4);
  self.sp.MouseClick=function(sender,args)
    self:Activate();
  end
  self.revMain=Turbine.UI.Lotro.Button();
  self.revMain:SetParent(self.tw);
  self.revMain:SetSize(128,20);
  self.revMain:SetText(REVERT_MAIN);
  self.revMain:SetPosition(32,500);
  self.revMain:SetEnabled(false);
  self.revMain.MouseDown=function(sender,args)
    self:Activate();
  end
  self.defMain=Turbine.UI.Lotro.Button();
  self.defMain:SetParent(self.tw);
  self.defMain:SetSize(128,20);
  self.defMain:SetText(DEFAULT_MAIN);
  self.defMain:SetPosition(163,500);
  self.defMain.MouseDown=function(sender,args)
    self:Activate();
  end
  self.accMain=Turbine.UI.Lotro.Button();
  self.accMain:SetParent(self.tw);
  self.accMain:SetSize(128,20);
  self.accMain:SetText(ACCEPT);
  self.accMain:SetPosition(294,500);
  self.accMain.MouseDown=function(sender,args)
    self:Activate();
  end

  self.hdr = Header();
  self.hdr.SetText(MISC);
  self.hdr:SetMouseVisible(false);
  --self.hdr:SetParent(self.tw);
  --self.hdr:SetPosition(30,60);
  self.sp:AddItem(self.hdr);

  self.rcHolder=Turbine.UI.Control();
  self.rcHolder:SetSize(420,30);
  self.rcHolder:SetMouseVisible(false);
  self.recenter = Turbine.UI.Lotro.Button();
  self.recenter:SetText(RECENTER);
  self.recenter:SetWidth(160);
  self.recenter:SetLeft(248);
  self.recenter:SetParent(self.rcHolder);
  self.recenter.MouseDown=function(sender,args)
    self:Activate();
  end
  self.sp:AddItem(self.rcHolder);

  self.sicHolder=Turbine.UI.Control();
  self.sicHolder:SetSize(420,30);
  self.sicHolder:SetMouseVisible(false);
  self.showincombat = DU_81.DUInterface.CheckBox(3);
  self.showincombat:SetChecked(true);
  self.showincombat:SetText(SHOW_IN_COMBAT);
  self.showincombat:SetParent(self.sicHolder);
  self.showincombat:SetLeft(55);
  self.showincombat.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.sicHolder);

  self.nocHolder=Turbine.UI.Control();
  self.nocHolder:SetSize(420,30);
  self.nocHolder:SetMouseVisible(false);
  self.noc = DU_81.DUInterface.DropDownBox({VITALS_100,MORALE_100,POWER_100,OUT_OF_COMBAT});
  self.noc:SetParent(self.nocHolder);
  self.noc:SetPosition(246,0);
  self.noc.doActive=function()
    self:Activate();
  end
  self.nocPanel=Turbine.UI.Window();
  self.nocPanel:SetVisible(true);
  self.nocPanel:SetParent(self.sp);
  self.nocPanel:SetPosition(0,0);
  self.nocPanel:SetSize(480,448);
  self.nocPanel:SetZOrder(99);
  self.nocPanel:SetVisible(false);
  self.nocLb = self.noc.listBox;
  self.nocLb:SetParent(self.nocPanel);
  self.nocLb:SetZOrder(99);
  self.nocLb:SetBlendMode(0);
  self.nocLb:SetBackColorBlendMode(0);
  self.nocLb:SetPosition(200,200);
  self.nocLabel=Turbine.UI.Label();
  self.nocLabel:SetParent(self.nocHolder);
  self.nocLabel:SetPosition(0,0);
  self.nocLabel:SetSize(246,20);
  self.nocLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.nocLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.nocLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.nocLabel:SetText(OOC_CRITERIA);
  self.nocLabel:SetMouseVisible(false);
  self.sp:AddItem(self.nocHolder);

  self.svHolder=Turbine.UI.Control();
  self.svHolder:SetSize(420,30);
  self.svHolder:SetMouseVisible(false);
  self.sv = DU_81.DUInterface.CheckBox(3);
  self.sv:SetChecked(true);
  self.sv:SetText(SHOW_VITALS);
  self.sv:SetParent(self.svHolder);
  self.sv:SetLeft(55);
  self.sv.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.svHolder);

  self.acHolder=Turbine.UI.Control();
  self.acHolder:SetSize(420,30);
  self.acHolder:SetMouseVisible(false);
  self.ac = DU_81.DUInterface.DropDownBox({ACCOUNT,CHAR,SERVER});
  self.ac:SetParent(self.acHolder);
  self.ac:SetPosition(246,0);
  self.ac.doActive=function()
    self:Activate();
  end
  self.acPanel=Turbine.UI.Window();
  self.acPanel:SetVisible(true);
  self.acPanel:SetParent(self.sp);
  self.acPanel:SetPosition(0,0);
  self.acPanel:SetSize(480,448);
  self.acPanel:SetZOrder(99);
  self.acPanel:SetVisible(false);
  self.acLb = self.ac.listBox;
  self.acLb:SetParent(self.acPanel);
  self.acLb:SetZOrder(99);
  self.acLb:SetBlendMode(0);
  self.acLb:SetBackColorBlendMode(0);
  self.acLb:SetPosition(200,200);
  self.acLabel=Turbine.UI.Label();
  self.acLabel:SetParent(self.acHolder);
  self.acLabel:SetPosition(0,0);
  self.acLabel:SetSize(246,20);
  self.acLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.acLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.acLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.acLabel:SetText(SCOPE);
  self.acLabel:SetMouseVisible(false);
  self.sp:AddItem(self.acHolder);

  -- Warning
  self.WarnHolder=Turbine.UI.Control();
  self.WarnHolder:SetSize(420,30);
  self.WarnHolder:SetMouseVisible(false);
  self.Warn = DU_81.DUInterface.CheckBox(3);
  self.Warn:SetChecked(true);
  self.Warn:SetText(SHOW_WARNING);
  self.Warn:SetParent(self.WarnHolder);
  self.Warn:SetLeft(55);
  self.Warn.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.WarnHolder);

  -- show timer
  self.ShowTimerHolder=Turbine.UI.Control();
  self.ShowTimerHolder:SetSize(420,30);
  self.ShowTimerHolder:SetMouseVisible(false);
  self.ShowTimer = DU_81.DUInterface.CheckBox(3);
  self.ShowTimer:SetChecked(true);
  self.ShowTimer:SetText(SHOW_TIME);
  self.ShowTimer:SetParent(self.ShowTimerHolder);
  self.ShowTimer:SetLeft(55);
  self.ShowTimer.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.ShowTimerHolder);

  -- use class specific resource
  self.ClassSpecificPowerBarHolder=Turbine.UI.Control();
  self.ClassSpecificPowerBarHolder:SetSize(420,30);
  self.ClassSpecificPowerBarHolder:SetMouseVisible(false);
  self.ClassSpecificPowerBar = DU_81.DUInterface.CheckBox(3);
  self.ClassSpecificPowerBar:SetChecked(true);
  self.ClassSpecificPowerBar:SetText(USE_CLASS_SPECIFIC_POWER_BAR);
  self.ClassSpecificPowerBar:SetParent(self.ClassSpecificPowerBarHolder);
  self.ClassSpecificPowerBar:SetLeft(55);
  self.ClassSpecificPowerBar.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.ClassSpecificPowerBarHolder);

  self.vitHdr=Header();
  self.vitHdr.SetText(PLAYER_VITALS);
  self.vitHdr:SetMouseVisible(false);
  self.sp:AddItem(self.vitHdr);

  self.voHolder = Turbine.UI.Control();
  self.voHolder:SetSize(420,45);
  self.voHolder:SetMouseVisible(false);
  self.voSld = DU_81.DUInterface.Slider();
  self.voSld:SetParent(self.voHolder);
  self.voSld:SetLeft(57);
  self.voSld:SetText(BAR_OPACITY);
  self.voSld.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.voHolder);

  self.bpHolder = Turbine.UI.Control();
  self.bpHolder:SetSize(420,45);
  self.bpHolder:SetMouseVisible(false);
  self.bpSld = DU_81.DUInterface.Slider();
  self.bpSld:SetParent(self.bpHolder);
  self.bpSld:SetLeft(57);
  self.bpSld:SetText(BAR_SPACING);
  self.bpSld.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.bpHolder);

  self.txHolder = Turbine.UI.Control();
  self.txHolder:SetSize(420,45);
  self.txHolder:SetMouseVisible(false);
  self.txSld = DU_81.DUInterface.Slider();
  self.txSld:SetParent(self.txHolder);
  self.txSld:SetLeft(57);
  self.txSld:SetText(TEXT_OPACITY);
  self.txSld.doActive=function()
    self:Activate();
  end
  self.sp:AddItem(self.txHolder);






  -- Text Panel--
  self.txw=Turbine.UI.Window();
  self.txw:SetVisible(true);
  self.txw:SetParent(self);
  self.txw:SetSize(575,534);
  self.txw:SetPosition(0,0);
  self.txw:SetMouseVisible(false);
  self.txw:SetVisible(false);

  self.sbt = Turbine.UI.Lotro.ScrollBar();
  self.sbt:SetOrientation(Turbine.UI.Orientation.Vertical);
  self.sbt:SetParent(self.txw);
  self.sbt:SetPosition(422,50);
  self.sbt:SetSize(10,448);
  self.sbt:SetZOrder(10);
  self.sbt:SetBackColorBlendMode(0);
  self.sbt:SetBlendMode(0);
  --self.sbt:SetVisible(false);

  self.txtPanel=Turbine.UI.ListBox();
  self.txtPanel:SetParent(self.txw);
  self.txtPanel:SetPosition(12,50);
  --self.sp:SetBackColor(Turbine.UI.Color(1,0,0));
  self.txtPanel:SetSize(420,448);
  self.txtPanel:SetVerticalScrollBar(self.sbt);
  --self.sp:SetVisible(false);
  self.txtPanel:SetOrientation( Turbine.UI.Orientation.Horizontal );
  self.txtPanel:SetMaxItemsPerLine(1);
  self.txtPanel:SetBackColorBlendMode(0);
  self.txtPanel:SetBlendMode(0);
  --self.txtPanel:SetVisible(false);
  self.txtPanel.MouseDown=function(sender,args)
    self:Activate();
  end
  self.revText=Turbine.UI.Lotro.Button();
  self.revText:SetParent(self.txw);
  self.revText:SetSize(128,20);
  self.revText:SetText(REVERT_TEXT);
  self.revText:SetPosition(32,500);
  self.revText:SetEnabled(false);
  self.revText.MouseDown=function(sender,args)
    self:Activate();
  end
  self.defText=Turbine.UI.Lotro.Button();
  self.defText:SetParent(self.txw);
  self.defText:SetSize(128,20);
  self.defText:SetText(DEFAULT_TEXT);
  self.defText:SetPosition(163,500);
  self.defText.MouseDown=function(sender,args)
    self:Activate();
  end
  self.accText=Turbine.UI.Lotro.Button();
  self.accText:SetParent(self.txw);
  self.accText:SetSize(128,20);
  self.accText:SetText(ACCEPT);
  self.accText:SetPosition(294,500);
  self.accText.MouseDown=function(sender,args)
    self:Activate();
  end
  self.pvtHdr=Header();
  self.pvtHdr.SetText(PLAYER_VITALS);

  self.txtPanel:AddItem(self.pvtHdr);

  self.gTxtPosHolder=Turbine.UI.Control();
  self.gTxtPosHolder:SetSize(420,30);
  self.gTxtPosHolder:SetMouseVisible(false);
  self.gTxtPos = DU_81.DUInterface.CheckBox(3);
  self.gTxtPos:SetChecked(true);
  self.gTxtPos:SetText(GROUP_TEXT_POS);
  self.gTxtPos:SetParent(self.gTxtPosHolder);
  self.gTxtPos:SetLeft(55);
  self.gTxtPos.doActive=function()
    self:Activate();
  end
  self.txtPanel:AddItem(self.gTxtPosHolder);

  self.txtPosHolder=Turbine.UI.Control();
  self.txtPosHolder:SetSize(420,30);
  self.txtPosHolder:SetMouseVisible(false);
  self.txtPos = DU_81.DUInterface.DropDownBox({BOTTOM_OUTSIDE,BOTTOM_CENTER,BOTTOM_INSIDE,MIDDLE_OUTSIDE,MIDDLE_INSIDE,TOP_OUTSIDE,TOP_CENTER,TOP_INSIDE});
  self.txtPos:SetParent(self.txtPosHolder);
  self.txtPos:SetPosition(246,0);
  self.txtPos.doActive=function()
    self:Activate();
  end
  self.txtPosPanel=Turbine.UI.Window();
  self.txtPosPanel:SetVisible(true);
  self.txtPosPanel:SetParent(self.txtPanel);
  self.txtPosPanel:SetPosition(0,0);
  self.txtPosPanel:SetSize(480,448);
  self.txtPosPanel:SetZOrder(99);
  self.txtPosPanel:SetVisible(false);
  self.txtPosLb = self.txtPos.listBox;
  self.txtPosLb:SetParent(self.txtPosPanel);
  self.txtPosLb:SetZOrder(99);
  self.txtPosLb:SetBlendMode(0);
  self.txtPosLb:SetBackColorBlendMode(0);
  self.txtPosLb:SetPosition(200,200);
  self.txtPosLabel=Turbine.UI.Label();
  self.txtPosLabel:SetParent(self.txtPosHolder);
  self.txtPosLabel:SetPosition(0,0);
  self.txtPosLabel:SetSize(246,20);
  self.txtPosLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.txtPosLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.txtPosLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.txtPosLabel:SetText(VITALS_TEXT_POS);
  self.txtPosLabel:SetMouseVisible(false);
  self.txtPanel:AddItem(self.txtPosHolder);

  self.txtPosPanel.MouseClick=function(sender,args)
    self.txtPos.Close();
    self.txtPosPanel:SetVisible(false);
  end


  self.txtPos.Opening =function()
    self.txtPosPanel:SetVisible(true);
    self.txtPosLb:SetPosition(246,self.txtPosHolder:GetTop()+21);

  end

  self.gTxtDisHolder=Turbine.UI.Control();
  self.gTxtDisHolder:SetSize(420,30);
  self.gTxtDisHolder:SetMouseVisible(false);
  self.gTxtDis = DU_81.DUInterface.CheckBox(3);
  self.gTxtDis:SetChecked(true);
  self.gTxtDis:SetText(GROUP_TEXT_DIS);
  self.gTxtDis:SetParent(self.gTxtDisHolder);
  self.gTxtDis:SetLeft(55);

  self.gTxtDis.doActive=function()
    self:Activate();
  end
  self.txtPanel:AddItem(self.gTxtDisHolder);
  self.txtDisHolder=Turbine.UI.Control();
  self.txtDisHolder:SetSize(420,30);
  self.txtDisHolder:SetMouseVisible(false);
  self.txtDis = DU_81.DUInterface.DropDownBox({PCT,REAL_NUMBERS,CUR_MAX,PCT_CUR_MAX});
  self.txtDis:SetParent(self.txtDisHolder);
  self.txtDis:SetPosition(246,0);

  self.txtDis.doActive=function()
    self:Activate();
  end
  self.txtDisPanel=Turbine.UI.Window();
  self.txtDisPanel:SetVisible(true);
  self.txtDisPanel:SetParent(self.txtPanel);
  self.txtDisPanel:SetPosition(0,0);
  self.txtDisPanel:SetSize(480,448);
  self.txtDisPanel:SetZOrder(99);
  self.txtDisPanel:SetVisible(false);
  self.txtDisLb = self.txtDis.listBox;
  self.txtDisLb:SetParent(self.txtDisPanel);
  self.txtDisLb:SetZOrder(99);
  self.txtDisLb:SetBlendMode(0);
  self.txtDisLb:SetBackColorBlendMode(0);
  self.txtDisLb:SetPosition(200,200);
  self.txtDisLabel=Turbine.UI.Label();
  self.txtDisLabel:SetParent(self.txtDisHolder);
  self.txtDisLabel:SetPosition(0,0);
  self.txtDisLabel:SetSize(246,20);
  self.txtDisLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.txtDisLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.txtDisLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.txtDisLabel:SetText(VITALS_TEXT_DIS);
  self.txtDisLabel:SetMouseVisible(false);
  self.txtPanel:AddItem(self.txtDisHolder);

  self.txtDisPanel.MouseClick=function(sender,args)
    self.txtDis.Close();
    self.txtDisPanel:SetVisible(false);
  end


  self.txtDis.Opening =function()
    self.txtDisPanel:SetVisible(true);
    self.txtDisLb:SetPosition(246,self.txtDisHolder:GetTop()+21);

  end

  self.pmtHdr=Header();
  self.pmtHdr.SetText(PLAYER_MORALE);
  self.txtPanel:AddItem(self.pmtHdr);

  self.pmbTxtPosHolder=Turbine.UI.Control();
  self.pmbTxtPosHolder:SetSize(420,30);
  self.pmbTxtPosHolder:SetMouseVisible(false);
  self.pmbTxtPos = DU_81.DUInterface.DropDownBox({BOTTOM_OUTSIDE,BOTTOM_CENTER,BOTTOM_INSIDE,MIDDLE_OUTSIDE,MIDDLE_INSIDE,TOP_OUTSIDE,TOP_CENTER,TOP_INSIDE});
  self.pmbTxtPos:SetParent(self.pmbTxtPosHolder);
  self.pmbTxtPos:SetPosition(246,0);
  self.pmbTxtPos.doActive=function()
    self:Activate();
  end
  self.pmbTxtPosPanel=Turbine.UI.Window();
  self.pmbTxtPosPanel:SetVisible(true);
  self.pmbTxtPosPanel:SetParent(self.txtPanel);
  self.pmbTxtPosPanel:SetPosition(0,0);
  self.pmbTxtPosPanel:SetSize(480,448);
  self.pmbTxtPosPanel:SetZOrder(99);
  self.pmbTxtPosPanel:SetVisible(false);
  self.pmbTxtPosLb = self.pmbTxtPos.listBox;
  self.pmbTxtPosLb:SetParent(self.pmbTxtPosPanel);
  self.pmbTxtPosLb:SetZOrder(99);
  self.pmbTxtPosLb:SetBlendMode(0);
  self.pmbTxtPosLb:SetBackColorBlendMode(0);
  self.pmbTxtPosLb:SetPosition(200,200);
  self.pmbTxtPosLabel=Turbine.UI.Label();
  self.pmbTxtPosLabel:SetParent(self.pmbTxtPosHolder);
  self.pmbTxtPosLabel:SetPosition(0,0);
  self.pmbTxtPosLabel:SetSize(246,20);
  self.pmbTxtPosLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.pmbTxtPosLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.pmbTxtPosLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.pmbTxtPosLabel:SetText(TEXT_POSITION);
  self.pmbTxtPosLabel:SetMouseVisible(false);
  self.txtPanel:AddItem(self.pmbTxtPosHolder);

  self.pmbTxtPosPanel.MouseClick=function(sender,args)
    self.pmbTxtPos.Close();
    self.pmbTxtPosPanel:SetVisible(false);
  end


  self.pmbTxtPos.Opening =function()
    self.pmbTxtPosPanel:SetVisible(true);
    self.pmbTxtPosLb:SetPosition(246,self.pmbTxtPosHolder:GetTop()+21);

  end


  self.pmbTxtDisHolder=Turbine.UI.Control();
  self.pmbTxtDisHolder:SetSize(420,30);
  self.pmbTxtDisHolder:SetMouseVisible(false);
  self.pmbTxtDis = DU_81.DUInterface.DropDownBox({PCT,REAL_NUMBERS,CUR_MAX,PCT_CUR_MAX});
  self.pmbTxtDis:SetParent(self.pmbTxtDisHolder);
  self.pmbTxtDis:SetPosition(246,0);
  self.pmbTxtDisPanel=Turbine.UI.Window();
  self.pmbTxtDisPanel:SetVisible(true);
  self.pmbTxtDisPanel:SetParent(self.txtPanel);
  self.pmbTxtDisPanel:SetPosition(0,0);
  self.pmbTxtDisPanel:SetSize(480,448);
  self.pmbTxtDisPanel:SetZOrder(99);
  self.pmbTxtDisPanel:SetVisible(false);
  self.pmbTxtDisLb = self.pmbTxtDis.listBox;
  self.pmbTxtDisLb:SetParent(self.pmbTxtDisPanel);
  self.pmbTxtDisLb:SetZOrder(99);
  self.pmbTxtDisLb:SetBlendMode(0);
  self.pmbTxtDisLb:SetBackColorBlendMode(0);
  self.pmbTxtDisLb:SetPosition(200,200);
  self.pmbTxtDisLabel=Turbine.UI.Label();
  self.pmbTxtDisLabel:SetParent(self.pmbTxtDisHolder);
  self.pmbTxtDisLabel:SetPosition(0,0);
  self.pmbTxtDisLabel:SetSize(246,20);
  self.pmbTxtDisLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.pmbTxtDisLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.pmbTxtDisLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.pmbTxtDisLabel:SetText(TEXT_DISPLAY);
  self.pmbTxtDisLabel:SetMouseVisible(false);
  self.txtPanel:AddItem(self.pmbTxtDisHolder);

  self.pmbTxtDisPanel.MouseClick=function(sender,args)
    self.pmbTxtDis.Close();
    self.pmbTxtDisPanel:SetVisible(false);
  end


  self.pmbTxtDis.Opening =function()
    self.pmbTxtDisPanel:SetVisible(true);
    self.pmbTxtDisLb:SetPosition(246,self.pmbTxtDisHolder:GetTop()+21);

  end


  self.pptHdr=Header();
  self.pptHdr.SetText(PLAYER_POWER);
  self.txtPanel:AddItem(self.pptHdr);

  self.ppbTxtPosHolder=Turbine.UI.Control();
  self.ppbTxtPosHolder:SetSize(420,30);
  self.ppbTxtPosHolder:SetMouseVisible(false);
  self.ppbTxtPos = DU_81.DUInterface.DropDownBox({BOTTOM_OUTSIDE,BOTTOM_CENTER,BOTTOM_INSIDE,MIDDLE_OUTSIDE,MIDDLE_INSIDE,TOP_OUTSIDE,TOP_CENTER,TOP_INSIDE});
  self.ppbTxtPos:SetParent(self.ppbTxtPosHolder);
  self.ppbTxtPos.doActive=function()
    self:Activate();
  end
  self.ppbTxtPos:SetPosition(246,0);
  self.ppbTxtPosPanel=Turbine.UI.Window();
  self.ppbTxtPosPanel:SetVisible(true);
  self.ppbTxtPosPanel:SetParent(self.txtPanel);
  self.ppbTxtPosPanel:SetPosition(0,0);
  self.ppbTxtPosPanel:SetSize(480,448);
  self.ppbTxtPosPanel:SetZOrder(99);
  self.ppbTxtPosPanel:SetVisible(false);
  self.ppbTxtPosLb = self.ppbTxtPos.listBox;
  self.ppbTxtPosLb:SetParent(self.ppbTxtPosPanel);
  self.ppbTxtPosLb:SetZOrder(99);
  self.ppbTxtPosLb:SetBlendMode(0);
  self.ppbTxtPosLb:SetBackColorBlendMode(0);
  self.ppbTxtPosLb:SetPosition(200,200);
  self.ppbTxtPosLabel=Turbine.UI.Label();
  self.ppbTxtPosLabel:SetParent(self.ppbTxtPosHolder);
  self.ppbTxtPosLabel:SetPosition(0,0);
  self.ppbTxtPosLabel:SetSize(246,20);
  self.ppbTxtPosLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.ppbTxtPosLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.ppbTxtPosLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.ppbTxtPosLabel:SetText(TEXT_POSITION);
  self.ppbTxtPosLabel:SetMouseVisible(false);
  self.txtPanel:AddItem(self.ppbTxtPosHolder);

  self.ppbTxtPosPanel.MouseClick=function(sender,args)
    self.ppbTxtPos.Close();
    self.ppbTxtPosPanel:SetVisible(false);
  end


  self.ppbTxtPos.Opening =function()
    self.ppbTxtPosPanel:SetVisible(true);
    self.ppbTxtPosLb:SetPosition(246,self.ppbTxtPosHolder:GetTop()+21);

  end


  self.ppbTxtDisHolder=Turbine.UI.Control();
  self.ppbTxtDisHolder:SetSize(420,30);
  self.ppbTxtDisHolder:SetMouseVisible(false);
  self.ppbTxtDis = DU_81.DUInterface.DropDownBox({PCT,REAL_NUMBERS,CUR_MAX,PCT_CUR_MAX});
  self.ppbTxtDis:SetParent(self.ppbTxtDisHolder);
  self.ppbTxtDis:SetPosition(246,0);
  self.ppbTxtDis.doActive=function()
    self:Activate();
  end
  self.ppbTxtDisPanel=Turbine.UI.Window();
  self.ppbTxtDisPanel:SetVisible(true);
  self.ppbTxtDisPanel:SetParent(self.txtPanel);
  self.ppbTxtDisPanel:SetPosition(0,0);
  self.ppbTxtDisPanel:SetSize(480,448);
  self.ppbTxtDisPanel:SetZOrder(99);
  self.ppbTxtDisPanel:SetVisible(false);
  self.ppbTxtDisLb = self.ppbTxtDis.listBox;
  self.ppbTxtDisLb:SetParent(self.ppbTxtDisPanel);
  self.ppbTxtDisLb:SetZOrder(99);
  self.ppbTxtDisLb:SetBlendMode(0);
  self.ppbTxtDisLb:SetBackColorBlendMode(0);
  self.ppbTxtDisLb:SetPosition(200,200);
  self.ppbTxtDisLabel=Turbine.UI.Label();
  self.ppbTxtDisLabel:SetParent(self.ppbTxtDisHolder);
  self.ppbTxtDisLabel:SetPosition(0,0);
  self.ppbTxtDisLabel:SetSize(246,20);
  self.ppbTxtDisLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.ppbTxtDisLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.ppbTxtDisLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.ppbTxtDisLabel:SetText(TEXT_DISPLAY);
  self.ppbTxtDisLabel:SetMouseVisible(false);
  self.txtPanel:AddItem(self.ppbTxtDisHolder);

  self.ppbTxtDisPanel.MouseClick=function(sender,args)
    self.ppbTxtDis.Close();
    self.ppbTxtDisPanel:SetVisible(false);
  end


  self.ppbTxtDis.Opening =function()
    self.ppbTxtDisPanel:SetVisible(true);
    self.ppbTxtDisLb:SetPosition(246,self.ppbTxtDisHolder:GetTop()+21);

  end


  self.pmbBcChanged=function()

  end

  self.pmbClrOneChanged=function()

  end

  self.pmbClrTwoChanged=function()

  end

  self.ppbBcChanged=function()

  end

  self.ppbClrOneChanged=function()

  end

  self.ppbClrTwoChanged=function()

  end











  -- Color Panel--
  self.clw=Turbine.UI.Window();
  self.clw:SetVisible(true);
  self.clw:SetParent(self);
  self.clw:SetSize(575,534);
  self.clw:SetPosition(0,0);
  self.clw:SetMouseVisible(false);
  self.clw:SetVisible(false);

  self.sbc = Turbine.UI.Lotro.ScrollBar();
  self.sbc:SetOrientation(Turbine.UI.Orientation.Vertical);
  self.sbc:SetParent(self.clw);
  self.sbc:SetPosition(422,50);
  self.sbc:SetSize(10,448);
  self.sbc:SetZOrder(10);
  self.sbc:SetBackColorBlendMode(0);
  self.sbc:SetBlendMode(0);
  --self.sbc:SetVisible(false);

  self.colPanel=Turbine.UI.ListBox();
  self.colPanel:SetParent(self.clw);
  self.colPanel:SetPosition(12,50);
  --self.sp:SetBackColor(Turbine.UI.Color(1,0,0));
  self.colPanel:SetSize(420,448);
  self.colPanel:SetVerticalScrollBar(self.sbc);
  --self.sp:SetVisible(false);
  self.colPanel:SetOrientation( Turbine.UI.Orientation.Horizontal );
  self.colPanel:SetMaxItemsPerLine(1);
  self.colPanel:SetBackColorBlendMode(0);
  self.colPanel:SetBlendMode(0);
  self.colPanel.MouseDown=function(sender,args)
    self:Activate();
  end

  self.revColor=Turbine.UI.Lotro.Button();
  self.revColor:SetParent(self.clw);
  self.revColor:SetSize(128,20);
  self.revColor:SetText(REVERT_COLOR);
  self.revColor:SetPosition(32,500);
  self.revColor:SetEnabled(false);
  self.revColor.MouseDown=function(sender,args)
    self:Activate();
  end
  self.defColor=Turbine.UI.Lotro.Button();
  self.defColor:SetParent(self.clw);
  self.defColor:SetSize(128,20);
  self.defColor:SetText(DEFAULT_COLOR);
  self.defColor:SetPosition(163,500);
  self.defColor.MouseDown=function(sender,args)
    self:Activate();
  end
  self.accColor=Turbine.UI.Lotro.Button();
  self.accColor:SetParent(self.clw);
  self.accColor:SetSize(128,20);
  self.accColor:SetText(ACCEPT);
  self.accColor:SetPosition(294,500);
  self.accColor.MouseDown=function(sender,args)
    self:Activate();
  end
  self.pmbcHdr=Header();
  self.pmbcHdr.SetText(PLAYER_MORALE);
  self.colPanel:AddItem(self.pmbcHdr);

  self.pmbBcHolder=Turbine.UI.Control();
  self.pmbBcHolder:SetSize(420,35);
  self.pmbBcHolder:SetMouseVisible(false);
  self.pmbBcLabel=Turbine.UI.Label();
  self.pmbBcLabel:SetParent(self.pmbBcHolder);
  self.pmbBcLabel:SetPosition(0,0);
  self.pmbBcLabel:SetSize(246,35);
  self.pmbBcLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.pmbBcLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.pmbBcLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.pmbBcLabel:SetText(BAR_COLOR);
  self.pmbBcLabel:SetMouseVisible(false);
  self.pmbBcColor=Turbine.UI.Lotro.TextBox();
  self.pmbBcColor:SetParent(self.pmbBcHolder);
  self.pmbBcColor:SetSize(35,25);
  self.pmbBcColor:SetPosition(265,5);
  self.pmbBcColor:SetReadOnly(true);
  self.pmbBcColor:SetMouseVisible(false);
  self.pmbBcCp=DU_81.DUInterface.ColorPicker();
  self.pmbBcCp:SetParent(self.pmbBcHolder);
  self.pmbBcCp:SetPosition(305,5);
  self.pmbBcCp.doActive=function()
    self:Activate();
  end
  self.pmbBcOL=Turbine.UI.Control();
  self.pmbBcOL:SetParent(self.pmbBcColor);
  self.pmbBcOL:SetSize(30,20);
  self.pmbBcOL:SetZOrder(5);
  self.pmbBcOL:SetPosition(2,2);
  self.pmbBcOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.pmbBcOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.pmbBcHolder);
  self.pmbBcCp.ColorChanged=function(sender,args)
    self.pmbBcOL:SetBackColor(args.Color);
    self.pmbBcChanged();
  end


  self.pmbTcHolder=Turbine.UI.Control();
  self.pmbTcHolder:SetSize(420,35);
  self.pmbTcHolder:SetMouseVisible(false);
  self.pmbTcLabel=Turbine.UI.Label();
  self.pmbTcLabel:SetParent(self.pmbTcHolder);
  self.pmbTcLabel:SetPosition(0,0);
  self.pmbTcLabel:SetSize(246,35);
  self.pmbTcLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.pmbTcLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.pmbTcLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.pmbTcLabel:SetText(TEXT_COLOR);
  self.pmbTcLabel:SetMouseVisible(false);
  self.pmbTcColor=Turbine.UI.Lotro.TextBox();
  self.pmbTcColor:SetParent(self.pmbTcHolder);
  self.pmbTcColor:SetSize(35,25);
  self.pmbTcColor:SetPosition(265,5);
  self.pmbTcColor:SetReadOnly(true);
  self.pmbTcColor:SetMouseVisible(false);
  self.pmbTcCp=DU_81.DUInterface.ColorPicker();
  self.pmbTcCp:SetParent(self.pmbTcHolder);
  self.pmbTcCp:SetPosition(305,5);
  self.pmbTcCp.doActive=function()
    self:Activate();
  end
  self.pmbTcOL=Turbine.UI.Control();
  self.pmbTcOL:SetParent(self.pmbTcColor);
  self.pmbTcOL:SetSize(30,20);
  self.pmbTcOL:SetZOrder(5);
  self.pmbTcOL:SetPosition(2,2);
  self.pmbTcOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.pmbTcOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.pmbTcHolder);
  self.pmbTcCp.ColorChanged=function(sender,args)
    self.pmbTcOL:SetBackColor(args.Color);
    self.pmbTcChanged();
  end







  self.pmbEceHolder=Turbine.UI.Control();
  self.pmbEceHolder:SetSize(420,30);
  self.pmbEceHolder:SetMouseVisible(false);
  self.pmbEce = DU_81.DUInterface.CheckBox(3);
  self.pmbEce:SetChecked(true);
  self.pmbEce:SetText(ENABLE_CE);
  self.pmbEce:SetParent(self.pmbEceHolder);
  self.pmbEce:SetLeft(55);
  self.pmbEce.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.pmbEceHolder);

  self.pmbEctHolder=Turbine.UI.Control();
  self.pmbEctHolder:SetSize(420,30);
  self.pmbEctHolder:SetMouseVisible(false);
  self.pmbEct = DU_81.DUInterface.CheckBox(3);
  self.pmbEct:SetChecked(true);
  self.pmbEct:SetText(ENABLE_CT);
  self.pmbEct:SetParent(self.pmbEctHolder);
  self.pmbEct:SetLeft(55);
  self.pmbEct.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.pmbEctHolder);

  self.pmbClrOneHolder=Turbine.UI.Control();
  self.pmbClrOneHolder:SetSize(420,35);
  self.pmbClrOneHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.pmbClrOneHolder:SetMouseVisible(false);
  self.pmbClrOneLabel=Turbine.UI.Label();
  self.pmbClrOneLabel:SetParent(self.pmbClrOneHolder);
  self.pmbClrOneLabel:SetPosition(0,0);
  self.pmbClrOneLabel:SetSize(246,35);
  self.pmbClrOneLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.pmbClrOneLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.pmbClrOneLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.pmbClrOneLabel:SetText(POINT_ONE_COLOR);
  self.pmbClrOneLabel:SetMouseVisible(false);
  self.pmbClrOneColor=Turbine.UI.Lotro.TextBox();
  self.pmbClrOneColor:SetParent(self.pmbClrOneHolder);
  self.pmbClrOneColor:SetSize(35,25);
  self.pmbClrOneColor:SetPosition(265,5);
  self.pmbClrOneColor:SetReadOnly(true);
  self.pmbClrOneColor:SetMouseVisible(false);
  self.pmbClrOneCp=DU_81.DUInterface.ColorPicker();
  self.pmbClrOneCp:SetParent(self.pmbClrOneHolder);
  self.pmbClrOneCp:SetPosition(305,5);
  self.pmbClrOneCp.doActive=function()
    self:Activate();
  end
  self.pmbClrOneOL=Turbine.UI.Control();
  self.pmbClrOneOL:SetParent(self.pmbClrOneColor);
  self.pmbClrOneOL:SetSize(30,20);
  self.pmbClrOneOL:SetZOrder(5);
  self.pmbClrOneOL:SetPosition(2,2);
  self.pmbClrOneOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.pmbClrOneOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.pmbClrOneHolder);
  self.pmbClrOneCp.ColorChanged=function(sender,args)
    self.pmbClrOneOL:SetBackColor(args.Color);
    self.pmbClrOneChanged();
  end

  self.pmbCpOneHolder = Turbine.UI.Control();
  self.pmbCpOneHolder:SetSize(420,45);
  self.pmbCpOneHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.pmbCpOneHolder:SetMouseVisible(false);
  self.pmbCpOneSld = DU_81.DUInterface.Slider();
  self.pmbCpOneSld:SetStep(0.01);
  self.pmbCpOneSld:SetParent(self.pmbCpOneHolder);
  self.pmbCpOneSld:SetLeft(57);
  self.pmbCpOneSld:SetText(POINT_ONE_LOC);
  self.pmbCpOneSld.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.pmbCpOneHolder);

  self.pmbEscHolder=Turbine.UI.Control();
  self.pmbEscHolder:SetSize(420,30);
  self.pmbEscHolder:SetMouseVisible(false);
  self.pmbEsc = DU_81.DUInterface.CheckBox(3);
  self.pmbEsc:SetChecked(true);
  self.pmbEsc:SetText(ENABLE_SECOND);
  self.pmbEsc:SetParent(self.pmbEscHolder);
  self.pmbEsc:SetLeft(55);
  self.pmbEsc.doActive=function()
    self:Activate()
  end
  self.colPanel:AddItem(self.pmbEscHolder);

  self.pmbClrTwoHolder=Turbine.UI.Control();
  self.pmbClrTwoHolder:SetSize(420,35);
  self.pmbClrTwoHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.pmbClrTwoHolder:SetMouseVisible(false);
  self.pmbClrTwoLabel=Turbine.UI.Label();
  self.pmbClrTwoLabel:SetParent(self.pmbClrTwoHolder);
  self.pmbClrTwoLabel:SetPosition(0,0);
  self.pmbClrTwoLabel:SetSize(246,35);
  self.pmbClrTwoLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.pmbClrTwoLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.pmbClrTwoLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.pmbClrTwoLabel:SetText(POINT_TWO_COLOR);
  self.pmbClrTwoLabel:SetMouseVisible(false);
  self.pmbClrTwoColor=Turbine.UI.Lotro.TextBox();
  self.pmbClrTwoColor:SetParent(self.pmbClrTwoHolder);
  self.pmbClrTwoColor:SetSize(35,25);
  self.pmbClrTwoColor:SetPosition(265,5);
  self.pmbClrTwoColor:SetReadOnly(true);
  self.pmbClrTwoColor:SetMouseVisible(false);
  self.pmbClrTwoCp=DU_81.DUInterface.ColorPicker();
  self.pmbClrTwoCp:SetParent(self.pmbClrTwoHolder);
  self.pmbClrTwoCp:SetPosition(305,5);
  self.pmbClrTwoCp.doActive=function()
    self:Activate();
  end
  self.pmbClrTwoOL=Turbine.UI.Control();
  self.pmbClrTwoOL:SetParent(self.pmbClrTwoColor);
  self.pmbClrTwoOL:SetSize(30,20);
  self.pmbClrTwoOL:SetZOrder(5);
  self.pmbClrTwoOL:SetPosition(2,2);
  self.pmbClrTwoOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.pmbClrTwoOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.pmbClrTwoHolder);
  self.pmbClrTwoCp.ColorChanged=function(sender,args)
    self.pmbClrTwoOL:SetBackColor(args.Color);
    self.pmbClrTwoChanged();
  end

  self.pmbCpTwoHolder = Turbine.UI.Control();
  self.pmbCpTwoHolder:SetSize(420,45);
  self.pmbCpTwoHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.pmbCpTwoHolder:SetMouseVisible(false);
  self.pmbCpTwoSld = DU_81.DUInterface.Slider();
  self.pmbCpTwoSld:SetStep(0.01);
  self.pmbCpTwoSld:SetParent(self.pmbCpTwoHolder);
  self.pmbCpTwoSld:SetLeft(57);
  self.pmbCpTwoSld:SetText(POINT_TWO_LOC);
  self.pmbCpTwoSld.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.pmbCpTwoHolder);


    self.ppbcHdr=Header();
  self.ppbcHdr.SetText(PLAYER_POWER);
  self.colPanel:AddItem(self.ppbcHdr);

  self.ppbBcHolder=Turbine.UI.Control();
  self.ppbBcHolder:SetSize(420,35);
  self.ppbBcHolder:SetMouseVisible(false);
  self.ppbBcLabel=Turbine.UI.Label();
  self.ppbBcLabel:SetParent(self.ppbBcHolder);
  self.ppbBcLabel:SetPosition(0,0);
  self.ppbBcLabel:SetSize(246,35);
  self.ppbBcLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.ppbBcLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.ppbBcLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.ppbBcLabel:SetText(BAR_COLOR);
  self.ppbBcLabel:SetMouseVisible(false);
  self.ppbBcColor=Turbine.UI.Lotro.TextBox();
  self.ppbBcColor:SetParent(self.ppbBcHolder);
  self.ppbBcColor:SetSize(35,25);
  self.ppbBcColor:SetPosition(265,5);
  self.ppbBcColor:SetReadOnly(true);
  self.ppbBcColor:SetMouseVisible(false);
  self.ppbBcCp=DU_81.DUInterface.ColorPicker();
  self.ppbBcCp:SetParent(self.ppbBcHolder);
  self.ppbBcCp:SetPosition(305,5);
  self.ppbBcCp.doActive=function()
    self:Activate();
  end
  self.ppbBcOL=Turbine.UI.Control();
  self.ppbBcOL:SetParent(self.ppbBcColor);
  self.ppbBcOL:SetSize(30,20);
  self.ppbBcOL:SetZOrder(5);
  self.ppbBcOL:SetPosition(2,2);
  self.ppbBcOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.ppbBcOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.ppbBcHolder);
  self.ppbBcCp.ColorChanged=function(sender,args)
    self.ppbBcOL:SetBackColor(args.Color);
    self.ppbBcChanged();
  end

  self.ppbTcHolder=Turbine.UI.Control();
  self.ppbTcHolder:SetSize(420,35);
  self.ppbTcHolder:SetMouseVisible(false);
  self.ppbTcLabel=Turbine.UI.Label();
  self.ppbTcLabel:SetParent(self.ppbTcHolder);
  self.ppbTcLabel:SetPosition(0,0);
  self.ppbTcLabel:SetSize(246,35);
  self.ppbTcLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.ppbTcLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.ppbTcLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.ppbTcLabel:SetText(TEXT_COLOR);
  self.ppbTcLabel:SetMouseVisible(false);
  self.ppbTcColor=Turbine.UI.Lotro.TextBox();
  self.ppbTcColor:SetParent(self.ppbTcHolder);
  self.ppbTcColor:SetSize(35,25);
  self.ppbTcColor:SetPosition(265,5);
  self.ppbTcColor:SetReadOnly(true);
  self.ppbTcColor:SetMouseVisible(false);
  self.ppbTcCp=DU_81.DUInterface.ColorPicker();
  self.ppbTcCp:SetParent(self.ppbTcHolder);
  self.ppbTcCp:SetPosition(305,5);
  self.ppbTcCp.doActive=function()
    self:Activate();
  end
  self.ppbTcOL=Turbine.UI.Control();
  self.ppbTcOL:SetParent(self.ppbTcColor);
  self.ppbTcOL:SetSize(30,20);
  self.ppbTcOL:SetZOrder(5);
  self.ppbTcOL:SetPosition(2,2);
  self.ppbTcOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.ppbTcOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.ppbTcHolder);
  self.ppbTcCp.ColorChanged=function(sender,args)
    self.ppbTcOL:SetBackColor(args.Color);
    self.ppbTcChanged();
  end

    self.ppbEceHolder=Turbine.UI.Control();
  self.ppbEceHolder:SetSize(420,30);
  self.ppbEceHolder:SetMouseVisible(false);
  self.ppbEce = DU_81.DUInterface.CheckBox(3);
  self.ppbEce:SetChecked(true);
  self.ppbEce:SetText(ENABLE_CE);
  self.ppbEce:SetParent(self.ppbEceHolder);
  self.ppbEce:SetLeft(55);
  self.ppbEce.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.ppbEceHolder);

  self.ppbEctHolder=Turbine.UI.Control();
  self.ppbEctHolder:SetSize(420,30);
  self.ppbEctHolder:SetMouseVisible(false);
  self.ppbEct = DU_81.DUInterface.CheckBox(3);
  self.ppbEct:SetChecked(true);
  self.ppbEct:SetText(ENABLE_CT);
  self.ppbEct:SetParent(self.ppbEctHolder);
  self.ppbEct:SetLeft(55);
  self.ppbEct.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.ppbEctHolder);

  self.ppbClrOneHolder=Turbine.UI.Control();
  self.ppbClrOneHolder:SetSize(420,35);
  self.ppbClrOneHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.ppbClrOneHolder:SetMouseVisible(false);
  self.ppbClrOneLabel=Turbine.UI.Label();
  self.ppbClrOneLabel:SetParent(self.ppbClrOneHolder);
  self.ppbClrOneLabel:SetPosition(0,0);
  self.ppbClrOneLabel:SetSize(246,35);
  self.ppbClrOneLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.ppbClrOneLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.ppbClrOneLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.ppbClrOneLabel:SetText(POINT_ONE_COLOR);
  self.ppbClrOneLabel:SetMouseVisible(false);
  self.ppbClrOneColor=Turbine.UI.Lotro.TextBox();
  self.ppbClrOneColor:SetParent(self.ppbClrOneHolder);
  self.ppbClrOneColor:SetSize(35,25);
  self.ppbClrOneColor:SetPosition(265,5);
  self.ppbClrOneColor:SetReadOnly(true);
  self.ppbClrOneColor:SetMouseVisible(false);
  self.ppbClrOneCp=DU_81.DUInterface.ColorPicker();
  self.ppbClrOneCp:SetParent(self.ppbClrOneHolder);
  self.ppbClrOneCp:SetPosition(305,5);
  self.ppbClrOneCp.doActive=function()
    self:Activate();
  end
  self.ppbClrOneOL=Turbine.UI.Control();
  self.ppbClrOneOL:SetParent(self.ppbClrOneColor);
  self.ppbClrOneOL:SetSize(30,20);
  self.ppbClrOneOL:SetZOrder(5);
  self.ppbClrOneOL:SetPosition(2,2);
  self.ppbClrOneOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.ppbClrOneOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.ppbClrOneHolder);
  self.ppbClrOneCp.ColorChanged=function(sender,args)
    self.ppbClrOneOL:SetBackColor(args.Color);
    self.ppbClrOneChanged();
  end

  self.ppbCpOneHolder = Turbine.UI.Control();
  self.ppbCpOneHolder:SetSize(420,45);
  self.ppbCpOneHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.ppbCpOneHolder:SetMouseVisible(false);
  self.ppbCpOneSld = DU_81.DUInterface.Slider();
  self.ppbCpOneSld:SetStep(0.01);
  self.ppbCpOneSld:SetParent(self.ppbCpOneHolder);
  self.ppbCpOneSld:SetLeft(57);
  self.ppbCpOneSld:SetText(POINT_ONE_LOC);
  self.ppbCpOneSld.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.ppbCpOneHolder);

  self.ppbEscHolder=Turbine.UI.Control();
  self.ppbEscHolder:SetSize(420,30);
  self.ppbEscHolder:SetMouseVisible(false);
  self.ppbEsc = DU_81.DUInterface.CheckBox(3);
  self.ppbEsc:SetChecked(true);
  self.ppbEsc:SetText(ENABLE_SECOND);
  self.ppbEsc:SetParent(self.ppbEscHolder);
  self.ppbEsc:SetLeft(55);
  self.ppbEsc.doActive=function()
    self:Activate()
  end
  self.colPanel:AddItem(self.ppbEscHolder);

  self.ppbClrTwoHolder=Turbine.UI.Control();
  self.ppbClrTwoHolder:SetSize(420,35);
  self.ppbClrTwoHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.ppbClrTwoHolder:SetMouseVisible(false);
  self.ppbClrTwoLabel=Turbine.UI.Label();
  self.ppbClrTwoLabel:SetParent(self.ppbClrTwoHolder);
  self.ppbClrTwoLabel:SetPosition(0,0);
  self.ppbClrTwoLabel:SetSize(246,35);
  self.ppbClrTwoLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.ppbClrTwoLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.ppbClrTwoLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.ppbClrTwoLabel:SetText(POINT_TWO_COLOR);
  self.ppbClrTwoLabel:SetMouseVisible(false);
  self.ppbClrTwoColor=Turbine.UI.Lotro.TextBox();
  self.ppbClrTwoColor:SetParent(self.ppbClrTwoHolder);
  self.ppbClrTwoColor:SetSize(35,25);
  self.ppbClrTwoColor:SetPosition(265,5);
  self.ppbClrTwoColor:SetReadOnly(true);
  self.ppbClrTwoColor:SetMouseVisible(false);
  self.ppbClrTwoCp=DU_81.DUInterface.ColorPicker();
  self.ppbClrTwoCp:SetParent(self.ppbClrTwoHolder);
  self.ppbClrTwoCp:SetPosition(305,5);
  self.ppbClrTwoCp.doActive=function()
    self:Activate();
  end
  self.ppbClrTwoOL=Turbine.UI.Control();
  self.ppbClrTwoOL:SetParent(self.ppbClrTwoColor);
  self.ppbClrTwoOL:SetSize(30,20);
  self.ppbClrTwoOL:SetZOrder(5);
  self.ppbClrTwoOL:SetPosition(2,2);
  self.ppbClrTwoOL:SetBackColor(Turbine.UI.Color(1,0,0));
  self.ppbClrTwoOL:SetMouseVisible(false);
  self.colPanel:AddItem(self.ppbClrTwoHolder);
  self.ppbClrTwoCp.ColorChanged=function(sender,args)
    self.ppbClrTwoOL:SetBackColor(args.Color);
    self.ppbClrTwoChanged();
  end

  self.ppbCpTwoHolder = Turbine.UI.Control();
  self.ppbCpTwoHolder:SetSize(420,45);
  self.ppbCpTwoHolder:SetBackColor(Turbine.UI.Color(0.01,0,0,0));
  self.ppbCpTwoHolder:SetMouseVisible(false);
  self.ppbCpTwoSld = DU_81.DUInterface.Slider();
  self.ppbCpTwoSld:SetStep(0.01);
  self.ppbCpTwoSld:SetParent(self.ppbCpTwoHolder);
  self.ppbCpTwoSld:SetLeft(57);
  self.ppbCpTwoSld:SetText(POINT_TWO_LOC);
  self.ppbCpTwoSld.doActive=function()
    self:Activate();
  end
  self.colPanel:AddItem(self.ppbCpTwoHolder);

  self.setupPanel=Turbine.UI.Window();
  self.setupPanel:SetVisible(true);
  self.setupPanel:SetParent(self);
  self.setupPanel:SetSize(140,507);
  self.setupPanel:SetBackground("DU_81/Palantir/Resources/Options_menu_side_bar.tga");
  self.setupPanel:SetPosition(self:GetWidth()-143,20);
  self.setupPanel:SetMouseVisible(false);
  self.alertHolder=Turbine.UI.Window();
  self.alertHolder:SetVisible(true);
  self.alertHolder:SetVisible(false);
  --self.alertHolder:SetMouseVisible(false);
  self.alertHolder:SetSize(Turbine.UI.Display:GetWidth(),Turbine.UI.Display:GetHeight());
  --self.alertHolder:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  self.alertHolder:SetZOrder(200);
  self.alert=DU_81.DUInterface.BaseWindow(512,172);
  self.alert:SetParent(self.alertHolder);
  self.alert:SetPosition((Turbine.UI.Display:GetWidth()-self.alert:GetWidth())/2,(Turbine.UI.Display:GetHeight()-self.alert:GetHeight())/2);
  self.alert:SetZOrder(99);
  self.alertText=Turbine.UI.Label();
  self.alertText:SetParent(self.alert);
  self.alertText:SetSize(512,20);
  self.alertText:SetForeColor(Turbine.UI.Color(210/255,255/255,0/255));
  self.alertText:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.alertText:SetFontStyle(Turbine.UI.FontStyle.Outline);
  self.alertText:SetOutlineColor(Turbine.UI.Color(0,0,0));
  self.alertText:SetFont(Turbine.UI.Lotro.Font.TrajanPro18);
  self.alertText:SetText(" "..ARE_YOU_SURE);
  self.alertText:SetPosition(0,54);
  self.alertYes=Turbine.UI.Lotro.Button();
  self.alertYes:SetParent(self.alert);
  self.alertYes:SetText(YES);
  self.alertYes:SetSize(106,20);
  self.alertYes:SetPosition(144,141);
  self.alertNo=Turbine.UI.Lotro.Button();
  self.alertNo:SetParent(self.alert);
  self.alertNo:SetText(NO);
  self.alertNo:SetSize(106,20);
  self.alertNo:SetPosition(263,141);
  self.nocPanel.MouseClick=function(sender,args)
    self.noc.Close();
    self.nocPanel:SetVisible(false);
  end


  self.noc.Opening =function()
    self.nocPanel:SetVisible(true);
    tl,tt=self.nocHolder:PointToScreen(self.nocHolder:GetPosition());
    self.nocLb:SetPosition(246,self.nocHolder:GetTop()+21);

  end

    self.acPanel.MouseClick=function(sender,args)
    self.ac.Close();
    self.acPanel:SetVisible(false);
  end


  self.ac.Opening =function()
    self.acPanel:SetVisible(true);
    tl,tt=self.acHolder:PointToScreen(self.acHolder:GetPosition());
    self.acLb:SetPosition(246,self.acHolder:GetTop()+21);

  end















  -- New panel for debuffs
  self.dbbpanel=Turbine.UI.Window();
  self.dbbpanel:SetVisible(true);
  self.dbbpanel:SetParent(self);
  self.dbbpanel:SetSize(575,534);
  self.dbbpanel:SetPosition(0,0);
  self.dbbpanel:SetMouseVisible(false);
  self.dbbpanel:SetVisible(false);

  self.sb4 = Turbine.UI.Lotro.ScrollBar();
  self.sb4:SetOrientation(Turbine.UI.Orientation.Vertical);
  self.sb4:SetParent(self.dbbpanel);
  self.sb4:SetPosition(422,50);
  self.sb4:SetSize(10,448);
  self.sb4:SetZOrder(10);
  self.sb4:SetBackColorBlendMode(0);
  self.sb4:SetBlendMode(0);

  self.sp4=Turbine.UI.ListBox();
  self.sp4:SetParent(self.dbbpanel);
  self.sp4:SetPosition(12,50);
  self.sp4:SetSize(420,448);
  self.sp4:SetVerticalScrollBar(self.sb4);
  self.sp4:SetOrientation( Turbine.UI.Orientation.Horizontal );
  self.sp4:SetMaxItemsPerLine(1);
  self.sp4:SetBackColorBlendMode(0);
  self.sp4:SetBlendMode(4);
  self.sp4.MouseClick=function(sender,args)
    self:Activate();
  end
  self.revMain4=Turbine.UI.Lotro.Button();
  self.revMain4:SetParent(self.dbbpanel);
  self.revMain4:SetSize(128,20);
  self.revMain4:SetText(REVERT_MAIN);
  self.revMain4:SetPosition(32,500);
  self.revMain4:SetEnabled(false);
  self.revMain4.MouseDown=function(sender,args)
    self:Activate();
  end
  self.defMain4=Turbine.UI.Lotro.Button();
  self.defMain4:SetParent(self.dbbpanel);
  self.defMain4:SetSize(128,20);
  self.defMain4:SetText(DEFAULT_MAIN);
  self.defMain4:SetPosition(163,500);
  self.defMain4.MouseDown=function(sender,args)
    self:Activate();
  end
  self.accMain4=Turbine.UI.Lotro.Button();
  self.accMain4:SetParent(self.dbbpanel);
  self.accMain4:SetSize(128,20);
  self.accMain4:SetText(ACCEPT);
  self.accMain4:SetPosition(294,500);
  self.accMain4.MouseDown=function(sender,args)
    self:Activate();
  end

  self.dbHdr=Header();
  self.dbHdr.SetText(DEBUFF_BAR);
  self.dbHdr:SetMouseVisible(false);
  self.sp4:AddItem(self.dbHdr);

  self.sdbHolder=Turbine.UI.Window();
  self.sdbHolder:SetVisible(true);
  self.sdbHolder:SetSize(420,30);
  self.sdbHolder:SetMouseVisible(false);
  self.sdb = DU_81.DUInterface.CheckBox(3);
  self.sdb:SetChecked(true);
  self.sdb:SetText(SHOW_DEBUFF);
  self.sdb:SetParent(self.sdbHolder);
  self.sdb:SetLeft(55);
  self.sdb.doActive=function()
    self:Activate();
  end
  self.sp4:AddItem(self.sdbHolder);

  self.sdb2Holder=Turbine.UI.Control();
  self.sdb2Holder:SetVisible(true);
  self.sdb2Holder:SetSize(420,30);
  self.sdb2Holder:SetMouseVisible(false);
  self.sdb2 = DU_81.DUInterface.CheckBox(3);
  self.sdb2:SetChecked(true);
  self.sdb2:SetText(DM_FLASH);
  self.sdb2:SetParent(self.sdb2Holder);
  self.sdb2:SetLeft(55);
  self.sdb2.doActive=function()
    self:Activate();
  end
  self.sp4:AddItem(self.sdb2Holder);

  self.debuffDisplayHolder=Turbine.UI.Control();
  self.debuffDisplayHolder:SetSize(420,30);
  self.debuffDisplayHolder:SetMouseVisible(false);
  self.debuffDisplay = DU_81.DUInterface.DropDownBox({DM_INDICATOR,DM_CURABLE,DM_INSTANT});
  self.debuffDisplay:SetParent(self.debuffDisplayHolder);
  self.debuffDisplay:SetPosition(246,0);
  self.debuffDisplay.doActive=function()
    self:Activate();
  end
  self.debuffDisplayPanel=Turbine.UI.Window();
  self.debuffDisplayPanel:SetVisible(true);
  self.debuffDisplayPanel:SetParent(self.sp4);
  self.debuffDisplayPanel:SetPosition(0,0);
  self.debuffDisplayPanel:SetSize(480,448);
  self.debuffDisplayPanel:SetZOrder(99);
  self.debuffDisplayPanel:SetVisible(false);
  self.debuffDisplayLb = self.debuffDisplay.listBox;
  self.debuffDisplayLb:SetParent(self.debuffDisplayPanel);
  self.debuffDisplayLb:SetZOrder(99);
  self.debuffDisplayLb:SetBlendMode(0);
  self.debuffDisplayLb:SetBackColorBlendMode(0);
  self.debuffDisplayLb:SetPosition(200,200);
  self.debuffDisplayLabel=Turbine.UI.Label();
  self.debuffDisplayLabel:SetParent(self.debuffDisplayHolder);
  self.debuffDisplayLabel:SetPosition(0,0);
  self.debuffDisplayLabel:SetSize(246,20);
  self.debuffDisplayLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.debuffDisplayLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.debuffDisplayLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.debuffDisplayLabel:SetText(DISPLAY_MODE);
  self.debuffDisplayLabel:SetMouseVisible(false);
  self.sp4:AddItem(self.debuffDisplayHolder);

  self.debuffDisplayPanel.MouseClick=function(sender,args)
    self.debuffDisplay.Close();
    self.debuffDisplayPanel:SetVisible(false);
  end


  self.debuffDisplay.Opening =function()
    self.debuffDisplayPanel:SetVisible(true);
    tl,tt=self.debuffDisplayHolder:PointToScreen(self.debuffDisplayHolder:GetPosition());
    self.debuffDisplayLb:SetPosition(246,self.debuffDisplayHolder:GetTop()+21);
  end

  self.dboHolder = Turbine.UI.Control();
  self.dboHolder:SetSize(420,45);
  self.dboHolder:SetMouseVisible(false);
  self.dboSld = DU_81.DUInterface.Slider();
  self.dboSld:SetParent(self.dboHolder);
  self.dboSld:SetLeft(57);
  self.dboSld:SetText(BAR_OPACITY);
  self.dboSld.doActive=function()
    self:Activate();
  end
  self.sp4:AddItem(self.dboHolder);

  self.dbiHolder = Turbine.UI.Control();
  self.dbiHolder:SetSize(420,45);
  self.dbiHolder:SetMouseVisible(false);
  self.dbiSld = DU_81.DUInterface.Slider();
  self.dbiSld:SetParent(self.dbiHolder);
  self.dbiSld:SetLeft(57);
  self.dbiSld:SetText(DEBUFF_INDICATOR);
  self.dbiSld.doActive=function()
    self:Activate();
  end
  self.sp4:AddItem(self.dbiHolder);

  self.dbpHolder = Turbine.UI.Control();
  self.dbpHolder:SetSize(420,45);
  self.dbpHolder:SetMouseVisible(false);
  self.dbpSld = DU_81.DUInterface.Slider();
  self.dbpSld:SetParent(self.dbpHolder);
  self.dbpSld:SetLeft(57);
  self.dbpSld:SetText(VERTICAL_POS);
  self.dbpSld:SetMinMax(0,1);
  self.dbpSld.doActive=function()
    self:Activate();
  end
  self.sp4:AddItem(self.dbpHolder);

  self.cdHdr=Header();
  self.cdHdr.SetText(COOLDOWNS);
  self.cdHdr:SetMouseVisible(false);
  self.sp4:AddItem(self.cdHdr);

  self.wndHolder=Turbine.UI.Control();
  self.wndHolder:SetSize(420,30);
  self.wndHolder:SetZOrder(40);
  self.wndHolder:SetMouseVisible(false);
  self.wnd = Turbine.UI.Lotro.TextBox();
  self.wnd:SetParent(self.wndHolder);
  self.wnd:SetPosition(316,0);
  self.wnd:SetSize(89,20);
  self.wnd:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.wnd:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.wnd:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.wnd:SetBlendMode(0);
  self.wnd:SetBackColorBlendMode(0);
  self.wnd:SetMultiline(false);
  self.wnd:SetWantsKeyEvents(true);

  self.wndLabel=Turbine.UI.Label();
  self.wndLabel:SetParent(self.wndHolder);
  self.wndLabel:SetPosition(0,0);
  self.wndLabel:SetSize(315,20);
  self.wndLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.wndLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.wndLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.wndLabel:SetText(WOUND_CD);
  self.wndLabel:SetMouseVisible(false);
  self.wnd:SetText("0");
  self.sp4:AddItem(self.wndHolder);

  self.frHolder=Turbine.UI.Control();
  self.frHolder:SetSize(420,30);
  self.frHolder:SetZOrder(40);
  self.frHolder:SetMouseVisible(false);
  self.fr = Turbine.UI.Lotro.TextBox();
  self.fr:SetParent(self.frHolder);
  self.fr:SetPosition(316,0);
  self.fr:SetSize(89,20);
  self.fr:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.fr:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.fr:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.fr:SetBlendMode(0);
  self.fr:SetBackColorBlendMode(0);
  self.fr:SetWantsKeyEvents(true);
  self.fr:SetMultiline(false);
  self.frLabel=Turbine.UI.Label();
  self.frLabel:SetParent(self.frHolder);
  self.frLabel:SetPosition(0,0);
  self.frLabel:SetSize(315,20);
  self.frLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.frLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.frLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.frLabel:SetText(FEAR_CD);
  self.frLabel:SetMouseVisible(false);
  self.fr:SetText("0");
  self.sp4:AddItem(self.frHolder);

  self.disHolder=Turbine.UI.Control();
  self.disHolder:SetSize(420,30);
  self.disHolder:SetZOrder(40);
  self.disHolder:SetMouseVisible(false);
  self.dis = Turbine.UI.Lotro.TextBox();
  self.dis:SetParent(self.disHolder);
  self.dis:SetPosition(316,0);
  self.dis:SetSize(89,20);
  self.dis:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.dis:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.dis:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.dis:SetBlendMode(0);
  self.dis:SetBackColorBlendMode(0);
  self.dis:SetWantsKeyEvents(true);
  self.dis:SetMultiline(false);
  self.disLabel=Turbine.UI.Label();
  self.disLabel:SetParent(self.disHolder);
  self.disLabel:SetPosition(0,0);
  self.disLabel:SetSize(315,20);
  self.disLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.disLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.disLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.disLabel:SetText(DISEASE_CD);
  self.disLabel:SetMouseVisible(false);
  self.dis:SetText("0");
  self.sp4:AddItem(self.disHolder);

  self.psnHolder=Turbine.UI.Control();
  self.psnHolder:SetSize(420,30);
  self.psnHolder:SetZOrder(40);
  self.psnHolder:SetMouseVisible(false);
  self.psn = Turbine.UI.Lotro.TextBox();
  self.psn:SetParent(self.psnHolder);
  self.psn:SetPosition(316,0);
  self.psn:SetSize(89,20);
  self.psn:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.psn:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.psn:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.psn:SetBlendMode(0);
  self.psn:SetBackColorBlendMode(0);
  self.psn:SetWantsKeyEvents(true);
  self.psn:SetMultiline(false);
  self.psnLabel=Turbine.UI.Label();
  self.psnLabel:SetParent(self.psnHolder);
  self.psnLabel:SetPosition(0,0);
  self.psnLabel:SetSize(315,20);
  self.psnLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.psnLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.psnLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.psnLabel:SetText(POISON_CD);
  self.psnLabel:SetMouseVisible(false);
  self.psn:SetText("0");
  self.sp4:AddItem(self.psnHolder);







  -- New panel for monitoring Effects
  self.mezpanel=Turbine.UI.Window();
  self.mezpanel:SetVisible(true);
  self.mezpanel:SetParent(self);
  self.mezpanel:SetSize(575,534);
  self.mezpanel:SetPosition(0,0);
  self.mezpanel:SetMouseVisible(false);
  self.mezpanel:SetVisible(false);

  self.sb5 = Turbine.UI.Lotro.ScrollBar();
  self.sb5:SetOrientation(Turbine.UI.Orientation.Vertical);
  self.sb5:SetParent(self.mezpanel);
  self.sb5:SetPosition(422,50);
  self.sb5:SetSize(10,448);
  self.sb5:SetZOrder(10);
  self.sb5:SetBackColorBlendMode(0);
  self.sb5:SetBlendMode(0);

  self.sp5=Turbine.UI.ListBox();
  self.sp5:SetParent(self.mezpanel);
  self.sp5:SetPosition(12,50);
  self.sp5:SetSize(420,448);
  self.sp5:SetVerticalScrollBar(self.sb5);
  self.sp5:SetOrientation( Turbine.UI.Orientation.Horizontal );
  self.sp5:SetMaxItemsPerLine(1);
  self.sp5:SetBackColorBlendMode(0);
  self.sp5:SetBlendMode(4);
  self.sp5.MouseClick=function(sender,args)
    self:Activate();
  end
  self.revMain5=Turbine.UI.Lotro.Button();
  self.revMain5:SetParent(self.mezpanel);
  self.revMain5:SetSize(128,20);
  self.revMain5:SetText(REVERT_MAIN);
  self.revMain5:SetPosition(32,500);
  self.revMain5:SetEnabled(false);
  self.revMain5.MouseDown=function(sender,args)
    self:Activate();
  end
  self.defMain5=Turbine.UI.Lotro.Button();
  self.defMain5:SetParent(self.mezpanel);
  self.defMain5:SetSize(128,20);
  self.defMain5:SetText(DEFAULT_MAIN);
  self.defMain5:SetPosition(163,500);
  self.defMain5.MouseDown=function(sender,args)
    self:Activate();
  end
  self.accMain5=Turbine.UI.Lotro.Button();
  self.accMain5:SetParent(self.mezpanel);
  self.accMain5:SetSize(128,20);
  self.accMain5:SetText(ACCEPT);
  self.accMain5:SetPosition(294,500);
  self.accMain5.MouseDown=function(sender,args)
    self:Activate();
  end

  self.mezHdr=Header();
  self.mezHdr.SetText(MEZ_MAIN_TITLE);
  self.mezHdr:SetMouseVisible(false);
  --self.mezHdr:SetParent(self.mezpanel);
  --self.mezHdrhdr:SetPosition(30,60);
  self.sp5:AddItem(self.mezHdr);

  self.mezCB1Holder=Turbine.UI.Control();
  self.mezCB1Holder:SetVisible(true);
  self.mezCB1Holder:SetSize(420,30);
  self.mezCB1Holder:SetMouseVisible(false);
  self.mezCB1 = DU_81.DUInterface.CheckBox(3);
  self.mezCB1:SetChecked(true);
  self.mezCB1:SetText(MEZ_ACTIVATE);
  self.mezCB1:SetParent(self.mezCB1Holder);
  self.mezCB1:SetLeft(55);
  self.mezCB1.doActive=function()
    self:Activate();
  end
  self.sp5:AddItem(self.mezCB1Holder);

  self.mezCB2Holder=Turbine.UI.Control();
  self.mezCB2Holder:SetVisible(true);
  self.mezCB2Holder:SetSize(420,30);
  self.mezCB2Holder:SetMouseVisible(false);
  self.mezCB2 = DU_81.DUInterface.CheckBox(3);
  self.mezCB2:SetChecked(true);
  self.mezCB2:SetText(MEZ_TIMER);
  self.mezCB2:SetParent(self.mezCB2Holder);
  self.mezCB2:SetLeft(55);
  self.mezCB2.doActive=function()
    self:Activate();
  end
  self.sp5:AddItem(self.mezCB2Holder);

  -- MezBarTimerTick
  self.tickHolder=Turbine.UI.Control();
  self.tickHolder:SetSize(420,30);
  self.tickHolder:SetZOrder(40);
  self.tickHolder:SetMouseVisible(false);
  self.tick = Turbine.UI.Lotro.TextBox();
  self.tick:SetParent(self.tickHolder);
  self.tick:SetPosition(316,0);
  self.tick:SetSize(89,20);
  self.tick:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.tick:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.tick:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.tick:SetBlendMode(0);
  self.tick:SetBackColorBlendMode(0);
  self.tick:SetWantsKeyEvents(true);
  self.tick:SetMultiline(false);
  self.tickLabel=Turbine.UI.Label();
  self.tickLabel:SetParent(self.tickHolder);
  self.tickLabel:SetPosition(0,0);
  self.tickLabel:SetSize(315,20);
  self.tickLabel:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.tickLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.tickLabel:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.tickLabel:SetText(MEZ_TIMER_TICK);--------------------
  self.tickLabel:SetMouseVisible(false);
  self.tick:SetText("60");
  self.sp5:AddItem(self.tickHolder);


  self.mezSld1Holder = Turbine.UI.Control();
  self.mezSld1Holder:SetSize(420,45);
  self.mezSld1Holder:SetMouseVisible(false);
  self.mezSld1 = DU_81.DUInterface.Slider();
  self.mezSld1:SetParent(self.mezSld1Holder);
  self.mezSld1:SetLeft(57);
  self.mezSld1:SetText(MEZ_OPACITY);
  self.mezSld1.doActive=function()
    self:Activate();
  end
  self.sp5:AddItem(self.mezSld1Holder);

  self.mezSld2Holder = Turbine.UI.Control();
  self.mezSld2Holder:SetSize(420,45);
  self.mezSld2Holder:SetMouseVisible(false);
  self.mezSld2 = DU_81.DUInterface.Slider();
  self.mezSld2:SetParent(self.mezSld2Holder);
  self.mezSld2:SetLeft(57);
  self.mezSld2:SetText(MEZ_VERTICAL);
  self.mezSld2.doActive=function()
    self:Activate();
  end
  self.sp5:AddItem(self.mezSld2Holder);

  self.mezHdr2=Header();
  self.mezHdr2.SetText(MEZ_LIST);
  self.mezHdr2:SetMouseVisible(false);
  self.sp5:AddItem(self.mezHdr2);

  -- mzb.AutoDetectDuration
  self.mezCB3Holder=Turbine.UI.Control();
  self.mezCB3Holder:SetVisible(true);
  self.mezCB3Holder:SetSize(420,30);
  self.mezCB3Holder:SetMouseVisible(false);
  self.mezCB3 = DU_81.DUInterface.CheckBox(3);
  self.mezCB3:SetChecked(true);
  self.mezCB3:SetText(MEZ_LIST_AUTO);
  self.mezCB3:SetParent(self.mezCB3Holder);
  self.mezCB3:SetLeft(55);
  self.mezCB3.doActive=function()
    self:Activate();
  end
  self.sp5:AddItem(self.mezCB3Holder);

  -- LM_1: MEZ_LM_MEZ="Blinding Flash";
  self.LM_1Holder=Turbine.UI.Control();
  self.LM_1Holder:SetSize(420,30);
  self.LM_1Holder:SetZOrder(40);
  self.LM_1Holder:SetMouseVisible(false);
  self.LM_1 = Turbine.UI.Lotro.TextBox();
  self.LM_1:SetParent(self.LM_1Holder);
  self.LM_1:SetPosition(316,0);
  self.LM_1:SetSize(89,20);
  self.LM_1:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.LM_1:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.LM_1:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.LM_1:SetBlendMode(0);
  self.LM_1:SetBackColorBlendMode(0);
  self.LM_1:SetWantsKeyEvents(true);
  self.LM_1:SetMultiline(false);
  self.LM_1Label=Turbine.UI.Label();
  self.LM_1Label:SetParent(self.LM_1Holder);
  self.LM_1Label:SetPosition(0,0);
  self.LM_1Label:SetSize(315,20);
  self.LM_1Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.LM_1Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.LM_1Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.LM_1Label:SetText(MEZ_LM_MEZ);
  self.LM_1Label:SetMouseVisible(false);
  self.LM_1:SetText("30");
  self.sp5:AddItem(self.LM_1Holder);

  -- LM_2: MEZ_LM_ROOT="Herb-lore"; Always 30"
  -- LM_3: MEZ_LM_DEBUFF="Sign of Power: Command";
  self.LM_3Holder=Turbine.UI.Control();
  self.LM_3Holder:SetSize(420,30);
  self.LM_3Holder:SetZOrder(40);
  self.LM_3Holder:SetMouseVisible(false);
  self.LM_3 = Turbine.UI.Lotro.TextBox();
  self.LM_3:SetParent(self.LM_3Holder);
  self.LM_3:SetPosition(316,0);
  self.LM_3:SetSize(89,20);
  self.LM_3:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.LM_3:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.LM_3:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.LM_3:SetBlendMode(0);
  self.LM_3:SetBackColorBlendMode(0);
  self.LM_3:SetWantsKeyEvents(true);
  self.LM_3:SetMultiline(false);
  self.LM_3Label=Turbine.UI.Label();
  self.LM_3Label:SetParent(self.LM_3Holder);
  self.LM_3Label:SetPosition(0,0);
  self.LM_3Label:SetSize(315,20);
  self.LM_3Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.LM_3Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.LM_3Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.LM_3Label:SetText(MEZ_LM_DEBUFF);
  self.LM_3Label:SetMouseVisible(false);
  self.LM_3:SetText("30");
  self.sp5:AddItem(self.LM_3Holder);

  -- LM_4: MEZ_LM_ANTISTUN="Sign of Power: Righteousness";
  self.LM_4Holder=Turbine.UI.Control();
  self.LM_4Holder:SetSize(420,30);
  self.LM_4Holder:SetZOrder(40);
  self.LM_4Holder:SetMouseVisible(false);
  self.LM_4 = Turbine.UI.Lotro.TextBox();
  self.LM_4:SetParent(self.LM_4Holder);
  self.LM_4:SetPosition(316,0);
  self.LM_4:SetSize(89,20);
  self.LM_4:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.LM_4:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.LM_4:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.LM_4:SetBlendMode(0);
  self.LM_4:SetBackColorBlendMode(0);
  self.LM_4:SetWantsKeyEvents(true);
  self.LM_4:SetMultiline(false);
  self.LM_4Label=Turbine.UI.Label();
  self.LM_4Label:SetParent(self.LM_4Holder);
  self.LM_4Label:SetPosition(0,0);
  self.LM_4Label:SetSize(315,20);
  self.LM_4Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.LM_4Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.LM_4Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.LM_4Label:SetText(MEZ_LM_ANTISTUN);
  self.LM_4Label:SetMouseVisible(false);
  self.LM_4:SetText("30");
  self.sp5:AddItem(self.LM_4Holder);


  -- MEZ_MIN_SOLI="Soliloquy of Spirit";
  self.MN_3Holder=Turbine.UI.Control();
  self.MN_3Holder:SetSize(420,30);
  self.MN_3Holder:SetZOrder(40);
  self.MN_3Holder:SetMouseVisible(false);
  self.MN_3 = Turbine.UI.Lotro.TextBox();
  self.MN_3:SetParent(self.MN_3Holder);
  self.MN_3:SetPosition(316,0);
  self.MN_3:SetSize(89,20);
  self.MN_3:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.MN_3:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.MN_3:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.MN_3:SetBlendMode(0);
  self.MN_3:SetBackColorBlendMode(0);
  self.MN_3:SetWantsKeyEvents(true);
  self.MN_3:SetMultiline(false);
  self.MN_3Label=Turbine.UI.Label();
  self.MN_3Label:SetParent(self.MN_3Holder);
  self.MN_3Label:SetPosition(0,0);
  self.MN_3Label:SetSize(315,20);
  self.MN_3Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.MN_3Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.MN_3Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.MN_3Label:SetText(MEZ_MIN_SOLI);
  self.MN_3Label:SetMouseVisible(false);
  self.MN_3:SetText("30");
  self.sp5:AddItem(self.MN_3Holder);


  -- MEZ_RK_ICE="All\195\169gorie du froid" -- Writ of Cold - Tier 1
  self.RK_1Holder=Turbine.UI.Control();
  self.RK_1Holder:SetSize(420,30);
  self.RK_1Holder:SetZOrder(40);
  self.RK_1Holder:SetMouseVisible(false);
  self.RK_1 = Turbine.UI.Lotro.TextBox();
  self.RK_1:SetParent(self.RK_1Holder);
  self.RK_1:SetPosition(316,0);
  self.RK_1:SetSize(89,20);
  self.RK_1:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.RK_1:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.RK_1:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.RK_1:SetBlendMode(0);
  self.RK_1:SetBackColorBlendMode(0);
  self.RK_1:SetWantsKeyEvents(true);
  self.RK_1:SetMultiline(false);
  self.RK_1Label=Turbine.UI.Label();
  self.RK_1Label:SetParent(self.RK_1Holder);
  self.RK_1Label:SetPosition(0,0);
  self.RK_1Label:SetSize(315,20);
  self.RK_1Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.RK_1Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.RK_1Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.RK_1Label:SetText(MEZ_RK_ICE);
  self.RK_1Label:SetMouseVisible(false);
  self.RK_1:SetText("20");
  self.sp5:AddItem(self.RK_1Holder);


  -- MEZ_RK_HEAL2="Pr\195\169lude à l'espoir" -- (Improved) Prelude to Hope
  self.RK_4Holder=Turbine.UI.Control();
  self.RK_4Holder:SetSize(420,30);
  self.RK_4Holder:SetZOrder(40);
  self.RK_4Holder:SetMouseVisible(false);
  self.RK_4 = Turbine.UI.Lotro.TextBox();
  self.RK_4:SetParent(self.RK_4Holder);
  self.RK_4:SetPosition(316,0);
  self.RK_4:SetSize(89,20);
  self.RK_4:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.RK_4:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.RK_4:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.RK_4:SetBlendMode(0);
  self.RK_4:SetBackColorBlendMode(0);
  self.RK_4:SetWantsKeyEvents(true);
  self.RK_4:SetMultiline(false);
  self.RK_4Label=Turbine.UI.Label();
  self.RK_4Label:SetParent(self.RK_4Holder);
  self.RK_4Label:SetPosition(0,0);
  self.RK_4Label:SetSize(315,20);
  self.RK_4Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.RK_4Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.RK_4Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.RK_4Label:SetText(MEZ_RK_HEAL2);
  self.RK_4Label:SetMouseVisible(false);
  self.RK_4:SetText("30");
  self.sp5:AddItem(self.RK_4Holder);


  -- MEZ_CA_MEZ="Enigme";
  self.CA_1Holder=Turbine.UI.Control();
  self.CA_1Holder:SetSize(420,30);
  self.CA_1Holder:SetZOrder(40);
  self.CA_1Holder:SetMouseVisible(false);
  self.CA_1 = Turbine.UI.Lotro.TextBox();
  self.CA_1:SetParent(self.CA_1Holder);
  self.CA_1:SetPosition(316,0);
  self.CA_1:SetSize(89,20);
  self.CA_1:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.CA_1:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.CA_1:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.CA_1:SetBlendMode(0);
  self.CA_1:SetBackColorBlendMode(0);
  self.CA_1:SetWantsKeyEvents(true);
  self.CA_1:SetMultiline(false);
  self.CA_1Label=Turbine.UI.Label();
  self.CA_1Label:SetParent(self.CA_1Holder);
  self.CA_1Label:SetPosition(0,0);
  self.CA_1Label:SetSize(315,20);
  self.CA_1Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.CA_1Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.CA_1Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.CA_1Label:SetText(MEZ_CA_MEZ);
  self.CA_1Label:SetMouseVisible(false);
  self.CA_1:SetText("30");
  self.sp5:AddItem(self.CA_1Holder);


  -- MEZ_CA_TRICK="Tours : Tous...";
  self.CA_3Holder=Turbine.UI.Control();
  self.CA_3Holder:SetSize(420,30);
  self.CA_3Holder:SetZOrder(40);
  self.CA_3Holder:SetMouseVisible(false);
  self.CA_3 = Turbine.UI.Lotro.TextBox();
  self.CA_3:SetParent(self.CA_3Holder);
  self.CA_3:SetPosition(316,0);
  self.CA_3:SetSize(89,20);
  self.CA_3:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.CA_3:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.CA_3:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.CA_3:SetBlendMode(0);
  self.CA_3:SetBackColorBlendMode(0);
  self.CA_3:SetWantsKeyEvents(true);
  self.CA_3:SetMultiline(false);
  self.CA_3Label=Turbine.UI.Label();
  self.CA_3Label:SetParent(self.CA_3Holder);
  self.CA_3Label:SetPosition(0,0);
  self.CA_3Label:SetSize(315,20);
  self.CA_3Label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.CA_3Label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.CA_3Label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.CA_3Label:SetText(MEZ_CA_TRICK);
  self.CA_3Label:SetMouseVisible(false);
  self.CA_3:SetText("30");
  self.sp5:AddItem(self.CA_3Holder);








  -- Buttons
  self.gen = ToggleButton();
  self.gen:SetParent(self.setupPanel);
  self.gen:SetPosition(7,100);
  self.gen.SetToggle(1);
  self.gen.SetText(MAIN);
  self.gen.doActive=function()
    self:Activate();
  end


  self.col = ToggleButton();
  self.col:SetParent(self.setupPanel);
  self.col:SetPosition(7,125);
  self.col.SetText(COLOR);
  self.col.doActive=function()
    self:Activate();
  end


  self.txt = ToggleButton();
  self.txt:SetParent(self.setupPanel);
  self.txt:SetPosition(7,150);
  self.txt.SetText(TEXT);
  self.txt.doActive=function()
    self:Activate();
  end

  self.dbbpanelbtn = ToggleButton();
  self.dbbpanelbtn:SetParent(self.setupPanel);
  self.dbbpanelbtn:SetPosition(7,175);
  self.dbbpanelbtn.SetText(DBB_BUTTON);
  self.dbbpanelbtn.doActive=function()
    self:Activate();
  end

  self.mezpanelbtn = ToggleButton();
  self.mezpanelbtn:SetParent(self.setupPanel);
  self.mezpanelbtn:SetPosition(7,200);
  self.mezpanelbtn.SetText(MEZ_BUTTON);
  self.mezpanelbtn.doActive=function()
    self:Activate();
  end

  self.gen.Click=function()
    self.tw:SetVisible(true);

    self.col.SetToggle(0);
    self.clw:SetVisible(false);
    self.txt.SetToggle(0);
    self.txw:SetVisible(false);
    self.dbbpanelbtn.SetToggle(0);
    self.dbbpanel:SetVisible(false);
    self.mezpanelbtn.SetToggle(0);
    self.mezpanel:SetVisible(false);
    self.potions_button:SetToggle(0)
    self.potions_panel:SetVisible(false)
  end

  self.col.Click=function()
    self.clw:SetVisible(true);

    self.gen.SetToggle(0);
    self.tw:SetVisible(false);
    self.txt.SetToggle(0);
    self.txw:SetVisible(false);
    self.dbbpanelbtn.SetToggle(0);
    self.dbbpanel:SetVisible(false);
    self.mezpanelbtn.SetToggle(0);
    self.mezpanel:SetVisible(false);
    self.potions_button:SetToggle(0)
    self.potions_panel:SetVisible(false)
  end

  self.txt.Click=function()
    self.txw:SetVisible(true);

		self.gen.SetToggle(0);
		self.tw:SetVisible(false);
		self.col.SetToggle(0);
		self.clw:SetVisible(false);
		self.dbbpanelbtn.SetToggle(0);
		self.dbbpanel:SetVisible(false);
		self.mezpanelbtn.SetToggle(0);
		self.mezpanel:SetVisible(false);
    self.potions_button:SetToggle(0)
    self.potions_panel:SetVisible(false)
	end

	self.dbbpanelbtn.Click=function()
		self.dbbpanel:SetVisible(true);

		self.txt.SetToggle(0);
		self.txw:SetVisible(false);
		self.gen.SetToggle(0);
		self.tw:SetVisible(false);
		self.col.SetToggle(0);
		self.clw:SetVisible(false);
		self.mezpanelbtn.SetToggle(0);
		self.mezpanel:SetVisible(false);
    self.potions_button:SetToggle(0)
    self.potions_panel:SetVisible(false)
	end

	self.mezpanelbtn.Click=function()
		self.mezpanel:SetVisible(true);

		self.txt.SetToggle(0);
		self.txw:SetVisible(false);
		self.gen.SetToggle(0);
		self.tw:SetVisible(false);
		self.col.SetToggle(0);
		self.clw:SetVisible(false);
		self.dbbpanelbtn.SetToggle(0);
		self.dbbpanel:SetVisible(false);
    self.potions_button:SetToggle(0)
    self.potions_panel:SetVisible(false)
	end

	self.showSetup = function()

	end

	self.closeSetup = function()

	end


  -- Potions
  -- Button
  self.potions_button = ToggleButton();
  self.potions_button:SetParent(self.setupPanel);
  self.potions_button:SetPosition(7,225);
  self.potions_button.SetText(POTIONS_BUTTON);
  self.potions_button.doActive=function()
    self:Activate();
  end

	self.potions_button.Click=function()
		self.potions_panel:SetVisible(true);

		self.txt.SetToggle(0);
		self.txw:SetVisible(false);
		self.gen.SetToggle(0);
		self.tw:SetVisible(false);
		self.col.SetToggle(0);
		self.clw:SetVisible(false);
		self.dbbpanelbtn.SetToggle(0);
		self.dbbpanel:SetVisible(false);
		self.mezpanelbtn.SetToggle(0);
		self.mezpanel:SetVisible(false);
	end

  -- Panel
  self.potions_panel = Turbine.UI.Window();
  self.potions_panel:SetVisible(true);
  self.potions_panel:SetParent(self);
  self.potions_panel:SetSize(575,534);
  self.potions_panel:SetPosition(0,0);
  self.potions_panel:SetMouseVisible(false);
  self.potions_panel:SetVisible(false);

  -- Activate potions

  -- Default button
  self.potions_default = Turbine.UI.Lotro.Button();
  self.potions_default:SetParent(self.potions_panel);
  self.potions_default:SetSize(128,20);
  self.potions_default:SetText(DEFAULT_MAIN);
  self.potions_default:SetPosition(163,500);
  self.potions_default.MouseDown = function(sender,args)
    self:Activate();
  end

  -- Accept button
  self.potions_accept = Turbine.UI.Lotro.Button();
  self.potions_accept:SetParent(self.potions_panel);
  self.potions_accept:SetSize(128,20);
  self.potions_accept:SetText(ACCEPT);
  self.potions_accept:SetPosition(294,500);
  self.potions_accept.MouseDown = function(sender,args)
    self:Activate();
  end

  -- ScrollBar
  self.potions_scroll_bar = Turbine.UI.Lotro.ScrollBar();
  self.potions_scroll_bar:SetOrientation(Turbine.UI.Orientation.Vertical);
  self.potions_scroll_bar:SetParent(self.potions_panel);
  self.potions_scroll_bar:SetPosition(422,50);
  self.potions_scroll_bar:SetSize(10,448);
  self.potions_scroll_bar:SetZOrder(10);
  self.potions_scroll_bar:SetBackColorBlendMode(0);
  self.potions_scroll_bar:SetBlendMode(0);

  -- List box
  self.potions_list_box = Turbine.UI.ListBox();
  self.potions_list_box:SetParent(self.potions_panel);
  self.potions_list_box:SetPosition(12,50);
  self.potions_list_box:SetSize(420,448);
  self.potions_list_box:SetVerticalScrollBar(self.potions_scroll_bar);
  self.potions_list_box:SetOrientation( Turbine.UI.Orientation.Horizontal );
  self.potions_list_box:SetMaxItemsPerLine(1);
  self.potions_list_box:SetBackColorBlendMode(0);
  self.potions_list_box:SetBlendMode(4);
  self.potions_list_box.MouseClick=function(sender,args)
    self:Activate();
  end

  -- Header
  self.potions_header = Header();
  self.potions_header.SetText(POTIONS_MAIN_HEADER);
  self.potions_header:SetMouseVisible(false);
  self.potions_list_box:AddItem(self.potions_header);

  -- Enable/disable potions
  self.potions_enable_holder = Turbine.UI.Control();
  self.potions_enable_holder:SetVisible(true);
  self.potions_enable_holder:SetSize(420,30);
  self.potions_enable_holder:SetMouseVisible(false);
  self.potions_enable = DU_81.DUInterface.CheckBox(3);
  self.potions_enable:SetChecked(true);
  self.potions_enable:SetText(POTIONS_ENABLED);
  self.potions_enable:SetParent(self.potions_enable_holder);
  self.potions_enable:SetLeft(55);
  self.potions_enable.doActive = function()
    self:Activate();
  end
  self.potions_list_box:AddItem(self.potions_enable_holder);

  -- Automatic drinking
  self.potions_automatic_holder = Turbine.UI.Control();
  self.potions_automatic_holder:SetSize(420,30);
  self.potions_automatic_holder:SetMouseVisible(false);
  self.potions_automatic = DU_81.DUInterface.DropDownBox(
    {POTIONS_AUTOMATIC_ALWAYS, POTIONS_AUTOMATIC_LOW});
  self.potions_automatic:SetParent(self.potions_automatic_holder);
  self.potions_automatic:SetPosition(246,0);
  self.potions_automatic.doActive = function()
    self:Activate();
  end
  self.potions_automatic_panel = Turbine.UI.Window();
  self.potions_automatic_panel:SetVisible(true);
  self.potions_automatic_panel:SetParent(self.potions_list_box);
  self.potions_automatic_panel:SetPosition(0,0);
  self.potions_automatic_panel:SetSize(480,448);
  self.potions_automatic_panel:SetZOrder(99);
  self.potions_automatic_panel:SetVisible(false);
  self.potions_automatic_list_box = self.potions_automatic.listBox;
  self.potions_automatic_list_box:SetParent(self.potions_automatic_panel);
  self.potions_automatic_list_box:SetZOrder(99);
  self.potions_automatic_list_box:SetBlendMode(0);
  self.potions_automatic_list_box:SetBackColorBlendMode(0);
  self.potions_automatic_list_box:SetPosition(200,200);
  self.potions_automatic_panel_label = Turbine.UI.Label();
  self.potions_automatic_panel_label:SetParent(self.potions_automatic_holder);
  self.potions_automatic_panel_label:SetPosition(0,0);
  self.potions_automatic_panel_label:SetSize(246,20);
  self.potions_automatic_panel_label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_automatic_panel_label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_automatic_panel_label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_automatic_panel_label:SetText(POTIONS_MODE);
  self.potions_automatic_panel_label:SetMouseVisible(false);
  self.potions_list_box:AddItem(self.potions_automatic_holder);

  self.potions_automatic_panel.MouseClick = function(sender,args)
    self.potions_automatic.Close();
    self.potions_automatic_panel:SetVisible(false);
  end


  self.potions_automatic.Opening =function()
    self.potions_automatic_panel:SetVisible(true);
    tl, tt = self.potions_automatic_holder:PointToScreen(
      self.potions_automatic_holder:GetPosition());
    self.potions_automatic_list_box:SetPosition(
      246, self.potions_automatic_holder:GetTop()+21);
  end

  -- Potions cooldown
  self.potions_cooldown_header = Header();
  self.potions_cooldown_header.SetText(POTIONS_COOLDOWN_HEADER);
  self.potions_cooldown_header:SetMouseVisible(false);
  self.potions_list_box:AddItem(self.potions_cooldown_header);

  -- Morale
  self.potions_morale_cooldown_holder = Turbine.UI.Control();
  self.potions_morale_cooldown_holder:SetSize(420,30);
  self.potions_morale_cooldown_holder:SetZOrder(40);
  self.potions_morale_cooldown_holder:SetMouseVisible(false);
  self.potions_morale_cooldown = Turbine.UI.Lotro.TextBox();
  self.potions_morale_cooldown:SetParent(self.potions_morale_cooldown_holder);
  self.potions_morale_cooldown:SetPosition(316,0);
  self.potions_morale_cooldown:SetSize(89,20);
  self.potions_morale_cooldown:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_morale_cooldown:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_morale_cooldown:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_morale_cooldown:SetBlendMode(0);
  self.potions_morale_cooldown:SetBackColorBlendMode(0);
  self.potions_morale_cooldown:SetMultiline(false);
  self.potions_morale_cooldown:SetWantsKeyEvents(true);

  self.potions_morale_cooldown_label = Turbine.UI.Label();
  self.potions_morale_cooldown_label:SetParent(self.potions_morale_cooldown_holder);
  self.potions_morale_cooldown_label:SetPosition(0,0);
  self.potions_morale_cooldown_label:SetSize(315,20);
  self.potions_morale_cooldown_label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_morale_cooldown_label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_morale_cooldown_label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_morale_cooldown_label:SetText(POTIONS_MORALE_COOLDOWN);
  self.potions_morale_cooldown_label:SetMouseVisible(false);
  self.potions_morale_cooldown:SetText("0");
  self.potions_list_box:AddItem(self.potions_morale_cooldown_holder);

  -- Power
  self.potions_power_cooldown_holder = Turbine.UI.Control();
  self.potions_power_cooldown_holder:SetSize(420,30);
  self.potions_power_cooldown_holder:SetZOrder(40);
  self.potions_power_cooldown_holder:SetMouseVisible(false);
  self.potions_power_cooldown = Turbine.UI.Lotro.TextBox();
  self.potions_power_cooldown:SetParent(self.potions_power_cooldown_holder);
  self.potions_power_cooldown:SetPosition(316,0);
  self.potions_power_cooldown:SetSize(89,20);
  self.potions_power_cooldown:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_power_cooldown:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_power_cooldown:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_power_cooldown:SetBlendMode(0);
  self.potions_power_cooldown:SetBackColorBlendMode(0);
  self.potions_power_cooldown:SetMultiline(false);
  self.potions_power_cooldown:SetWantsKeyEvents(true);

  self.potions_power_cooldown_label = Turbine.UI.Label();
  self.potions_power_cooldown_label:SetParent(self.potions_power_cooldown_holder);
  self.potions_power_cooldown_label:SetPosition(0,0);
  self.potions_power_cooldown_label:SetSize(315,20);
  self.potions_power_cooldown_label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_power_cooldown_label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_power_cooldown_label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_power_cooldown_label:SetText(POTIONS_POWER_COOLDOWN);
  self.potions_power_cooldown_label:SetMouseVisible(false);
  self.potions_power_cooldown:SetText("0");
  self.potions_list_box:AddItem(self.potions_power_cooldown_holder);

  -- Thresholds
  self.potions_threshold_header = Header();
  self.potions_threshold_header.SetText(POTIONS_THRESHOLD_HEADER);
  self.potions_threshold_header:SetMouseVisible(false);
  self.potions_list_box:AddItem(self.potions_threshold_header);

  -- Morale
  self.potions_morale_threshold_holder = Turbine.UI.Control();
  self.potions_morale_threshold_holder:SetSize(420,30);
  self.potions_morale_threshold_holder:SetZOrder(40);
  self.potions_morale_threshold_holder:SetMouseVisible(false);
  self.potions_morale_threshold = Turbine.UI.Lotro.TextBox();
  self.potions_morale_threshold:SetParent(self.potions_morale_threshold_holder);
  self.potions_morale_threshold:SetPosition(316,0);
  self.potions_morale_threshold:SetSize(89,20);
  self.potions_morale_threshold:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_morale_threshold:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_morale_threshold:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_morale_threshold:SetBlendMode(0);
  self.potions_morale_threshold:SetBackColorBlendMode(0);
  self.potions_morale_threshold:SetMultiline(false);
  self.potions_morale_threshold:SetWantsKeyEvents(true);

  self.potions_morale_threshold_label = Turbine.UI.Label();
  self.potions_morale_threshold_label:SetParent(self.potions_morale_threshold_holder);
  self.potions_morale_threshold_label:SetPosition(0,0);
  self.potions_morale_threshold_label:SetSize(315,20);
  self.potions_morale_threshold_label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_morale_threshold_label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_morale_threshold_label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_morale_threshold_label:SetText(POTIONS_MORALE_THRESHOLD);
  self.potions_morale_threshold_label:SetMouseVisible(false);
  self.potions_morale_threshold:SetText("0");
  self.potions_list_box:AddItem(self.potions_morale_threshold_holder);

  -- Power
  self.potions_power_threshold_holder = Turbine.UI.Control();
  self.potions_power_threshold_holder:SetSize(420,30);
  self.potions_power_threshold_holder:SetZOrder(40);
  self.potions_power_threshold_holder:SetMouseVisible(false);
  self.potions_power_threshold = Turbine.UI.Lotro.TextBox();
  self.potions_power_threshold:SetParent(self.potions_power_threshold_holder);
  self.potions_power_threshold:SetPosition(316,0);
  self.potions_power_threshold:SetSize(89,20);
  self.potions_power_threshold:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_power_threshold:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_power_threshold:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_power_threshold:SetBlendMode(0);
  self.potions_power_threshold:SetBackColorBlendMode(0);
  self.potions_power_threshold:SetMultiline(false);
  self.potions_power_threshold:SetWantsKeyEvents(true);

  self.potions_power_threshold_label = Turbine.UI.Label();
  self.potions_power_threshold_label:SetParent(self.potions_power_threshold_holder);
  self.potions_power_threshold_label:SetPosition(0,0);
  self.potions_power_threshold_label:SetSize(315,20);
  self.potions_power_threshold_label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
  self.potions_power_threshold_label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
  self.potions_power_threshold_label:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
  self.potions_power_threshold_label:SetText(POTIONS_POWER_THRESHOLD);
  self.potions_power_threshold_label:SetMouseVisible(false);
  self.potions_power_threshold:SetText("0");
  self.potions_list_box:AddItem(self.potions_power_threshold_holder);

end
