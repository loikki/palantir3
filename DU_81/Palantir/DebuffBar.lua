import "DU_81.Palantir";
import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
--import "DU_81.Utilsx.Class";

DebuffBar = class( Turbine.UI.Window );

function DebuffBar:Constructor()
  Turbine.UI.Window.Constructor( self );
    self:SetVisible(true);
    self:SetMouseVisible(false);
    bTestInProgress = false;

    function self:SetDisplayMode2(b)
      if (b) then
        mhFlash = 1;
      else
        mhFlash = 0;
      end
    end

    function self:SetDisplayMode(num)
      if(num==1)then -- Display only debuff info
        self.displayMode=1;
        self.bw:SetVisible(false);
        self.cdHolder:SetVisible(false);
        self.bbg:SetVisible(false);
        self.wChar1:SetVisible(false);
        self.wChar2:SetVisible(false);
        self.wInf:SetVisible(false);
        self.fChar1:SetVisible(false);
        self.fChar2:SetVisible(false);
        self.fInf:SetVisible(false);
        self.dChar1:SetVisible(false);
        self.dChar2:SetVisible(false);
        self.dInf:SetVisible(false);
        self.pChar1:SetVisible(false);
        self.pChar2:SetVisible(false);
        self.pInf:SetVisible(false);
        self.qsWindow:SetVisible(false);
        mhButtonAvoid = 3;
      else    -- Display full Debuff bar
        self.displayMode=2;
        self.bw:SetVisible(true);
        self.bw:SetMouseVisible(false);
        self.cdHolder:SetVisible(true);
        self.cdHolder:SetMouseVisible(false);
        self.bbg:SetVisible(true);
        self.qsWindow:SetVisible(true);
        self:SetOpacity(1);
        self.updateCounts();
        mhButtonAvoid = 3;
      end
      if(num==3)then -- Display only debuff info and instant curing
        self.displayMode = 3;
        mhButtonAvoid = 0;
      end
    end

    self.ShowInd = function(bool)
      if(bool==true)then
        self.wLit:SetVisible(true);
        self.fLit:SetVisible(true);
        self.dLit:SetVisible(true);
        self.pLit:SetVisible(true);
      else
        self.wLit:SetVisible(false);
        self.fLit:SetVisible(false);
        self.dLit:SetVisible(false);
        self.pLit:SetVisible(false);
      end
    end

    self.GetIndicatorCount=function()
      local val=0;
      if (self.wLit:IsVisible()==false and self.fLit:IsVisible()==false and self.dLit:IsVisible()==false and self.pLit:IsVisible()==false)then
        val=0;
      else
        val=1;
      end
      return val;
    end

    self.SetBarOpacity = function(num)
      self.dbb:SetOpacity(num);
      self.bbg:SetOpacity(num);
    end

    self.SetIndOpacity = function(num)
      self.wLit:SetOpacity(num);
      self.fLit:SetOpacity(num);
      self.dLit:SetOpacity(num);
      self.pLit:SetOpacity(num);
      self.cdHolder:SetOpacity(num);
    end

    self.showQuickslots = function() -- 4 placeholders to choose Pot or skill
      self.qsWindow:SetPosition(-6,90);
      self.qsWindow:SetHeight(48);
      self.wqs:SetSize(36,36);
      self.wqs:SetPosition(0,0);
      self.fqs:SetSize(36,36);
      self.fqs:SetPosition(36,0);
      self.dqs:SetSize(36,36);
      self.dqs:SetPosition(72,0);
      self.pqs:SetSize(36,36);
      self.pqs:SetPosition(108,0);
      self.qsWindow:SetOpacity(1);
      self.wqs:SetVisible(true);
      self.fqs:SetVisible(true);
      self.dqs:SetVisible(true);
      self.pqs:SetVisible(true);
      -- reset buttons
      self.wbtn:SetVisible(true);
      self.wbtn:SetMouseVisible(true);
      self.fbtn:SetVisible(true);
      self.fbtn:SetMouseVisible(true);
      self.dbtn:SetVisible(true);
      self.dbtn:SetMouseVisible(true);
      self.pbtn:SetVisible(true);
      self.pbtn:SetMouseVisible(true);
    end

    self.hideQuickslots = function() -- Move them where the debuffs are shown.
      self.qsWindow:SetPosition(10,5);
      self.qsWindow:SetHeight(40);
      self.wqs:SetPosition(3,0);
      self.wqs:SetSize(28,25);
      self.fqs:SetPosition(29,10);
      self.fqs:SetSize(28,22);
      self.dqs:SetPosition(55,10);
      self.dqs:SetSize(28,22);
      self.pqs:SetPosition(81,0);
      self.pqs:SetSize(28,25);
      self.qsWindow:SetOpacity(.01);
      -- Quikslots will be enabled / disabled when checking for Diseased/Feared/poisoned/wounded time
      --self.wqs:SetVisible(false);
      --self.fqs:SetVisible(false);
      --self.dqs:SetVisible(false);
      --self.pqs:SetVisible(false);
      -- reset buttons
      self.wbtn:SetVisible(false);
      self.wbtn:SetMouseVisible(false);
      self.fbtn:SetVisible(false);
      self.fbtn:SetMouseVisible(false);
      self.dbtn:SetVisible(false);
      self.dbtn:SetMouseVisible(false);
      self.pbtn:SetVisible(false);
      self.pbtn:SetMouseVisible(false);

    end

    self.Refresh = function()
      wsc = self.wqs:GetShortcut();
      fsc = self.fqs:GetShortcut();
      dsc = self.dqs:GetShortcut();
      psc = self.pqs:GetShortcut();

      if (wsc:GetType() == 0)then-- or self.dm==1)then
        self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_mask2.tga");
        self.wChar1:SetVisible(false);
        self.wChar2:SetVisible(false);
        self.wInf:SetVisible(false);
        self.wBlock=true;
        self.wCount = 0;
      elseif (wsc:GetType()==2)then
        if(self.wCount > 0 and self.wCD ~= 0)then
          if(self.wTimerOn==false)then
            self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_green2.tga");
          else
            self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_red2.tga");
          end
          self.wBlock=false;
        else
          self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_mask2.tga");
          self.wBlock=true;
        end
      else
          if(self.wTimerOn==false)then
            self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_green2.tga");
          else
            self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_red2.tga");
          end
          self.wBlock=false;
      end

      if (fsc:GetType() == 0  or self.dm==1)then
        self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_mask2.tga");
        self.fChar1:SetVisible(false);
        self.fChar2:SetVisible(false);
        self.fInf:SetVisible(false);
        self.fBlock=true;
        self.fCount=0;
        self.fCount = 0;--tonumber(self.qslot.sTwoQty);
        --self.fqs:SetShortcut(nil);
        self.fqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
      elseif (fsc:GetType()==2)then
        if(self.fCount > 0 and self.fCD ~= 0)then
          if(self.fTimerOn==false)then
            self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_green2.tga");
          else
            self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_red2.tga");
          end
          self.fBlock=false;
        else
          self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_mask2.tga");
          self.fBlock=true;
        end
      else
          if(self.fTimerOn==false)then
            self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_green2.tga");
          else
            self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_red2.tga");
          end
          self.fBlock=false;
      end

      if (dsc:GetType() == 0 or self.dm==1)then
        self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_mask2.tga");
        self.dChar1:SetVisible(false);
        self.dChar2:SetVisible(false);
        self.dInf:SetVisible(false);
        self.dBlock=true;
        self.dCount=0;
        self.dCount = 0;--tonumber(self.qslot.sThreeQty);
        --self.dqs:SetShortcut(nil);
        self.dqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
      elseif (dsc:GetType()==2)then
        if(self.dCount > 0 and self.dCD ~= 0)then
          if(self.dTimerOn==false)then
            self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_green2.tga");
          else
            self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_red2.tga");
          end
          self.dBlock=false;
        else
          self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_mask2.tga");
          self.dBlock=true;
        end
      else
          if(self.dTimerOn==false)then
            self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_green2.tga");
          else
            self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_red2.tga");
          end
          self.dBlock=false;
      end

      if (psc:GetType() == 0 or self.dm==1)then
        self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_mask2.tga");
        self.pChar1:SetVisible(false);
        self.pChar2:SetVisible(false);
        self.pInf:SetVisible(false);
        self.pBlock=true;
        self.pCount=0;
        self.pCount = tonumber(self.qslot.sFourQty);
        --self.pqs:SetShortcut(nil);
        self.pqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
      elseif (psc:GetType()==2)then
        if(self.pCount > 0 and self.pCD ~= 0)then
          if(self.pTimerOn==false)then
            self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_green2.tga");
          else
            self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_red2.tga");
          end
          self.pBlock=false;
        else
          self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_mask2.tga");
          self.pBlock=true;
        end
      else
          if(self.pTimerOn==false)then
            self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_green2.tga");
          else
            self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_red2.tga");
          end
          self.pBlock=false;
      end
    end


    self.SetNum = function(chra,chrb,slot,num)
      if (self.displayMode == 1 ) then
                                chra:SetVisible(false);
                                chrb:SetVisible(false);
      elseif (num < 10)then
        chra:SetVisible(true);
          chra:SetBackground("DU_81/Palantir/Resources/DBB/Slot"..slot.."/"..string.sub(num,1)..".tga");
        chrb:SetVisible(false);
      else
        chra:SetVisible(true);
        chrb:SetVisible(true);
        chra:SetBackground("DU_81/Palantir/Resources/DBB/Slot"..slot.."/"..string.sub(num,1,1)..".tga");
        chrb:SetBackground("DU_81/Palantir/Resources/DBB/Slot"..slot.."/"..string.sub(num,2)..".tga");
      end
    end


self.getEffects = function()
  el=self.player:GetEffects();
  local wCountx=0;
  local fCountx=0;
  local dCountx=0;
  local pCountx=0;
  if(el:GetCount() > 0) then
    for x=1,(el:GetCount()) do
      ef = el:Get(x);
      local elName=ef:GetName() -- Don't fast-clean Silenced ( eg saroumane )
      if (ef:IsDebuff()==true)then
        if(ef:GetCategory() == Turbine.Gameplay.EffectCategory.Wound and ef:IsCurable()==true)then
          wCountx=wCountx+1;
        elseif (ef:GetCategory() == Turbine.Gameplay.EffectCategory.Fear and ef:IsCurable()==true)then
          if (elName ~= "Silenced" and elName ~= "R\195\169duit au silence")then
            fCountx=fCountx+1;
          end
        elseif (ef:GetCategory() == Turbine.Gameplay.EffectCategory.Disease and ef:IsCurable()==true)then
          dCountx=dCountx+1;
        elseif (ef:GetCategory() == Turbine.Gameplay.EffectCategory.Poison and ef:IsCurable()==true)then
          pCountx=pCountx+1;
        end
      end
    end
  end

  self.indicatorCount=wCountx+fCountx+dCountx+pCountx;
  if (self.indicatorCount == 0)then
    -- no debuff
    x = 5; y= -40;
    self.mhButton:SetPosition(x,y);
    self.movingHolder:SetBackColor(Turbine.UI.Color(0, 0, 0, 0));
    self.movingHolder:SetVisible(false);
    self.movingHolder:SetMouseVisible(false);
    self.mhButton:SetShortcut(nil);
    self.mhButton:SetVisible(false);
    if (self.displayMode == 3 ) then
      if (mhButtonAvoid ~= 0)then
        mhButtonAvoid = 0;
        --Turbine.Shell.WriteLine("Palantir mhButtonAvoid => 0 nomore debuff");
      end
    end;
  else
    if (mhButtonAvoid == 1) then                    --1: Potion avoided, remove everythings, will reset when everything has ended
      x = 5; y= -40;
      self.mhButton:SetPosition(x,y);
      self.movingHolder:SetBackColor(Turbine.UI.Color(0, 0, 0, 0));
      self.movingHolder:SetVisible(false);
      self.movingHolder:SetMouseVisible(false);
      self.mhButton:SetShortcut(nil);
      self.mhButton:SetVisible(false);
    elseif (mhButtonAvoid == 2) then
      --2: Potion drunk, remove everythings, resets at end of CD
      x = 5; y= -40;
      self.mhButton:SetPosition(x,y);
      self.movingHolder:SetBackColor(Turbine.UI.Color(0, 0, 0, 0));
      self.movingHolder:SetVisible(false);
      self.movingHolder:SetMouseVisible(false);
      self.mhButton:SetShortcut(nil);
      self.mhButton:SetVisible(false);
    elseif (mhButtonAvoid == 3) then                --3: Instant potion option is not activated.
      --
    elseif (mhButtonAvoid == 5) then                --5: temporary setting waiting for CDs to finish
      if(self.wTimerOn==false and self.fTimerOn==false and self.dTimerOn==false and self.pTimerOn==false) then
        mhButtonAvoid = 0;
        --Turbine.Shell.WriteLine("Palantir mhButtonAvoid 5 => 0 nomore CD");
      end
    elseif (mhButtonAvoid == 0) then                -- 0: Normal prossessing 1 or more cleanable debuff present
      x,y = self.movingHolder:GetMousePosition();
      x = x-20; y= y-25;
    end
  end

  if (wCountx > 0)then
    self.wLit:SetVisible(true);
    self.wqs:SetVisible(true); -- Needed to enable the shortcut
    if (mhFlash == 1) then
      self.movingHolder:SetBackColor(Turbine.UI.Color(0.2, 1, 0.1, 0.01));
      self.movingHolder:SetVisible(true);
      self.movingHolder:SetMouseVisible(false);
    end
    if (mhButtonAvoid == 0 and self.wTimerOn==false and self.wqs:GetShortcut():GetData() ~= "")then
      self.mhButton:SetPosition(x,y);
      self.movingHolder:SetVisible(true);
      self.movingHolder:SetMouseVisible(false);
      mhButtonQST = self.wqs:GetShortcut():GetType();
      mhButtonQSD = self.wqs:GetShortcut():GetData();
      pcall(function() self.mhButton:SetShortcut(Turbine.UI.Lotro.Shortcut( mhButtonQST,  mhButtonQSD)); end);
      self.mhButton:SetMouseVisible(true);
      self.mhButton:SetVisible(true);
    else
      x = 5; y= -40;
      self.mhButton:SetPosition(x,y);
      if (mhFlash == 1 and mhButtonAvoid == 3) then  -- FlashScreenennabled but not Instantpot
      else
        self.movingHolder:SetVisible(false);
        self.movingHolder:SetMouseVisible(false);
      end
      self.mhButton:SetShortcut(nil);
      self.mhButton:SetVisible(false);
    end;
  else
    self.wLit:SetVisible(false);
    if (bTestInProgress == false) then self.wqs:SetVisible(false); end
  end

  if (fCountx > 0)then
    self.fLit:SetVisible(true);
    self.fqs:SetVisible(true); -- Needed to enable the shortcut
                 if (mhFlash == 1) then
                        self.movingHolder:SetBackColor(Turbine.UI.Color(0.2, 0.8, 0.45, 0.85));
                        self.movingHolder:SetVisible(true);
                        self.movingHolder:SetMouseVisible(false);
                 end
    if (mhButtonAvoid == 0 and self.fTimerOn==false and self.fqs:GetShortcut():GetData() ~= "")then
                        self.mhButton:SetPosition(x,y);
                        self.movingHolder:SetVisible(true);
                        self.movingHolder:SetMouseVisible(false);
                        mhButtonQST = self.fqs:GetShortcut():GetType();
                        mhButtonQSD = self.fqs:GetShortcut():GetData();
                        pcall(function() self.mhButton:SetShortcut(Turbine.UI.Lotro.Shortcut( mhButtonQST,  mhButtonQSD)); end);
                        self.mhButton:SetMouseVisible(true);
                        self.mhButton:SetVisible(true);
                else
                        x = 5; y= -40;
                        self.mhButton:SetPosition(x,y);
                        if (mhFlash == 1 and mhButtonAvoid == 3) then  -- FlashScreenennabled but not Instantpot
                        else
                                self.movingHolder:SetVisible(false);
                                self.movingHolder:SetMouseVisible(false);
                        end
                        self.mhButton:SetShortcut(nil);
                        self.mhButton:SetVisible(false);
                end;
  else
    self.fLit:SetVisible(false);
    if (bTestInProgress == false) then self.fqs:SetVisible(false); end
  end
  if (dCountx > 0)then
    self.dLit:SetVisible(true);
    self.dqs:SetVisible(true); -- Needed to enable the shortcut
                if (mhFlash == 1) then
                        self.movingHolder:SetBackColor(Turbine.UI.Color(0.2, 0.85, 0.7, 0.1));
                        self.movingHolder:SetVisible(true);
                        self.movingHolder:SetMouseVisible(false);
                end
    if (mhButtonAvoid == 0 and self.dTimerOn==false and self.dqs:GetShortcut():GetData() ~= "")then
                        self.mhButton:SetPosition(x,y);
                        self.movingHolder:SetVisible(true);
                        self.movingHolder:SetMouseVisible(false);
                        mhButtonQST = self.dqs:GetShortcut():GetType();
                        mhButtonQSD = self.dqs:GetShortcut():GetData();
                        pcall(function() self.mhButton:SetShortcut(Turbine.UI.Lotro.Shortcut( mhButtonQST,  mhButtonQSD)); end);
                        self.mhButton:SetMouseVisible(true);
                        self.mhButton:SetVisible(true);
                else
                        x = 5; y= -40;
                        self.mhButton:SetPosition(x,y);
                        if (mhFlash == 1 and mhButtonAvoid == 3) then  -- FlashScreenennabled but not Instantpot
                        else
                                self.movingHolder:SetVisible(false);
                                self.movingHolder:SetMouseVisible(false);
                        end
                        self.mhButton:SetShortcut(nil);
                        self.mhButton:SetVisible(false);
                end;
  else
    self.dLit:SetVisible(false);
    if (bTestInProgress == false) then self.dqs:SetVisible(false); end
  end
  if (pCountx > 0)then
    self.pLit:SetVisible(true);
    self.pqs:SetVisible(true); -- Needed to enable the shortcut
                if (mhFlash == 1) then
                        self.movingHolder:SetBackColor(Turbine.UI.Color(0.2, 0.15, 0.7, 0.25));
                        self.movingHolder:SetVisible(true);
                        self.movingHolder:SetMouseVisible(false);
                end
    if (mhButtonAvoid == 0 and self.pTimerOn==false and self.pqs:GetShortcut():GetData() ~= "")then
                        --self.mhButton:SetShortcut(nil); -- Ca empèche le click gauche :(
                        --self.mhButton:SetVisible(false);-- Ca empèche le click gauche :(
                        self.mhButton:SetPosition(x,y);
                        self.movingHolder:SetVisible(true);
                        self.movingHolder:SetMouseVisible(false);
                        mhButtonQST = self.pqs:GetShortcut():GetType();
                        mhButtonQSD = self.pqs:GetShortcut():GetData();
                        pcall(function() self.mhButton:SetShortcut(Turbine.UI.Lotro.Shortcut( mhButtonQST,  mhButtonQSD)); end);
                        self.mhButton:SetMouseVisible(true);
                        self.mhButton:SetVisible(true);
                else
                        x = 5; y= -40;
                        self.mhButton:SetPosition(x,y);
                        if (mhFlash == 1 and mhButtonAvoid == 3) then  -- FlashScreenennabled but not Instantpot
                        else
                                self.movingHolder:SetVisible(false);
                                self.movingHolder:SetMouseVisible(false);
                        end
                        self.mhButton:SetShortcut(nil);
                        self.mhButton:SetVisible(false);
                end;
  else
    self.pLit:SetVisible(false);
    if (bTestInProgress == false) then self.pqs:SetVisible(false); end
  end
end


  self.updateCounts=function()
    wsc = self.wqs:GetShortcut();
    fsc = self.fqs:GetShortcut();
    dsc = self.dqs:GetShortcut();
    psc = self.pqs:GetShortcut();
    if (wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.wInf:SetVisible(false);
      self.SetNum(self.wChar1,self.wChar2,1,self.wCount);
    elseif (wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
      self.wChar1:SetVisible(false);
      self.wChar2:SetVisible(false);
      if (self.displayMode >= 1) then
                                self.wInf:SetVisible(true);
                        end
    end
    if (fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.fInf:SetVisible(false);
      self.SetNum(self.fChar1,self.fChar2,2,self.fCount);
    elseif (fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
      self.fChar1:SetVisible(false);
      self.fChar2:SetVisible(false);
      if (self.displayMode >= 1) then
                                self.fInf:SetVisible(true);
                        end
    end
    if (dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.dInf:SetVisible(false);
      self.SetNum(self.dChar1,self.dChar2,3,self.dCount);
    elseif (dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
      self.dChar1:SetVisible(false);
      self.dChar2:SetVisible(false);
      if (self.displayMode >= 1) then
                                self.dInf:SetVisible(true);
                        end
    end
    if (psc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.pInf:SetVisible(false);
      self.SetNum(self.pChar1,self.pChar2,4,self.pCount);
    elseif (psc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
      self.pChar1:SetVisible(false);
      self.pChar2:SetVisible(false);
      if (self.displayMode >= 1) then
                                self.pInf:SetVisible(true);
                        end
    end

  end

  self.updateCooldowns=function(w,f,d,p)
    if(w == 0 and self.wCD == 30 ) then
      self.wsc=self.wqs:GetShortcut();
      if (self.wsc:GetData() ~= "") then
        if (self.wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item) then
          self.witem=self.wsc:GetItem();
          success,mycat = pcall(self.witem.GetCategory);
          if success == false then
            mycat=0;
          end
          if (mycat == Turbine.Gameplay.ItemCategory.Potion) then
            self.wCD = 30;
          end
        end
      end
    else
      self.wCD=w;
    end
    if(f == 0 and self.fCD == 30 ) then
      self.fsc=self.fqs:GetShortcut();
      if (self.fsc:GetData() ~= "") then
        if (self.fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item) then
          self.fitem=self.fsc:GetItem();
          success,mycat = pcall(self.fitem.GetCategory);
          if success == false then
            mycat=0;
          end
          if (mycat == Turbine.Gameplay.ItemCategory.Potion) then
            self.fCD = 30;
          end
        end
      end
    else
      self.fCD=f;
    end
    if(d == 0 and self.dCD == 30 ) then
      self.dsc=self.dqs:GetShortcut();
      if (self.dsc:GetData() ~= "") then
        if (self.dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item) then
          self.ditem=self.dsc:GetItem();
          success,mycat = pcall(self.ditem.GetCategory);
          if success == false then
            mycat=0;
          end
          if (mycat == Turbine.Gameplay.ItemCategory.Potion) then
            self.dCD = 30;
          end
        end
      end
    else
      self.dCD=d;
    end
    if(p == 0 and self.pCD == 30 ) then
      self.psc=self.pqs:GetShortcut();
      if (self.psc:GetData() ~= "") then
        if (self.psc:GetType() == Turbine.UI.Lotro.ShortcutType.Item) then
          self.pitem=self.psc:GetItem();
          success,mycat = pcall(self.pitem.GetCategory);
          if success == false then
            mycat=0;
          end
          if (mycat == Turbine.Gameplay.ItemCategory.Potion) then
            self.pCD = 30;
          end
        end
      end
    else
      self.pCD=p;
    end
    self.qslot.sOneCD=self.wCD;
    self.qslot.sTwoCD=self.fCD;
    self.qslot.sThreeCD=self.dCD;
    self.qslot.sFourCD=self.pCD;
    self.Refresh();
  end

  self.updateQty=function(w,f,d,p)
                if (self.wCount~=0) then self.wCount = tonumber(w); end;
                if (self.fCount~=0) then self.fCount = tonumber(f); end;
                if (self.dCount~=0) then self.dCount = tonumber(d); end;
                if (self.pCount~=0) then self.pCount = tonumber(p); end;
    self.qslot.sOneQty   = self.wCount;
    self.qslot.sTwoQty   = self.fCount;
    self.qslot.sThreeQty = self.dCount;
    self.qslot.sFourQty  = self.pCount;
    --self.Refresh();
  end

  self.displayMode=2;
  self.Override=false;
  self.player=Turbine.Gameplay.LocalPlayer.GetInstance();
  self.wBlock=true;
  self.fBlock=true;
  self.dBlock=true;
  self.pBlock=true;
  self.indOpacity=.8;
  self.dbOpacity=.5;
  self.qslot={sOneType="",sOneData="",sOneCD="0",sOneQty="0",sTwoType="",sTwoData="",sTwoCD="0",sTwoQty="0",sThreeType="",sThreeData="",sThreeCD="0",sThreeQty="0",sFourType="",sFourData="",sFourCD="0",sFourQty="0"};
  self:SetWantsUpdates(true);
  self:SetSize(134,150);   --self:SetSize(134,80);
  self.indicatorCount=0;
  self.dbb=Turbine.UI.Window();
  self.dbb:SetVisible(true);
  self.dbb:SetParent(self);
  self.dbb:SetSize(114,33);
  self.dbb:SetPosition(10,5);
  self.dbb:SetBackground("DU_81/Palantir/Resources/DBB/dbbc.tga");
  self.dbb:SetVisible(true);
  self.dbb:SetWantsKeyEvents(false);
  self.dbb:SetWantsUpdates(true);
  self.dbb:SetMouseVisible(false);
  self.dbb:SetOpacity(self.dbOpacity);
  self.dbb:SetBlendMode(4);
  self.dbb:SetZOrder(20);

  self.bw=Turbine.UI.Window(); -- Area containing the 4 CD bars
  self.bw:SetSize(90,17);
  self.bw:SetPosition(23,0);
  self.bw:SetParent(self);
  self.bw:SetVisible(true);
  --self.bw:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  --self.bw:SetBlendMode(4);
  --self.bw:SetBackColorBlendMode(2);
  --self.bw:SetStretchMode(2);
  self.bw:SetMouseVisible(false);

  self.bbg=Turbine.UI.Window();-- Area containing the 4 CD bars
  self.bbg:SetVisible(true);
  self.bbg:SetParent(self);
  self.bbg:SetSize(90,17);
  self.bbg:SetPosition(22,0);
  self.bbg:SetZOrder(25);
  self.bbg:SetBackground("DU_81/Palantir/Resources/DBB/bar_bg.tga");
  self.bbg:SetMouseVisible(false);
  self.bbg:SetOpacity(self.dbOpacity);
  --self.bbg:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  --self.bbg:SetBlendMode(4);
  --self.bbg:SetBackColorBlendMode(4);
  --self.bbg:SetStretchMode(2);
  self.qsWindow=Turbine.UI.Window(); -- Holder for buttons to place pots at setting time.
  self.qsWindow:SetVisible(true);
  self.qsWindow:SetParent(self);
  self.qsWindow:SetPosition(10,5);
  self.qsWindow:SetSize(146,40);
  self.qsWindow:SetMouseVisible(false);
  self.qsWindow:SetBackColor(Turbine.UI.Color(.05,1,1,1));

  self.wqs=Turbine.UI.Lotro.Quickslot(); -- and the 4 places for 4 pots
  self.wqs:SetVisible(true);
  self.wqs:SetParent(self.qsWindow);
  self.wqs:SetPosition(3,0);
  self.wqs:SetSize(28,25);
  self.fqs=Turbine.UI.Lotro.Quickslot();
  self.fqs:SetVisible(true);
  self.fqs:SetParent(self.qsWindow);
  self.fqs:SetPosition(29,10);
  self.fqs:SetSize(28,22);
  self.dqs=Turbine.UI.Lotro.Quickslot();
  self.dqs:SetVisible(true);
  self.dqs:SetParent(self.qsWindow);
  self.dqs:SetPosition(55,10);
  self.dqs:SetSize(28,22);
  self.pqs=Turbine.UI.Lotro.Quickslot();
  self.pqs:SetVisible(true);
  self.pqs:SetParent(self.qsWindow);
  self.pqs:SetPosition(81,0);
  self.pqs:SetSize(28,25);

  self.wbtn=Turbine.UI.Window(); -- and the 4 removal buttons
  self.wbtn:SetParent(self);
  self.wbtn:SetSize(24,8);
  self.wbtn:SetPosition(1,127);
  self.wbtn:SetBackColor(Turbine.UI.Color(.9,0.01,0.01,0.01));
  self.wbtn.MouseClick=function(sender,args)
    self.wqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
    self.qslot.sOneType="";  self.qslot.sOneData="";  self.qslot.sOneCD="0";self.qslot.sOneQty="0";
  end
  self.fbtn=Turbine.UI.Window(); -- and the 4 removal buttons
  self.fbtn:SetParent(self);
  self.fbtn:SetSize(24,8);
  self.fbtn:SetPosition(37,127);
  self.fbtn:SetBackColor(Turbine.UI.Color(.9,0.01,0.01,0.01));
  self.fbtn.MouseClick=function(sender,args)
    self.fqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
    self.qslot.sTwoType="";  self.qslot.sTwoData="";  self.qslot.sTwoCD="0";self.qslot.sTwoQty="0";
  end
  self.dbtn=Turbine.UI.Window(); -- and the 4 removal buttons
  self.dbtn:SetParent(self);
  self.dbtn:SetSize(24,8);
  self.dbtn:SetPosition(73,127);
  self.dbtn:SetBackColor(Turbine.UI.Color(.9,0.01,0.01,0.01));
  self.dbtn.MouseClick=function(sender,args)
    self.dqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
    self.qslot.sThreeType="";self.qslot.sThreeData="";self.qslot.sThreeCD="0";self.qslot.sThreeQty="0";
  end
  self.pbtn=Turbine.UI.Window(); -- and the 4 removal buttons
  self.pbtn:SetParent(self);
  self.pbtn:SetSize(24,8);
  self.pbtn:SetPosition(109,127);
  self.pbtn:SetBackColor(Turbine.UI.Color(.9,0.01,0.01,0.01));
  self.pbtn.MouseClick=function(sender,args)
    self.pqs:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
    self.qslot.sFourType=""; self.qslot.sFourData=""; self.qslot.sFourCD="0"; self.qslot.sFourQty="0";
  end

-- indicator help :) Only when setup window is here

  self.wqs.MouseEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.wLit:SetVisible(true);
      self.fLit:SetVisible(false);
      self.dLit:SetVisible(false);
      self.pLit:SetVisible(false);
    end
  end
  self.wqs.DragEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.wLit:SetVisible(true);
      self.fLit:SetVisible(false);
      self.dLit:SetVisible(false);
      self.pLit:SetVisible(false);
    end
  end
  self.wqs.DragLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.wLit:SetVisible(false);
    end
  end
  self.wqs.MouseLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.wLit:SetVisible(false);
    end
  end


  self.fqs.MouseEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.fLit:SetVisible(true);
      self.wLit:SetVisible(false);
      self.dLit:SetVisible(false);
      self.pLit:SetVisible(false);
    end
  end
  self.fqs.DragEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.fLit:SetVisible(true);
      self.wLit:SetVisible(false);
      self.dLit:SetVisible(false);
      self.pLit:SetVisible(false);
    end
  end
  self.fqs.DragLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.fLit:SetVisible(false);
    end
  end
  self.fqs.MouseLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.fLit:SetVisible(false);
    end
  end


  self.dqs.MouseEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.dLit:SetVisible(true);
      self.wLit:SetVisible(false);
      self.fLit:SetVisible(false);
      self.pLit:SetVisible(false);
    end
  end
  self.dqs.DragEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.dLit:SetVisible(true);
      self.wLit:SetVisible(false);
      self.fLit:SetVisible(false);
      self.pLit:SetVisible(false);
    end
  end
  self.dqs.DragLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.dLit:SetVisible(false);
    end
  end
  self.dqs.MouseLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.dLit:SetVisible(false);
    end
  end


  self.pqs.MouseEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.pLit:SetVisible(true);
      self.wLit:SetVisible(false);
      self.fLit:SetVisible(false);
      self.dLit:SetVisible(false);
    end
  end
  self.pqs.DragEnter=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.pLit:SetVisible(true);
      self.wLit:SetVisible(false);
      self.fLit:SetVisible(false);
      self.dLit:SetVisible(false);
    end
  end
  self.pqs.DragLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.pLit:SetVisible(false);
    end
  end
  self.pqs.MouseLeave=function(sender,args)
    if (self.Override==true and self:GetParent().setupVisible==true)then
      self.pLit:SetVisible(false);
    end
  end

  -- Instant cleaner
  --self.BackPackQuantity = 0;

  self.movingHolder = Turbine.UI.Window();
  self.movingHolder:SetVisible(false);
  self.movingHolder:SetPosition(0,0);
  self.movingHolder:SetSize(Turbine.UI.Display.GetSize());
  self.movingHolder:SetBackColor(Turbine.UI.Color(0,0,0,0));
  self.mhButton=Turbine.UI.Lotro.Quickslot();
  self.mhButton:SetParent(self.movingHolder);
  self.mhButton:SetPosition(0,0);
  self.mhButton:SetSize(36,36);
        self.mhButton:SetVisible(false);
        self.mhButton:SetShortcut(Turbine.UI.Lotro.Shortcut(Turbine.UI.Lotro.ShortcutType.Undefined,""));
        self.mhButton:SetUseOnRightClick(true);
        mhFlash = 0;
  mhButtonAvoid = 0;
  if (self.displayMode >= 3 ) then mhButtonAvoid = 0;
  else mhButtonAvoid = 3;
        end;
  self.mhButton.MouseClick=function(sender,args)
                if (mhButtonAvoid == 0) then mhButtonAvoid = 2;
                --Turbine.Shell.WriteLine("Palantir mhButtonAvoid => 2(click)");
    self.mhTimer:Start();
                end
  end
  self.mhButton.MouseWheel = function(sender, args)
                if (mhButtonAvoid == 0) then mhButtonAvoid = 1;
                --Turbine.Shell.WriteLine("Palantir mhButtonAvoid => 1(molette)");
                end
  end
  self.mhTimer = DU_81.Utilsx.Timer();
  self.mhTimer.AutoReset=true;
  self.mhTimer.Interval=1;
  self.mhTimer.TimerElapsed=function(sender,args)
    if (self.mhTimer.count >= 3)then
                        mhButtonAvoid = 5;
                        --Turbine.Shell.WriteLine("Palantir mhButtonAvoid => 5 (onTimer");
                        self.mhTimer:Stop();
    end
  end

  -- wound

  self.wCD = 0;
  self.wCount = 0;
  self.wTimerOn=false;
  self.wTimer = DU_81.Utilsx.Timer();
  self.wTimer.AutoReset=true;
  self.wTimer.Interval=1;

  self.wLit = Turbine.UI.Window();
  self.wLit:SetParent(self.dbb);
  self.wLit:SetZOrder(27);
  self.wLit:SetSize(34,34);
  self.wLit:SetPosition(0,0);
  self.wLit:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/ind2.tga");
  self.wLit:SetMouseVisible(false);
  self.wLit:SetBlendMode(4);
  self.wLit:SetOpacity(self.indOpacity);
  self.wLit:SetVisible(false);

  self.cdHolder=Turbine.UI.Window();
  self.cdHolder:SetVisible(true);
  self.cdHolder:SetParent(self);
  self.cdHolder:SetSize(90,17);
  self.cdHolder:SetPosition(22,0);

  self.wCdInd = Turbine.UI.Control();
  self.wCdInd:SetParent(self.cdHolder)
  self.wCdInd:SetSize(20,10);
  self.wCdInd:SetPosition(2,2);
  self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_green2.tga");
  self.wCdInd:SetZOrder(26);
  self.wCdInd:SetMouseVisible(false);
  self.wCdInd:SetBlendMode(4);

  self.wCdMask=Turbine.UI.Control();
  self.wCdMask:SetParent(self.wCdInd);
  self.wCdMask:SetSize(0,10);
  self.wCdMask:SetPosition(0,0);
  self.wCdMask:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_mask2.tga");
  self.wCdMask:SetBlendMode(5);
  self.wCdMask:SetMouseVisible(false);

  self.wChar = Turbine.UI.Control();
  self.wChar:SetParent(self);
  self.wChar:SetZOrder(26);
  self.wChar:SetPosition(14,25)
  self.wChar:SetSize(20,16);
  self.wChar:SetMouseVisible(false);
  self.wChar:SetBlendMode(2);
  self.wChar:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  self.wChar:SetBackColorBlendMode(2);
  self.wChar1 = Turbine.UI.Control();
  self.wChar1:SetParent(self.wChar);
  self.wChar1:SetSize(11,11);
  self.wChar1:SetPosition(0,0);
  self.wChar1:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/0.tga");
  self.wChar1:SetVisible(false);
  self.wChar1:SetMouseVisible(false);
  self.wChar1:SetBlendMode(4);
  self.wChar1:SetBackColorBlendMode(4);
  self.wChar2 = Turbine.UI.Control();
  self.wChar2:SetParent(self.wChar);
  self.wChar2:SetSize(11,11);
  self.wChar2:SetPosition(8,3);
  self.wChar2:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/0.tga");
  self.wChar2:SetVisible(false);
  self.wChar2:SetMouseVisible(false);
  self.wChar2:SetBlendMode(4);
  self.wChar2:SetBackColorBlendMode(4);
  self.wInf = Turbine.UI.Control();
  self.wInf:SetParent(self.wChar);
  self.wInf:SetSize(14,11);
  self.wInf:SetPosition(2,1);
  self.wInf:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/inf.tga");
  self.wInf:SetVisible(false);
  self.wInf:SetMouseVisible(false);
  self.wInf:SetBlendMode(4);
  self.wInf:SetBackColorBlendMode(4);
  self:hideQuickslots();
  self.wsc=self.wqs:GetShortcut();

  self.wqs.ShortcutChanged=function(sender,args)
  -- Called when potions or removed from a stack
  -- also called by system when loading/unloading Palantir
    self.wCdMask:SetWidth(0);
    self.wTimer:Stop();
    self.wTimerOn=false;
    self.wBlock=false;
    self.wsc=Turbine.UI.Lotro.Shortcut();
    self.wsc=self.wqs:GetShortcut();
    if (self.wsc:GetData() ~= "")then
      if (self.wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
        self.wInf:SetVisible(false);
        self.witem=self.wsc:GetItem();
        success,self.wCount = pcall(self.witem.GetQuantity);
                if success == false then
                    self.wCount = tonumber(self.qslot.sOneQty);
                elseif (self.witem:GetCategory() == Turbine.Gameplay.ItemCategory.Potion) then
                    -- ItemCategory
                    if (self.wCD==0) then
            self.wCD = 30;
            self.qslot.sOneCD=30;
        end  end
                self.qslot.sOneQty = tostring(self.wCount);

        -- Surcharge the function for this Shortcut's instance
        self.witem.QuantityChanged = function(sender,args)
                                        -- If quantity reduced by 1, expect potion drunk
          local oCount=self.wCount;
          self.wCount=self.witem:GetQuantity();
                    self.qslot.sOneQty = tostring(self.wCount);
          self.SetNum(self.wChar1,self.wChar2,1,self.wCount);
          if(oCount-self.wCount == 1)then
            if (self.fInf:IsVisible()==false and self.fBlock==false)then
              self.fCooldown();
            end
            if (self.dInf:IsVisible()==false and self.dBlock==false)then
              self.dCooldown();
            end
            if (self.pInf:IsVisible()==false and self.pBlock==false)then
              self.pCooldown();
            end
                        self.wCooldown();
          end
        end

        self.SetNum(self.wChar1,self.wChar2,1,self.wCount);
      elseif (self.wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.wChar1:SetVisible(false);
        self.wChar2:SetVisible(false);
                                if (self.displayMode >= 1) then
                                        self.wInf:SetVisible(true);
                                end
      end
      self.qslot.sOneType = self.wsc:GetType();
      self.qslot.sOneData = self.wsc:GetData();
    end
    self.Refresh();
  end

  self.wCooldown = function ()
    self.wTimer:Start();
    self.wTimerOn=true;
    self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_red2.tga");
    self.wCdMask:SetWidth(0);
    self.wBlock=true;
  end

  self.wqs.MouseClick=function(sender,args)
    -- Check real quantity of Shortcut 's stack
    self.wsc=self.wqs:GetShortcut();
    if (self.wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.witem=self.wsc:GetItem();
      success,self.wCount = pcall(self.witem.GetQuantity);
                        if success == false then self.wCount=0;
                        end  -- For safety purpose shouldn't happen
                end

    if(self.wBlock==false)then -- if the indicator is green
      if (self.wsc:GetType() == 2 and self.wCount==0)then
                                -- if no potion available,
                                -- self.wqs.ShortcutChanged will clean the shortut
                                -- or find a new stackof the same potion

      elseif (self.wsc:GetType() == 2 and self.wCount==1)then
        -- if last potion, handle CD indicators
                                self.wCount = 0;
        self.wCooldown();
        if (self.fInf:IsVisible()==false and self.fBlock==false)then
          self.fCooldown();
        end
        if (self.dInf:IsVisible()==false and self.dBlock==false)then
          self.dCooldown();
        end
        if (self.pInf:IsVisible()==false and self.pBlock==false)then
          self.pCooldown();
        end
        self.SetNum(self.wChar1,self.wChar2,1,self.wCount);

      elseif (self.wsc:GetType() == 2 and self.wCount>1)then
                                -- if 2+ potions self.witem.QuantityChanged will handle CD indicators
        self.SetNum(self.wChar1,self.wChar2,1,self.wCount);

      elseif (self.wsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
                                -- Skill so no quantity
        self.wCooldown();
      end
                        self.qslot.sOneQty = tostring(self.wCount);
    --else -- self.wBlock==true  -- if the indicator is red
                        -- Clicked Empty stack: self.wqs.ShortcutChanged will clean the shortut
                        --                      or find a new stack of the same potion
                        -- Clicked stak 1+    : Lotro will prevent drinking the pot
    end
  end

  self.wTimer.TimerElapsed=function(sender,args)
    if (self.wTimer.count == self.wCD or self.wCD==0)then
      self.wCdMask:SetWidth(0);
      self.wTimer:Stop();
      self.wTimerOn=false;
      self.wBlock=false;
      self.wCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot1/bar_green2.tga");
      self.Refresh();
    else
      timePct = self.wTimer.count/self.wCD
      pix = 20 * timePct;
      self.wCdMask:SetWidth(0 + pix);
    end
    self.qslot.sOneType = wsc:GetType();
    self.qslot.sOneData = wsc:GetData();
  end

  --fear

  self.fCD = 0;
  self.fCount=0;
  self.fTimerOn=false;
  self.fTimer = DU_81.Utilsx.Timer();
  self.fTimer.AutoReset=true;
  self.fTimer.Interval=1;

  self.fLit = Turbine.UI.Window();
  self.fLit:SetParent(self.dbb);
  self.fLit:SetZOrder(28);
  self.fLit:SetSize(28,34);
  self.fLit:SetPosition(30,0);
  self.fLit:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/ind2.tga");
  self.fLit:SetMouseVisible(false);
  self.fLit:SetBlendMode(4);
  self.fLit:SetVisible(false);
  self.fLit:SetOpacity(self.indOpacity);

  self.fCdInd = Turbine.UI.Control();
  self.fCdInd:SetParent(self.cdHolder)
  self.fCdInd:SetSize(19,6);
  self.fCdInd:SetPosition(24,10);
  self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_green2.tga");
  self.fCdInd:SetZOrder(26);
  self.fCdInd:SetMouseVisible(false);
  self.fCdInd:SetBlendMode(4);

  self.fCdMask=Turbine.UI.Control();
  self.fCdMask:SetParent(self.fCdInd);
  self.fCdMask:SetSize(0,6);
  self.fCdMask:SetPosition(0,0);
  self.fCdMask:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_mask2.tga");
  self.fCdMask:SetBlendMode(4);
  self.fCdMask:SetMouseVisible(false);

  self.fChar = Turbine.UI.Control();
  self.fChar:SetParent(self);
  self.fChar:SetZOrder(26);
  self.fChar:SetPosition(42,31)
  self.fChar:SetSize(20,16);
  self.fChar:SetMouseVisible(false);
  self.fChar:SetBlendMode(2);
  self.fChar:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  self.fChar:SetBackColorBlendMode(2);
  self.fChar1 = Turbine.UI.Control();
  self.fChar1:SetParent(self.fChar);
  self.fChar1:SetSize(10,11);
  self.fChar1:SetPosition(0,3);
  self.fChar1:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/0.tga");
  self.fChar1:SetVisible(false);
  self.fChar1:SetMouseVisible(false);
  self.fChar1:SetBlendMode(4);
  self.fChar1:SetBackColorBlendMode(4);
  self.fChar2 = Turbine.UI.Control();
  self.fChar2:SetParent(self.fChar);
  self.fChar2:SetSize(10,11);
  self.fChar2:SetPosition(8,4);
  self.fChar2:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/0.tga");
  self.fChar2:SetVisible(false);
  self.fChar2:SetMouseVisible(false);
  self.fChar2:SetBlendMode(4);
  self.fChar2:SetBackColorBlendMode(4);
  self.fInf = Turbine.UI.Control();
  self.fInf:SetParent(self.fChar);
  self.fInf:SetSize(15,9);
  self.fInf:SetPosition(2,4);
  self.fInf:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/inf.tga");
  self.fInf:SetVisible(false);
  self.fInf:SetMouseVisible(false);
  self.fInf:SetBlendMode(4);
  self.fInf:SetBackColorBlendMode(4);
  self:hideQuickslots();
  self.fsc=self.fqs:GetShortcut();

  self.fqs.ShortcutChanged=function(sender,args)
    self.fCdMask:SetWidth(0);
    self.fTimer:Stop();
    self.fTimerOn=false;
    self.fBlock=false;
    self.fsc=Turbine.UI.Lotro.Shortcut();
    self.fsc=self.fqs:GetShortcut();
    if (self.fsc:GetData() ~= "")then
      if (self.fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
        self.fInf:SetVisible(false);
        self.fitem=self.fsc:GetItem();
        success,self.fCount = pcall(self.fitem.GetQuantity);
                if success == false then
                    self.fCount=tonumber(self.qslot.sTwoQty);
                elseif (self.fitem:GetCategory() == Turbine.Gameplay.ItemCategory.Potion) then
                    -- ItemCategory
                    if (self.fCD==0) then
                        self.fCD = 30;
                        self.qslot.sTwoCD=30;
                    end
                end
                self.qslot.sTwoQty = tostring(self.fCount);

        -- Surcharge the function for this Shortcut's instance
        self.fitem.QuantityChanged = function(sender,args)
          local oCount=self.fCount;
          self.fCount=self.fitem:GetQuantity();
                    self.qslot.sTwoQty = tostring(self.fCount);
          self.SetNum(self.fChar1,self.fChar2,2,self.fCount);
          if(oCount-self.fCount == 1)then
            if (self.wInf:IsVisible()==false and self.wBlock==false)then
              self.wCooldown();
            end
            if (self.dInf:IsVisible()==false and self.dBlock==false)then
              self.dCooldown();
            end
            if (self.pInf:IsVisible()==false and self.pBlock==false)then
              self.pCooldown();
            end
                        self.fCooldown();
          end
        end

        self.SetNum(self.fChar1,self.fChar2,2,self.fCount);
      elseif (self.fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.fChar1:SetVisible(false);
        self.fChar2:SetVisible(false);
                                if (self.displayMode >= 1) then
                                        self.fInf:SetVisible(true);
                                end
      end
      self.qslot.sTwoType = self.fsc:GetType();
      self.qslot.sTwoData = self.fsc:GetData();
    end
    self.Refresh();
  end

  self.fCooldown = function ()
    self.fTimer:Start();
    self.fTimerOn=true;
    self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_red2.tga");
    self.fCdMask:SetWidth(0);
    self.fBlock=true;
  end

  self.fqs.MouseClick=function(sender,args)
    self.fsc=self.fqs:GetShortcut();
    if (self.fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.fitem=self.fsc:GetItem();
      success,self.fCount = pcall(self.fitem.GetQuantity);
                        if success == false then self.fCount=0; end
                end
    if(self.fBlock==false)then
      if (self.fsc:GetType() == 2 and self.fCount==0)then
      elseif (self.fsc:GetType() == 2 and self.fCount==1)then
                                self.fCount = 0;
        self.fCooldown();
        if (self.wInf:IsVisible()==false and self.wBlock==false)then
          self.wCooldown();
        end
        if (self.dInf:IsVisible()==false and self.dBlock==false)then
          self.dCooldown();
        end
        if (self.pInf:IsVisible()==false and self.pBlock==false)then
          self.pCooldown();
        end
        self.SetNum(self.fChar1,self.fChar2,2,self.fCount);

      elseif (self.fsc:GetType() == 2 and self.fCount>1)then
        self.SetNum(self.fChar1,self.fChar2,2,self.fCount);

      elseif (self.fsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.fCooldown();
      end
                        self.qslot.sTwoQty = tostring(self.fCount);
    end
  end

  self.fTimer.TimerElapsed=function(sender,args)
    if (self.fTimer.count == self.fCD or self.fCD==0)then
      self.fCdMask:SetWidth(0);
      self.fTimer:Stop();
      self.fTimerOn=false;
      self.fBlock=false;
      self.fCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot2/bar_green2.tga");
      self.Refresh();
    else
      timePct = self.fTimer.count/self.fCD
      pix = 19 * timePct;
      self.fCdMask:SetWidth(0 + pix);
    end
    self.qslot.sTwoType = fsc:GetType();
    self.qslot.sTwoData = fsc:GetData();
  end

  --disease

  self.dCD = 0;
  self.dCount=0;
  self.dTimerOn=false;
  self.dTimer = DU_81.Utilsx.Timer();
  self.dTimer.AutoReset=true;
  self.dTimer.Interval=1;

  self.dLit = Turbine.UI.Window();
  self.dLit:SetParent(self.dbb);
  self.dLit:SetZOrder(29);
  self.dLit:SetSize(26,34);
  self.dLit:SetPosition(58,0);
  self.dLit:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/ind2.tga");
  self.dLit:SetMouseVisible(false);
  self.dLit:SetOpacity(self.indOpacity);
  self.dLit:SetBlendMode(4);
  self.dLit:SetVisible(false);

  self.dCdInd = Turbine.UI.Control();
  self.dCdInd:SetParent(self.cdHolder)
  self.dCdInd:SetSize(19,6);
  self.dCdInd:SetPosition(47,10);
  self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_green2.tga");
  self.dCdInd:SetZOrder(26);
  self.dCdInd:SetMouseVisible(false);
  self.dCdInd:SetBlendMode(4);

  self.dCdMask=Turbine.UI.Control();
  self.dCdMask:SetParent(self.dCdInd);
  self.dCdMask:SetSize(0,6);
  self.dCdMask:SetPosition(0,0);
  self.dCdMask:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_mask2.tga");
  self.dCdMask:SetBlendMode(4);
  self.dCdMask:SetMouseVisible(false);

  self.dChar = Turbine.UI.Control();
  self.dChar:SetParent(self);
  self.dChar:SetZOrder(26);
  self.dChar:SetPosition(72,31)
  self.dChar:SetSize(20,16);
  self.dChar:SetMouseVisible(false);
  self.dChar:SetBlendMode(2);
  self.dChar:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  self.dChar:SetBackColorBlendMode(2);
  self.dChar1 = Turbine.UI.Control();
  self.dChar1:SetParent(self.dChar);
  self.dChar1:SetSize(10,10);
  self.dChar1:SetPosition(0,6);
  self.dChar1:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/0.tga");
  self.dChar1:SetVisible(false);
  self.dChar1:SetMouseVisible(false);
  self.dChar1:SetBlendMode(4);
  self.dChar1:SetBackColorBlendMode(4);
  self.dChar2 = Turbine.UI.Control();
  self.dChar2:SetParent(self.dChar);
  self.dChar2:SetSize(10,10);
  self.dChar2:SetPosition(8,5);
  self.dChar2:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/0.tga");
  self.dChar2:SetVisible(false);
  self.dChar2:SetMouseVisible(false);
  self.dChar2:SetBlendMode(4);
  self.dChar2:SetBackColorBlendMode(4);
  self.dInf = Turbine.UI.Control();
  self.dInf:SetParent(self.dChar);
  self.dInf:SetSize(14,9);
  self.dInf:SetPosition(2,4);
  self.dInf:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/inf.tga");
  self.dInf:SetVisible(false);
  self.dInf:SetMouseVisible(false);
  self.dInf:SetBlendMode(4);
  self.dInf:SetBackColorBlendMode(4);
  self:hideQuickslots();
  self.dsc=self.dqs:GetShortcut();

  self.dqs.ShortcutChanged=function(sender,args)
  -- Called when potions or removed from a stack
  -- also called by system when loading/unloading Palantir
    self.dCdMask:SetWidth(0);
    self.dTimer:Stop();
    self.dTimerOn=false;
    self.dBlock=false;
    self.dsc=Turbine.UI.Lotro.Shortcut();
    self.dsc=self.dqs:GetShortcut();
    if (self.dsc:GetData() ~= "")then
      if (self.dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
        self.dInf:SetVisible(false);
        self.ditem=self.dsc:GetItem();
        success,self.dCount = pcall(self.ditem.GetQuantity);
                if success == false then
                    self.dCount = tonumber(self.qslot.sThreeQty);
                elseif (self.ditem:GetCategory() == Turbine.Gameplay.ItemCategory.Potion) then
                    -- ItemCategory
                    if (self.dCD==0) then
            self.dCD = 30;
                        self.qslot.sThreeCD=30;
        end end
                 self.qslot.sThreeQty = tostring(self.dCount);

        -- Surcharge the function for this Shortcut's instance
        self.ditem.QuantityChanged = function(sender,args)
          local oCount=self.dCount;
          self.dCount=self.ditem:GetQuantity();
                    self.qslot.sThreeQty = tostring(self.dCount);
                    self.SetNum(self.dChar1,self.dChar2,3,self.dCount);
          if((oCount-self.dCount) == 1)then
            if (self.wInf:IsVisible()==false and self.wBlock==false)then
              self.wCooldown();
            end
            if (self.fInf:IsVisible()==false and self.fBlock==false)then
              self.fCooldown();
            end
            if (self.pInf:IsVisible()==false and self.pBlock==false)then
              self.pCooldown();
            end
            self.dCooldown();
          end
        end

        self.SetNum(self.dChar1,self.dChar2,3,self.dCount);
      elseif (self.dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.dChar1:SetVisible(false);
        self.dChar2:SetVisible(false);
                                if (self.displayMode >= 1) then
                                        self.dInf:SetVisible(true);
                                end
      end
      self.qslot.sThreeType = self.dsc:GetType();
      self.qslot.sThreeData = self.dsc:GetData();
    end
    self.Refresh();
  end

  self.dCooldown = function ()
    self.dTimer:Start();
    self.dTimerOn=true;
    self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_red2.tga");
    self.dCdMask:SetWidth(0);
    self.dBlock=true;
  end

  self.dqs.MouseClick=function(sender,args)
    self.dsc=self.dqs:GetShortcut();
    if (self.dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.ditem=self.dsc:GetItem();
      success,self.dCount = pcall(self.ditem.GetQuantity);
                        if success == false then self.dCount=0; end
                end
    if(self.dBlock==false)then
      if (self.dsc:GetType() == 2 and self.dCount==0)then
      elseif (self.dsc:GetType() == 2 and self.dCount==1)then
                                self.dCount = 0;
        self.dCooldown();
        if (self.wInf:IsVisible()==false and self.wBlock==false)then
          self.wCooldown();
        end
        if (self.fInf:IsVisible()==false and self.dBlock==false)then
          self.dCooldown();
        end
        if (self.pInf:IsVisible()==false and self.pBlock==false)then
          self.pCooldown();
        end
        self.SetNum(self.dChar1,self.dChar2,3,self.dCount);

      elseif (self.dsc:GetType() == 2 and self.dCount>1)then
        self.SetNum(self.dChar1,self.dChar2,3,self.dCount);

      elseif (self.dsc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.dCooldown();
      end
                        self.qslot.sThreeQty = tostring(self.dCount);
    end
  end

  self.dTimer.TimerElapsed=function(sender,args)
    if (self.dTimer.count == self.dCD or self.dCD==0)then
      self.dCdMask:SetWidth(0);
      self.dTimer:Stop();
      self.dTimerOn=false;
      self.dBlock=false;
      self.dCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot3/bar_green2.tga");
      self.Refresh();
    else
      timePct = self.dTimer.count/self.dCD
      pix = 19 * timePct;
      self.dCdMask:SetWidth(0 + pix);
    end
    self.qslot.sThreeType = dsc:GetType();
    self.qslot.sThreeData = dsc:GetData();
  end

  --poison

  self.pCD = 0;
  self.pCount=0;
  self.pTimerOn=false;
  self.pTimer = DU_81.Utilsx.Timer();
  self.pTimer.AutoReset=true;
  self.pTimer.Interval=1;

  self.pLit = Turbine.UI.Window();
  self.pLit:SetParent(self.dbb);
  self.pLit:SetZOrder(30);
  self.pLit:SetSize(32,34);
  self.pLit:SetPosition(82,0);
  self.pLit:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/ind2.tga");
  self.pLit:SetMouseVisible(false);
  self.pLit:SetBlendMode(4);
  self.pLit:SetOpacity(self.indOpacity);
  self.pLit:SetVisible(false);

  self.pCdInd = Turbine.UI.Control();
  self.pCdInd:SetParent(self.cdHolder)
  self.pCdInd:SetSize(19,11);
  self.pCdInd:SetPosition(69,1);
  self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_green2.tga");
  self.pCdInd:SetZOrder(26);
  self.pCdInd:SetMouseVisible(false);
  self.pCdInd:SetBlendMode(4);

  self.pCdMask=Turbine.UI.Control();
  self.pCdMask:SetParent(self.pCdInd);
  self.pCdMask:SetSize(0,11);
  self.pCdMask:SetPosition(0,0);
  self.pCdMask:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_mask2.tga");
  self.pCdMask:SetBlendMode(4);
  self.pCdMask:SetMouseVisible(false);
  --self.pCdMask:SetZOrder(27);

  self.pChar = Turbine.UI.Control();
  self.pChar:SetParent(self);
  self.pChar:SetZOrder(26);
  self.pChar:SetPosition(102,23)
  self.pChar:SetSize(20,16);
  self.pChar:SetMouseVisible(false);
  self.pChar:SetBlendMode(2);
  self.pChar:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  self.pChar:SetBackColorBlendMode(2);
  self.pChar1 = Turbine.UI.Control();
  self.pChar1:SetParent(self.pChar);
  self.pChar1:SetSize(10,11);
  self.pChar1:SetPosition(0,6);
  self.pChar1:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/0.tga");
  self.pChar1:SetVisible(false);
  self.pChar1:SetMouseVisible(false);
  self.pChar1:SetBlendMode(4);
  self.pChar1:SetBackColorBlendMode(4);
  self.pChar2 = Turbine.UI.Control();
  self.pChar2:SetParent(self.pChar);
  self.pChar2:SetSize(10,11);
  self.pChar2:SetPosition(8,3);
  self.pChar2:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/0.tga");
  self.pChar2:SetVisible(false);
  self.pChar2:SetMouseVisible(false);
  self.pChar2:SetBlendMode(4);
  self.pChar2:SetBackColorBlendMode(4);
  self.pInf = Turbine.UI.Control();
  self.pInf:SetParent(self.pChar);
  self.pInf:SetSize(14,10);
  self.pInf:SetPosition(1,5);
  self.pInf:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/inf.tga");
  self.pInf:SetVisible(false);
  self.pInf:SetMouseVisible(false);
  self.pInf:SetBlendMode(4);
  self.pInf:SetBackColorBlendMode(4);
  self:hideQuickslots();
  self.psc=self.pqs:GetShortcut();

  self.pqs.ShortcutChanged=function(sender,args)
    self.pCdMask:SetWidth(0);
    self.pTimer:Stop();
    self.pTimerOn=false;
    self.pBlock=false;
    self.psc=Turbine.UI.Lotro.Shortcut();
    self.psc=self.pqs:GetShortcut();
    if (self.psc:GetData() ~= "")then
      if (self.psc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
        self.pInf:SetVisible(false);
        self.pitem=self.psc:GetItem();
        success,self.pCount = pcall(self.pitem.GetQuantity);
                if success == false then  self.pCount=0;
                     self.pCount = tonumber(self.qslot.sFourQty);
                elseif (self.pitem:GetCategory() == Turbine.Gameplay.ItemCategory.Potion) then
                    -- ItemCategory
          if (self.pCD==0) then
                        self.pCD = 30;
                        self.qslot.sFourCD=30;
        end end
                self.qslot.sFourQty = tostring(self.pCount);

        -- Surcharge the function for this Shortcut's instance
        self.pitem.QuantityChanged = function(sender,args)
          local oCount=self.pCount;
          self.pCount=self.pitem:GetQuantity();
          self.qslot.sFourQty = tostring(self.pCount);
          self.SetNum(self.pChar1,self.pChar2,4,self.pCount);
          if(oCount-self.pCount == 1)then
            if (self.wInf:IsVisible()==false and self.wBlock==false)then
              self.wCooldown();
            end
            if (self.fInf:IsVisible()==false and self.fBlock==false)then
              self.fCooldown();
            end
            if (self.dInf:IsVisible()==false and self.dBlock==false)then
              self.dCooldown();
            end
            self.pCooldown();
          end
        end

        self.SetNum(self.pChar1,self.pChar2,4,self.pCount);
      elseif (self.psc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.pChar1:SetVisible(false);
        self.pChar2:SetVisible(false);
                if (self.displayMode >= 1) then
                    self.pInf:SetVisible(true);
                end
      end
      self.qslot.sFourType = self.psc:GetType();
      self.qslot.sFourData = self.psc:GetData();
    end
    self.Refresh();
  end

  self.pCooldown = function ()
    self.pTimer:Start();
    self.pTimerOn=true;
    self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_red2.tga");
    self.pCdMask:SetWidth(0);
    self.pBlock=true;
  end

  self.pqs.MouseClick=function(sender,args)
    self.psc=self.pqs:GetShortcut();
    if (self.psc:GetType() == Turbine.UI.Lotro.ShortcutType.Item)then
      self.pitem=self.psc:GetItem();
      success,self.pCount = pcall(self.pitem.GetQuantity);
                        if success == false then self.pCount=0; end
                end
    if(self.pBlock==false)then
      if (self.psc:GetType() == 2 and self.pCount==0)then
      elseif (self.psc:GetType() == 2 and self.pCount==1)then
                                self.pCount = 0;
        self.pCooldown();
        if (self.wInf:IsVisible()==false and self.wBlock==false)then
          self.wCooldown();
        end
        if (self.fInf:IsVisible()==false and self.dBlock==false)then
          self.dCooldown();
        end
        if (self.dInf:IsVisible()==false and selfdBlock==false)then
          self.dCooldown();
        end
        self.SetNum(self.pChar1,self.pChar2,4,self.pCount);

      elseif (self.psc:GetType() == 2 and self.pCount>1)then
        self.SetNum(self.pChar1,self.pChar2,4,self.pCount);

      elseif (self.psc:GetType() == Turbine.UI.Lotro.ShortcutType.Skill)then
        self.pCooldown();
      end
                        self.qslot.sFourQty = tostring(self.pCount);
    end
  end

  self.pTimer.TimerElapsed=function(sender,args)
    if (self.pTimer.count == self.pCD or self.pCD==0)then
      self.pCdMask:SetWidth(0);
      self.pTimer:Stop();
      self.pTimerOn=false;
      self.pBlock=false;--self.aClick(self.pqs);
      self.pCdInd:SetBackground("DU_81/Palantir/Resources/DBB/Slot4/bar_green2.tga");
      self.Refresh();
    else
      timePct = self.pTimer.count/self.pCD
      pix = 19 * timePct;
      self.pCdMask:SetWidth(0 + pix);
    end
    self.qslot.sFourType = psc:GetType();
    self.qslot.sFourData = psc:GetData();
  end

  self.Refresh();
        self.Update = function(sender,args)
    if(self.Override==false and self:IsVisible())then
      self.getEffects();
      --self.dsc = self.dqs:GetShortcut(); ---??????
    end
  end
end
--Turbine.Shell.WriteLine("Palantir: " .. tostring(nitem1) .. " (" .. tostring(qitem1)..") :"..tostring(iitem1:GetCategory()));
--shellCommand1 = Turbine.ShellCommand()
--Turbine.Shell.AddCommand('PHH', shellCommand1)
--[[
  ]]--
