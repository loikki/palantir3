import "DU_81.Palantir";
import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";


MezBar = class( Turbine.UI.Window );

function MezBar:Constructor()
  Turbine.UI.Window.Constructor( self );
    self:SetVisible(true);
  self:SetMouseVisible(false);
  self.local_player = Turbine.Gameplay.LocalPlayer.GetInstance();
  self.player_name  = self.local_player:GetName();
  self.player_class = self.local_player:GetClass();
  self.mezOpacity=1;
  self:SetWantsUpdates(true);
  self:SetSize(114,33);

    --MezBar data
  self.displayMode=1;
  self.Override=false;
  self.ACD=30;
  self.BCD=30;
  self.CCD=30;
  self.DCD=30;
  self.AutoDetectDuration = true;

  self.LM_1=30; -- Mez 5"..35"
  self.LM_2=30; -- Root
  self.LM_3=30; -- Debuff 30"..90"
  self.LM_4=30; -- AntiStun 30"..90"

  self.MN_1=15; -- Fear
  self.MN_2=30; -- Chant des morts
  self.MN_3=30; -- Soliloquy 30"..72"
  -- self.MN_4=30; -- Heart of C

  self.RK_1=20;  -- Writ of Cold - May vary
  self.RK_1lvl=1;  -- Writ of Cold - level 123
  self.RK_1resist = false;
  self.RK_2=20;  -- Writ of Fire, Scathing Mockery, Essay of Fire
  self.RK_2lvl=1;  -- Writ of Fire, level 123
  -- self.RK_3S=20; -- Thunderous Words Mots de tonnerre
  -- self.RK_4S=20; -- Harsh Debate Débat houleux
  -- self.RK_3H=20; -- Writ of Health Allégorie de la santé on self
  -- self.RK_4H=30; -- Prelude to Hope Prélude à l'espoir on self
  -- MEZ_RK_CRIT="Runes chargées" -- Runes chargées
  -- MEZ_RK_15="Temp\195\168te hivernale" -- Tempète hivernale - 5

  self.CA_1=30; -- Mez Riddle
  self.CA_2=15; -- Root ASmallSnag
  self.CA_2adv=30; -- Root Quite a Snag
  self.CA_3=30; -- Debuff Trics 30"..
  self.CA_4=30; -- Debuff Addle

  self.CH_1=15; -- Fear Bard's Arrow
  self.CH_1bis=30; -- Explosive arrow's Improved distracting Shot
  self.CH_2=30; -- Root Rain of Thorns
  -- self.CH_3=15; -- Fleetness
  -- self.CH_3adv=30; -- Improved Fleetness, 30"
  -- self.CH_4=15; -- Autobuff Needful Haste	15"..30"



  ----------------------------------------------------------------------
  ---------------------------  Helper functions ------------------------
  ----------------------------------------------------------------------

  self.SetBarOpacity = function(num)
      local n = tonumber(num); -- Sometimes receives a Table instead of number
      if ( n ~= nil )then
        self.MezBG:SetOpacity(num);
        if (n<0.2) then   n = n * 2;
        else				n = n + .2;
        end;
        if (n > 0.8) then   n = (n-0.8)/2 +.8;
        end
        self.TimerArea:SetOpacity(n); -- SetTextOpacity
        if ((num <= 0.04) or (self.IsVisible() == false)) then
          self.AMask:SetOpacity(0);
          self.BMask:SetOpacity(0);
          self.CMask:SetOpacity(0);
          self.DMask:SetOpacity(0);
        else
        self.AMask:SetOpacity(num/2 +.5);
        self.BMask:SetOpacity(num/2 +.5);
        self.CMask:SetOpacity(num/2 +.5);
        self.DMask:SetOpacity(num/2 +.5);
        end
        self.AMask:SetOpacity(num/2 +.5);
        self.BMask:SetOpacity(num/2 +.5);
        self.DMask:SetOpacity(num/2 +.5);
        self.MyPts:SetOpacity(num/2 +.5);
        self.MyCrit:SetOpacity(num/2 +.5);
      end
    end


  -- Combat Timer Start/Stop and clean Mez bars when combat is ending
  self.ShowTimer = true;
  self.PlayerInCombat	= false;
  self.InCombatChanged = function(bool)
    if ( bool == true) then
      self.CbtTimerStarting();
      self.PlayerInCombat	= true;
    else
      self.CbtTimerStopNow();
      self.PlayerInCombat	= false;
      -- Stop Timers; Reset/Hide nearlly everything
      self.ATimer:Stop();
      self.ATimerOn=false;
      self.AMask:SetVisible(false);
      self.AMask:SetSize(114,33);
      self.AMask:SetPosition(0,0);
      self.ABar:SetVisible(false);
      self.ABar2:SetVisible(false);
      self.ABlock = 0;
      self.BTimer:Stop();
      self.BTimerOn=false;
      self.BMask:SetVisible(false);
      self.BMask:SetSize(114,33);
      self.BMask:SetPosition(0,0);
      self.BBar:SetVisible(false);
      self.BBar2:SetVisible(false);
      self.BBlock = 0;
      if ((self.player_class == Turbine.Gameplay.Class.LoreMaster) or (self.player_class == Turbine.Gameplay.Class.Burglar))then
        self.CTimer:Stop();
        self.CTimerOn=false;
        self.CMask:SetVisible(false);
        self.CMask:SetSize(114,33);
        self.CMask:SetPosition(0,0);
        self.CBar:SetVisible(false);
        self.CBlock = 0;
      end
      if (self.player_class == Turbine.Gameplay.Class.Burglar)then
        self.DTimer:Stop();
        self.DTimerOn=false;
        self.DMask:SetVisible(false);
        self.DMask:SetSize(114,33);
        self.DMask:SetPosition(0,0);
        self.DBar:SetVisible(false);
        self.DBlock = 0;
      end
    end
  end


  -- Use or not the Combat timer
  MezCbtTimerVisible = false;
  self.SetCbtTimerVisible = function()
      MezCbtTimerVisible= self.TimerArea.Visible;
  end


  -- Setting of tick for combat timer
  self.updateTick = function(num)
      local n = tonumber(num); -- Sometimes receives a Table instead of number
      if ( n ~= nil )then
        if ( n <15) then n = 15; end --15 as minimal Tick
        self.TimerTick = n;
      end
  end


  -- Localisation
  ImportMezInRealLocale = function()
      if(Turbine.Shell.IsCommand("zusatzmodule")==true)then
        import "DU_81.Palantir.Locale.DE_Mez";
        return "de";
      elseif(Turbine.Shell.IsCommand("conseil")==true)then
        import "DU_81.Palantir.Locale.FR_Mez";
        return "fr";
      else
        import "DU_81.Palantir.Locale.EN_Mez";
        return "en";
      end
  end
  self.MezLanguage = ImportMezInRealLocale();

  -- Useless ?
  self.Refresh = function()			-- Obsolète
  end
    self.getEffects = function ()		-- Obsolète
    --[[
    local el = self.local_player:GetEffects();
      if(el:GetCount() > 0) then
        for x=1,(el:GetCount()) do
          ef = el:Get(x);
          if (ef:IsDebuff()==false)then
            local efName=ef:GetName();
            local elDuration=ef:GetDuration();
                        if (efName == "888Esprit de soliloque") then
              --self.ACD = elDuration;
                            --self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_mez.tga");
                            --self.ABarStarting();
            end end	end	end
      ]]--
  end




  ----------------------------------------------------------------------
  --------------------------- Parsing functions ------------------------
  ----------------------------------------------------------------------

  -- Extract duration off an effect on self
    self.getEffectDuration = function (inef)
      --if (self.AutoDetectDuration == false) then return -1; end
      local el = self.local_player:GetEffects();
      if(el:GetCount() > 0) then
        for x=1,(el:GetCount()) do
          ef = el:Get(x);
          if (ef:IsDebuff()==false)then
            local efName= ef:GetName();
            local elDuration = ef:GetDuration();
            -- remove special character. They can be different in effect name and parsed line (ie: CR)
            local inef2 = string.gsub(inef,"%A", " ");
            local efName2= string.gsub(efName,"%A", " ");
            -- Turbine.Shell.WriteLine("Duration? : "..tostring(inef2).." / "..tostring(efName2));
            if (efName2 == inef2) then --The skill nane used here is retrieved from combat line
              if (elDuration == nil) then
                return -1;
              else
                return elDuration;
      end	end	end	end	end
      return -1;
  end


  -- When dead effects->Cleared seems to be fired instead of individual removing
  PalantirEffectClearedCallback = function(sender, args)
      self.ShowRKbuffs(0); -- Hide WinterStorm's Dot
      self.ShowRKbuffs(-2); -- Hide Charged Runes Dot
      self.WS5_Effect:SetEffect(nil);
      self.WS5_Effect:SetVisible(false);
      -- Reset/Hide some bars
      if ((self.player_class == Turbine.Gameplay.Class.RuneKeeper) or (self.player_class == Turbine.Gameplay.Class.Hunter))then
      self.CTimer:Stop();
      self.CTimerOn=false;
      self.CMask:SetVisible(false);
      self.CMask:SetSize(114,33);
      self.CMask:SetPosition(0,0);
      self.CBar:SetVisible(false);
      self.CBlock = 0;
      end
      if ((self.player_class == Turbine.Gameplay.Class.RuneKeeper) or (self.player_class == Turbine.Gameplay.Class.Hunter))then
      self.DTimer:Stop();
      self.DTimerOn=false;
      self.DMask:SetVisible(false);
      self.DMask:SetSize(114,33);
      self.DMask:SetPosition(0,0);
      self.DBar:SetVisible(false);
      self.DBlock = 0;
      end

  end


  -- Hide effect displayed if it's elapsed
  PalantirEffectRemovedCallback = function(sender, args)
      local ef = args.Effect;
      if (ef == nil) then return; end
      local efName = ef:GetName();
      --Turbine.Shell.WriteLine("Palantir: effect removed: ".. efName);
      local oldef = self.WS5_Effect:GetEffect();
      if (oldef ~= nil)then -- Hide WS5_Effect
        local oldefName = oldef:GetName();
        if (efName == oldefName) then
          self.WS5_Effect:SetEffect(nil);
          self.WS5_Effect:SetVisible(false);
        end
      end
      if (string.find(efName, DU_81.Palantir.Locale.RKeeper[8]) ~= nil) then -- Winter-storm
      --Turbine.Shell.WriteLine("Palantir: effect removed: ".. efName);
        if (string.find(efName, tostring(self.VisibleDots)) ~= nil) then
        --if (self.PlayerInCombat == false) then
          -- EffectAdded "WS 3" can trigger BEFORE EffectRemoved "WS 2"
          self.ShowRKbuffs(0);
        end
      end
      if (string.find(efName, DU_81.Palantir.Locale.RKeeper[7]) ~= nil) then -- Charged Runes
        self.ShowRKbuffs(-2); -- Hide Charged Runes Dot
      end
  end


  -- A new effect was detected on self
  PalantirEffectAddedCallback = function(sender, args)
      -- Parse effects only when an effect is added for RK storms buffs.
      local el = self.local_player:GetEffects();
      local ef = el:Get(args.Index);
      local efName = ef:GetName();
      --Turbine.Shell.WriteLine("Palantir: new effect added: ".. efName);
      if (ef:IsDebuff()==false)then
        local elDuration=ef:GetDuration();
        if (self.player_class == Turbine.Gameplay.Class.RuneKeeper)then
          local player_attributes = self.local_player:GetClassAttributes();
          local rk_equi = player_attributes:GetAttunement();
          if (rk_equi <10) then	-- Damage attunement, show combat buffs
            if (efName == DU_81.Palantir.Locale.RKeeper[5]) then
              self.CCD = elDuration;
              self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_rk.tga");
              if (self.CBlock ~= 0)then
                self.CTimer:Stop();
                self.CTimerOn=false;
              end
              self.CBarStarting();
            elseif (efName == DU_81.Palantir.Locale.RKeeper[6]) then
              self.DCD = elDuration;
              self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_rk.tga");
              if (self.DBlock ~= 0)then
                self.DTimer:Stop();
                self.DTimerOn=false;
              end
              self.DBarStarting();
            elseif (string.find(efName, DU_81.Palantir.Locale.RKeeper[7]) ~= nil) then -- Charged Runes
              self.ShowRKbuffs(-1);
            elseif (string.find(efName, DU_81.Palantir.Locale.RKeeper[8]) ~= nil) then -- Winter-storm
              local lvl = string.match (efName, '%d');
              if (tonumber(lvl) == 5) then
                self.ShowRKbuffs(0);
                self.WS5_Effect:SetEffect(ef);
                self.WS5_Effect:SetVisible(true);
              else
                self.ShowRKbuffs(tonumber(lvl));
              end
            end
          else -- Heal attunment, show self-heals
            if (string.find(efName, DU_81.Palantir.Locale.RKeeper[9]) ~= nil)then
              self.CCD = elDuration;
              self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_heal.tga");
              if (self.CBlock ~= 0)then
                self.CTimer:Stop();
                self.CTimerOn=false;
              end
              self.CBarStarting();
            elseif (string.find(efName, DU_81.Palantir.Locale.RKeeper[10]) ~= nil)then --Allégorie de la santé - niveau 2
              self.DCD = elDuration;
              self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_heal.tga");
              if (self.DBlock ~= 0)then
                self.DTimer:Stop();
                self.DTimerOn=false;
              end
              self.DBarStarting();
            end
          end
        elseif (self.player_class == Turbine.Gameplay.Class.Minstrel)then
          if (string.find(efName, DU_81.Palantir.Locale.Minstr[4]) ~= nil) then -- "C\197\147ur de la communaut\195\169"
            self.DCD = elDuration;
            self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_heal.tga");
            self.DBarStarting();
          end
        elseif (self.player_class == Turbine.Gameplay.Class.Hunter)then
          if (string.find(efName, DU_81.Palantir.Locale.Chasso[3]) ~= nil) then -- -- Fleetness Célérité (ou Improved)
            self.CCD = elDuration;
            self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_buff.tga");
            self.CBarStarting();
          elseif (string.find(efName, DU_81.Palantir.Locale.Chasso[4]) ~= nil) then -- --  Needful Haste
            self.DCD = elDuration;
            self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_buff.tga");
            self.DBarStarting();
          end
        end
      end
  end



  -- Renvoie un nom sans les articles éventuels
  self.clean_name = function(name)
      -- Articles virés: [L][L*] [The][the] avec ou sans espace après, majuscule ou minuscule.
      -- Oyapok, LeOyapok, Le Oyapok, LaOyapok, La Oyapok, L' Oyapok, L’Oyapok, The Oyapok
      if (name == nil) then return nil; end
      if (self.MezLanguage =="fr")then
        return string.gsub(name, '^[Ll].-(%u)', '%1');
      elseif (self.MezLanguage =="en")then
        return string.gsub(name,"^[Tt]he ","");
      else -- de
        name = string.gsub(name, "^[Dd]ie ", "");
        name = string.gsub(name, "^[Dd]em ", "");
        name = string.gsub(name, "^[Dd]er ", "");
        name = string.gsub(name, "^[Dd]en ", "");
        return name;
      end
      return name;
  end



  -- parse one line of combat for buffs/debuffs
  self.ParseMezLine= function (line)
      local match, caster_name, target_name, skill_name, Writ_lvl, skill_tier;

      -- FR: Les compétences avec / sans dégats associés n'utilisent pas la même phrase
      if(self.MezLanguage == "fr")then
        -- FR: Compétences sans dégâts: Debuffs, Mez, etc... (Dégâts absorbés ==> Texte différent).
        --         L' Oyapok a marqué un coup avec Lumière aveuglante améliorée sur le Grifferoche juvénile.
        --RESIST-- Oyapok a essayé d'utiliser Signe du pouvoir : commandement amélioré sur le Mannequin d'entraînement mais il a résisté la tentative.
        --         La ligne "résisté" est à la place de la ligne concernée
        --IMMUNE-- Oyapok a essayé d'utiliser Déclencher Hébétement sur le Mannequin d'entraînement mais il a était immunisé contre la tentative.
        --         La ligne "immunisé" suit la ligne du debuff. (Pas exploité par Palantir II v2.10)
        caster_name, skill_name, target_name = string.match(line, '^(.*) a marqu\195\169 un coup.* avec (.*) sur (.*)%.$');
        caster_name = self.clean_name(caster_name);
        target_name = self.clean_name(target_name);
        if ((skill_name ~= nil) and (caster_name == self.player_name)) then
          -- 3: Skill returned by line parsing, no damage, only French
          return 3, target_name, skill_name, -1;
        end
        -- FR: Compétences avec dégâts associés: Allégorie du Froid, Flèche de Bard
        -- Gemstone a infligé un coup critique avec Allégorie du froid sur l’Archer des parjures pour 1,163 points de type Froid à l'entité Moral.
        -- Blosco a infligé un coup critique avec Flèche de Bard sur le Crébain nicheur pour 1,456 points de type Beleriand à l'entité Moral.
        caster_name, skill_name, target_name = string.match(line, '^(.*) a inflig\195\169 un coup.* avec (.*) sur (.*)%.$');
        if((skill_name ~= nil) and (caster_name == self.player_name)) then
          caster_name = self.clean_name(caster_name);
          target_name = self.clean_name(target_name);
          -- Tick Allégorie du feu,: if "niveau X" in the line, it's only a tick, let's find the Tier, but don't return a cast!
          -- Gemstone a infligé un coup avec Allégorie du feu - niveau 1 sur l’Archer des parjures pour 220 points de type Feu
          Writ_lvl = string.match(skill_name,"^.*niveau (.*)$");
          if(Writ_lvl ~= nil) then
            skill_tier = tonumber(Writ_lvl);
            self.RK_2lvl = skill_tier;
            -- 0: Writ's tick line, don't return.
            return 0, "", "", -1;
          end
          -- 2: Skill returned by line parsing, with damage, only French (Allégorie du Froid, Flèche de Bard)
          return 2, target_name, skill_name, -1;
        end
      -- EN, DE: Damage or not, line will be the same ...
      elseif(self.MezLanguage =="en")then
        caster_name ,skill_name, target_name = string.match(line,"^(.*) scored a .*hit with (.*) on (.*)%.$");
        caster_name = self.clean_name(caster_name);
        target_name = self.clean_name(target_name);
        if((skill_name ~= nil) and (caster_name == self.player_name))then
        -- Writ of Fire: if "Tier X" in the line, it's only a tick, let's find the Tier, but don't return a cast!
        -- Gemstone scored a hit with Writ of Fire - Tier 1 on the Celebrant Brigand for 214 Fire damage to Morale.
          Writ_lvl = string.match(skill_name,"^.*Tier (.*)$");
          if(Writ_lvl ~= nil) then
            skill_tier = tonumber(Writ_lvl);
            self.RK_2lvl = skill_tier;
            -- 0: Writ's tick line, don't return.
            return 0, "", "", -1;
          end
        end
        if ((skill_name ~= nil) and (caster_name == self.player_name)) then
          -- 1: Skill returned by line parsing
          return 1, target_name, skill_name, -1;
        end
      elseif(self.MezLanguage =="de")then
        caster_name, skill_name, target_name = string.match(line,"^(.*) gelang ein .*Treffer mit \"(.*)\" gegen (.*)%.$");
        caster_name = self.clean_name(caster_name);
        target_name = self.clean_name(target_name);
        if((skill_name ~= nil) and (caster_name == self.player_name))then
        -- Sinnbild des Feuers: if "Stufe X" in the line, it's only a tick, let's find the Tier, but don't return a cast!
        -- Dem Gemstone gelang ein Treffer mit "Sinnbild des Feuers - Stufe 2" gegen den Ochsensippe-Schildträger für 289 Punkte Schaden des Typs "Feuer" auf Moral.
        Writ_lvl = string.match(skill_name,"^.*Stufe (.*)$");
          if(Writ_lvl ~= nil) then
            skill_tier = tonumber(Writ_lvl);
            self.RK_2lvl = skill_tier;
            -- 0: Writ's tick line, don't return.
            return 0, "", "", -1;
          end
        end
        if ((skill_name ~= nil) and (caster_name == self.player_name)) then
          -- 1: Skill returned by line parsing
          return 1, target_name, skill_name, -1;
        end
      end

      -- FR, EN, DE: Writ of Cold, slow effect have been resisted: Ligne précédente d'évitements du debuff lenteur
      -- Partial (debuff) resist: Gemstone a essayé d'utiliser Allégorie du froid - niveau 3 sur le Archer du Clan du Bœuf mais elle a résisté la tentative.
      --                          Gemstone a infligé un coup avec Allégorie du froid sur le Archer du Clan du Bœuf pour 1,163 points de type Froid à l'entité Moral.
      -- Full résist:             Gemstone a essayé d'utiliser Allégorie du froid sur le Archer des parjures mais il a résisté la tentative
      -- Partial (debuff) resist: Gemstone tried to use Writ of Cold - Tier 1 on the Oathbreaker Archer but he resisted the attempt.
      --                          Gemstone scored a hit with Writ of Cold on the Oathbreaker Archer for 751 Frost damage to Morale.
      -- Full résist:             Gemstone tried to use Writ of Cold on the Oathbreaker Archer but he resisted the attempt.
      -- Partial (debuff) resist: Der Gemstone wollte den Eidbrecher-Bogenschütze mit "Sinnbild der Kälte - Stufe 3" treffen, aber er konterte den Versuch mit "Widerstehen".
      --                          Dem Gemstone gelang ein Treffer mit "Sinnbild der Kälte" gegen den Eidbrecher-Bogenschütze für 645 Punkte Schaden des Typs "Frost" auf Moral.
      -- Full résist:             Der Gemstone wollte den Eidbrecher-Bogenschütze mit "Sinnbild der Kälte" treffen, aber er konterte den Versuch mit "Widerstehen".
      -- Partial (debuff) resist  ==> Prevent next Damage line starting a TimerBar		.
      -- Full resist              ==> no Tier shown, no damage line after.
      if (self.MezLanguage =="fr")then
        caster_name, Writ_lvl, target_name = string.match(line, '^(.*) a essay\195\169 d\'utiliser All\195\169gorie du froid.*niveau (.*) sur (.*) mais .* la tentative%.$');
      elseif(self.MezLanguage =="en")then
        caster_name, Writ_lvl, target_name = string.match(line, '^(.*) tried to use Writ of Cold.*Tier (.*) on (.*) but he.*%.$');
      else -- de
        caster_name, target_name, Writ_lvl = string.match(line, '^(.*) wollte (.*) mit \"Sinnbild der Kälte.*Stufe (.*)\" treffen, aber er konterte.*%.$');
      end
      caster_name = self.clean_name(caster_name);
      if((Writ_lvl ~= nil) and (caster_name == self.player_name)) then
        skill_tier = tonumber(Writ_lvl);
        if ((skill_tier == 1) or (skill_tier == 2) or (skill_tier == 3)) then
          -- 1.2.3: Level of Writ of frost but only when resisted :(
          self.RK_1lvl = skill_tier;
          self.RK_1resist = true;
        end
        return 0, "", "", -1;
      end

      -- FR, EN, DE: Buffs "benefit"
      -- Oyapok a appliqué un benefit avec In extremis Terenndul.
      -- Oyapok a appliqué un benefit avec Connaissance de l'air continue au Esprit du corbeau.
      -- Oyapok a appliqué un benefit avec Signe du pouvoir : Intégrité Oyapok.
      if (self.MezLanguage =="fr")then
        caster_name, skill_name, target_name = string.match(line, '^(.*) a appliqu\195\169 un benefit avec (.*) ([^%s]+)%.$');
        if ((skill_name~= nil) and (string.find(skill_name," au ") ~=nil))then
          caster_name, skill_name, target_name = string.match(line, '^(.*) a appliqu\195\169 un benefit avec (.*) au (.*)%.$');
        end
      elseif(self.MezLanguage =="en")then
        caster_name, skill_name, target_name = string.match(line,"^(.*) applied a benefit with (.*) on (.*)%.$");
      elseif(self.MezLanguage =="de")then -- de
        caster_name, skill_name, target_name = string.match(line,"^(.*) wandte \"Vorteil\" mit \"(.*)\" auf (.*) an%.$");
      end
      caster_name = self.clean_name(caster_name);
      target_name = self.clean_name(target_name);
      if ((skill_name~= nil) and (caster_name == self.player_name)) then
        -- 4: Benefit returned by line parsing
        if (self.player_name == target_name) then
          -- if Target = caster, retrieve duration.
          return 4, target_name, skill_name, self.getEffectDuration(skill_name);
        else
          return 4, target_name, skill_name, -1;
        end
      end
      return 0, "", "", -1;
  end




  -- parse combat text
  ParseMezText= function (sender, args)
      -- Parse line for Warning
      if (args.ChatType == Turbine.ChatType.Say) then
        ParseWarningText(sender, args);
        return;
      elseif (args.ChatType ~= Turbine.ChatType.PlayerCombat) then return;
      end

      -- grab line from combat log, strip it of color, trim it, and parse it according to the localized parsing function
      local mezType,targetName,skillName, sDuration	= self.ParseMezLine(string.gsub(string.gsub(args.Message,"<rgb=#......>(.*)</rgb>","%1"),"^%s*(.-)%s*$", "%1"));
      if (mezType == nil) then -- For safety
        return;
      end
      -- Mez Type 0: Writ's tick line, don't return.
      -- Mez Type 1: Skill returned by line parsing English or German.
      -- Mez Type 2: Skill returned by line parsing, with damage, only French. (Allégorie du Froid, Flèche de Bard)
      -- Mez Type 3: Skill returned by line parsing, no damage, only French.
      -- Mez Type 4: Benefit returned by line parsing.

      if (mezType >= 1) then
      --[[ -- Maybe usefull later ?
        local advanced_skill = false;
        if (string.find(skillName, "Advanced") ~= nil) then
          advanced_skill = true;
        end
        if (string.find(skillName, "m\195\169lior\195\169") ~= nil) then
          advanced_skill = true;
        end
        if (string.find(skillName, "Verbesser") ~= nil) then
          advanced_skill = true;
        end   ]]--

        --Turbine.Shell.WriteLine("Palantir: Lined parsed, detected: "..skillName);
        -- Lore Master
        if (self.player_class == Turbine.Gameplay.Class.LoreMaster)then
          for k,v in pairs(DU_81.Palantir.Locale.LMaste)
          do  if string.find(skillName, v) ~= nil then
              mezType = 10 + k;
            end
          end
          if (mezType == 11) then -- Mez Lumière aveuglante, possible dual Timer
            if (self.ABlock == 0)then
              self.ACD = self.LM_1;
              self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_mez.tga");
              self.ABarStarting();
            elseif (self.ATimer.count >1) then
              -- Set ABar timer as dual timer.
              self.ABlock = self.ATimer.count;
              self.ATimer.count = 0
              self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_mez.tga");	--Removes redif applicable
            end
          elseif (mezType == 12) then -- Root Connaissance des plantes, possible dual timer (but rare!)
            if (self.BBlock == 0)then
              self.BCD = self.LM_2;
              self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
              self.BBarStarting();
            elseif (self.BTimer.count >1) then
              -- Set BBar timer as dual timer, only if more than 1" later
              self.BBlock = self.BTimer.count;
              self.BTimer.count = 0
              self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
            end
          elseif (mezType == 13) or (mezType == 15) then -- Debuff Signe du pouvoir : Injonction, et commandement amélioré
            -- Prediction is debuff on same Tgt(AOE): renew the bar each time
            self.CCD = self.LM_3;
            self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_debuff.tga");
            if (self.CBlock ~= 0)then
              self.CTimer:Stop();
              self.CTimerOn=false;
            end
            self.CBarStarting();
          elseif (mezType == 14) then -- Debuff Signe du pouvoir : Intégrité
            -- Prediction is a buff sequence every time buff is (near) down:renew if less than 15sec remaining
            -- Turbine.Shell.WriteLine("Palantir, AntiStun: "..skillName.." / "..tostring(sDuration));
            if (sDuration == -1) then
              self.DCD = self.LM_4;
            else
              self.DCD = sDuration;
              if (self.AutoDetectDuration == true) then self.LM_4 = sDuration end
            end
            self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_buff.tga");
            if (self.DBlock ~= 0)then
              if ((self.DCD - self.DTimer.count) <= 15)then
                self.DTimer:Stop();
                self.DTimerOn=false;
                self.DBarStarting();
              end
            else
              self.DBarStarting();
            end
          end


        -- Minstrel
        elseif (self.player_class == Turbine.Gameplay.Class.Minstrel)then
          for k,v in pairs(DU_81.Palantir.Locale.Minstr)
          do  if string.find(skillName, v) ~= nil then
              mezType = 20 + k;
            end
          end
          if (mezType == 21) then -- Fear no CD reset, & CD > Duration.
            self.ACD = self.MN_1;
            self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_fear.tga");
            self.ABarStarting();
          elseif (mezType == 22) then -- Chant des morts no CD reset, & CD > Duration.
            self.BCD = self.MN_2;
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
            self.BBarStarting();
          elseif (mezType == 23) then -- Esprit de Soliloque CD <= Duration, dual possible
            if (self.CBlock == 0)then
              if (sDuration == -1) then
                self.CCD = self.MN_3;
              else
                self.CCD = sDuration;
                if (self.AutoDetectDuration == true) then self.MN_3 = sDuration end
              end
              self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_heal.tga");
              self.CBarStarting();
            elseif (self.CTimer.count >1) then
            -- Set CBar as dual timer
              self.CBlock = self.CTimer.count;
              self.CTimer.count = 0
              --self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_heal.tga");
            end
          end

        -- Rune keeper
        elseif (self.player_class == Turbine.Gameplay.Class.RuneKeeper)then
          for k,v in pairs(DU_81.Palantir.Locale.RKeeper)
          do if string.find(skillName, v) ~= nil then
              mezType = 30 + k;
            end
          end

          local player_attributes = LocalPlayer:GetClassAttributes();
          local rk_equi = player_attributes:GetAttunement();
          if ((mezType == 31) and (self.PlayerInCombat	== true)) then -- Writ of Cold, renewbar each cast.
            if (self.RK_1resist == true) then
              self.RK_1resist = false;
            else
              self.ACD = self.RK_1;
              self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_rk.tga");
              if (self.ABlock ~= 0)then
                self.ATimer:Stop();
                self.ATimerOn=false;
              end
              self.ABarStarting();
            end;
          elseif((mezType == 32) and (self.PlayerInCombat	== true)) then -- Writ of fire DOT, renewbar each cast.
            self.BCD = self.RK_2;
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_rk.tga");
            if (self.BBlock ~= 0)then
              self.BTimer:Stop();
              self.BTimerOn=false;
            end
            self.BBarStarting();
          elseif((mezType == 33) and (self.PlayerInCombat	== true)) then -- Scathing Mockery + Improved
            self.BCD = self.RK_2;
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_rk.tga");
            if (self.BBlock ~= 0)then
              self.BTimer:Stop();
              self.BTimerOn=false;
            end
            self.BBarStarting();
          elseif((mezType == 34) and (self.PlayerInCombat	== true)) then -- + Improved Essay of Fire
            self.BCD = self.RK_2;
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_rk.tga");
            if (self.BBlock ~= 0)then
              self.BTimer:Stop();
              self.BTimerOn=false;
            end
            self.BBarStarting();
          end


        -- Burglar
        elseif (self.player_class == Turbine.Gameplay.Class.Burglar)then
          for k,v in pairs(DU_81.Palantir.Locale.Cambio)
          do  if string.find(skillName, v) ~= nil then
              mezType = 40 + k;
            end
          end
          if (mezType == 41) then -- Mez Riddle, red, no dual
            if (self.ABlock == 0)then
              self.ACD = self.CA_1;
              self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_mez.tga");
              self.ABarStarting();
            end
          elseif (mezType == 42) then -- Root Snag , red, no dual 15", CD 5'
            if (self.BBlock == 0)then
              self.BCD = self.CA_2;
              self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
              self.BBarStarting();
            end
          elseif (mezType == 45) then -- Root Quite a Snag, red, dual 30", CD 15""..60" (advanced version)
            if (self.BBlock == 0)then
              self.BCD = self.CA_2adv;
              self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
              self.BBarStarting();
            elseif (self.BTimer.count >1) then
              -- Set BBar timer as dual timer, only if more than 1" later
              self.BBlock = self.BTimer.count;
              self.BTimer.count = 0
              self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
            end
          elseif (mezType == 43) then -- Tour Trick 30"..35"
            if (self.BBlock == 0)then
              self.BCD = self.CA_3;
              self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/c_debuff.tga");
              self.BBarStarting();
            end
          elseif (mezType == 44) then -- Addle Embrouillement 30" CD15"--
            if (self.DBlock == 0)then
              self.DCD = self.CA_4;
              self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_debuff.tga");
              self.DBarStarting();
            elseif (self.DTimer.count >1) then
              -- Set DBar timer as dual timer, only if more than 1" later
              self.DBlock = self.DTimer.count;
              self.DTimer.count = 0
              self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_debuff.tga");
            end
          end


        -- Hunter
        elseif (self.player_class == Turbine.Gameplay.Class.Hunter)then
          for k,v in pairs(DU_81.Palantir.Locale.Chasso)
          do  if string.find(skillName, v) ~= nil then
              mezType = 50 + k;
            end
          end
          if (mezType == 51) then -- Fear no CD reset, & CD(30") > Duration(15").
            self.ACD = self.CH_1;
            self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_fear.tga");
            self.ABarStarting();
          elseif (mezType == 52) then -- Root no CD reset, & CD(3') > Duration(30").
            self.BCD = self.CH_2;
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
            self.BBarStarting();
          elseif (mezType == 55) then -- Distracting Shot + legendary trait, Duration(30") CD 30".
          --[[ -- Maybe later ?
            self.ACD = self.CH_1bis;
            self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_mez.tga");
            if (self.ABlock ~= 0)then
              self.ATimer:Stop();
              self.ATimerOn=false;
            end
            self.ABarStarting(); ]]--
          end
        end
      elseif (mezType == 0) then
        -- Nothing
      end
  end





  --Window containing all Timer Bars
  self.MezBG=Turbine.UI.Window();
  self.MezBG:SetVisible(true);
  self.MezBG:SetParent(self);
  self.MezBG:SetSize(114,33);
  self.MezBG:SetPosition(0,25);
  self.MezBG:SetBackground("DU_81/Palantir/Resources/Mez/mezbg.tga");
  self.MezBG:SetVisible(true);
  self.MezBG:SetWantsKeyEvents(false);
  self.MezBG:SetWantsUpdates(true);
  self.MezBG:SetMouseVisible(false);
  self.MezBG:SetBlendMode(4);
  self.MezBG:SetZOrder(10);


  -- Container for RK's effects and dots
  self.MyPts = Turbine.UI.Window();
    self.MyPts:SetParent(self.MezBG);
  self.MyPts:SetVisible(true);
  self.MyPts:SetSize(37,33); -- 47,57,67,77 to show points.
  self.MyPts:SetPosition(0,0);
  self.MyPts:SetBackground("DU_81/Palantir/Resources/Mez/rk_dots.tga");
  self.VisibleDots = 0;
  self.MyCrit = Turbine.UI.Window();
    self.MyCrit:SetParent(self.MezBG);
  self.MyCrit:SetVisible(true);
  self.MyCrit:SetSize(10,33);
  self.MyCrit:SetPosition(0,0);
  self.MyCrit:SetBackground("DU_81/Palantir/Resources/Mez/rk_crit.tga");
  self.WS5_Effect = Turbine.UI.Lotro.EffectDisplay();
    self.WS5_Effect:SetParent(self.MezBG);
  self.WS5_Effect:SetVisible(false);
  self.WS5_Effect:SetSize(24,24);
  self.WS5_Effect:SetPosition(45,20);
  self.ShowRKbuffs = function(num)
      if (num == 1 ) then
        self.MyPts:SetSize(47,33); -- 47,57,67,77 to show points.
        self.VisibleDots = 1;
        self.WS5_Effect:SetVisible(false); -- Safety
      elseif (num == 2 ) then
        self.MyPts:SetSize(57,33); -- 47,57,67,77 to show points.
        self.VisibleDots = 2;
        self.WS5_Effect:SetVisible(false); -- Safety
      elseif (num == 3 ) then
        self.MyPts:SetSize(67,33); -- 47,57,67,77 to show points.
        self.VisibleDots = 3;
        self.WS5_Effect:SetVisible(false); -- Safety
      elseif (num == 4 ) then
        self.MyPts:SetSize(77,33); -- 47,57,67,77 to show points.
        self.VisibleDots = 4;
        self.WS5_Effect:SetVisible(false); -- Safety
      elseif (num == 0 ) then
        self.MyPts:SetSize(10,33); -- 47,57,67,77 to show points.
        self.VisibleDots = 0;
      elseif (num == -1 ) then
        self.MyCrit:SetSize(114,33);-- Show Crit buff
      elseif (num == -2 ) then
        self.MyCrit:SetSize(10,33);
      end
  end

  -- Container for WARNING
  self.ShowWarnings = true;
  self.TestPanel=Turbine.UI.Control();
  self.TestPanel:SetParent(self);
  self.TestPanel:SetSize(420,120); -- step 1
  if (self.MezLanguage =="fr")then -- step 2, you could also use a tga file source
    self.TestPanel:SetBackground("DU_81/Palantir/Resources/Mez/warn_fr.tga")
  elseif (self.MezLanguage =="de")then
    self.TestPanel:SetBackground("DU_81/Palantir/Resources/Mez/warn_de.tga")
  else -- en
    self.TestPanel:SetBackground("DU_81/Palantir/Resources/Mez/warn_en.tga")
  end
  self.PanelVerticalPosition = 65;
  self.TestPanel:SetStretchMode(1); -- step 3
  self.TestPanel:SetPosition(65,self.PanelVerticalPosition);
  self.TestPanel:SetSize(420,120); -- step 4
  self.TestPanel:SetOpacity(1)
  self.TestPanel:SetVisible(false);
  self.TestPanel:SetBlendMode(0);
  self.WarnNowTimer = DU_81.Utilsx.Timer();
  self.WarnNowTimer.AutoReset=true;
  self.WarnNowTimer.Interval=0.1;
  ParseWarningText= function (sender, args)
      if (self.ShowWarnings == false)then return; end
      if (string.find(args.Message, self.player_name) ~= nil) then
        --self.TestPanel:SetVisible(true);
        self.TestPanel:SetOpacity(1)
        self.TestPanel:SetSize(420,120);
        self.TestPanel:SetPosition(-153,self.PanelVerticalPosition);
        self.TestPanel:SetVisible(true);
        self.WarnNowTimer:Start();
      end
  end
  self.WarnNowTimer.TimerElapsed=function(sender,args)
      if (self.WarnNowTimer.count >= 30)then
        self.WarnNowTimer:Stop();
        self.TestPanel:SetVisible(false);
      end
      local mod = self.WarnNowTimer.count*self.WarnNowTimer.Interval - math.floor(self.WarnNowTimer.count*self.WarnNowTimer.Interval);
      local xsize = 420 * ((1.0-mod)/2 + 0.5);
      local ysize = 120 * ((1.0-mod)/2 + 0.5);
      self.TestPanel:SetOpacity(1.0 - mod);
      self.TestPanel:SetSize(xsize,ysize);
      -- self.TestPanel:SetPosition(57+xsize/-2.0, 125-ysize/2.0);
      self.TestPanel:SetPosition(57+xsize/-2.0, (self.PanelVerticalPosition + 60)-ysize/2.0);
  end


  -- Container for Combat Timer
  self.TimerTick = 60;
  self.TimerTickCount = 60;
  self.TimerArea=Turbine.UI.Window();
  self.TimerArea:SetParent(self.MezBG);
  self.TimerArea:SetVisible(true);
  self.TimerArea:SetSize(40,30);-- (114,33);
  self.TimerArea:SetPosition(37,0);--(0,25);
  self.CbtTimeLbl=Turbine.UI.Label();
  self.CbtTimeLbl:SetParent(self.TimerArea);
  self.CbtTimeLbl:SetPosition(0,0);
  self.CbtTimeLbl:SetForeColor(Turbine.UI.Color(1,.5,.5,.7));
  self.CbtTimeLbl:SetBackColor(Turbine.UI.Color(.01,0,0,0));
  self.CbtTimeLbl:SetZOrder(21);
  self.CbtTimeLbl:SetSize(40,24);
  self.CbtTimeLbl:SetMouseVisible(false);
  self.CbtTimeLbl:SetFontStyle( Turbine.UI.FontStyle.Outline );
  self.CbtTimeLbl:SetOutlineColor( Turbine.UI.Color( 1,0, 0, 0 ) );
  self.CbtTimeLbl:SetFont(Turbine.UI.Lotro.Font.TrajanProBold16);
  self.CbtTimeLbl:SetTextAlignment( Turbine.UI.ContentAlignment.MiddleCenter );
  self.CbtTimeLbl:SetText("0:00");
  self.CbtTimerOn=false;
  self.CbtTimer = DU_81.Utilsx.Timer();
  self.CbtTimer.AutoReset=true;
  self.CbtTimer.Interval=1;
  self.CbtTimerStarting = function ()
    self.CbtTimer:Start();
    self.CbtTimeLbl:SetForeColor(Turbine.UI.Color(1,0,1,0));
    self.CbtTimerOn=true;
    self.TimerTickCount = self.TimerTick;
    self.CbtTimeLbl:SetVisible(self.ShowTimer)
  end
  self.CbtTimerStopNow = function ()
        self.CbtTimer:Stop();
    self.CbtTimeLbl:SetForeColor(Turbine.UI.Color(1,.5,.5,.7));
        self.CbtTimerOn=false;
  end
  self.CbtTimer.TimerElapsed=function(sender,args)
    self.TimerTickCount = self.TimerTickCount - 1;
    if (self.TimerTickCount <=0)then
      self.TimerTickCount = self.TimerTick;
    elseif (self.TimerTickCount <=2)then
      self.CbtTimeLbl:SetForeColor(Turbine.UI.Color(1,1,0,0)); -- Red
    elseif (self.TimerTickCount <=6)then
      self.CbtTimeLbl:SetForeColor(Turbine.UI.Color(1,1,1,0)); --Yellow
    else
      self.CbtTimeLbl:SetForeColor(Turbine.UI.Color(1,0,1,0)); --Green
    end
    local value = self.CbtTimer.count;
      if (value >= 3600) then value = value -3600; end-- mx to 59'59
        local sec = math.fmod(value, 3600) / 60;
    if (value >= 60) then
      value = ("%d:%02d"):format(value / 60, value % 60);
    else
      value = ("0:%02d"):format(value);
    end
    self.CbtTimeLbl:SetText(value);
  end


  -- 1st ( left) Effect timer
  self.AMask=Turbine.UI.Window() --Masque pour A bar
  self.AMask:SetVisible(false)
  self.AMask:SetParent(self.MezBG);
  self.AMask:SetSize(114,33);
  self.AMask:SetZOrder(0);
  self.AMask:SetPosition(0,0);
  self.AMask:SetMouseVisible(false);
  self.AMask:SetBackColor(Turbine.UI.Color(.001,0,0,0));
  self.ABar=Turbine.UI.Control(); -- A bar
  self.ABar:SetParent(self.AMask);
  self.ABar:SetZOrder(18);
  self.ABar:SetSize(114,33);
  self.ABar:SetPosition(0,0);
  self.ABar:SetMouseVisible(false);
  self.ABar:SetVisible(false);
  self.ABar2=Turbine.UI.Control(); -- A bar2
  self.ABar2:SetParent(self.AMask);
  self.ABar2:SetZOrder(19);
  self.ABar2:SetSize(114,33);
  self.ABar2:SetPosition(0,0);
  self.ABar2:SetBackground("DU_81/Palantir/Resources/Mez/a_dual.tga");
  self.ABar2:SetMouseVisible(false);
  self.ABar2:SetVisible(false);
  self.ABlock = 0; -- 0: No Mez, -1:Single Mez, 1+:Two Mez
  self.ATimerOn=false;
  self.ATimer = DU_81.Utilsx.Timer();
  self.ATimer.AutoReset=true;
  self.ATimer.Interval=1;
  self.ABarStarting = function ()
      self.ATimer:Start();
      self.ATimerOn=true;
      self.AMask:SetVisible(true);
      self.AMask:SetSize(114,30);
      self.AMask:SetPosition(0,3);
      self.ABar:SetVisible(true);
      self.ABar:SetPosition(0,-3.0);
      self.ABar2:SetVisible(false);
      self.ABar2:SetPosition(0,-3.0);
      self.ABlock = -1;   -- Bar is showing
  end
  self.ATimer.TimerElapsed=function(sender,args)
      if (self.ATimer.count >= self.ACD)then		-- Timers ending; Rest/Hide everything
        self.ATimer:Stop();
        self.ATimerOn=false;
        self.AMask:SetVisible(false);
        self.AMask:SetSize(114,33);
        self.AMask:SetPosition(0,0);
        self.ABar:SetVisible(false);
        self.ABar2:SetVisible(false);
        self.ABlock = 0;
      else
        pix = math.floor((30 * self.ATimer.count)/self.ACD)    -- TimerCount: 1..x, pix: 1..30 (pixels à cacher)
        self.AMask:SetSize(114,30-pix);
        self.AMask:SetPosition(0,3+pix);
        self.ABar:SetPosition(0,0-(3+pix));
        if (self.ABlock > 0) then	-- dual color Timer
          pix2 = math.floor((30 * (self.ATimer.count+self.ABlock))/self.ACD)     -- 1..30  (Pixels de ABar à cacher par ABar2)
          self.ABar2:SetSize(114,3+pix2);           -- ABar2: Taille
          self.ABar2:SetPosition(0,0-(3+pix));		-- ABar2: Même position que ABar
          if (self.ATimer.count+self.ABlock  >= self.ACD)then
            self.ABar2:SetVisible(false);       -- Revert to single Mez timer
            self.ABlock = -1;
            self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_mez.tga");
          else
            self.ABar2:SetVisible(true);      -- Show dual timer
          end
        end
        -- Passer les 5 dernières secondes en rouge
        if (self.player_class ~= Turbine.Gameplay.Class.RuneKeeper)then
          if ((self.ABlock == -1) and (self.ATimer.count >= (self.ACD-5))) then
            self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_red.tga");
          elseif ((self.ABlock > 0) and (self.ATimer.count + self.ABlock >= (self.ACD-5))) then
            self.ABar:SetBackground("DU_81/Palantir/Resources/Mez/a_red.tga");
          end
        end
      end
  end


  -- 2nd Effect timer
  self.BMask=Turbine.UI.Window() --Masque pour B bar
  self.BMask:SetVisible(false)
  self.BMask:SetParent(self.MezBG);
  self.BMask:SetSize(114,33);
  self.BMask:SetZOrder(0);
  self.BMask:SetPosition(0,0);
  self.BMask:SetMouseVisible(false);
  self.BMask:SetBackColor(Turbine.UI.Color(.001,0,0,0));
  self.BBar=Turbine.UI.Control(); -- B bar
  self.BBar:SetParent(self.BMask);
  self.BBar:SetZOrder(16);
  self.BBar:SetSize(114,33);
  self.BBar:SetPosition(0,0);
  self.BBar:SetMouseVisible(false);
  self.BBar:SetVisible(false);
  self.BBar2=Turbine.UI.Control(); -- B bar2
  self.BBar2:SetParent(self.BMask);
  self.BBar2:SetZOrder(17);
  self.BBar2:SetSize(114,33);
  self.BBar2:SetPosition(0,0);
  self.BBar2:SetBackground("DU_81/Palantir/Resources/Mez/b_dual.tga");
  self.BBar2:SetMouseVisible(false);
  self.BBar2:SetVisible(false);
  self.BBlock = 0; -- 0: No Mez, -1:Single Mez, 1+:Two Mez
  self.BTimerOn=false;
  self.BTimer = DU_81.Utilsx.Timer();
  self.BTimer.AutoReset=true;
  self.BTimer.Interval=1;
  self.BBarStarting = function ()
      self.BTimer:Start();
      self.BTimerOn=true;
      self.BMask:SetVisible(true);
      self.BMask:SetSize(114,30);
      self.BMask:SetPosition(0,0);
      self.BBar:SetVisible(true);
      self.BBar:SetPosition(0,0);
      self.BBar2:SetVisible(false);
      self.BBar2:SetPosition(0,0);
            self.BBlock = -1;   -- Bar is showing
  end
  self.BTimer.TimerElapsed=function(sender,args)
      if (self.BTimer.count >= self.BCD)then
        self.BTimer:Stop();
        self.BTimerOn=false;
        self.BMask:SetVisible(false);
        self.BMask:SetSize(114,33);
        self.BMask:SetPosition(0,0);
        self.BBar:SetVisible(false);
        self.BBar2:SetVisible(false);
        self.BBlock = 0;
      else
        pix = math.floor((30 * self.BTimer.count)/self.BCD)
        self.BMask:SetSize(114,30-pix);
        self.BMask:SetPosition(0,pix);
        self.BBar:SetPosition(0,0-pix);

        if (self.BBlock > 0) then	-- dual color Timer
          pix2 = math.floor((30 * (self.BTimer.count+self.BBlock))/self.BCD)     -- 1..30  (Pixels de BBar à cacher par BBar2)
          self.BBar2:SetSize(114,pix2);         -- BBar2: Taille
          self.BBar2:SetPosition(0,0-pix);		-- BBar2: Même position que BBar
          if (self.BTimer.count+self.BBlock  >= self.BCD)then
            self.BBar2:SetVisible(false);       -- Revert to single Mez timer
            self.BBlock = -1;
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_root.tga");
          else
            self.BBar2:SetVisible(true);      -- Show dual timer
          end
        end
        -- Passer les 5 dernières secondes en rouge
        if (self.player_class ~= Turbine.Gameplay.Class.RuneKeeper)then
          if ((self.BBlock == -1) and (self.BTimer.count >= (self.BCD-5))) then
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_red.tga");
          elseif ((self.BBlock > 0) and (self.BTimer.count + self.BBlock >= (self.BCD-5))) then
            self.BBar:SetBackground("DU_81/Palantir/Resources/Mez/b_red.tga");
          end
        end
      end
  end


  -- 3rd Effect timer
  self.CMask=Turbine.UI.Window() --Masque pour C bar
  self.CMask:SetVisible(false)
  self.CMask:SetParent(self.MezBG);
  self.CMask:SetSize(114,33);
  self.CMask:SetZOrder(0);
  self.CMask:SetPosition(0,0);
  self.CMask:SetMouseVisible(false);
  self.CMask:SetBackColor(Turbine.UI.Color(.001,0,0,0));
  self.CBar=Turbine.UI.Control(); -- C bar
  self.CBar:SetParent(self.CMask);
  self.CBar:SetZOrder(14);
  self.CBar:SetSize(114,33);
  self.CBar:SetPosition(0,0);
  self.CBar:SetMouseVisible(false);
  self.CBar:SetVisible(false);
  self.CBar2=Turbine.UI.Control(); -- C bar2
  self.CBar2:SetParent(self.CMask);
  self.CBar2:SetZOrder(15);
  self.CBar2:SetSize(114,33);
  self.CBar2:SetPosition(0,0);
  self.CBar2:SetBackground("DU_81/Palantir/Resources/Mez/c_dual.tga");
  self.CBar2:SetMouseVisible(false);
  self.CBar2:SetVisible(false);
  self.CBlock = 0; -- 0: No Mez, -1:Single Mez, 1+:Two Mez
  self.CTimerOn=false;
  self.CTimer = DU_81.Utilsx.Timer();
  self.CTimer.AutoReset=true;
  self.CTimer.Interval=1;
  self.CBarStarting = function ()
      self.CTimer:Start();
      self.CTimerOn=true;
      --if (self.IsVisible() == true) then
        self.CMask:SetVisible(true);
      --end
      self.CMask:SetSize(114,33);
      self.CMask:SetPosition(0,0);
      self.CBar:SetVisible(true);
      self.CBar:SetPosition(0,0);
      self.CBar2:SetVisible(false);
      self.CBar2:SetPosition(0,0);
            self.CBlock = -1;   -- Bar is showing
  end
  self.CTimer.TimerElapsed=function(sender,args)
      if (self.CTimer.count >= self.CCD)then
        self.CTimer:Stop();
        self.CTimerOn=false;
        self.CMask:SetVisible(false);
        self.CMask:SetSize(114,33);
        self.CMask:SetPosition(0,0);
        self.CBar:SetVisible(false);
        self.CBar2:SetVisible(false);
        self.CBlock = 0;
      else
        pix = math.floor((30 * self.CTimer.count)/self.CCD)
        self.CMask:SetSize(114,30-pix);
        self.CMask:SetPosition(0,pix);
        self.CBar:SetPosition(0,0-pix);

        if (self.CBlock > 0) then	-- dual color Timer
          pix2 = math.floor((30 * (self.CTimer.count+self.CBlock))/self.CCD)     -- 1..30  (Pixels de CBar à cacher par CBar2)
          self.CBar2:SetSize(114,pix2);         -- CBar2: Taille
          self.CBar2:SetPosition(0,0-pix);		-- CBar2: Même position que CBar
          if (self.CTimer.count+self.CBlock  >= self.CCD)then
            self.CBar2:SetVisible(false);       -- Revert to single Mez timer
            self.CBlock = -1;
            self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_heal.tga");
          else
            self.CBar2:SetVisible(true);      -- Show dual timer
          end
        end
        -- Passer les 5 dernières secondes en rouge
        --if ((self.CBlock == -1) and (self.CTimer.count >= (self.CCD-5))) then
        --	self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_red.tga");
        --elseif ((self.CBlock > 0) and (self.CTimer.count + self.CBlock >= (self.CCD-5))) then
        --	self.CBar:SetBackground("DU_81/Palantir/Resources/Mez/c_red.tga");
        --end
      end
  end


  -- Last (right) Effect timer
  self.DMask=Turbine.UI.Window() --Masque pour D bar
  self.DMask:SetVisible(false)
  self.DMask:SetParent(self.MezBG);
  self.DMask:SetSize(114,33);
  self.DMask:SetZOrder(0);
  self.DMask:SetPosition(0,0);
  self.DMask:SetMouseVisible(false);
  self.DMask:SetBackColor(Turbine.UI.Color(.001,0,0,0));
  self.DBar=Turbine.UI.Control(); -- D bar
  self.DBar:SetParent(self.DMask);
  self.DBar:SetZOrder(12);
  self.DBar:SetSize(114,33);
  self.DBar:SetPosition(0,0);
  self.DBar:SetMouseVisible(false);
  self.DBar:SetVisible(false);
  self.DBar2=Turbine.UI.Control(); -- D bar2
  self.DBar2:SetParent(self.DMask);
  self.DBar2:SetZOrder(13);
  self.DBar2:SetSize(114,33);
  self.DBar2:SetPosition(0,0);
  self.DBar2:SetBackground("DU_81/Palantir/Resources/Mez/d_dual.tga");
  self.DBar2:SetMouseVisible(false);
  self.DBar2:SetVisible(false);
  self.DBlock = 0; -- 0: No Mez, -1:Single Mez, 1+:Two Mez
  self.DTimerOn=false;
  self.DTimer = DU_81.Utilsx.Timer();
  self.DTimer.AutoReset=true;
  self.DTimer.Interval=1;
  self.DBarStarting = function ()
      self.DTimer:Start();
      self.DTimerOn=true;
      self.DMask:SetVisible(true);
      self.DMask:SetSize(114,30);
      self.DMask:SetPosition(0,3);
      self.DBar:SetVisible(true);
      self.DBar:SetPosition(0,-3.0);
      self.DBar2:SetVisible(false);
      self.DBar2:SetPosition(0,-3.0);
      self.DBlock = -1;
  end
  self.DTimer.TimerElapsed=function(sender,args)
      if (self.DTimer.count >= self.DCD)then
        self.DTimer:Stop();
        self.DTimerOn=false;
        self.DMask:SetVisible(false);
        self.DMask:SetSize(114,33);
        self.DMask:SetPosition(0,0);
        self.DBar:SetVisible(false);
        self.DBar2:SetVisible(false);
        self.DBlock = 0;
      else
        pix = math.floor((30 * self.DTimer.count)/self.DCD)
        self.DMask:SetSize(114,30-pix);
        self.DMask:SetPosition(0,3+pix);
        self.DBar:SetPosition(0,0-(3+pix));
        if (self.DBlock > 0) then	-- dual color Timer
          pix2 = math.floor((30 * (self.DTimer.count+self.DBlock))/self.DCD)     -- 1..30  (Pixels de DBar à cacher par DBar2)
          self.DBar2:SetSize(114,3+pix2);           -- DBar2: Taille
          self.DBar2:SetPosition(0,0-(3+pix));		-- DBar2: Même position que DBar
          if (self.DTimer.count+self.DBlock  >= self.DCD)then
            self.DBar2:SetVisible(false);       -- Revert to single Mez timer
            self.DBlock = -1;
            self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_heal.tga");
          else
            self.DBar2:SetVisible(true);      -- Show dual timer
          end
        end
        -- Passer les 5 dernières secondes en rouge
        --if ((self.DBlock == -1) and (self.DTimer.count >= (self.DCD-5))) then
        --	self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_red.tga");
        --elseif ((self.DBlock > 0) and (self.DTimer.count + self.DBlock >= (self.DCD-5))) then
        --	self.DBar:SetBackground("DU_81/Palantir/Resources/Mez/d_red.tga");
        --end
      end
  end


  self.Refresh();
        self.Update = function(sender,args)
    if(self.Override==false and self:IsVisible())then
      self.getEffects();
    end
  end
end
--Turbine.Shell.WriteLine("Palantir: " .. tostring(nitem1) .. " (" .. tostring(qitem1)..") );
-- shellCommand1 = Turbine.ShellCommand()
--[[
  ]]--
