import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
--import "DU_81.Utilsx.Class";

ToggleButton = class( Turbine.UI.Window );

function ToggleButton:Constructor()
	Turbine.UI.Window.Constructor( self );
	self:SetVisible(true);
	self:SetSize(126,20); 
	self:SetBackground("DU_81/Palantir/Resources/textbutton_left_normal.tga");
	self.Toggle=0;
	self.label=Turbine.UI.Label();
	self.label:SetParent(self);
	self.label:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
	self.label:SetSize(126,20);
	self.label:SetPosition(0,0);
	self.label:SetForeColor(Turbine.UI.Color(197/255,162/255,57/255));
	self.label:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
	self.label:SetZOrder(20);
	self.label:SetText("General");
	self.label:SetMouseVisible(false);
	
	self.SetText=function(str)
		self.label:SetText(str);
	end
	
	self.MouseEnter=function(sender,args)
		if(self.Toggle==0)then
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_mouseover.tga");
			self.label:SetForeColor(Turbine.UI.Color(252/255,252/255,34/255));
		else
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_highlighted_mouseover.tga");
			self.label:SetForeColor(Turbine.UI.Color(255/255,228/255,104/255));
		end
	end
	
	self.MouseLeave=function(sender,args)
		if(self.Toggle==0)then
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_normal.tga");
			self.label:SetForeColor(Turbine.UI.Color(197/255,162/255,57/255));
		else
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_highlighted_normal.tga");
			self.label:SetForeColor(Turbine.UI.Color(250/255,250/255,2/255));
		end
	end
	
	self.MouseDown=function(sender,args)
		self.label:SetForeColor(Turbine.UI.Color(186/255,186/255,186/255));
		self.doActive();
		if(self.Toggle==0)then
		
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_pressed.tga");
			
		else
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_highlighted_pressed.tga");
		end
	end
	
	self.MouseUp=function(sender,args)
		
	end
	
	self.Click = function(sender,args)
	
	end
	
	self.MouseClick=function(sender,args)
		if(self.Toggle==0)then
			self.Toggle=1;
			self.label:SetForeColor(Turbine.UI.Color(255/255,228/255,104/255));
		end
		self:SetBackground("DU_81/Palantir/Resources/textbutton_left_highlighted_mouseover.tga");
		self.Click();
	end
	
	self.SetToggle=function(num)
		self.Toggle=num;
		if (num==1)then
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_highlighted_normal.tga");
			self.label:SetForeColor(Turbine.UI.Color(250/255,250/255,2/255));
		else
			self:SetBackground("DU_81/Palantir/Resources/textbutton_left_normal.tga");
			self.label:SetForeColor(Turbine.UI.Color(197/255,162/255,57/255));
		end
	end
	self.doActive=function()
	
	end
	
	
	
end