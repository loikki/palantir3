-- English effects
-- Translate as well in SetupWindow.lua 


LMaste={"Blinding Flash",					-- "Blinding Flash", "Improved Blinding Flash"
		"Herb.*lore",						-- "Herb-lore" 
		"Sign of Power.*Command",			-- "Sign of Power: Command", "Improved Sign of Power: Command"
		"Sign of Power.*Righteousness"} 	-- "Sign of Power: Righteousness"

Minstr={"Invocation of Elbereth", -- "Invocation of Elbereth"
		"Song of the Dead",       -- "Song of the Dead"
		"Soliloquy of Spirit",	  -- "Soliloquy of Spirit"
		"Fellowship's Heart"}  --  

RKeeper={"Writ of Cold", 	-- Writ of Cold - Tier 1 Allégorie du froid
		"Writ of Fire",    	-- Writ of Fire 
		"Scathing Mockery",	-- Scathing Mockery + Improved
		"Essay of Fire",	-- + Improved Essay of Fire
		"Thunderous Words",	-- Thunderous Words 
		"Harsh Debate",		-- Harsh Debate
		"Charged Runes",	-- Charged Runes
		"Winter.*storm", 	-- Winter-storm
		"Writ of Health", 	-- Writ of Health
		"Prelude to Hope",}	-- Prelude to Hope	
		
Cambio={"Riddle", -- And Improved
		"A Small Snag",		-- A Small Snag
		"Trick",       		-- (All) Trick : xxx
		"Addle",			-- Addle
		"Quite a Snag"} 	-- Legendary version
		
Chasso={"Bard\'s Arrow",
		"Rain of Thorns",
		"Fleetness", -- And Improved Fleetness
		"Needful Haste",
		"Distracting Shot"}	-- Distracting Shot 30" with leg trait
	
	