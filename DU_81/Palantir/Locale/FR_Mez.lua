-- French effects

--### �=\195\160
--### �=\195\162
--### �=\195\164
--### �=\195\167
--### �=\195\169 en Maj: \195\137
--### �=\195\168
--### �=\195\169 en Maj: \195\138
--### �=\195\187
--### �=\197\147
--### [espace]=\194\160
--### .* pour remplacer une caract�re introuvable par du g�n�rique


LMaste={"Lumi\195\168re aveuglante",
		"Connaissance des plantes",
		"commandement",			   --Signe du pouvoir�: commandement am�lior� 'Signe du pouvoir\194\160:\nInjonction';
		"Int\195\169grit\195\169", --'Signe du pouvoir\194\160:\nInt\195\169grit\195\169'
		"njonction"} 			  -- 'Signe du pouvoir\194\160:\nInjonction'; Nom diff�rent a bas niveau...

Minstr={"Invocation d'Elbereth", ---- 'Invocation d'Elbereth';
		"Chant des morts",      --L['SongOfTheDead'] = 'Chant des morts';
		"Esprit de soliloque",
		"C\197\147ur de la communaut\195\169"}  -- C�ur de la communaut�

RKeeper={"All\195\169gorie du froid", 	-- Writ of Cold - Tier 1 All�gorie du froid
		"All\195\169gorie du feu",    	-- Writ of Fire 
		"Moquerie cinglante",			-- Scathing Mockery + Improved
		"Essai de feu",					-- + Improved Essay of Fire
		"Mots de tonnerre",				-- Thunderous Words 
		"D\195\169bat houleux",			-- Harsh Debate
		"Runes charg\195\169es",		-- Charged Runes
		"Temp.*te hivernale", 	-- Winter-storm Temp�te hivernale - 2
		"All\195\169gorie de la sant\195\169", -- Writ of Health
		"Pr\195\169lude \195\160 l'espoir",}	-- Prelude to Hope	
		
Cambio={"Enigme", 	-- Riddle 'Enigme improvis\195\169e'30"-40" CD1' ou 30"
		"Petit accrochage",	-- ['ASmallSnag'] = 'Petit accrochage'; 15" CD5'
		"Tour",   	-- Trick.
		"Embrouillement",-- ['Addle'] = 'Embrouillement'; debuff 30" CD15"
		"Bel accroc"} --version l�gendaire: ['Bel accroc'] = 'Quite a Snag';  30", CD 15"..60"
		
Chasso={"Fl\195\168che de Bard",			-- Bard\'s Arrow � v�rifier
		"Pluie d\'\195\169pine",			-- Rain of Thorns � v�rifier
		"C\195\169\l195\169rit\195\169", 	-- Fleetness C�l�rit� (ou Improved)
		"H\195\162te n\195\169cessaire",	--  Needful Haste 
		"Tir de distraction"}	-- Distracting Shot
	