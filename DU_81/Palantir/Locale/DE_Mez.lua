-- German effects
--### �=\195\164
--### �=\195\182
-- Translate as well in SetupWindow.lua 

LMaste={"Blendender Blitzstrahl",		-- "Blendender Blitzstrahl", "Verbesserung: Blendender Blitzstrahl"
		"Kr\195\164uterkunde",			-- "Kr\195\164uterkunde"
		"Befehl",						-- "Zeichen der Macht: Befehl", "Zeichen der Macht: Verbesserter Befehl"
		"Rechtschaffenheit"} 			-- "Z.d.M.: Rechtschaffenheit"

Minstr={"Schrei der Valar", 			-- "Schrei der Valar" (Invocation of Elbereth)
		"Lied der Toten",       		-- "Lied der Toten"
		"Selbstgespr\195\164ch des Geistes",	-- "Selbstgespr\195\164ch des Geistes" (Soliloquy)
		"Herz der Gef\195\164hrten"} 	--  TBD Fellowship's Heart Herz der Gef�hrten

RKeeper={"Sinnbild der K\195\164lte", 	-- Writ of Cold - Tier 1 All�gorie du froid
		"Sinnbild des Feuers",    		-- Writ of Fire 
		"Beissende Verh\195\182hnung",	-- Scathing Mockery + Improved
		"Abhandlung des Feuers",		-- + Improved Essay of Fire
		"Worte wie Donnerhall",			-- Thunderous Words 
		"Gereizte Debatte",				-- Harsh Debate
		"Geladen",						-- Charged Runes
		"Wintersturm", 					-- Winter-storm --Wintersturm - 2
		"Sinnbild der Gesundheit", 		-- Writ of Health
		"Pr\195\164ludium der Hoffnung",}	-- Prelude to Hope	--Meister der Trag�die - 1
		
Cambio={"R\195\164tsel", 				-- Riddle
		"Ein kleiner Schnitt", 			-- A Small Snag
		"Trick", --  					-- (All) Trick : xxx
		"Tumult", 						-- Addle 
		"Ein ziemlicher Haken"} 		-- Quite a Snag
		
Chasso={"Bards Pfeil", 					-- "Bard\'s Arrow",
		"Dornenregen", 			-- "Rain of Thorns",
		"Schnellf\195\188.*igkeit", 	-- + "Verbesserte Schnellf\195\188ssigkeit"
		"N\195\182tige Hast",			-- "Needful Haste"" 
		"Distracting Shot"} 			-- Distracting Shot improved with leg trait

