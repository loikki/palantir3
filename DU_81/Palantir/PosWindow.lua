import "DU_81.DUInterface";
import "DU_81.Utilsx";
import "Turbine.UI";
import "Turbine.UI.Lotro";
import "Turbine.Gameplay";
--import "DU_81.Utilsx.Class";

PosWindow = class( Turbine.UI.Window );

function PosWindow:Constructor()
	Turbine.UI.Window.Constructor( self );
	self:SetVisible(true);
	self.ttX=0;
	self.tty=0;
	self.getLeft=0;
	self.getTop=0;
	
	self:SetMouseVisible(false);
	self:SetVisible(false);
	
	self.mouseDown=false;
	self.topBar=Turbine.UI.Window();
	self.topBar:SetVisible(true);
	self.topBar:SetMouseVisible(true);
	self.topBar:SetParent(self);
	self.topBar:SetPosition(0,0);

	self.topBar:SetBackground("DU_81/Palantir/Resources/MoveBar.tga");
	self.barLabel=Turbine.UI.Label();
	self.barLabel:SetParent(self.topBar);
	self.barLabel:SetFont(Turbine.UI.Lotro.Font.Verdana12);
	self.barLabel:SetText("Palantir");
	self.barLabel:SetSize(60,12);
	self.barLabel:SetFontStyle(Turbine.UI.FontStyle.Outline);
	
	self.barLabel:SetMouseVisible(false);
	self.topBarGem = Turbine.UI.Control();
	self.topBarGem:SetParent(self.topBar);
	self.topBarGem:SetSize(21,20);
	self.topBarGem:SetBackground("DU_81/Palantir/Resources/MoveBar_Gem.tga");
	self.topBarGem:SetPosition(0,0);
	self.topBarGem:SetMouseVisible(false);
	self.toolTip=Turbine.UI.Window();
	self.toolTip:SetVisible(true);
	self.toolTip:SetParent(self);
	self.toolTip:SetSize(247,36);
	self.toolTip:SetBackground("DU_81/Palantir/Resources/tooltip.tga");
	self.toolTip:SetPosition(0,0);
	self.toolTip:SetVisible(false);
	self.toolTip:SetZOrder(99);
	
	self.toolLabel=Turbine.UI.Label();
	self.toolLabel:SetParent(self.toolTip);
	self.toolLabel:SetSize(200,20);
	self.toolLabel:SetFont(Turbine.UI.Lotro.Font.TrajanPro16);
	self.toolLabel:SetForeColor(Turbine.UI.Color(0.764,0.709,0.522));
	self.toolLabel:SetText("Palantir");
	self.toolLabel:SetPosition(8,3);
	
	self.shadowWindow=Turbine.UI.Window();
	self.shadowWindow:SetVisible(true);
	self.shadowWindow:SetParent(self);
	self.shadowWindow:SetPosition(0,20);
	
	self.shadowWindow:SetVisible(false);
	
	self.ul = Turbine.UI.Control();
	self.ul:SetParent(self.shadowWindow);
	self.ul:SetSize(10,10);
	self.ul:SetPosition(0,0);
	self.ul:SetBackground("DU_81/Palantir/Resources/ULeftb.tga");
	
	self.left = Turbine.UI.Control();
	self.left:SetParent(self.shadowWindow);
	
	self.left:SetPosition(0,10);
	self.left:SetBackground("DU_81/Palantir/Resources/Left.tga");

	self.ll = Turbine.UI.Control();
	self.ll:SetParent(self.shadowWindow);
	self.ll:SetSize(10,10);
	
	self.ll:SetBackground("DU_81/Palantir/Resources/LLeft.tga");

	self.lower = Turbine.UI.Control();
	self.lower:SetParent(self.shadowWindow);
	
	self.lower:SetBackground("DU_81/Palantir/Resources/Lower.tga");

	self.lr = Turbine.UI.Control();
	self.lr:SetParent(self.shadowWindow);
	self.lr:SetSize(10,10);
	
	self.lr:SetBackground("DU_81/Palantir/Resources/LRight.tga");

	self.right = Turbine.UI.Control();
	self.right:SetParent(self.shadowWindow);
	
	self.right:SetBackground("DU_81/Palantir/Resources/Right.tga");

	self.ur = Turbine.UI.Control();
	self.ur:SetParent(self.shadowWindow);
	self.ur:SetSize(10,10);
	
	self.ur:SetBackground("DU_81/Palantir/Resources/URight.tga");

	self.upper = Turbine.UI.Control();
	self.upper:SetParent(self.shadowWindow);
	
	self.upper:SetPosition(10,0);
	self.upper:SetBackground("DU_81/Palantir/Resources/Upper.tga");

	self.ttTimer=DU_81.Utilsx.Timer();
	self.ttTimer.Interval=0.9;
	self.ttTimer.AutoReset=false;
	

	self.topBar.MouseEnter=function(sender,args)
		self.shadowWindow:SetVisible(true);
			
			self.ttX=Turbine.UI.Display:GetMouseX();
			self.ttY=Turbine.UI.Display:GetMouseY();
			self:SetTooltip();
			if(self.mouseDown==false)then
				self.ttTimer.Start();
			end
	end

	
	self.topBar.MouseLeave=function(sender,args)
		self.shadowWindow:SetVisible(false);
		self.toolTip:SetVisible(false);
		self.ttTimer.Stop();
		self.ttX=0;
		self.ttY=0;
	end


	self.SizeChanged=function(sender,args)
	local test=self:GetParent();
	self.getLeft,self.getTop=test:GetPosition();
	self.posWidth,self.posHeight=test:GetSize();
	
	self.topBar:SetSize(self.posWidth,20);
	self.barLabel:SetPosition((self.posWidth-60+20)/2,4);
	self.shadowWindow:SetSize(self.posWidth,self.posHeight-20);
	self.left:SetSize(10,self.shadowWindow:GetHeight()-20);
	self.ll:SetPosition(0,self.shadowWindow:GetHeight()-10);
	self.lower:SetSize(self.shadowWindow:GetWidth()-20,10);
	self.lower:SetPosition(10,self.shadowWindow:GetHeight()-10);
	self.lr:SetPosition(self.shadowWindow:GetWidth()-10,self.shadowWindow:GetHeight()-10);
	self.right:SetSize(10,self.shadowWindow:GetHeight()-20);
	self.right:SetPosition(self.shadowWindow:GetWidth()-10,10);
	self.ur:SetPosition(self.shadowWindow:GetWidth()-10,0);
	self.upper:SetSize(self.shadowWindow:GetWidth()-20,10);
	end
	
	self.ttTimer.TimerElapsed=function(sender,args)
		self.toolTip:SetVisible(true);
		self.SetTooltip();
	end
	
	self.SetTooltip=function()
		self.toolTip:SetPosition((self.ttX-self.getLeft)+40,(self.ttY-self.getTop)+40);
	end
	self.doMouse=function(dir)
		if(dir==1)then
			self.mouseDown=false;
			self.topBar:SetBackground("DU_81/Palantir/Resources/MoveBar.tga");
			self.topBarGem:SetBackground("DU_81/Palantir/Resources/MoveBar_Gem.tga");
		else
			self.mouseDown=true;
			self.topBar:SetBackground("DU_81/Palantir/Resources/MoveBarClick.tga");
			self.topBarGem:SetBackground("DU_81/Palantir/Resources/MoveBarClick_Gem.tga");
			self.ttTimer.Stop();
		end
	end
	
end