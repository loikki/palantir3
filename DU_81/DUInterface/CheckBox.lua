



CheckBox = class( Turbine.UI.Control );
function CheckBox:Constructor()
	Turbine.UI.Control.Constructor( self );
	self:SetSize(350,16);
	self:SetMouseVisible(false);
	self.checkBox = Turbine.UI.Lotro.CheckBox();
	self.checkBox:SetParent(self);
	self.checkBox:SetCheckAlignment(0);
	self.checkBox:SetPosition(334,0);
	self.checkBox:SetChecked(true);
	
	
	
	self.title=Turbine.UI.Label();
	self.title:SetParent(self);
	self:SetBackColor(Turbine.UI.Color(.9,0,0,0));
	self.title:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
	self.title:SetFontStyle(Turbine.UI.FontStyle.None );
	self.title:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
	self.title:SetText("Awesome Box");
	self.title:SetSize(330,16)
	self.title:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleRight);
	self.title:SetMouseVisible(false);
	function self:SetAlignment(num)
		if(num==0)then
			self.checkBox:SetLeft(0);
			self.title:SetLeft(20);
		else
			self.checkBox:SetLeft(234);
			self.title:SetLeft(0);
		end
	end
	
	
	function self:SetEnabled(dis)
		self.checkBox:SetEnabled(dis);
                if (dis == false) then self.title:SetForeColor(Turbine.UI.Color(0.5,0.749,0.749,0.749));
                else                   self.title:SetForeColor(Turbine.UI.Color(1,  0.96,0.874,0.576));
                end
	end
	
	function self:IsEnabled()
		self.checkBox:IsEnabled(dis);
	end
	
	function self:SetChecked(chk)
		self.checkBox:SetChecked(chk);
	end
	
	function self:SetText(text)
		self.title:SetText(text)
	end
	
	function self:GetText(text)
		return self.title:GetText();
	end
	
	function self:IsChecked()
		return self.checkBox:IsChecked();
	end
	
	self.checkBox.CheckedChanged=function(sender,args)
		self.CheckedChanged();
		self.doActive();
	end
	
	self.CheckedChanged=function()
	
	end
	self.doActive=function()
	
	end
	

end