
--import "DU_81.Utilsx.Class";


Window = class( Turbine.UI.Lotro.Window );
function Window:Constructor(width,height)
	Turbine.UI.Lotro.Window.Constructor( self );
	self:SetVisible(true);
	self.tWindowHeight=100;
	self.tWindowWidth=150;
	self.Height=100;
	self.Width=150;
	self.tBarWidth=35;
	self.mWindow=Turbine.UI.Window();
	self.mWindow:SetVisible(true);
	self.mWindow:SetParent(self);
	self.mWindow:SetPosition(0,0);
	if width == nil then
		width=200;
	end
	if height==nil then
	  height=200;
	end
	self.mWindow:SetSize(width,height);
	self.mWindow:SetMouseVisible(false);
	self:SetSize(width,height);

	self:SetOpacity(0);
	self.bg=Turbine.UI.Window();
	self.bg:SetVisible(true);
	
	self.bg:SetParent(self.mWindow);
	self.bg:SetPosition(36,36);
	self.bg:SetSize(30,30);
	self.bg:SetBackground("DU_81/DUInterface/Resources/window_bgb.tga");
	self.ul=Turbine.UI.Window();
	self.ul:SetVisible(true);
	self.ul:SetParent(self.mWindow);
	self.ul:SetPosition(0,18);
	self.ul:SetSize(36,36);
	self.ul:SetBackground("DU_81/DUInterface/Resources/box_silver_upper_left.tga");
	self.ul:SetStretchMode(2);
		self.l=Turbine.UI.Window();
		self.l:SetVisible(true);
	self.l:SetParent(self.mWindow);
	self.l:SetPosition(0,54);
	self.l:SetSize(36,self:GetHeight()-90);
	self.l:SetBackground("DU_81/DUInterface/Resources/box_silver_side_left.tga");
	self.l:SetStretchMode(2);
	self.ll=Turbine.UI.Window();
	self.ll:SetVisible(true);
	self.ll:SetParent(self.mWindow)
	self.ll:SetPosition(0,self:GetHeight()-36);
	self.ll:SetSize(36,36);
	self.ll:SetBackground("DU_81/DUInterface/Resources/box_silver_bottom_left.tga");
	self.ll:SetStretchMode(2);
	self.u=Turbine.UI.Window();
	self.u:SetVisible(true);
	self.u:SetParent(self.mWindow)
	self.u:SetPosition(36,18);
	self.u:SetSize(self:GetWidth()-72,36);
	self.u:SetBackground("DU_81/DUInterface/Resources/box_silver_upper.tga");
	self.u:SetMouseVisible(false);
	self.u:SetStretchMode(2);
	self.ur=Turbine.UI.Window();
	self.ur:SetVisible(true);
	self.ur:SetParent(self.mWindow)
	self.ur:SetPosition(self:GetWidth()-36,18);
	self.ur:SetSize(36,36);
	self.ur:SetBackground("DU_81/DUInterface/Resources/box_silver_upper_right.tga");
	self.ur:SetStretchMode(2);
	self.r=Turbine.UI.Window();
	self.r:SetVisible(true);
	self.r:SetParent(self.mWindow)
	self.r:SetPosition(self:GetWidth()-36,54);
	self.r:SetSize(36,self:GetHeight()-90);
	self.r:SetBackground("DU_81/DUInterface/Resources/box_silver_side_right.tga");
	self.r:SetStretchMode(2);
	self.lr=Turbine.UI.Window();
	self.lr:SetVisible(true);
	self.lr:SetParent(self.mWindow)
	self.lr:SetPosition(self:GetWidth()-36,self:GetHeight()-36);
	self.lr:SetSize(36,36);
	self.lr:SetBackground("DU_81/DUInterface/Resources/box_silver_lower_right.tga");
	self.lr:SetStretchMode(2);
	self.b=Turbine.UI.Window();
	self.b:SetVisible(true);
	self.b:SetParent(self.mWindow)
	self.b:SetPosition(36,self:GetHeight()-36);
	self.b:SetSize(self:GetWidth()-72,36);
	self.b:SetBackground("DU_81/DUInterface/Resources/box_silver_bottom.tga");
	self.b:SetStretchMode(2);
	self.tbHolder=Turbine.UI.Control();
	self.tbHolder:SetVisible(true);
	self.tbHolder:SetMouseVisible(false);
	self.tbHolder:SetParent(self.mWindow);
	self.tbHolder:SetSize(105,42);
	self.tbHolder:SetPosition(22,-6);
	self.tbLeft=Turbine.UI.Window();
	self.tbLeft:SetVisible(true);
	self.tbLeft:SetParent(self.tbHolder);
	self.tbLeft:SetMouseVisible(false);
	self.tbLeft:SetSize(35,42);
	self.tbLeft:SetBackground("DU_81/DUInterface/Resources/base_box_titlebar_left.tga");
	self.tbRight=Turbine.UI.Window();
	self.tbRight:SetVisible(true);
	self.tbRight:SetParent(self.tbHolder);
	self.tbRight:SetMouseVisible(false);
	self.tbRight:SetSize(35,42);
	self.tbRight:SetPosition(self.tbHolder:GetWidth()-35,0)
	self.tbRight:SetBackground("DU_81/DUInterface/Resources/base_box_titlebar_right.tga");
	self.tbCenter=Turbine.UI.Window();
	self.tbCenter:SetVisible(true);
	self.tbCenter:SetParent(self.tbHolder);
	self.tbCenter:SetMouseVisible(false);
	self.tbCenter:SetSize(self.tbHolder:GetWidth()-70,42);
	self.tbCenter:SetPosition(self.tbLeft:GetLeft()+35,0)
	self.tbCenter:SetBackground("DU_81/DUInterface/Resources/base_box_titlebar_top.tga");
	self.text = Turbine.UI.Label();
	self.text:SetParent(self.tbCenter);
	self.text:SetFont(Turbine.UI.Lotro.Font.TrajanPro18);
	self.text:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleCenter);
	self.text:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
	self.text:SetFontStyle(Turbine.UI.FontStyle.Outline );
	self.text:SetOutlineColor(Turbine.UI.Color(.6,0,0,0));
	self.text:SetTop(15);
	self.text:SetZOrder(99);
	self.text:SetMouseVisible(false);
	self.clButton=Turbine.UI.Window();
	self.clButton:SetVisible(true);
	self.clButton:SetParent(self);
	self.clButton:SetSize(17,17);
	self.clButton:SetBackground("DU_81/DUInterface/Resources/titlebar_X_2.tga");
	
	self.clButton.MouseEnter=function(sender,args)
	self.clButton:SetBackground("DU_81/DUInterface/Resources/titlebar_X_2_mouseover.tga");
	end
	
	self.clButton.MouseLeave=function(sender,args)
	self.clButton:SetBackground("DU_81/DUInterface/Resources/titlebar_X_2.tga");
	end
	
	self.clButton.MouseDown=function(sender,args)
	self.clButton:SetBackground("DU_81/DUInterface/Resources/titlebar_X_2_pressed.tga");
	end
	
	self.clButton.MouseClick=function(sender,args)
	self.clButton:SetBackground("DU_81/DUInterface/Resources/titlebar_X_2.tga");
	self:Activate();
	self:Close();
	end

	self.Activated=function(sender,args)


	end
	
	self.Deactivated=function(sender,args)
		self:SetZOrder(0);
	end
	
	self.ul.MouseClick=function(sender,args)
		self:Activate();
	end
	self.u.MouseClick=function(sender,args)
		self:Activate();
	end
	self.ur.MouseClick=function(sender,args)
		self:Activate();
	end
	self.l.MouseClick=function(sender,args)
		self:Activate();
	end
	self.r.MouseClick=function(sender,args)
		self:Activate();
	end
	self.ll.MouseClick=function(sender,args)
		self:Activate();
	end
	self.b.MouseClick=function(sender,args)
		self:Activate();
	end
	self.lr.MouseClick=function(sender,args)
		self:Activate();
	end
	self.bg.MouseClick=function(sender,args)
		self:Activate();
	end
function self:getFunctions()
	Turbine.Shell.WriteLine("in gf");
	self.fa={implementation="Turbine.UI.Lotro.Window"};
	
	return self.fa;
end
	
end