
--import "DU_81.Utilsx.Class";


Slider = class( Turbine.UI.Control );
function Slider:Constructor()
	Turbine.UI.Control.Constructor( self );
	self:SetWidth(348);
	self:SetMouseVisible(false);
	self:SetBackColor(Turbine.UI.Color(.5,0,0,0));
	self:SetSize(self:GetWidth(),36);
	
	self.sliderBg=Turbine.UI.Control();
	
	self.sliderBg:SetParent(self);
	self.sliderBg:SetBackground("DU_81/DUInterface/Resources/slider_background.tga");
	self.sliderBg:SetSize((self:GetWidth()-59),16);
	self.sliderBg:SetPosition(31,20);
	
	self.min=0;
	self.max=1;
	self.pixPerVal=(self.max-self.min)/(self.sliderBg:GetWidth()-16);
	self.step = 0.02;
	self.pixPerStep=(self.step/self.pixPerVal);
	self.sliderVal=0;
	self.places=2;
	self.lArrow=Turbine.UI.Control();
	self.lArrow:SetParent(self);
	self.lArrow:SetBackground("DU_81/DUInterface/Resources/slider_leftarrow.tga");
	self.lArrow:SetSize(16,16);
	self.lArrow:SetPosition(15,20);
	self.lArrow:SetWantsUpdates(true);
	self.rArrow=Turbine.UI.Control();
	self.rArrow:SetParent(self);
	self.rArrow:SetBackground("DU_81/DUInterface/Resources/slider_rightarrow.tga");
	self.rArrow:SetSize(16,16);
	self.rArrow:SetPosition((31+self.sliderBg:GetWidth()),20);
	self.rArrow:SetWantsUpdates(true);
	self.widget=Turbine.UI.Control();
	self.widget:SetParent(self.sliderBg);
	self.widget:SetSize(16,16)
	self.widget:SetZOrder(20);
	self.widget:SetBlendMode(4);
	self.lIsDown=false;
	self.lDelay=0;
	self.rIsDown=false;
	self.rDelay=0;
	self.lastPos = 0;
	self.dragOff = 0;
	self.isEnabled=true;
	self.oldValue=0;
	self.isDown=false;
	self.widget:SetBackground("DU_81/DUInterface/Resources/slider_widget.tga");
	function round(num, idp)
  	local mult = 10^(idp or 0)
  	return math.floor(num * mult + 0.5) / mult
	end
	
	function self.updatePos()
		widgetX,widgetY=self.widget:GetPosition();
		self.sliderVal=round(widgetX*self.pixPerVal,self.places)+self.min;
	 	if(self.sliderVal < self.min) then self.sliderVal=self.min end
		if(self.sliderVal > self.max) then self.sliderVal=self.max end
		
		self.value:SetText(valPad(self.sliderVal));
		return self.sliderVal;
	end
	
	function valPad(num)
		
		if (num==0)then
			numString="0";
			if (self.places > 0)then
				numString=numString..".";
				for i =1,self.places do
					numString=numString.."0";
				end
			end
		else
			numString=tostring(num);
			dotLoc=string.find(numString,".",0,true);
			strLen=string.len(numString);
			if(dotLoc==nil)then
				if(self.places > 0)then
					numString=numString..".";
					for i =1,self.places do
						numString=numString.."0";
					end
				end
			else
				if(strLen-(dotLoc+1) < self.places)then
					for i =(strLen-(dotLoc-1)),self.places do
						numString=numString.."0";
					end
				end
			end
		end
		return numString;
	end
	
	function init()
		self.sliderBg:SetSize((self:GetWidth()-59),16);
		self.pixPerVal=(self.max-self.min)/(self.sliderBg:GetWidth()-16);
		self.pixPerStep=(self.step/self.pixPerVal);
		self.rArrow:SetPosition((31+self.sliderBg:GetWidth()),20);
		self.value:SetPosition(self:GetWidth()-50,0);
		self.widget:SetPosition(self.sliderVal/self.pixPerVal,0);
		self.updatePos();
	end
	self.title=Turbine.UI.Label();
	self.title:SetPosition(0,0);
	
	self.title:SetParent(self);
	self.title:SetBackColor(Turbine.UI.Color(.5,0,0,0));
	self.title:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
	self.title:SetFontStyle(Turbine.UI.FontStyle.None );
	self.title:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
	self.title:SetText("Ambient Light");
	self.title:SetSize(self.sliderBg:GetWidth(),20)
	self.title:SetTextAlignment(Turbine.UI.ContentAlignment.TopLeft);
	self.title:SetMouseVisible(false);
	self.value=Turbine.UI.Label();
	self.value:SetPosition(self:GetWidth()-50,0);
	self.value:SetText(valPad(self.updatePos()));
	self.value:SetParent(self);
	self.value:SetBackColor(Turbine.UI.Color(.5,0,0,0));
	self.value:SetFont(Turbine.UI.Lotro.Font.TrajanPro14);
	self.value:SetFontStyle(Turbine.UI.FontStyle.None );
	self.value:SetForeColor(Turbine.UI.Color(1,0.96,0.874,0.576));
	self.value:SetTextAlignment(Turbine.UI.ContentAlignment.TopRight);
	self.value:SetSize(50,20);
	self.value:SetMouseVisible(false);

	

	
	self.sliderBg.MouseClick = function(sender,args)
		self.doActive();
		if(self.isEnabled==true)then
			self.widget:SetPosition((args.X-8),0);
			self.updatePos();
			if((self.sliderVal % self.step)~=0)then
				pos=(round((self.sliderVal/self.step),self.places))*self.step;
				self:SetValue(pos);
			end
		end
	end
	
	self.lArrow.MouseHover = function(sender,args)
		if(self.isEnabled==true)then
			self.lArrow:SetBackground("DU_81/DUInterface/Resources/slider_leftarrow_highlight.tga");
		end
	end
	self.lArrow.MouseLeave = function(sender,args)
		if(self.isEnabled==true)then
			self.lArrow:SetBackground("DU_81/DUInterface/Resources/slider_leftarrow.tga");
		end
	end
	self.lArrow.MouseDown = function(sender,args)
		self.doActive();
		if(self.isEnabled==true)then
			self.lArrow:SetBackground("DU_81/DUInterface/Resources/slider_leftarrow_pressed.tga");
			self.lDelay=7;
			self.lIsDown = true;
		end
	end
	self.lArrow.MouseUp = function(sender,args)
		if(self.isEnabled==true)then
			self.lArrow:SetBackground("DU_81/DUInterface/Resources/slider_leftarrow.tga");
			self.lIsDown=false;
		end
	end
	self.lArrow.Update=function(sender,args)
		if (self:GetValue() ~= self.oldValue)then
			self.valueChanged();
			self.oldValue = self:GetValue();
		end
		if(self.isEnabled==true)then
			if(self.lIsDown == true)then
				if (self.lDelay==10)then
					curVal=self.updatePos();
					nextVal = curVal - self.step;
					pixPos = round(nextVal/self.pixPerVal,0);
					widgetX,widgetY=self.widget:GetPosition();
					if(widgetX > 0)then
						self.widget:SetPosition(pixPos,0);
						self.updatePos();
						self.lDelay=0;
					end
				else
					self.lDelay=self.lDelay+1;
				end
			end
		end
	end
	self.rArrow.MouseHover = function(sender,args)
		if(self.isEnabled==true)then	
			self.rArrow:SetBackground("DU_81/DUInterface/Resources/slider_rightarrow_highlight.tga");
		end
	end
	self.rArrow.MouseLeave = function(sender,args)
		if(self.isEnabled==true)then
			self.rArrow:SetBackground("DU_81/DUInterface/Resources/slider_rightarrow.tga");
		end
	end
	self.rArrow.MouseDown = function(sender,args)
		self.doActive();
		if(self.isEnabled==true)then
			self.rArrow:SetBackground("DU_81/DUInterface/Resources/slider_rightarrow_pressed.tga");
			self.rDelay=7;
			self.rIsDown = true;
		end
	end
	
	self.rArrow.MouseUp = function(sender,args)
		if(self.isEnabled==true)then
			self.rArrow:SetBackground("DU_81/DUInterface/Resources/slider_rightarrow.tga");
			self.rIsDown = false;
		end
	end
	self.rArrow.Update=function(sender,args)
		if(self.isEnabled==true)then
			if(self.rIsDown == true)then
				if (self.rDelay==10)then
					curVal=self.updatePos();
					nextVal = curVal + self.step;
					pixPos = round(nextVal/self.pixPerVal,0);
					widgetX,widgetY=self.widget:GetPosition();
					if(widgetX < (self.sliderBg:GetWidth()-16))then
						self.widget:SetPosition(pixPos,0);
						self.updatePos();
						self.rDelay=0;
					end
				else
					self.rDelay=self.rDelay+1;
				end
			end
		end
	end
	self.widget.MouseDown = function(sender,args)
		self.isDown=true;
		self.dragOff=args.X;
		self.doActive();
	end
	self.widget.MouseUp = function(sender,args)
		self.isDown=false;
	end
	
	self.widget.MouseMove = function(sender,args)
		
			  
			if(self.isEnabled==true and self.isDown==true)then	
				widgetX,widgetY=self.widget:GetPosition();
				
				
				if((widgetX-self.dragOff+args.X) > widgetX)then
					if(widgetX < (self.sliderBg:GetWidth()-16))then
						pos=round(round(((widgetX-self.dragOff+args.X)/self.pixPerStep),0)*self.pixPerStep,0)
						self.widget:SetPosition(pos,0);
					else
						self.widget:SetPosition((self.sliderBg:GetWidth()-16),0);
					end
				else
					if(widgetX > 0)then
						pos=round(round(((widgetX-self.dragOff+args.X)/self.pixPerStep),0)*self.pixPerStep,0)
						self.widget:SetPosition(pos,0);
					else
						self.widget:SetPosition(0,0);
					end
				end
				
				
				
				self.updatePos();
			end
	end

	function self:SetMinMax(n,x)
		self.min=n;
		self.max=x;
		init();
		self.updatePos();
	end
	
	function self:GetMinMax()
		return self.min,self.max;
	end

		
	function self:SetValue(theval)

	if((theval % self.step)==0)then
		if (theval >= self.min)then
			if (theval <= self.max)then
				self.widget:SetPosition(((theval-self.min)/self.pixPerVal),0);
				self.updatePos();
			end
		end
	else
		if (theval >= self.min)then
			if (theval <= self.max)then
				clVal=round((theval-self.min)/self.step,0);
				self.widget:SetPosition(((clVal*self.step)/self.pixPerVal),0);
				self.updatePos();
			end
		end
	end
	widgetX,widgetY = self.widget:GetPosition();
	
	end
	function self:GetValue()
		return self.updatePos();
	end
	
	function self:SetText(text)
		self.title:SetText(text);
	end
	
	function self:GetText()
		return self.title:GetText();
	end
	function self:SetSize(x,y)
		self:SetWidth(x);
		self:SetHeight(36);
		init();
	end
	function self:SetStep(st)
		self.step=st;
		init();
		self.updatePos();
	end
	function self:GetStep()
		return self.step;
	end
	function self:SetPlaces(pl)
		self.places=pl;
		init();
		self.updatePos();
	end
	function self:SetEnabled(tf)
		self.isEnabled=tf;
		if (tf==false)then
			self.widget:SetBackground("DU_81/DUInterface/Resources/slider_widget_ghosted.tga");
		else
			self.widget:SetBackground("DU_81/DUInterface/Resources/slider_widget.tga");
		end
	end
	function self:IsEnabled()
		return self.isEnabled;
	end
	
	self.valueChanged = function()
	
	end
	self.doActive=function()
	
	end
end