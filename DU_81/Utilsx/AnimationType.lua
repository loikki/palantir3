

AnimationType={Ramp=0,Curve=1,Cone=2,Hill=3,Sawtooth=4,Wave=5};

--Ramp: A linear, single-direction Animation
--Curve: A single-direction curve Animation
--Cone: A linear, bi-directional Animation that ends up where it started
--Hill: A bi-directional curve Animation that ends up where it started
--Sawtooth: A repeating Cone
--Wave: A repeating Hill