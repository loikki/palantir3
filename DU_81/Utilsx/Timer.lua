

Timer = class( Turbine.UI.Control );

function Timer:Constructor()
	Turbine.UI.Control.Constructor( self );
	self:SetWantsUpdates(true);
	self.AutoReset=false;
	self.Enabled=false;
	self.Interval=0;
	self.count = 0;
	self.seconds=0;
	self.initTime=Turbine.Engine.GetGameTime();
	
	self.Start=function()
		self.Enabled=true;
		self.initTime=Turbine.Engine.GetGameTime();
		self:SetWantsUpdates(true);
	end
	
	self.Stop=function()
		self.Enabled=false;
		self.count=0;
		self.seconds=0;
		self:SetWantsUpdates(false);
	end
	self.Close=function()
		--self=nil;
	end
	
	self.TimerElapsed=function(sender,args)
	
	end
	
	self.Update=function(sender,args)
		if(self.Enabled==true)then
			
			self.seconds = Turbine.Engine.GetGameTime() - self.initTime;
			
			if(self.seconds >= self.Interval)then
				self.count = self.count+1
				self:TimerElapsed(self,nil);
				
				if (self.AutoReset==false)then
					self:Stop();
				end
				self.seconds=0;
				self.initTime=Turbine.Engine.GetGameTime();
			end
		
		end
	
	end
	
end