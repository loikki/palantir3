

Animator = class( DU_81.Utilsx.Timer );

function Animator:Constructor()
	DU_81.Utilsx.Timer.Constructor( self );
	
	self.twindow=Turbine.UI.Window();
	self.twindow:SetBackColor(Turbine.UI.Color(0,1,0,0))
	self.twindow:SetParent(self);
	self.startY=self.twindow:GetTop();

	self.AutoReset=true;
	self.Interval=0.05;
	self:Start();
	self.step=0;
	self.freq=60;
	self.mag=.5;
	self.init=1;
	self.lval=0;
	self.hval=1;
	self.done=false;
	self.aType=3;
	self.r=0;
	self.x=0;
	self.y=0;
	self.ms = Turbine.Engine.GetGameTime();
	
	
	--while (self.x < self.freq*3)do
		--self.y = self.y + 1;
	--	self.y = self.y % self.freq;
	--	self.x = self.x+1;
		--Turbine.Shell.WriteLine(self.y);
	--end
	
	while (self.x < self.freq*3)do
		self.y = self.y + 1;
		
		self.x = self.x+1;
		--Turbine.Shell.WriteLine(self.y);
	end
	
	self.round=function(num, idp)
  	local mult = 10^(idp or 0)
  	return math.floor(num * mult + 0.5) / mult
	end
	
	self.findStep=function(val)
		local sinval = val + (0-self.mag)
		while(self.done ~= true) do
			local now = Turbine.Engine.GetGameTime();
			local dt = now - self.ms;
			self.step=self.step+1;
			local r = (self.step/self.freq) * 2.0*math.pi;
			local x = 0 + r*180/math.pi;
  		local y = (self.mag)*math.sin(r);
			--Turbine.Shell.WriteLine(sinval);
			if (y+(self.hval-self.mag)==val)then
				self.done=true;
				--Turbine.Shell.WriteLine(self.step);
				self:Start();
			end
		end
	end
	
	self.TimerElapsed=function(sender,args)
		local now = Turbine.Engine.GetGameTime();
		local dt = now - self.ms;
		self.step=self.step+1;
		--Turbine.Shell.WriteLine(((self.init-self.mag)/self.mag)/math.pi/2);
		--Turbine.Shell.WriteLine(self.step);
		self.r=0;
		self.x=0;
		self.y=0;
		if (self.aType==0)then
			--sine
			self.r = (self.step/self.freq) * 2.0*math.pi;
			self.x = 0 + self.r*180/math.pi;
 		 	self.y = math.sin(self.r);
 			if (self.y==-1)then
 				--Turbine.Shell.WriteLine(self.x)
 			end 
 		 	
 		 	self.twindow:SetBackColor(Turbine.UI.Color(((self.mag)*(self.y))+.5,1,0,0));
  	elseif (self.aType==1)then
  		--square
  		self.r = (self.step/self.freq) * 2.0*math.pi;
			self.x = 0 + self.r*180/math.pi;
  		self.y =(self.round(math.sin(self.r),1));
  		self.twindow:SetBackColor(Turbine.UI.Color(self.round(((self.mag)*(self.y))+.5,0),1,0,0));
 		elseif (self.aType==2)then
 			--triangle
  		self.x=self.step*(self.freq/10);
  		self.y = ((self.freq/2) - math.abs(self.step % (self.freq) - (self.freq/2)))*2/(self.freq/2)-1;
  		
  		self.twindow:SetBackColor(Turbine.UI.Color(((self.mag)*(self.y))+.5,1,0,0));
  	elseif (self.aType==3)then
  		--sawtooth
  		self.x=self.step*(self.freq/10);
  		self.y = (self.step % self.freq)/(self.freq/2)-1;
  		self.twindow:SetBackColor(Turbine.UI.Color(((self.mag)*(self.y))+.5,1,0,0));
  	elseif(self.aType==4)then
  		--inverse sawtooth
  		self.x=self.step*(self.freq/10);
  		self.y = ((self.freq/2) - (self.step % (self.freq) - (self.freq/2)))*2/(self.freq/2)-1;
  		self.twindow:SetBackColor(Turbine.UI.Color(((self.mag)*(self.y))+.5,1,0,0));
  	elseif(self.aType==5)then
  		--convex curve
  		self.x=self.step*(self.freq/10);
  		self.y=math.pow(math.abs((self.step % self.freq)-(self.freq/2)),2.0);
  		--Turbine.Shell.WriteLine(self.y);
  	elseif(self.aType==6)then
  		--concave curve
  		self.x=self.step*(self.freq/10);
  		
  	end
	--Turbine.Shell.WriteLine(y+(self.hval-self.mag));
  	
	end
	

	--self.findStep(1);
	
	
	
end