# Palantir3

Palantir is a highly customizable Heads-Up Display(HUD) for LotRO that displays critical combat-related information at center-screen in an unobtrusive way.
This allows the player to get the information they need without having to take their eyes off of the business at hand, as they would if they were to depend on the default UI elements. 
This project is imported from https://www.lotrointerface.com/downloads/info743-PalantirII.html



## Features

- Player morale and power display. Vitals bars support custom coloring.
- Curable debuff display with 4 sections that will “light” up (i.e. wound,fear,disease,poison) and “click to cure”.
- Integration with UI Positioning mode (ctrl+\) and Hide UI (F12) with appropriate results.
- The option to display Palantir always, or only in combat.
- Many, many options.

### Features Palantir II

- Curable debuff support instant curing with Saroumane's fear exclusion.
- Some bug or annoyance fixes.
- Combat timer with intervals reminder.
- Mez, Root, Fear, HOT, Buff, Debuff timer bars.
- Storm RK buff effect timers.
- Warnings for quicker reactions. 

### Features Palantir III
- Beorning support
- Potion quickslots (suggestion from Ingolmo, German translation by Myrridin)
- Option to remove the in-combat timer
- Option to replace the power by a class specific resource
- Fix mez options
- Removed the automatic drinking due to a bug
